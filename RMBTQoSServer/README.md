# RMBTQoSServer

**Try not to modify this server a lot. If it's really needed, try to merge it into RTR's open-rmbt.**

RMBTQoSServer is a direct copy of RTR's open-rmbt QoS server with the only difference, that certificate is not included in the source binary but alongside it.
