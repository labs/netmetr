## Production QoS server

Runtime dependencies are:
* Java 8 or higher

Copy `RMBTQoSServer.jar` wherever you like.

TODO some resources for tests need to be hosted on a web server (already setup on netmetr.cz website and configured in
 default config, so it's not necessary to change anything when restoring backup)

# Configuration

When running, the server will load file `config.properties` from its working directory. Example configuration is in the
 source code repository - file `RMBTQoSServer/config.properties`. We use it pretty much unchanged in the production config.

Notable options:
* IP address to listen on (default is 0.0.0.0)
* ports to run tests on
  * by default privileged ports are required
  * the ports are configured in the database within the test descriptions, it's not different from default config
* secret key
    should be the same as configured in the control server database (probably same as all the others) 
* SSL
  * SSL is implemented, but we keep it disabled
  * the certificate is loaded from within the binary, it's probably not there if you haven't built it with it manually.
  But that shouldn't matter when it is disabled. 

# Run

Simply invoke `java -jar RMBTQoSServer.jar`. To stop it, just kill it.
