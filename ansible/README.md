# NetMetr installation

## 1. Servers

First of all, you need to obtain 3 servers running Debian Buster. They should have public IPv4 and IPv6 addresses.
You should be able to access them using SSH and have access to root via sudo. There should be some domains pointed to
the boxes.

## 2. Local configuration and data

If you work at CZ.NIC and want to deploy to our official server, get your hands on Ansible Vault Password used for encryption of secrets and save it in file named `.vault_pass` in this
 directory. To get it, ask a maintainer of this repository. The configuration in the repository can be then used pretty much as is, just edit the `ansible_user` in the file `production.yml`.

If you are not deploying to the official servers, you have to edit the Ansible configuration.
Copy file `production.yml` (or edit it, if you wish) somewhere else. From now on, we will call
 the new file `inventory.yml`. In the `inventory.yml` file, edit all the field to match your
  deployment. That means IP adresses, domains, paths to certificates etc. The format of the file should be pretty self-explanatory.  
Also, edit `ansible_user` to the user you have on your servers. This user account is used for making an SSH
connection. When copying from the `production.yml` file, you can see some encrypted values using Ansible Vault. You can safely delete them and replace them with you own values. They are all secrets of some sorts, so if you just put a long alphanumeric strings there, it should be ok.

## 3. Build all

Next, you should build NetMetr locally. Follow instructions provided in top-level `README.md`.

## 4. Init database

### Clean setup

Run this to install the database and init schemas...

```bash
ansible-playbook --become --ask-become-pass --inventory-file inventory.yml --vault-password-file .vault_pass netmetr-dbinit.yml
```

### From backup

Run this to install PostgreSQL on the server with all required extensions:

```bash
ansible-playbook --become --ask-become-pass --inventory-file inventory.yml --vault-password-file .vault_pass netmetr-dbinit-nodata.yml
```

Backup production database:

```sh
sudo -u postgres pg_dumpall | gzip | ssh user@new_server "cat > /home/user/production_dump.sql.gz"
```

Then restore the database from previously made dump. If you are upgrading PostGIS, it will print out quite a few errors.
Ignore them, it should be ok.

```bash
zcat production_dump.sql.gz | sudo -u postgres psql postgres
```

Change the password of database user manually to be consistent with what you configured in the inventory.

```
sudo -u postgres psql
postgres=# ALTER USER rmbt WITH PASSWORD 'YOUR_NEW_PASSWORD';
```

Check table `test_servers` and fix the secret keys stored in there. And if you are doing an upgrade, be prepared to fix any
incompatibilities in newer versions of postgres extensions. So far, only discovered incompatibilities were in trigger
`trigger_test` and it's usage of `{x,y}{max,min}` methods. They should be renamed to `st_{x,y}{max,min}`. 

## 5. Installation

Run this command:

```bash
ansible-playbook --become --ask-become-pass --inventory-file inventory.yml --vault-password-file .vault_pass netmetr.yml
```

It will connect to the configured servers and install all the software. It will also start the appropriate services.

## 6. Finalize

Run the installation command again. It will start all the services again and they should all stay running after this.
You are done.

# Result

After installation, there will be 3 servers - each with service netmetr-{servername} running on it. You can call for
example `sudo journalctl -xefu netmetr-control` to get to the logs of control server. Similar for other servers.
