#!/bin/bash
set -e

echo "Initializing tables in DB rmbt"

psql_rmbt() {
    psql -v ON_ERROR_STOP=1 --username "rmbt" --dbname "rmbt"
}

for f in /tmp/dbinit/init-db-sql/*; do
    echo "Processing file $f"
    cat "$f" | psql_rmbt
done