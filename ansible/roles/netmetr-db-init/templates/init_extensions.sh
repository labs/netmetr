#!/usr/bin/env bash

echo "CREATE EXTENSION IF NOT EXISTS quantile" | psql --dbname rmbt
#psql_rmbt < `pg_config --sharedir`/extension/quantile--1.1.5.sql

echo "CREATE EXTENSION IF NOT EXISTS hstore;" | psql --dbname rmbt
echo 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";' | psql --dbname rmbt
echo 'CREATE EXTENSION IF NOT EXISTS "postgis";' | psql --dbname rmbt
echo 'CREATE EXTENSION IF NOT EXISTS "postgis_topology";' | psql --dbname rmbt