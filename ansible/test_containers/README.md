# Test containers

This directory contains definition for test containers which could be used with Ansible to test everything localy.

## Dependencies

* podman
    * Docker is not usable, because it can't run systemd
* root privileges on your testing system
* certificates copied from beta servers (ugly, but hard to do it differently) placed in certs directory
    * the certs directory will be copied to the root of all servers, so include the full path


## Design

The containers are meant to be run once and then used again and again. When you stop them (manually or by shutting down the host), they will be removed and will have to be initialized again.

## Usage

```sh
# to build and run the containers
sudo ./start.sh

# to stop and remove the containers
sudo ./stop.sh
```

## Network

You can add this to your hosts file (or just use the ip):

```
10.88.1.1   control.netmetr
10.88.1.2   speed.netmetr
10.88.1.3   qos.netmetr
```

## Accessing the containers

You can just `sudo podman exec -ti control.netmetr bash` and similar. Other option is, that all containers run ssh server. You can login as a `dev` user with password `password`. The issue with that is `known_hosts` file... :(

## Tips & tricks

You can copy `/etc/nginx/dhparam.pem` file to certs directory to make the installation faster (and less secure, but we
don't care).