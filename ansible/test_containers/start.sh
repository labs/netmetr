#!/usr/bin/env bash

if test "$(id -u)" != 0; then
  echo "This script must be run as root"
  exit 1
fi

podman build -t netmetr_base .

podman run --detach --cap-add=CAP_AUDIT_WRITE --memory 256m --cpus=0.8 --hostname=speed --name=speed.netmetr --ip=10.88.1.2 --add-host=netmetr-qos.labs.nic.cz:10.88.1.3 --add-host=netmetr-control.labs.nic.cz:10.88.1.1 --add-host=netmetr-speed.labs.nic.cz:10.88.1.2 --add-host=localhost:127.0.0.1 --rm=true netmetr_base
podman run --detach --cap-add=CAP_AUDIT_WRITE --hostname=qos --name=qos.netmetr --ip=10.88.1.3 --add-host=netmetr-qos.labs.nic.cz:10.88.1.3 --add-host=netmetr-control.labs.nic.cz:10.88.1.1 --add-host=netmetr-speed.labs.nic.cz:10.88.1.2 --add-host=localhost:127.0.0.1 --rm=true netmetr_base
podman run --detach --cap-add=CAP_AUDIT_WRITE --hostname=control --name=control.netmetr --ip=10.88.1.1 --add-host=netmetr-qos.labs.nic.cz:10.88.1.3 --add-host=netmetr-control.labs.nic.cz:10.88.1.1 --add-host=netmetr-speed.labs.nic.cz:10.88.1.2 --add-host=localhost:127.0.0.1 --rm=true netmetr_base

# backup /etc/hosts file and patch it with new domains
cp /etc/hosts /etc/hosts.netmetr_backup
{
  echo -e "10.88.1.1\tnetmetr-control.labs.nic.cz"
  echo -e "10.88.1.2\tnetmetr-speed.labs.nic.cz"
  echo -e "10.88.1.3\tnetmetr-qos.labs.nic.cz"
} >> /etc/hosts
