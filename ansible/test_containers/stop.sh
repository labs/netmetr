#!/usr/bin/env bash

if test "$(id -u)" != 0; then
  echo "This script must be run as root"
  exit 1
fi

podman stop speed.netmetr control.netmetr qos.netmetr

# and just to be sure
# podman rm speed.netmetr control.netmetr qos.netmetr

# revert changes made to the hosts file
mv /etc/hosts.netmetr_backup /etc/hosts