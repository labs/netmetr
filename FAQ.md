# I am an internet provider:

## How do I change the provider name you show to your users?

The provider name displayed in test results depends on the origin of the test request. We use its source IP address to perform an ASN lookup, and we show the AS name to our users.

Technically, we use [Team Cymru's lookup tool](https://asn.cymru.com/cgi-bin/whois.cgi). The name returned from there is the same as we show to our users.

If you want to change the name that's shown, please change it in the Whois database managed by your RIR. For example, to update it in the region managed by RIPE, you can follow [this manual](https://www.ripe.net/manage-ips-and-asns/db/support/updating-the-ripe-database).
