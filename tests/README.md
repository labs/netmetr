# Integration tests in this directory

## How does it work

`client_signatures` directory contains recorded communication with NetMetr
clients. The files there are generated using `extract_http.py` script
from standard PCAPs. `test.py` script then, when run, goes through every
client signature in that directory and runs the same requests against
local server at `control.netmetr` (that's hardcoded at the beginning of
the script). The requests are modified on the fly - UUIDs are exchanged
to match what the server currently expects.

## Dependencies

* **Python 3.7** or higher
* `tcpflow`

## Limitations

`extract_http.py` script is stupid. It does not work with Keep-Alive
HTTP connections, HTTP/2 and more. It only supports single request per TCP
stream. It will also break when gzip is used.

It was developed mainly due to an inability to test with mobile apps. For
that use case, it work good enough. However, that's pretty much it.