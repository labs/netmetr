#!/usr/bin/python

import requests
import json
import os.path
from typing import *
import re
from termcolor import colored
from collections import defaultdict
import itertools

HOST_TRANSLATION_TABLE = {
    "control-4.netmetr.cz:8080": "control.netmetr",
    "control-6.netmetr.cz:8080": "control.netmetr",
    "control.netmetr.cz": "control.netmetr",
}

def assert_same_structure(o1, o2, exclude_keys=set(), path=""):
    if isinstance(o1, dict):
        assert isinstance(o2, dict), f"One object has object value, while the other one is of a different type. Path '{path}'"
        for key in itertools.chain(o1.keys(), o2.keys()):
            assert key in o2, f"One object contains key '{key}', while the other one does not. Path '{path}'"
            if key in exclude_keys:
                continue
            assert_same_structure(o1[key], o2[key], exclude_keys, f"{path}.{key}")
    elif isinstance(o1, list):
        assert isinstance(o2, list), f"One object has list while the other something else. Path to object '{path}'"
        assert len(o1) == len(o2), f"Array lengths mismatch. Path to array '{path}'"
        for i in range(len(o1)):
            assert_same_structure(o1[i], o2[i], exclude_keys, f"{path}[{i}]")
    else:
        # primitive types
        assert (isinstance(o1, str) and isinstance(o2, str)) or \
               (isinstance(o1, int) and isinstance(o2, int)) or \
               (isinstance(o1, float) and isinstance(o2, float)), \
            f"Primitive type values mismatch. Path to values '{path}'"


class RecordedHTTPOneSide(object):
    def __init__(self, re_dict):
        self.headers = re_dict['headers'].split('\n')
        self.payload = re_dict['payload']

    def get_headers(self):
        return {h[:h.index(': ')]: h[h.index(': ') + 2:] for h in self.headers[1:]}


class RecordedAPICall(object):
    def __init__(self, call_dict):
        self.request = RecordedHTTPOneSide(call_dict['request'])
        self.response = RecordedHTTPOneSide(call_dict['response'])

    def get_host(self) -> str:
        for header in self.request.headers:
            if header.startswith('Host:'):
                return header.replace('Host:', '').strip()
        raise Exception("Request does not contain host header!")

    def get_method(self) -> str:
        method = self.request.headers[0].split(' ')[0]
        assert (method in {'POST', 'OPTIONS', 'GET'})
        return method

    def get_path(self) -> str:
        path = self.request.headers[0].split(' ')[1]
        return path

    def get_request_payload(self) -> str:
        return self.request.payload

    def get_response_payload(self) -> str:
        return self.response.payload

    def get_request_headers(self) -> Dict[str, str]:
        return self.request.get_headers()

    def get_response_headers(self) -> Dict[str, str]:
        return self.response.get_headers()

    def get_http_status(self) -> int:
        return int(self.response.headers[0].split(' ')[1])

    @staticmethod
    def load_calls_from_file(filename: str) -> List['RecordedAPICall']:
        data = None
        with open(filename, 'r') as f:
            data = f.read()

        parsed_data = json.loads(data)
        calls = []
        for req in parsed_data:
            calls.append(RecordedAPICall(req))
        return calls


class DataParser(object):
    def get_test_token(self, response) -> str:
        pass

    def get_client_uuid(self, response) -> str:
        pass

    def get_test_uuid(self, response) -> str:
        pass

    def get_open_test_uuid(self, response) -> str:
        pass


class RegexDataParser(DataParser):
    TEST_TOKEN_REGEX = re.compile(
        r"\"test_token\"[\s]*:[\s]*\"([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}_[0-9]{10,}_[a-zA-Z0-9+\\/]{27,}(=|u003d))\"")
    CLIENT_UUID_REGEX = re.compile(
        r"\"uuid\"[\s]*:[\s]*\"([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-([a-fA-F0-9]{12}))\"")
    TEST_UUID_REGEX = re.compile(
        r"\"test_uuid\"[\s]*:[\s]*\"([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-([a-fA-F0-9]{12}))\"")
    OPEN_TEST_UUID_REGEX = re.compile(
        r"\"open_test_uuid\"[\s]*:[\s]*\"(O[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-([a-fA-F0-9]{12}))\"")

    @staticmethod
    def _parse(regex, response):
        tokens = re.findall(regex, response)
        # assert (len(tokens) < 2)

        if len(tokens) != 0:
            return tokens[0][0]
        else:
            return None

    def get_test_token(self, response) -> str:
        return RegexDataParser._parse(RegexDataParser.TEST_TOKEN_REGEX, response)

    def get_client_uuid(self, response) -> str:
        return RegexDataParser._parse(RegexDataParser.CLIENT_UUID_REGEX, response)

    def get_test_uuid(self, response) -> str:
        return RegexDataParser._parse(RegexDataParser.TEST_UUID_REGEX, response)

    def get_open_test_uuid(self, response) -> str:
        return RegexDataParser._parse(RegexDataParser.OPEN_TEST_UUID_REGEX, response)


class DataPatcher(object):
    def set_test_token(self, request, token):
        pass

    def set_client_uuid(self, request, uuid):
        pass

    def set_test_uuid(self, request, uuid):
        pass


class RegexDataPatcher(DataPatcher):
    @staticmethod
    def _patch(regex, data, request):
        if len(re.findall(regex, request)) > 0:
            return re.sub(
                regex,
                f"{data}".replace('\\','\\\\'),
                request
            )
        else:
            return request

    def set_test_token(self, request, token):
        return RegexDataPatcher._patch(
            RegexDataParser.TEST_TOKEN_REGEX,
            f"\"test_token\":\"{token}\"",
            request
        )

    def set_client_uuid(self, request, uuid):
        return RegexDataPatcher._patch(
            RegexDataParser.CLIENT_UUID_REGEX,
            f"\"uuid\":\"{uuid}\"",
            request
        )

    def set_test_uuid(self, request, uuid):
        return RegexDataPatcher._patch(
            RegexDataParser.TEST_UUID_REGEX,
            f"\"test_uuid\":\"{uuid}\"",
            request
        )


class CallExecutor(object):
    """
    Objects of this class are able to replay requests to the NetMetr API. They live-path the communication, so that
    all UUIDs and similar are not reused.
    """

    PARSERS: Dict[str, DataParser] = defaultdict(lambda: RegexDataParser())
    PATCHERS: Dict[str, DataPatcher] = defaultdict(lambda: RegexDataPatcher())

    def __init__(self):
        self.uuid: str = None
        self.test_token: str = None
        self.test_uuid: str = None
        self.open_test_uuid: str = None
        self.did_run_sync = False

    def _translate_host(self, host):
        if host in HOST_TRANSLATION_TABLE:
            return HOST_TRANSLATION_TABLE[host]
        else:
            print(colored(f"Host \"{host}\" not in HOST_TRANSLATION_TABLE"), 'red')
            print('This call will be skipped')
            return None

    def replay_request(self, api_call: RecordedAPICall) -> str:
        path = api_call.get_path()
        # special case for /RMBTControlServer/opentests/{uuid}
        if path.startswith('/RMBTControlServer/opentests/'):
            path = '/RMBTControlServer/opentests/' + self.open_test_uuid
        # store that sync was run (length of history will differ because of that)
        if path == "/RMBTControlServer/sync":
            self.did_run_sync = True

        print(colored(api_call.get_method() + " " + path + ":", 'yellow'))

        host = self._translate_host(api_call.get_host())
        if host is None:
            return None

        # Payload patching
        data = api_call.get_request_payload()
        print(colored("Request before patching:", 'green'))
        print(data)
        patcher = CallExecutor.PATCHERS[path]
        data = patcher.set_client_uuid(data, self.uuid)
        data = patcher.set_test_uuid(data, self.test_uuid)
        data = patcher.set_test_token(data, self.test_token)
        print(colored("Request after patching:", 'green'))
        print(data)

        # headers patching
        headers = api_call.get_request_headers()
        headers['Host'] = host

        # make request
        req = requests.request(api_call.get_method(), f"http://{host}{path}",
                               data=data.encode(),
                               headers=headers
                               )

        # parse response
        response = req.content.decode('utf8')
        parser = CallExecutor.PARSERS[path]
        client_uuid = parser.get_client_uuid(response)
        if client_uuid is not None: self.uuid = client_uuid
        test_token = parser.get_test_token(response)
        if test_token is not None: self.test_token = test_token
        test_uuid = parser.get_test_uuid(response)
        if test_uuid is not None: self.test_uuid = test_uuid
        open_test_uuid = parser.get_open_test_uuid(response)
        if open_test_uuid is not None: self.open_test_uuid = open_test_uuid

        print(colored('Response:', 'green'))
        print(response)
        print(colored('Expected response:', 'green'))
        print(api_call.get_response_payload())
        print()

        self._validate_response(api_call, response, req)
        return response

    def replay_all(self, calls: List[RecordedAPICall]):
        for c in calls:
            self.replay_request(c)

    def _validate_response(self, api_call: RecordedAPICall, response: str, request: requests.Response):
        assert (request.status_code == api_call.get_http_status()), "Unexpected HTTP status code"

        if api_call.get_path() == '/RMBTControlServer/testresultdetail':
            # we allow bigger difference here, because of different type of result networks
            # also when there is NDT in one of the results and not the other one, we say its ok. This is caused
            # by limitations of this test tool, because we can't select the same test from history to get detail of
            assert (0.8 * len(api_call.get_response_payload()) <= len(response) <= 1.2 * len(api_call.get_response_payload())
                    or
                    ('NDT' in api_call.get_response_payload()) ^ ('NDT' in response))
        elif api_call.get_path().startswith('/RMBTControlServer/opentests/'):
            # this can differ a bit more then the default due to the same reason as above - the result could be of
            # a different test than what was recorded
            assert (0.8 * len(api_call.get_response_payload()) <= len(response) <= 1.2 * len(api_call.get_response_payload()))
        elif api_call.get_path() == '/RMBTControlServer/history':
            # after sync was run, there will be more history results then in the test run
            if self.did_run_sync:
                resp = json.loads(response)
                assert('history' in resp)
                assert('error' in resp)
        else:
            real = json.loads(response)
            expected = json.loads(api_call.get_response_payload())

            assert_same_structure(real, expected, exclude_keys=set(["history"]))

            # default sanity check
            assert (0.90 * len(api_call.get_response_payload()) <= len(response) <= 1.1 * len(api_call.get_response_payload()))

DIR = 'client_signatures'
for file in os.listdir(DIR):
    if file.endswith(".json"):
        pth = os.path.join(DIR, file)
        print(colored(f"Running tests from {pth}", 'red'))

        calls = RecordedAPICall.load_calls_from_file(pth)
        ce = CallExecutor()
        ce.replay_all(calls)
