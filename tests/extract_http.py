#!/usr/bin/python

import json
import os
import shutil
import subprocess
from typing import *

Flows = Dict[Tuple[str, str], bytes]


def run_tcpflow(filename):
    subprocess.run(["tcpflow", "-o", "_flows", "-r", filename, "-X", "report.xml"])


def load_all_flows() -> Flows:
    flows = {}
    for filename in os.listdir("_flows"):

        if filename[-2] == 'c':
            continue

        src = filename.split("-")[0]
        dst = filename.split("-")[1]

        with open(f"_flows/{filename}", 'rb') as f:
            flows[(src, dst)] = f.read()
    return flows


def get_http_requests(flows) -> Set[Tuple[str, str]]:
    requests = set()
    for key in flows:
        data: bytes = flows[key]
        if data.startswith("POST /RMBT".encode()) or data.startswith("GET /RMBT".encode()) or data.startswith(
                "OPTIONS /RMBT".encode()):
            requests.add(key)
    return requests


def pair_requests_with_responses(request_addrs):
    return set(map(lambda conn: (conn, (conn[1], conn[0])), request_addrs))


class Request(object):
    def __init__(self, request: str, response: str, req_addr):
        self.request_filename = req_addr

        request = request.replace("\r\n", "\n")
        response = response.replace("\r\n", "\n")

        self.request_headers = request.split('\n\n')[0]
        self.request_body = request.split('\n\n')[1]

        self.response_headers = response.split('\n\n')[0]
        self.response_body = response.split('\n\n')[1]

    def get_date(self):
        import xml.etree.ElementTree as ET
        with open("report.xml", "r") as fp:
            root = ET.parse(fp)

        filename = "_flows/" + self.request_filename[0] + "-" + self.request_filename[1]
        fileobjects = root.findall(f".//*/fileobject") #/[filename = '_flows/{filename}']/tcpflow
        for fo in fileobjects:
            fn = fo.find("filename")
            if fn is not None and fn.text == filename:
                r = fo.find("tcpflow").attrib["startime"]
                return r

        raise Exception("Nenasto to, co to melo v tom reportu")

    def as_dict(self):
        return \
            {'request': {
                'headers': self.request_headers,
                'payload': self.request_body
            },
                'response': {
                    'headers': self.response_headers,
                    'payload': self.response_body
                }
            }


def parse_requests(requests, flows) -> List[Request]:
    result = []
    for req, resp in requests:

        # TODO test even images...
        if b'image/png' in flows[resp]:
            continue
        result.append(Request(flows[req].decode('utf8'), flows[resp].decode('utf8'), req))
    return result


def sort_requests(requests):
    requests.sort(key=lambda r: r.get_date())
    return requests


def parse_pcap(filename):
    run_tcpflow(filename)
    flows = load_all_flows()
    flows_reqs = get_http_requests(flows)
    request_addrs = pair_requests_with_responses(flows_reqs)
    requests = parse_requests(request_addrs, flows)
    requests = sort_requests(requests)


    http_data = []
    for r in requests:
        http_data.append(r.as_dict())

    print(json.dumps(http_data, indent=4))

    os.remove('report.xml')
    shutil.rmtree('_flows')


def parse_arguments():
    import argparse

    parser = argparse.ArgumentParser(description='Process PCAP file and output extracted HTTP requests to stdout.')
    parser.add_argument('filename', metavar='file', type=str,
                    help='recording of client communication')

    return parser.parse_args()

# parse_pcap('client_signatures/android_app.pcapng')
args = parse_arguments()
parse_pcap(args.filename)