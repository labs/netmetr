# NetMetr

*NetMetr* is an open source, multi-threaded bandwidth test written in Java and
C based on Open-RMBT, consisting of:

- control server on Jetty+Restlet
- test server (direct copy of https://github.com/rtr-nettest/rmbt-server)
- QoS test server (direct copy of RMBTQoSServer/ from https://github.com/rtr-nettest/open-rmbt) 

## FAQ

See [FAQ.md](FAQ.md).

## Server APIs

Server APIs are documented using OpenAPI format. Web-based documentation can be found on any running
instance of NetMetr on addresses:

* `$NETMETR_CONTROL_DOMAIN/RMBTControlServer/api/v1/openapi` (for example [here](https://control.netmetr.cz/RMBTControlServer/api/v1/openapi))
    * intended mainly for controlling measurements and their configuration
* `$NETMETR_CONTROL_DOMAIN/RMBTStatisticServer/api/v1/openapi` (for example [here](https://control.netmetr.cz/RMBTStatisticServer/api/v1/openapi))
    * intended for statistical analysis of the data

## Dependencies

See [DEPENDENCIES.md](DEPENDENCIES.md)

## Installation

### Build for production

Make sure you have Java11 installed on your machine and `java -version` says, it's version 11.

Run `gradle clean build`. This will generate files:
* `NetmetrControl/build/libs/NetmetrControl.jar`
* `RMBTQoSServer/build/libs/RMBTQoSServer.jar`

### System setup

See [ansible/README.md](ansible/README.md)

## Licence

*NetMetr* is released under the [Apache License, Version 2.0]. It was developed by CZ.NIC.

*Open-RMBT* is released under the [Apache License, Version 2.0]. It was developed
by [alladin-IT GmbH] and [SPECURE GmbH] and financed by the
[Austrian Regulatory Authority for Broadcasting and Telecommunications (RTR-GmbH)] and the [Agency for communication networks and services of the Republic of Slovenia].

 [alladin-IT GmbH]: https://alladin.at/
 [SPECURE GmbH]: https://www.specure.com/
 [Austrian Regulatory Authority for Broadcasting and Telecommunications (RTR-GmbH)]: https://www.rtr.at/
 [Agency for communication networks and services of the Republic of Slovenia]: http://www.akos-rs.si/
 [Apache License, Version 2.0]: http://www.apache.org/licenses/LICENSE-2.0

