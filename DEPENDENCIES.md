The following third party libraries are required dependencies:

### Guava ###

- Apache 2.0. License
- available at <https://code.google.com/p/guava-libraries/>


### dnsjava ###

- BSD License
- available at <http://www.xbill.org/dnsjava/>


### PostgreSQL JDBC Driver ###

- BSD License
- available at <http://jdbc.postgresql.org/>


### JSON in Java ###

- MIT License (+ "The Software shall be used for Good, not Evil.")
- available at <http://www.json.org/java/index.html>


### Apache Commons ###

- Apache 2.0 License
- available at <http://commons.apache.org/>


### Restlet Framework ###

- Version: 2.3
- Licenses:
  - Apache 2.0
  - LGPL license version 3.0
  - LGPL license version 2.1 
  - CDDL license version 1.0 or
  - EPL license version 1.0
- available at <http://restlet.org/>

### Restlet Framework GSON Extension ###

### GSON ###

### swagger-codegen ###

### PostGIS/ODBC ###

- Version: 2.1
- Licenses:
  - GPL license version 2.0 (for PostGIS)
  - LGPL license version 2.1 (for PostGIS/JDBC)
- available at <http://postgis.net/>
  