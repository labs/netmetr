package cz.nic.netmetr.server.statistic;

import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ApproveMonthResource extends KeyCheckResource {

    private static final String REVISED_MONTHS_UPDATE       = "UPDATE audit_results SET checked = true WHERE checked = false AND (EXTRACT (year FROM timestamp_month)) = ? AND (EXTRACT (month FROM timestamp_month)) = ?";
    private static final String MONTH_PARAMETER             = "month";
    private static final String YEAR_PARAMETER              = "year";
    private static final String ILLEGAL_PARAMETERS_MESSAGE  = "ERROR: illegal parameters!";
    private static final String OK_MESSAGE                  = "OK: ";

    @Override
    protected Representation processRequest(String entity) {
        int     month   = -1;
        int     year    = -1;

        try {
            if (entity != null) {
                final JSONObject request = new JSONObject(entity);
                month   = request.optInt(MONTH_PARAMETER, month);
                year    = request.optInt(YEAR_PARAMETER, year);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (year < 0 || month < 1 || month > 12) {
            return new StringRepresentation(ILLEGAL_PARAMETERS_MESSAGE);
        }

        PreparedStatement   ps = null;
        ResultSet           rs = null;

        try (var conn = getDbConnectionFromApp().getConnection()) {
            ps = conn.prepareStatement(REVISED_MONTHS_UPDATE);
            ps.setInt(1, year);
            ps.setInt(2, month);

            return new StringRepresentation(OK_MESSAGE + ps.executeUpdate());
        } catch (final SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}