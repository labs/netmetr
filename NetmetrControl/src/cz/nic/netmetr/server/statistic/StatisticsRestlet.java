/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.statistic;

import cz.nic.netmetr.boot.DbConnection;
import cz.nic.netmetr.shared.Classification;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

public class StatisticsRestlet extends AbstractStatisticsRestlet {

    Classification classification = Classification.getInstance(DbConnection.getInstance());

	@Override
    protected void processRequest(JSONObject answer, StatisticParameters params, boolean useMobileProvider, String sqlTimeCondition, String sqlImplausibleCondition, boolean signalMobile, String where) {
    	PreparedStatement ps = null;
        ResultSet rs = null;
        try (var conn = DbConnection.getInstance().getConnection()) {
            final JSONArray providers = new JSONArray();
            answer.put("providers", providers);

            ps = selectProviders(conn,true, params.getQuantile(), params.getAccuracy(), params.getCountry(), useMobileProvider, where, signalMobile, sqlTimeCondition, sqlImplausibleCondition);
            if (!ps.execute())
            	return;

            rs = fillJSON(params.getLang(), ps, providers);

            ps = selectProviders(conn, false, params.getQuantile(), params.getAccuracy(), params.getCountry(), useMobileProvider, where, signalMobile, sqlTimeCondition, sqlImplausibleCondition);
            if (!ps.execute())
            	return;

            final JSONArray providersSumsArray = new JSONArray();
            rs = fillJSON(params.getLang(), ps, providersSumsArray);

            if (providersSumsArray.length() == 1)
                answer.put("providers_sums", providersSumsArray.get(0));

            final JSONArray countries = new JSONArray(getCountries());
            answer.put("countries", countries);

        } catch (final Exception e) {
        	e.printStackTrace();
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception ex) { ex.printStackTrace(); }
            try { if (ps != null) ps.close(); } catch (Exception ex) { ex.printStackTrace(); }
        }

    }

    private Set<String> getCountries() throws SQLException {
    	Set<String> countries = new TreeSet<>();
		String sql = "WITH RECURSIVE t(n) AS ( "
				+ "SELECT MIN(mobile_network_id) FROM test"
				+ " UNION"
				+ " SELECT (SELECT mobile_network_id FROM test WHERE mobile_network_id > n"
				+ " ORDER BY mobile_network_id LIMIT 1)"
				+ " FROM t WHERE n IS NOT NULL"
				+ " )"
				+ "SELECT upper(mccmnc2name.country) FROM t LEFT JOIN mccmnc2name ON n=mccmnc2name.uid WHERE NOT mccmnc2name.country IS NULL GROUP BY mccmnc2name.country;";

		try (var conn = DbConnection.getInstance().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
		    ResultSet rs = ps.executeQuery())
	    {
    		while(rs.next())
    			countries.add(rs.getString(1));
    		return countries;
	    }
    }

    private PreparedStatement selectProviders(Connection conn, final boolean group, final float quantile, final double accuracy, final String country, final boolean useMobileProvider,
                                              final String where, final boolean signalMobile, final String sqlTimeCondition, String sqlImplausibleCondition) throws SQLException
    {
        PreparedStatement ps;
        String sql = String.format("SELECT" +
                (group ? " COALESCE(adm.fullname, t.model) model," : "") +
                " count(t.uid) count," +

                " quantile(speed_download::bigint, ?::double precision) quantile_down," +
                " quantile(speed_upload::bigint, ?::double precision) quantile_up," +
                " quantile(signal_strength::bigint, ?::double precision) quantile_signal," +
                " quantile(ping_shortest::bigint, ?::double precision) quantile_ping," +

                " sum((speed_download >= ?)::int)::double precision / count(speed_download) down_green," +
                " sum((speed_download < ? and speed_download >= ?)::int)::double precision / count(speed_download) down_yellow," +
                " sum((speed_download < ?)::int)::double precision / count(speed_download) down_red," +

                " sum((speed_upload >= ?)::int)::double precision / count(speed_upload) up_green," +
                " sum((speed_upload < ? and speed_upload >= ?)::int)::double precision / count(speed_upload) up_yellow," +
                " sum((speed_upload < ?)::int)::double precision / count(speed_upload) up_red," +

                " sum((signal_strength >= ?)::int)::double precision / count(signal_strength) signal_green," +
                " sum((signal_strength < ? and signal_strength >= ?)::int)::double precision / count(signal_strength) signal_yellow," +
                " sum((signal_strength < ?)::int)::double precision / count(signal_strength) signal_red," +

                " sum((ping_shortest <= ?)::int)::double precision / count(ping_shortest) ping_green," +
                " sum((ping_shortest > ? and ping_shortest <= ?)::int)::double precision / count(ping_shortest) ping_yellow," +
                " sum((ping_shortest > ?)::int)::double precision / count(ping_shortest) ping_red" +

                " FROM test t" +
                " LEFT JOIN device_map adm ON adm.codename=t.model" +
                " LEFT JOIN network_type nt ON nt.uid=t.network_type" +
                " WHERE %s" +
                " AND t.deleted = false AND t.status = 'FINISHED' AND t.test_type_id = 1 " +
                sqlImplausibleCondition +
                sqlTimeCondition +
                (useMobileProvider ? " AND t.mobile_provider_id IS NOT NULL" : "") +
                ((accuracy > 0) ? " AND t.geo_accuracy < ?" : "") +
                (group ? " GROUP BY COALESCE(adm.fullname, t.model) HAVING count(t.uid) > 10" : "") +
                " ORDER BY count DESC"
                , where);
        if (country != null) {
        	sql = String.format("SELECT" +
        			((group && useMobileProvider) ? " p.name AS name, p.shortname AS shortname, p.mccmnc AS sim_mcc_mnc, " : "") +
                    ((group && !useMobileProvider) ? " public_ip_as_name AS name, public_ip_as_name AS shortname, t.public_ip_asn AS asn,  " : "") +
                    " count(t.uid) count," +

                    " quantile(speed_download::bigint, ?::double precision) quantile_down," +
                    " quantile(speed_upload::bigint, ?::double precision) quantile_up," +
                    " quantile(signal_strength::bigint, ?::double precision) quantile_signal," +
                    " quantile(ping_shortest::bigint, ?::double precision) quantile_ping," +

                    " sum((speed_download >= ?)::int)::double precision / count(speed_download) down_green," +
                    " sum((speed_download < ? and speed_download >= ?)::int)::double precision / count(speed_download) down_yellow," +
                    " sum((speed_download < ?)::int)::double precision / count(speed_download) down_red," +

                    " sum((speed_upload >= ?)::int)::double precision / count(speed_upload) up_green," +
                    " sum((speed_upload < ? and speed_upload >= ?)::int)::double precision / count(speed_upload) up_yellow," +
                    " sum((speed_upload < ?)::int)::double precision / count(speed_upload) up_red," +

                    " sum((signal_strength >= ?)::int)::double precision / count(signal_strength) signal_green," +
                    " sum((signal_strength < ? and signal_strength >= ?)::int)::double precision / count(signal_strength) signal_yellow," +
                    " sum((signal_strength < ?)::int)::double precision / count(signal_strength) signal_red," +

                    " sum((ping_shortest <= ?)::int)::double precision / count(ping_shortest) ping_green," +
                    " sum((ping_shortest > ? and ping_shortest <= ?)::int)::double precision / count(ping_shortest) ping_yellow," +
                    " sum((ping_shortest > ?)::int)::double precision / count(ping_shortest) ping_red" +

                    " FROM test t" +
                    " LEFT JOIN device_map adm ON adm.codename=t.model" +
                    " LEFT JOIN network_type nt ON nt.uid=t.network_type" +
                    (useMobileProvider ? " LEFT JOIN mccmnc2name p ON p.uid = t.mobile_sim_id" : "") +
                    " WHERE %s" +
                    " AND " + (useMobileProvider?"p.country = ? AND ((t.country_location IS NULL OR t.country_location = ?)  AND ((NOT t.roaming_type = 2) OR t.roaming_type IS NULL))":"t.country_geoip = ? ") +
                    " AND t.deleted = false AND t.status = 'FINISHED' AND t.test_type_id = 1 " +
                    sqlImplausibleCondition +
                    sqlTimeCondition +
                    ((accuracy > 0) ? " AND t.geo_accuracy < ?" : "") +
					((group && useMobileProvider) ? " GROUP BY p.name, p.shortname, p.mccmnc HAVING count(t.uid) > 10" : "") +
					((group && !useMobileProvider) ? " GROUP BY public_ip_as_name, public_ip_as_name, t.public_ip_asn HAVING count(t.uid) > 10" : "") +
                    " ORDER BY count DESC"
                    , where);
        }

        ps = conn.prepareStatement(sql);

        int i = 1;
        for (int j = 0; j < 3; j++)
            ps.setFloat(i++, quantile);
        ps.setFloat(i++, 1 - quantile); // inverse for ping

        final int[] td = classification.THRESHOLD_DOWNLOAD;
        ps.setInt(i++, td[0]);
        ps.setInt(i++, td[0]);
        ps.setInt(i++, td[1]);
        ps.setInt(i++, td[1]);

        final int[] tu = classification.THRESHOLD_UPLOAD;
        ps.setInt(i++, tu[0]);
        ps.setInt(i++, tu[0]);
        ps.setInt(i++, tu[1]);
        ps.setInt(i++, tu[1]);

        final int[] ts = signalMobile ? classification.THRESHOLD_SIGNAL_MOBILE : classification.THRESHOLD_SIGNAL_WIFI;
        ps.setInt(i++, ts[0]);
        ps.setInt(i++, ts[0]);
        ps.setInt(i++, ts[1]);
        ps.setInt(i++, ts[1]);

        final int[] tp = classification.THRESHOLD_PING;
        ps.setInt(i++, tp[0]);
        ps.setInt(i++, tp[0]);
        ps.setInt(i++, tp[1]);
        ps.setInt(i++, tp[1]);

        if (country != null) {
        	if (useMobileProvider) {
        		ps.setString(i++, country.toLowerCase()); //mccmnc2name.country
        		ps.setString(i++, country.toUpperCase()); //country_location
        	}
        	else {
        		ps.setString(i++, country.toUpperCase());
        	}
        }

        if (accuracy>0) {
        	ps.setDouble(i++, accuracy);
        }

        return ps;
    }
}
