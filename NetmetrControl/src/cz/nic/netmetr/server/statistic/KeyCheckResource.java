package cz.nic.netmetr.server.statistic;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import cz.nic.netmetr.shared.config.GlobalConfig;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.io.IOException;
import java.sql.SQLException;

public abstract class KeyCheckResource extends ServerResource {

    private static final String KEY_PARAMETER               = "key";
    private static final String INVALID_KEY_MESSAGE         = "ERROR: invalid key!";

    @Get
    @Post("json")
    public final Representation request(final String entity) throws IOException, SQLException {
        String key = null;

        try {
            if (entity != null) {
                JSONObject request = new JSONObject(entity);
                key = request.getString(KEY_PARAMETER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (keyCheckEnabled() && !checkKey(key)) {
            return new StringRepresentation(INVALID_KEY_MESSAGE);
        }

        return processRequest(entity);
    }

    protected boolean keyCheckEnabled() {
        return true;
    }

    private boolean checkKey(String requestSecret) {
        String serverSecret = GlobalConfig.getInstance().getRmbtSecretKey();
        if (serverSecret == null) {
            return true;
        }
        String serverSecretSha256 = Hashing.sha256().hashString(serverSecret, Charsets.UTF_8).toString();
        return requestSecret != null && !requestSecret.isEmpty() && requestSecret.equals(serverSecretSha256);
    }

    protected abstract Representation processRequest(String entity) throws SQLException, IOException;
}