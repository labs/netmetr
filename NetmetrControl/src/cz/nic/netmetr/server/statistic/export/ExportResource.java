/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.statistic.export;

import com.google.common.base.Objects;
import cz.nic.netmetr.server.statistic.KeyCheckResource;
import org.restlet.Request;
import org.restlet.representation.Representation;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ExportResource extends KeyCheckResource {
    private static final ExportCommon.AdditionalReportFieldsData reportDataFields = new ExportCommon.AdditionalReportFieldsData(new String[]{
            "tcp_open", "tcp_total", "udp_open", "udp_total", "packet_loss_down", "packet_loss_up",
            "peak_down_kbit", "peak_up_kbit"
    });
    private static final boolean ZIP_EXPORT = true;
    private static final ConcurrentHashMap<Arguments, ReadWriteLock> fileCacheLocks = new ConcurrentHashMap<>();
    /**
     * if true, the current export will write all data to the csv, if false only the last 31 days will be exported
     **/
    private static final boolean ALLOW_FULL_EXPORT = false;
    private static final long CACHE_THRESHOLD_MS = 3 * 24 * 3600 * 1000; //3 days

    protected String getFilenameTemplateCSV() {
        return "NetMetr-opendata-%YEAR%-%MONTH%.csv";
    }

    protected String getFilenameTemplateZIP() {
        return "NetMetr-opendata-%YEAR%-%MONTH%.zip";
    }

    /**
     * Request handling function. Checks internally for cached responses, if they are not present or if they are old,
     * generates new.
     *
     * @param entity
     * @return
     * @throws SQLException
     * @throws IOException
     */
    @Override
    protected Representation processRequest(String entity) throws SQLException, IOException {
        var arguments = new Arguments(getRequest());

        if (!isAudited(arguments.getYear(), arguments.getMonth()))
            throw new RuntimeException("The selected time range is not accepted for export!");

        // get file handle to the potential cache
        final File tmpDir = new File(System.getProperty("java.io.tmpdir"));
        final File cachedFile = new File(tmpDir, ((ZIP_EXPORT) ? getFilename(getFilenameTemplateZIP(), arguments) : getFilename(getFilenameTemplateCSV(), arguments)));

        // try to read already generated file (read lock)
        Lock readLock = fileCacheLocks.computeIfAbsent(arguments, a -> new ReentrantReadWriteLock()).readLock();
        readLock.lock();
        try {
            if (cachedFile.exists() && ((cachedFile.lastModified() + CACHE_THRESHOLD_MS) > Instant.now().toEpochMilli()))
                return ExportCommon.fileToOutputRepresentation(cachedFile, ZIP_EXPORT);

        } finally {
            readLock.unlock();
        }


        // if we get here, it means, that the cached file does not exist
        // let's generate it
        Lock writeLock = fileCacheLocks.get(arguments).writeLock();
        writeLock.lock();
        try {
            // when the file exists and it's old, remove it
            if (cachedFile.exists() && !((cachedFile.lastModified() + CACHE_THRESHOLD_MS) > Instant.now().toEpochMilli()))
                Files.delete(cachedFile.toPath());

            // we can be second thread which gets here.
            if (!cachedFile.exists())
                generateResponseFile(cachedFile, arguments);
        } finally {
            writeLock.unlock();
        }


        // when here, the result should already be generated
        // so let's try again to return it
        readLock = fileCacheLocks.computeIfAbsent(arguments, a -> new ReentrantReadWriteLock()).readLock();
        readLock.lock();
        try {
            if (cachedFile.exists())
                return ExportCommon.fileToOutputRepresentation(cachedFile, ZIP_EXPORT);

        } finally {
            readLock.unlock();
        }


        // ehm, something wrong must have happened. There's nothing more we can do. Let's just fail!
        throw new RuntimeException("Export file generation failed due to some weird race conditions.");
    }


    private void createExportFile(File outfile, final PreparedStatement ps, final ExportCommon.AdditionalReportFieldsData reportFieldsData) throws SQLException, IOException {
        try (var lineStream = ExportCommon.queryToCSVStream(null, ps, reportFieldsData)) {
            if (ZIP_EXPORT)
                ExportCommon.writeCSVLinesToZipFile(outfile, lineStream);
            else
                ExportCommon.writeCSVLinesToPlainFile(outfile, lineStream);
        } catch (final RuntimeException e) {
            if (e.getCause() instanceof SQLException)  // unwrap exceptions from within the stream
                throw (SQLException) e.getCause();
            throw e;
        }
    }

    @Override
    protected boolean keyCheckEnabled() {
        return false;
    }

    private String getFilename(String template, Arguments arguments) {
        int year = arguments.getYear();
        int month = arguments.getMonth();

        // if year and month are in valid ranges, use them to construct the filename
        if (year < 2099 && month > 0 && month <= 12 && year > 2000)
            return template.replace("%YEAR%", Integer.toString(year)).replace("%MONTH%", String.format("%02d", month));

        throw new RuntimeException("Year and month are in wrong range. This code should have been unreachable.");
    }

    private void generateResponseFile(File outfile, Arguments args) throws SQLException, IOException {
        final String timeClause = (args.getYear() > 0 && args.getMonth() > 0 && args.getMonth() <= 12 && args.getYear() > 2000) ?
                " AND (EXTRACT (month FROM t.time AT TIME ZONE 'UTC') = " + args.getMonth() + ") AND (EXTRACT (year FROM t.time AT TIME ZONE 'UTC') = " + args.getYear() + ") "
                : (ALLOW_FULL_EXPORT ? "" : " AND time > current_date - interval '31 days' ");

        final String sql = ExportCommon.getExportSql(this) +
                " WHERE " +
                " t.deleted = false" +
                timeClause +
                " AND status = 'FINISHED'" +
                " ORDER BY t.uid";

        try (Connection conn = getDbConnectionFromApp().getConnection()) {
            try (final PreparedStatement ps = ExportCommon.createPreparedStatement(sql, conn, getDbConnectionFromApp())) {
                createExportFile(outfile, ps, reportDataFields);
            }
        }
    }

    protected boolean isAudited(int year, int month) throws SQLException {
        return ExportCommon.isAudited(getDbConnectionFromApp(), year, month);
    }


    private static final class Arguments {
        private final int year;
        private final int month;

        public Arguments(Request request) {
            int y = LocalDate.now().getYear();
            int m = LocalDate.now().getMonthValue();
            if (request.getAttributes().containsKey("year")) {
                try {
                    y = Integer.parseInt(request.getAttributes().get("year").toString());
                    m = Integer.parseInt(request.getAttributes().get("month").toString());
                } catch (NumberFormatException ex) {
                    //Nothing -> fallback to normal request at this point
                }
            }
            year = y;
            month = m;
        }

        public int getYear() {
            return year;
        }

        public int getMonth() {
            return month;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Arguments arguments = (Arguments) o;
            return year == arguments.year &&
                    month == arguments.month;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(year, month);
        }
    }

}
