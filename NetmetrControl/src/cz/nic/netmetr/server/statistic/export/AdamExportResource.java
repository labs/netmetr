package cz.nic.netmetr.server.statistic.export;

import cz.nic.netmetr.server.statistic.ServerResource;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Resource for handling export to CZ.NIC's ADAM analytics system. Exported data are not in any way secret, so access
 * is not restricted in any way.
 */
public class AdamExportResource extends ServerResource {
    @Get
    public Representation request(final String entity) throws SQLException, ControlServerException {
        var intervalStartUTC = getQueryValue("interval_start_time_utc");
        var intervalDuration = getQueryValue("interval_duration_secs");

        // required arguments check
        if (intervalDuration == null || intervalStartUTC == null)
            throw new ControlServerException("missing mandatory arguments");

        // value range check
        var intervalDurationLong = Long.parseLong(intervalDuration);
        if (intervalDurationLong > (50*365*24*3600))
            throw new ControlServerException("Timespan too large. Queries are limited to 50 years.");

        String sql = "SELECT" +
                " to_char(test.time at time zone 'UTC', 'YYYY-MM-DD\"T\"HH24:MI:SS') as timestamp," +
                " network_type.type as network_type," +
                " test.geo_lat as latitude," +
                " test.geo_long as longitude," +
                " test.device as device," +
                " test.model as device_model," +
                " test.product as device_product," +
                " test.speed_upload as speed_upload_kbps," +
                " test.speed_download as speed_download_kbps," +
                " test.country_geoip as country_geoip" +

                " from test, network_type where " +
                "test.network_type = network_type.uid" +
                " and (test.time at time zone 'UTC') >= to_timestamp(?, 'YYYY-MM-DD\"T\"HH24:MI:SS')" +
                " and (test.time at time zone 'UTC') < (to_timestamp(?, 'YYYY-MM-DD\"T\"HH24:MI:SS') + ?::interval) " +
                "ORDER BY test.time ASC";

        var conn = this.getDbConnectionFromApp().getConnection(); // connection will be closed asynchronously after the stream ends
        PreparedStatement ps = conn.prepareStatement(sql);  // prepared statement will be closed asyncronously after the stream ends
        ps.setString(1, intervalStartUTC);
        ps.setString(2, intervalStartUTC);
        ps.setString(3, intervalDurationLong + " sec");

        return ExportCommon.csvStreamToOutputRepresentation(ExportCommon.queryToCSVStream(conn, ps, null));
    }
}
