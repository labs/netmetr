package cz.nic.netmetr.server.statistic.export;

import com.google.common.base.Strings;
import cz.nic.netmetr.server.statistic.ServerResource;
import cz.nic.netmetr.shared.db.Client;
import cz.nic.netmetr.shared.exceptions.InvalidRequestException;
import org.restlet.representation.OutputRepresentation;
import org.restlet.resource.Get;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class MyDataExportResource extends ServerResource {

    private static final String DATA_FILTER =
            " WHERE t.deleted = false AND t.implausible = false AND t.status = 'FINISHED' ORDER BY t.client_time DESC";
    private static final String FILTERED_TABLE =
            "(select * from test where client_id IN (SELECT ? UNION SELECT uid FROM client WHERE sync_group_id = ? ))";

    @Get
    public OutputRepresentation handleGet() throws SQLException, InvalidRequestException {
        Connection conn = getDbConnectionFromApp().getConnection(); // this connection will be closed asynchronously after the returned stream ends

        String clientUUID = getRequest().getAttributes().get("client_uuid").toString();

        // get client by the UUID
        final Client client = new Client(conn);
        if (Strings.isNullOrEmpty(clientUUID) || client.getClientByUuid(UUID.fromString(clientUUID)) <= 0) {
            throw InvalidRequestException.fromMessageCode("ERROR_REQUEST_NO_UUID");
        }

        // prepare the SQL query - inspired by HistoryResource in control server
        /* ugly hack with the replacement, but massive (60x better) performance optimization */
        final String sql = ExportCommon.getExportSql(this, true).replace("$test$", FILTERED_TABLE) + DATA_FILTER;

        // run it and dump it...
        final PreparedStatement ps = ExportCommon.createPreparedStatement(sql, conn, getDbConnectionFromApp()); // not autoclose, see bellow

        // arguments
        ps.setLong(7, client.getUid());
        ps.setInt(8, client.getSync_group_id());

        return ExportCommon.csvStreamToZipOutputRepresentation(ExportCommon.queryToCSVStream(conn, ps, null), "NetMetr-my-tests.zip");
    }
}
