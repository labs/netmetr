package cz.nic.netmetr.server.statistic.export;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ForumBannerGenerator extends AbstractImageGenerator {
    private static final int BANNER_WIDTH  = 560;
    private static final int BANNER_HEIGHT = 274;

    private static final String UTC_FORMAT      = "%s UTC";

    private static final String TEMPLATE_FORMAT = "forumbanner_%s.png";

    private static final String PRIMARY_FONT    = "Roboto-Light.ttf";
    private static final String SECONDARY_FONT  = "Roboto-Medium.ttf";

    private Font primaryFont   = null;
    private Font secondaryFont = null;
    private Logger logger = LoggerFactory.getLogger(ForumBannerGenerator.class);

    private enum UnknownValueMapping {
        EN("en", "Unknown"),
        CS("cs", "Neznamé"),
        DE("de", "Unbekannt");

        private static final String FALLBACK = "n/a";

        private final String lang;
        private final String unknownValue;

        UnknownValueMapping(String lang, String unknownValue) {
            this.lang = lang;
            this.unknownValue = unknownValue;
        }

        protected static String getUnknownValue(String lang) {
            for(UnknownValueMapping item : values()) {
                if(item.lang.equals(lang)) return item.unknownValue;
            }

            return FALLBACK;
        }
    }

    private enum AdditionalValuePositionMapping {
        EN("en", 151, 432),
        CS("cs", 130, 394),
        DE("de", 128, 398);

        private static final AdditionalValuePositionMapping FALLBACK = AdditionalValuePositionMapping.EN;

        private final String lang;
        private final int firstColumnX, secondColumnX;

        AdditionalValuePositionMapping(String lang, int firstColumnX, int secondColumnX) {
            this.lang = lang;
            this.firstColumnX = firstColumnX;
            this.secondColumnX = secondColumnX;
        }

        protected static int getFirstColumnX(String lang) {
            for(AdditionalValuePositionMapping item : values()) {
                if(item.lang.equals(lang)) return item.firstColumnX;
            }

            return FALLBACK.firstColumnX;
        }

        protected static int getSecondColumnX(String lang) {
            for(AdditionalValuePositionMapping item : values()) {
                if(item.lang.equals(lang)) return item.secondColumnX;
            }

            return FALLBACK.secondColumnX;
        }
    }

    @Override
    public BufferedImage generateImage(String lang, double upload, double download, double ping, String isp, String typ, String signal, String os, String formattedTime, String formattedDate) throws IOException {
        //Initial setup of final image
        BufferedImage finalImage = new BufferedImage(BANNER_WIDTH, BANNER_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D finalImageGraphics = finalImage.createGraphics();
        finalImageGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        prepareFonts();
        renderTemplate(lang, finalImageGraphics);

        String downloadValue = formatNumber(download, lang);
        String uploadValue   = formatNumber(upload,   lang);
        String pingValue     = formatNumber(ping,     lang);

        int optimalFontSize = getOptimalFontSize(
                new String[] {downloadValue, uploadValue, pingValue},
                new int[] {80, 74, 66, 60, 54, 48, 42, 36, 30, 24},
                167,
                primaryFont,
                finalImageGraphics
                );

        if(optimalFontSize == -1) {
            //At this point something went horribly wrong. Our values wont fit in the image even on the smallest size.
            //We should probably give up on life at this point.
            //TODO - Error handling?
            logger.warn(ForumBannerGenerator.class + " Couldn't find optimal font size!");
            optimalFontSize = 1; //Why not?
        }

        renderMainValue(downloadValue,  93, 118, optimalFontSize, finalImageGraphics);
        renderMainValue(uploadValue,   280, 118, optimalFontSize, finalImageGraphics);
        renderMainValue(pingValue,     469, 118, optimalFontSize, finalImageGraphics);

        String unknownText = UnknownValueMapping.getUnknownValue(lang);

        renderAdditionalValue(isp,    unknownText, AdditionalValuePositionMapping.getFirstColumnX(lang),  219, finalImageGraphics);
        renderAdditionalValue(typ,    unknownText, AdditionalValuePositionMapping.getFirstColumnX(lang),  254, finalImageGraphics);
        renderAdditionalValue(signal, unknownText, AdditionalValuePositionMapping.getSecondColumnX(lang), 219, finalImageGraphics);
        renderAdditionalValue(os,     unknownText, AdditionalValuePositionMapping.getSecondColumnX(lang), 254, finalImageGraphics);

        renderTimeDateValue(formattedDate, unknownText, 550, 16, finalImageGraphics);
        renderTimeDateValue(String.format(UTC_FORMAT, formattedTime), unknownText, 550, 40, finalImageGraphics);

        return finalImage;
    }

    private void prepareFonts() {
        primaryFont   = loadFont(PRIMARY_FONT);
        secondaryFont = loadFont(SECONDARY_FONT);
    }

    private void renderTemplate(String language, Graphics2D graphics) {
        try {
            BufferedImage templateImage = loadTemplateImage(String.format(TEMPLATE_FORMAT, language));
            graphics.drawImage(templateImage, null, 0, 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets optimal size from possibleSizes for a font and maximumSize.
     * @param values - Strings that need to fit in to maximumSize when rendered
     * @param possibleSizes - Possible sizes to try
     * @param maximumSize - All Strings from values array need to be smaller than this
     * @param font - Font to use
     * @param graphics - Graphics to use
     * @return Actual optimal font size or -1 on failure.
     */
    private int getOptimalFontSize(String[] values, int[] possibleSizes, int maximumSize, Font font, Graphics2D graphics) {
        for(int currentSize : possibleSizes) {
            Font currentFont = font.deriveFont((float)(currentSize));
            FontMetrics fontMetrics = graphics.getFontMetrics(currentFont);

            boolean success = true;

            for(String currentValue : values) {
                if(fontMetrics.stringWidth(currentValue) > maximumSize) {
                    success = false;
                    break;
                }
            }

            if(success) return currentSize;
        }

        //Fallback - return -1
        return -1;
    }

    private void renderMainValue(String value, int centerX, int centerY, float fontSize, Graphics2D graphics) {
        graphics.setColor(Color.WHITE);

        Font font = primaryFont.deriveFont(fontSize); //Need to be float
        graphics.setFont(font);

        FontMetrics fontMetrics = graphics.getFontMetrics(font);
        int width  = fontMetrics.stringWidth(value);
        int height = fontMetrics.getHeight();

        graphics.drawString(value, centerX - width/2f, centerY + height/2f);
    }

    private void renderAdditionalValue(String value, String unknownValue, int posX, int posY, Graphics2D graphics) {
        if(value == null || value.isEmpty()) value = unknownValue;

        graphics.setColor(Color.WHITE);

        Font font = primaryFont.deriveFont(18f);
        graphics.setFont(font);

        FontMetrics fontMetrics = graphics.getFontMetrics(font);
        int height = fontMetrics.getHeight();

        graphics.drawString(value, posX, posY + height/2f);
    }

    private void renderTimeDateValue(String value, String unknownValue, int posX, int posY, Graphics2D graphics) {
        if(value == null || value.isEmpty()) value = unknownValue;

        graphics.setColor(new Color(22, 47, 103));

        Font font = secondaryFont.deriveFont(20f);
        graphics.setFont(font);

        FontMetrics fontMetrics = graphics.getFontMetrics(font);
        int width  = fontMetrics.stringWidth(value);
        int height = fontMetrics.getHeight();

        graphics.drawString(value, posX - width, posY + height/2f);
    }
}
