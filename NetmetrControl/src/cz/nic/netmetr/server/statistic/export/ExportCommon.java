package cz.nic.netmetr.server.statistic.export;

import cz.nic.netmetr.shared.Settings;
import cz.nic.netmetr.shared.SettingsHelper;
import cz.nic.netmetr.shared.db.DbConnection;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.representation.OutputRepresentation;

import java.io.*;
import java.sql.*;
import java.util.Arrays;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

final class ExportCommon {
    /* Note: parts of this query were manually optimised - mainly the subqueries instead of joins. They lead to much
       better performance. When updating it in future, be sure to test how your change performs.

       The way it is now makes bulk export queries a bit slower than with usual joins. But on the other hand,
       queries asking for smaller data sets are much faster. We could optimise by not reusing the same query
       again. But that does not seem necessary with another optimisation of data export caching.

       For the old query, look at commit 84fe251a.
     */
    private final static String EXPORT_SQL = "SELECT" +
            " ('P' || t.open_uuid) open_uuid," +
            " ('O' || t.open_test_uuid) open_test_uuid," +
            " to_char(t.time AT TIME ZONE 'UTC', 'YYYY-MM-DD HH24:MI:SS') \"time_utc\"," +
            " nt.group_name cat_technology," +
            " nt.name network_type," +
            " (CASE WHEN (t.geo_accuracy < ?) AND (t.geo_provider != 'manual') AND (t.geo_provider != 'geocoder') THEN" +
            " t.geo_lat" +
            " WHEN (t.geo_accuracy < ?) THEN" +
            " ROUND(t.geo_lat*1111)/1111" +
            " ELSE null" +
            " END) lat," +
            " (CASE WHEN (t.geo_accuracy < ?) AND (t.geo_provider != 'manual') AND (t.geo_provider != 'geocoder') THEN" +
            " t.geo_long" +
            " WHEN (t.geo_accuracy < ?) THEN" +
            " ROUND(t.geo_long*741)/741 " +
            " ELSE null" +
            " END) long," +
            " (CASE WHEN ((t.geo_provider = 'manual') OR (t.geo_provider = 'geocoder')) THEN" +
            " 'rastered'" + //make raster transparent
            " ELSE t.geo_provider" +
            " END) loc_src," +
            " (CASE WHEN (t.geo_accuracy < ?) AND (t.geo_provider != 'manual') AND (t.geo_provider != 'geocoder') " +
            " THEN t.geo_accuracy " +
            " WHEN (t.geo_accuracy < 100) AND ((t.geo_provider = 'manual') OR (t.geo_provider = 'geocoder')) THEN 100" + // limit accuracy to 100m
            " WHEN (t.geo_accuracy < ?) THEN t.geo_accuracy" +
            " ELSE null END) loc_accuracy, " +
            " data->>'region' region," +
            " data->>'municipality' municipality," +
            " data->>'settlement' settlement," +
            " data->>'whitespace' whitespot," +
            " (CASE WHEN (t.zip_code < 1000 OR t.zip_code > 9999) THEN null ELSE t.zip_code END) zip_code," +
            " t.speed_download download_kbit," +
            " t.speed_upload upload_kbit," +
            " (t.ping_median::float / 1000000) ping_ms," +
            " t.lte_rsrp," +
            " ts.name server_name," +
            " duration test_duration," +
            " num_threads," +
            " (CASE WHEN publish_public_data THEN t.plattform ELSE NULL END) platform," +
            " (CASE WHEN publish_public_data THEN COALESCE(adm.fullname, t.model) ELSE NULL END) model," +
            " client_software_version client_version," +
            " network_operator network_mcc_mnc," +
            " network_operator_name network_name," +
            " network_sim_operator sim_mcc_mnc," +
            " nat_type," +
            " public_ip_asn asn," +
            " (CASE WHEN publish_public_data THEN client_public_ip_anonymized ELSE NULL END) ip_anonym," +
            " (ndt.s2cspd*1000)::int ndt_download_kbit," +
            " (ndt.c2sspd*1000)::int ndt_upload_kbit," +
            " COALESCE(t.implausible, false) implausible," +
            " t.signal_strength, " +
            " ((EXTRACT (EPOCH FROM (t.timestamp - t.time))) * 1000) speed_test_duration_ms, " +
            " (select location_id from cell_location cl where cl.test_id = t.uid order by cl.test_id limit 1) cell_id," +
            " (select area_code from cell_location cl where cl.test_id = t.uid order by cl.test_id limit 1) lac," +
            " (select tags from audit a where t.uid=a.uid) tags," +
            " tt.description test_type";
    private final static String EXPORT_SQL_SUFFIX =
            ", t.additional_report_fields " +
                    " FROM $test$ t" +
                    " LEFT JOIN network_type nt ON nt.uid=t.network_type" +
                    " LEFT JOIN device_map adm ON adm.codename=t.model" +
                    " LEFT JOIN test_server ts ON ts.uid=t.server_id" +
                    " LEFT JOIN test_ndt ndt ON t.uid=ndt.test_id" +
                    " LEFT JOIN test_type tt ON tt.id=t.test_type_id";

    /**
     * Takes ownership of the given PreparedStatement and its undelying database connection. User must not close it manually.
     *
     * @param connToClose      Connection that should be closed as soon as the returned stream ends
     * @param ps               PreparedStatement which will be executed and whose ResultSet will be used
     * @param reportFieldsData
     * @return closeable stream with data from the result set
     * @throws SQLException
     */
    static Stream<String[]> queryToCSVStream(final Connection connToClose, final PreparedStatement ps, final AdditionalReportFieldsData reportFieldsData) throws SQLException {
        final ResultSet rs = ps.executeQuery();

        // create CSV header
        final ResultSetMetaData meta = rs.getMetaData();
        final int columnCount = meta.getColumnCount();
        final int columnCountWithReportFields = columnCount + (reportFieldsData != null ? reportFieldsData.getColumns().length : 0);
        final String[] columns = new String[columnCountWithReportFields];
        for (int i = 0; i < columnCount; i++) {
            columns[i] = meta.getColumnName(i + 1);
        }
        for (int i = columnCount; i < columnCountWithReportFields; i++) {
            columns[i] = reportFieldsData.columns[i - columnCount];
        }

        // test whether the query contains additional report fields
        final boolean hasAdditionalReportFields;
        boolean tmp;
        try {
            rs.findColumn("additional_report_fields");
            tmp = true;
        } catch (SQLException e) {
            tmp = false;
        }
        hasAdditionalReportFields = tmp;

        // prepare functions for the stream
        Predicate<String[]> wrappedHasNext = cols -> {
            try {
                return rs.next();
            } catch (SQLException e) {
                throw new RuntimeException("querying of next value failed", e);
            }
        };
        UnaryOperator<String[]> next = cols -> {
            //final String[] line = new String[columnCountWithReportFields];
            try {
                String[] line = cols;
                Arrays.fill(line, null); // clear previous row

                for (int i = 0; i < columnCount; i++) {
                    final Object obj = rs.getObject(i + 1);
                    line[i] = obj == null ? null : obj.toString();
                }

                if (hasAdditionalReportFields) {
                    try {
                        final String reportData = rs.getString("additional_report_fields");
                        if (reportData != null) {
                            final JSONObject reportJson = new JSONObject(reportData);
                            for (int i = columnCount; i < columnCountWithReportFields; i++) {
                                final Object obj = reportJson.opt(reportFieldsData.getColumns()[i - columnCount]);
                                line[i] = obj == null ? null : obj.toString();
                            }
                        }
                    } catch (final JSONException e) {
                        e.printStackTrace();
                    }
                }

                return line;
            } catch (SQLException e) {
                throw new RuntimeException("querying failed", e);
            }
        };

        // closing of the result set
        Runnable closeResultSet = () -> {
            try {
                rs.close();
                ps.close();
                if (connToClose != null)
                    connToClose.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        };

        // actual stream creation
        var stream = Stream.iterate(columns, wrappedHasNext, next);
        return stream.onClose(closeResultSet);
    }

    static void writeCSVLinesAsZipToStream(OutputStream out, final Stream<String[]> csvLines) throws IOException {
        try (ZipOutputStream outstream = new ZipOutputStream(out)) {

            // add licence to the archive
            final ZipEntry zeLicense = new ZipEntry("LICENSE.txt");
            outstream.putNextEntry(zeLicense);
            final InputStream licenseIS = ExportCommon.class.getResourceAsStream("DATA_LICENSE.txt");
            IOUtils.copy(licenseIS, outstream);
            licenseIS.close();

            final ZipEntry zeCsv = new ZipEntry("data.csv");
            outstream.putNextEntry(zeCsv);

            writeCSVLinesToStream(csvLines, outstream);
        }
    }

    static void writeCSVLinesToZipFile(File outfile, final Stream<String[]> csvLines) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(outfile)) {
            writeCSVLinesAsZipToStream(fos, csvLines);
        }
    }

    static void writeCSVLinesToPlainFile(File outfile, final Stream<String[]> csvLines) throws IOException {
        try (OutputStream outstream = new FileOutputStream(outfile)) {
            writeCSVLinesToStream(csvLines, outstream);
        }
    }

    static void writeCSVLinesToStream(Stream<String[]> csvLines, OutputStream outstream) throws IOException {
        try (csvLines; final CSVPrinter csvPrinter = new CSVPrinter(new OutputStreamWriter(outstream), CSVFormat.RFC4180)) {
            csvLines.forEachOrdered(line -> {
                try {
                    for (final String f : line)
                        csvPrinter.print(f);
                    csvPrinter.println();
                } catch (IOException e) {
                    throw new RuntimeException("IO failure", e);
                }
            });

            csvPrinter.flush();
        } catch (RuntimeException e) {
            if (e.getCause() instanceof IOException)  // unwrap stream exceptions
                throw (IOException) e.getCause();
            throw e;
        }
    }

    static boolean isAudited(DbConnection dbConn, int year, int month) throws SQLException {
        try (Connection conn = dbConn.getConnection()) {
            final String auditResultCheck = "SELECT checked FROM audit_results WHERE (EXTRACT (year FROM timestamp_month)) = ? AND (EXTRACT (month FROM timestamp_month)) = ?";
            try (var ps = conn.prepareStatement(auditResultCheck)) {
                ps.setInt(1, year);
                ps.setInt(2, month);

                if (!ps.execute()) {
                    return false;
                }

                try (var rs = ps.getResultSet()) {
                    return rs.next() && rs.getBoolean(1);
                }
            }
        }
    }

    static OutputRepresentation csvStreamToOutputRepresentation(final Stream<String[]> csvLines) {
        return new OutputRepresentation(MediaType.TEXT_CSV) {
            @Override
            public void write(OutputStream out) throws IOException {
                writeCSVLinesToStream(csvLines, out);
                out.close();
            }
        };
    }

    static OutputRepresentation csvStreamToZipOutputRepresentation(final Stream<String[]> csvLines, String filename) {
        final OutputRepresentation result = new OutputRepresentation(MediaType.APPLICATION_ZIP) {

            @Override
            public void write(OutputStream out) throws IOException {
                writeCSVLinesAsZipToStream(out, csvLines);
                out.close();
            }
        };


        final Disposition disposition = new Disposition(Disposition.TYPE_ATTACHMENT);
        disposition.setFilename(filename);
        result.setDisposition(disposition);

        return result;
    }

    static OutputRepresentation fileToOutputRepresentation(File cachedFile, boolean isZip) {
        final OutputRepresentation result = new OutputRepresentation(isZip ? MediaType.APPLICATION_ZIP
                : MediaType.TEXT_CSV) {

            @Override
            public void write(OutputStream out) throws IOException {
                InputStream is = new FileInputStream(cachedFile);
                IOUtils.copy(is, out);
                out.close();
            }

        };

        if (isZip) {
            final Disposition disposition = new Disposition(Disposition.TYPE_ATTACHMENT);
            disposition.setFilename(cachedFile.getName());
            result.setDisposition(disposition);
        }

        return result;
    }

    /**
     * creates a prepared statement where the first 6 parameters are already set (accuracy)
     *
     * @param sql
     * @param conn   Used to create the prepared statement. Left opened afterwards.
     * @param dbConn Used to retrieve settings, does not leave anything opened
     * @return
     * @throws SQLException
     */
    static PreparedStatement createPreparedStatement(final String sql, final Connection conn, final DbConnection dbConn) throws SQLException {
        final PreparedStatement ps = conn.prepareStatement(sql);
        //insert filter for accuracy
        double accuracy = Double.parseDouble(SettingsHelper.getSetting(dbConn, "rmbt_geo_accuracy_detail_limit", null));
        ps.setDouble(1, accuracy);
        ps.setDouble(2, accuracy);
        ps.setDouble(3, accuracy);
        ps.setDouble(4, accuracy);
        ps.setDouble(5, accuracy);
        ps.setDouble(6, accuracy);

        ps.closeOnCompletion();

        return ps;
    }

    static String getExportSql(Settings settings, boolean leaveTableNamePattern) {
        /* the flag leaveTableNamePattern is used for massive performance improvement in MyDataExportResource.java */

        final String hasAdvSpeedOption = settings.getSetting("has_advertised_speed_option");

        String sql = EXPORT_SQL +
                ((hasAdvSpeedOption != null && Boolean.parseBoolean(hasAdvSpeedOption)) ?
                        "t.adv_spd_option_name advertised_internet_connection, " +
                                "t.adv_spd_up_kbit advertised_up_kbit, " +
                                "t.adv_spd_down_kbit advertised_down_kbit, " +
                                "(t.adv_spd_up_kbit - t.speed_upload) deviation_advertised_up_kbit, " +
                                "(t.adv_spd_down_kbit - t.speed_download) deviation_advertised_down_kbit, "
                        : "") +
                EXPORT_SQL_SUFFIX;

        if (leaveTableNamePattern) return sql;
        else return sql.replace("$test$", "test");
    }

    static String getExportSql(Settings settings) {
        return getExportSql(settings, false);
    }

    static final class AdditionalReportFieldsData {
        final String[] columns;

        public AdditionalReportFieldsData(final String[] columns) {
            this.columns = columns;
        }

        public String[] getColumns() {
            return columns;
        }
    }
}
