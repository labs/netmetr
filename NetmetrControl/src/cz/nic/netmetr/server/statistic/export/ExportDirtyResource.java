package cz.nic.netmetr.server.statistic.export;

public class ExportDirtyResource extends ExportResource {
    @Override
    protected String getFilenameTemplateCSV() {
        return "NetMetr-opendata-dirty-%YEAR%-%MONTH%.csv";
    }

    @Override
    protected String getFilenameTemplateZIP() {
        return "NetMetr-opendata-dirty-%YEAR%-%MONTH%.zip";
    }

    @Override
    protected boolean keyCheckEnabled() {
        return true;
    }

    @Override
    protected boolean isAudited(int year, int month) {
        return true;
    }
}
