/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.statistic.export;

import cz.nic.netmetr.server.statistic.ServerResource;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class TestExportResource extends ServerResource {

    @Get
    public Representation request(final String entity) throws SQLException {
        final UUID openUUID = UUID.fromString(getRequest().getAttributes().get("open_test_uuid").toString());

        final String sql = ExportCommon.getExportSql(this) +
                " WHERE " +
                " t.deleted = false" +
                " AND t.open_test_uuid = ?::uuid";

        var conn = getDbConnectionFromApp().getConnection();
        final PreparedStatement ps = ExportCommon.createPreparedStatement(sql, conn, getDbConnectionFromApp()); // the prepared statement, its connection and the created result set will be closed by the query tool
        ps.setObject(7, openUUID.toString());

        return ExportCommon.csvStreamToZipOutputRepresentation(ExportCommon.queryToCSVStream(conn, ps, null), "O" + openUUID.toString() + ".zip");
    }
}
