package cz.nic.netmetr.server.statistic.export;

import cz.nic.netmetr.shared.SignificantFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.text.FieldPosition;
import java.text.Format;
import java.util.Locale;

public abstract class AbstractImageGenerator {
    private static final String TEMPLATES_PATH = "templates/";
    private static final String FONT_PATH      = "fonts/";

    private static final String FALLBACK_FONT_NAME = "Droid Sans";
    private Logger logger = LoggerFactory.getLogger(AbstractImageGenerator.class);

    /**
     * Generates a image for showing the user its speed test result
     * @param lang: Language of the image, currently either 'de' or 'en'
     * @param upload: Upload speed in mbps
     * @param download: Download speed in mbps
     * @param ping: Ping in ms
     * @param isp: ISP name
     * @param typ: Test type (LAN, 3G, 4G, etc.)
     * @param signal: Signal strength in dbm
     * @param os: Plattform used for conducting the test (Android, IOS, Applet, Browser)
     * @return rendered image
     */
    public abstract BufferedImage generateImage(
            String lang,
            double upload,
            double download,
            double ping,
            String isp,
            String typ,
            String signal,
            String os,
            String formattedTime,
            String formattedDate
    ) throws IOException;

    protected BufferedImage loadTemplateImage(String name) throws IOException {
        return ImageIO.read(super.getClass().getResourceAsStream(TEMPLATES_PATH + name));
    }

    protected Font loadFont(String name) {
        try {
            InputStream fontInputStream = super.getClass().getResourceAsStream(FONT_PATH + name);
            return Font.createFont(Font.TRUETYPE_FONT, fontInputStream);
        } catch (FontFormatException | IOException e) {
            logger.warn("Couldn't load font w/ name: " + name + ". Using fallback font!", e);
            return new Font(FALLBACK_FONT_NAME, Font.PLAIN, 1);
        }
    }


    /**
     * Formats a number to 2 significant digits
     * @param number the number
     * @return the formatted number
     */
    protected String formatNumber(double number, String lang) {
        final Locale locale = new Locale(lang);
        final Format format = new SignificantFormat(2, locale);

        final StringBuffer buf = format.format(number, new StringBuffer(), new FieldPosition(0));
        return buf.toString();
    }

    protected void drawCenteredString(String s, int x, int y, int w, int h, Graphics g) {
        FontMetrics fm = g.getFontMetrics();
        x += (w - fm.stringWidth(s)) / 2;
        y += (fm.getAscent() + (h - (fm.getAscent() + fm.getDescent())) / 2);
        g.drawString(s, x, y);
    }
}
