/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.statistic;

import cz.nic.netmetr.boot.DbConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DevicesRestlet extends AbstractStatisticsRestlet {

    @Override
    protected void processRequest(JSONObject answer, StatisticParameters params, boolean useMobileProvider, String sqlTimeCondition, String sqlImplausibleCondition, boolean signalMobile, String where) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try (var conn = DbConnection.getInstance().getConnection()){
            final JSONArray devices = new JSONArray();
            answer.put("devices", devices);

            ps = selectDevices(conn, true, params.getQuantile(), params.getAccuracy(), params.getCountry(), useMobileProvider, where, params.getMaxDevices(), sqlTimeCondition, sqlImplausibleCondition);
            if (!ps.execute())
                return;
            rs = fillJSON(params.getLang(), ps, devices);

            ps = selectDevices(conn,false, params.getQuantile(), params.getAccuracy(), params.getCountry(), useMobileProvider, where, params.getMaxDevices(), sqlTimeCondition, sqlImplausibleCondition);
            if (!ps.execute())
                return;
            final JSONArray devicesSumsArray = new JSONArray();
            rs = fillJSON(params.getLang(), ps, devicesSumsArray);
            if (devicesSumsArray.length() == 1)
                answer.put("devices_sums", devicesSumsArray.get(0));

        } catch (final JSONException e) {
            e.printStackTrace();
        } catch (final SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private PreparedStatement selectDevices(Connection conn, final boolean group, final float quantile, final double accuracy, final String country, final boolean useMobileProvider, final String where, final int maxDevices, final String sqlTimeCondition, final String sqlImplausibleCondition) throws SQLException {
        PreparedStatement ps;
        String sql = String.format("SELECT" +
                (group ? " COALESCE(adm.fullname, t.model) model," : "") +
                " count(t.uid) count," + " quantile(speed_download::bigint, ?::double precision) quantile_down," +
                " quantile(speed_upload::bigint, ?::double precision) quantile_up," +
                " quantile(ping_shortest::bigint, ?::double precision) quantile_ping" +
                " FROM test t" +
                " LEFT JOIN device_map adm ON adm.codename=t.model" +
                " LEFT JOIN network_type nt ON nt.uid=t.network_type" +
                " WHERE %s" +
                " AND t.deleted = false AND t.status = 'FINISHED' AND t.test_type_id = 1 " +
                sqlImplausibleCondition +
                sqlTimeCondition +
                (useMobileProvider ? " AND t.mobile_provider_id IS NOT NULL" : "") +
                ((accuracy > 0) ? " AND t.geo_accuracy < ?" : "") + 
                (group ? " GROUP BY COALESCE(adm.fullname, t.model) HAVING count(t.uid) > 10" : "") +
                " ORDER BY count DESC" +
                " LIMIT %d", where, maxDevices);
        if (country != null) {
        	sql = String.format("SELECT" +
                    (group ? " COALESCE(adm.fullname, t.model) model," : "") +
                    " count(t.uid) count," + " quantile(speed_download::bigint, ?::double precision) quantile_down," +
                    " quantile(speed_upload::bigint, ?::double precision) quantile_up," +
                    " quantile(ping_shortest::bigint, ?::double precision) quantile_ping" +
                    " FROM test t" +
                    " LEFT JOIN device_map adm ON adm.codename=t.model" +
                    " LEFT JOIN network_type nt ON nt.uid=t.network_type" +
                    (useMobileProvider ? " LEFT JOIN mccmnc2name p ON p.uid = t.mobile_sim_id" : "") +
                    " WHERE %s" +
                    " AND t.deleted = false AND t.status = 'FINISHED' AND t.test_type_id = 1 " +
                    sqlImplausibleCondition +
                    sqlTimeCondition +
                    " AND " + (useMobileProvider?"p.country = ? AND ((t.country_location IS NULL OR t.country_location = ?)  AND ((NOT t.roaming_type = 2) OR t.roaming_type IS NULL))":"t.country_geoip = ? ") +
                    ((accuracy > 0) ? " AND t.geo_accuracy < ?" : "") + 
                    (group ? " GROUP BY COALESCE(adm.fullname, t.model) HAVING count(t.uid) > 10" : "") +
                    " ORDER BY count DESC" +
                    " LIMIT %d", where, maxDevices);
        }
        
        ps = conn.prepareStatement(sql);
        
        int i = 1;
        for (int j = 0; j < 2; j++)
            ps.setFloat(i++, quantile);
        ps.setFloat(i++, 1 - quantile); // inverse for ping
        
        if (country != null) {       	
        	if (useMobileProvider) {
        		ps.setString(i++, country.toLowerCase()); //mccmnc2name.country
        		ps.setString(i++, country.toUpperCase()); //country_location
        	}
        	else {
        		ps.setString(i++, country.toUpperCase());
        	}
        }
        
        if (accuracy>0) {
        	ps.setDouble(i++, accuracy);
        }

        return ps;
    }
}
