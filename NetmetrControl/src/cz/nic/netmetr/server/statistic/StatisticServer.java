/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.statistic;

import cz.nic.netmetr.server.statistic.export.*;
import cz.nic.netmetr.shared.db.DbConnection;
import cz.nic.netmetr.shared.filter.LoggingRequestIdFilter;
import cz.nic.netmetr.shared.resources.openapi.OpenAPIUIResource;
import cz.nic.netmetr.shared.restlet.filter.DefaultHeadersFilter;
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;
import org.restlet.routing.Template;

public class StatisticServer extends Application {

    final DbConnection dbConnection;

    public StatisticServer(DbConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public DbConnection getDbConnection() {
        return dbConnection;
    }

    /**
     * The Restlet instance that will call the correct resource depending up on
     * URL mapped to it.
     *
     * @return -- The resource Restlet mapped to the URL.
     */
    @Override
    public Restlet createInboundRoot() {

        final Router router = new Router(getContext());

        router.attach("/version", VersionResource.class);

        router.attach("/statistics", new StatisticsRestlet());

        router.attach("/devices", new DevicesRestlet());
        router.attach("/months", RevisedMonthsResource.class);
        router.attach("/approve", ApproveMonthResource.class);
        router.attach("/monthlyerrors", MonthlyErrorsResource.class);

        router.attach("/export/O{open_test_uuid}", TestExportResource.class, Template.MODE_STARTS_WITH);
        router.attach("/export/NetMetr-opendata-{year}-{month}.", ExportResource.class, Template.MODE_STARTS_WITH);
        router.attach("/exportDirty/NetMetr-opendata-dirty-{year}-{month}.", ExportDirtyResource.class, Template.MODE_STARTS_WITH);
        router.attach("/export/client/{client_uuid}/syncgroup/NetMetr-mydata.zip", MyDataExportResource.class);
        router.attach("/export/adam", AdamExportResource.class); // data export to CZ.NIC's system called ADAM
        router.attach("/export", ExportResource.class, Template.MODE_STARTS_WITH);

        router.attach("/{lang}/{open_test_uuid}/{size}.png", ImageExport.class);

        // administrative resources (access restrictions might be applied to /admin/ 

        router.attach("/opentests", OpenTestSearchResource.class);

        router.attach("/opentests/histogra{histogram}", OpenTestSearchResource.class);

        router.attach("/opentests/search", OpenTestSearchResource.class, Template.MODE_STARTS_WITH);

        router.attach("/opentests/O{open_test_uuid}&sender={sender}", OpenTestResource.class);
        router.attach("/opentests/O{open_test_uuid}", OpenTestResource.class);

        router.attach("/admin/usage", UsageResource.class);
        router.attach("/admin/usageJSON", UsageJSONResource.class);

        // openapi docs
        router.attach("/api/v1/openapi", OpenAPIUIResource.class);
        router.attach("/api/v1/openapi.yaml", OpenAPISpecResource.class);


        // adds cors headers etc
        DefaultHeadersFilter defaultHeadersFilter = new DefaultHeadersFilter();
        defaultHeadersFilter.setNext(router);

        // configure filter which adds IDs to logs
        LoggingRequestIdFilter loggingRequestIdFilter = new LoggingRequestIdFilter();
        loggingRequestIdFilter.setNext(defaultHeadersFilter);

        return loggingRequestIdFilter;
    }

}
