package cz.nic.netmetr.server.statistic;

import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RevisedMonthsResource extends ServerResource {

    private static final String     REVISED_MONTHS_SELECT   = "SELECT (EXTRACT (year FROM timestamp_month)), (EXTRACT (month FROM timestamp_month)) FROM audit_results WHERE checked = ?";
    private static final String     REVISED_MONTHS_KEY      = "revised_months";
    private static final String     CHECKED_PARAMETER       = "checked";
    private static final boolean    DEFAULT_CHECKED         = true;

    @Get
    @Post("json")
    public String request(final String entity) {
        boolean checked = DEFAULT_CHECKED;
        try {
            if (entity != null) {
                final JSONObject request = new JSONObject(entity);
                checked = request.optBoolean(CHECKED_PARAMETER, checked);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        final JSONObject answer = new JSONObject();
        
        PreparedStatement   ps = null;
        ResultSet           rs = null;

        try (var conn = getDbConnectionFromApp().getConnection()) {
            ps = conn.prepareStatement(REVISED_MONTHS_SELECT);
            ps.setBoolean(1, checked);
            if (!ps.execute()) {
                return null;
            }
            rs = ps.getResultSet();

            Map<Integer, List<Integer>> resultMap = new HashMap<>();
            while (rs.next()) {
                Integer year = rs.getInt(1);
                final Integer month = rs.getInt(2);
                if (!resultMap.containsKey(year)) {
                    resultMap.put(year, new ArrayList<Integer>() {{add(month);}});
                } else {
                    resultMap.get(year).add(month);
                }
            }

            answer.append(REVISED_MONTHS_KEY, resultMap);

            return answer.toString();
        } catch (final JSONException e) {
            e.printStackTrace();
        } catch (final SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}