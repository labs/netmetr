/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.statistic;

import cz.nic.netmetr.shared.Classification;
import cz.nic.netmetr.shared.ResourceManager;
import cz.nic.netmetr.shared.Settings;
import cz.nic.netmetr.shared.SettingsHelper;
import cz.nic.netmetr.shared.db.DbConnection;
import org.restlet.data.Header;
import org.restlet.data.Reference;
import org.restlet.representation.Representation;
import org.restlet.resource.Options;
import org.restlet.resource.ResourceException;
import org.restlet.util.Series;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;

public class ServerResource extends org.restlet.resource.ServerResource implements Settings {
    protected ResourceBundle labels;
    protected ResourceBundle settings;
    protected Classification classification;
    private Logger logger = LoggerFactory.getLogger(ServerResource.class);

    protected DbConnection getDbConnectionFromApp() {
        return ((StatisticServer) getApplication()).getDbConnection();
    }
    @Override
    public void doInit() throws ResourceException
    {
        super.doInit();
        classification = Classification.getInstance(((StatisticServer)getApplication()).getDbConnection());


        settings = ResourceManager.getCfgBundle();
        // Set default Language for System
        Locale.setDefault(new Locale(settings.getString("RMBT_DEFAULT_LANGUAGE")));
        labels = ResourceManager.getSysMsgBundle();
    }
    
    @Options
    public void doOptions(final Representation entity) {
        // must be here due to CORS test from the browser
    }
    
    @SuppressWarnings("unchecked")
    public String getIP()
    {
        final Series<Header> headers = (Series<Header>) getRequest().getAttributes().get("org.restlet.http.headers");
        final String realIp = headers.getFirstValue("X-Real-IP", true);
        if (realIp != null)
            return realIp;
        else
            return getRequest().getClientInfo().getAddress();
    }
    
    @SuppressWarnings("unchecked")
    public Reference getURL()
    {
        final Series<Header> headers = (Series<Header>) getRequest().getAttributes().get("org.restlet.http.headers");
        final String realURL = headers.getFirstValue("X-Real-URL", true);
        if (realURL != null)
            return new Reference(realURL);
        else
            return getRequest().getOriginalRef();
    }

    @Override
    public String getSetting(String key)
    {
        return getSetting(key, null);
    }
    
    // TODO: add caching!
    @Override
    public String getSetting(String key, String lang) {
        return SettingsHelper.getSetting(getDbConnectionFromApp(), key, lang);
    }
}
