package cz.nic.netmetr.server.statistic;

import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class MonthlyErrorsResource extends ServerResource {

    private static final String MONTH_PARAMETER         = "month";
    private static final String YEAR_PARAMETER          = "year";

    private static final String ILLEGAL_PARAMETERS_MESSAGE  = "ERROR: illegal parameters!";

    private static final String MONTHLY_ERRORS_SELECT   = "SELECT a.tags, count(a.tags) FROM audit a INNER JOIN test t ON t.uid = a.uid WHERE a.tags != '{}' AND (EXTRACT (year FROM t.time)) = ? AND (EXTRACT (month FROM t.time)) = ? GROUP BY a.tags";
    private static final String MONTHLY_ERRORS_KEY      = "monthly_errors";

    @Get
    @Post("json")
    public final String request(final String entity) {
        int     month   = -1;
        int     year    = -1;
        try {
            if (entity != null) {
                final JSONObject request = new JSONObject(entity);
                month   = request.getInt(MONTH_PARAMETER);
                year    = request.getInt(YEAR_PARAMETER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (year < -1 || month < 1 || month > 12) {
            return ILLEGAL_PARAMETERS_MESSAGE;
        }

        final JSONObject answer = new JSONObject();

        PreparedStatement   ps = null;
        ResultSet           rs = null;

        try (var conn = getDbConnectionFromApp().getConnection()){
            ps = conn.prepareStatement(MONTHLY_ERRORS_SELECT);
            ps.setInt(1, year);
            ps.setInt(2, month);

            if (!ps.execute()) {
                return null;
            }
            rs = ps.getResultSet();

            Map<Array, Integer> resultMap = new HashMap<>();
            while (rs.next()) {
                Array errorCodes = rs.getArray(1);
                final Integer count = rs.getInt(2);
                if (!resultMap.containsKey(year)) {
                    resultMap.put(errorCodes, count);
                } else {
                    resultMap.put(errorCodes, resultMap.get(errorCodes) + count);
                }
            }

            answer.append(MONTHLY_ERRORS_KEY, resultMap);

            return answer.toString();
        } catch (final JSONException e) {
            e.printStackTrace();
        } catch (final SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}