/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.statistic;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import cz.nic.netmetr.boot.DbConnection;
import cz.nic.netmetr.shared.SettingsHelper;
import org.joda.time.Instant;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.MediaType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.Duration;
import java.util.Optional;

public abstract class AbstractStatisticsRestlet extends Restlet {

    @Override
    public void handle(Request request, Response response) {
        response.setEntity(request(request.getEntityAsText()), MediaType.APPLICATION_JSON);
    }

    /*
     Because the cache works on Strings, it can contain the same thing multiple times, because someone asked
     with differently ordered arguments or similar. We ignore it and cache all possibilities, it's still an improvement
     on not caching at all.
     */
    private final LoadingCache<Optional<String>, String> cache = Caffeine.newBuilder()
            .expireAfterWrite(Duration.ofHours(1))
            .refreshAfterWrite(Duration.ofMinutes(10))
            .maximumSize(1_000)
            .<Optional<String>, String>buildAsync(key -> requestRaw(key.orElse(null)))
            .synchronous();

    private final String request(final String entity) {
        // the function requestRaw get's called by the cache
        return cache.get(Optional.ofNullable(entity));
    }

    private final String requestRaw(final String entity) {
        final StatisticParameters params = new StatisticParameters(SettingsHelper.getSetting(DbConnection.getInstance(),"RMBT_DEFAULT_LANGUAGE"), entity, getClass());
        

        final float quantile                      = params.getQuantile();
        final int durationDays                    = params.getDuration();
        final String type                         = params.getType();
        final String networkTypeGroup             = params.getNetworkTypeGroup();
        final int year                            = params.getYear();
        final int month                           = params.getMonth();
        final boolean monthOrYearSet              = params.isMonthOrYearSet();
        final ImplausibleFilter implausibleFilter = params.getImplausibleFilter();

        final String sqlTimeCondition;
        if (monthOrYearSet) {
            sqlTimeCondition = String.format(" AND EXTRACT(month FROM t.time) = %d AND EXTRACT(year FROM t.time) = %d", month, year);
        } else {
            sqlTimeCondition = String.format(" AND t.time > NOW() -  CAST((%d || ' days') AS INTERVAL)", durationDays);
        }

        String sqlImplausibleCondition = String.format("AND t.implausible %s", implausibleFilter.getSqlValue());
        
        boolean useMobileProvider = false;
        
        final boolean signalMobile;
        final String where;
        if (type.equals("mobile")) {
            signalMobile = true;
            useMobileProvider = true;

            if (networkTypeGroup == null) {
                where = "nt.type = 'MOBILE'";
            } else {
                if ("2G".equalsIgnoreCase(networkTypeGroup))
                    where = "nt.group_name = '2G'";
                else if ("3G".equalsIgnoreCase(networkTypeGroup))
                    where = "nt.group_name = '3G'";
                else if ("4G".equalsIgnoreCase(networkTypeGroup))
                    where = "nt.group_name = '4G'";
                else if ("5G".equalsIgnoreCase(networkTypeGroup))
                    where = "nt.group_name = '5G'";
                else if ("mixed".equalsIgnoreCase(networkTypeGroup))
                    // Note: this is weird. When adding 5G, I didn't understand this. How is this any different than
                    // not specifying any group? I just blindly added the 5G values to the list bellow for good measure.
                    // However, I am kind of convinced that it is pointless and that I could as well remove it completely.
                    //
                    // If you have a better idea what this means, please leave a comment and/or fix it. Thanks. :)
                    //
                    // Technical note: cz.nic.netmetr.model.NetworkType is an enum defining all these types...
                    where = "nt.group_name IN ('2G/3G','2G/4G','3G/4G','2G/3G/4G', '2G/3G/4G/5G', '3G/4G/5G', '4G/5G')";
                else
                    where = "1=0";
            }
        } else if (type.equals("wifi")) {
            where = "nt.type='WLAN'";
            signalMobile = false;
        } else if (type.equals("browser")) {
            where = "nt.type = 'LAN'";
            signalMobile = false;
        } else {   // invalid request
            where = "1=0";
            signalMobile = false;
        }
        
        final JSONObject answer = new JSONObject();
        answer.put("responseGeneratedAt", Instant.now().toDateTime().toString());
        answer.put("quantile", quantile);
        answer.put("type", type);
        if (monthOrYearSet) {
            answer.put("month", month);
            answer.put("year", year);
        } else {
            answer.put("duration", durationDays);
        }

        processRequest(answer, params, useMobileProvider, sqlTimeCondition, sqlImplausibleCondition, signalMobile, where);

        return answer.toString();
    }

    protected abstract void processRequest(JSONObject answer, StatisticParameters params, boolean useMobileProvider, String sqlTimeCondition, String sqlImplausibleCondition, boolean signalMobile, String where);
    
    protected static ResultSet fillJSON(final String lang, final PreparedStatement ps, final JSONArray providers) throws SQLException, JSONException {
        ResultSet rs;
        rs = ps.getResultSet();
        final ResultSetMetaData metaData = rs.getMetaData();
        final int columnCount = metaData.getColumnCount();
        while (rs.next()) {
            final JSONObject obj = new JSONObject();
            for (int j = 1; j <= columnCount; j++) {
                final String colName = metaData.getColumnName(j);
                Object data = rs.getObject(j);
                if (colName.equals("name") && data == null)
                    if (lang != null && lang.equals("de"))
                        data = "Andere Betreiber";
                    else
                        data = "Other operators";
                if (colName.equals("shortname") && data == null) {
                    if (lang != null && lang.equals("de"))
                            data = "Andere";
                        else
                            data = "Others";
                }
                obj.put(colName, data);
            }
            providers.put(obj);
        }
        return rs;
    }
}
