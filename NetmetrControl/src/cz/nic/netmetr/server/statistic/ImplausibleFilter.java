package cz.nic.netmetr.server.statistic;

public enum ImplausibleFilter {
    FALSE   ("false", "= false"),
    TRUE    ("true", "= true"),
    ALL     ("all", "IN (true, false)");

    public static final ImplausibleFilter DEFAULT_VALUE = FALSE;

    String value;
    String sqlValue;

    ImplausibleFilter(String value, String sqlValue) {
        this.value = value;
        this.sqlValue = sqlValue;
    }

    public String getSqlValue() {
        return sqlValue;
    }

    public static ImplausibleFilter getImplausibleFilterForValue(String value) {
        for (ImplausibleFilter implausibleFilter: ImplausibleFilter.values()) {
            if (implausibleFilter.value.equals(value.trim().toLowerCase())) {
                return implausibleFilter;
            }
        }
        return DEFAULT_VALUE;
    }
}
