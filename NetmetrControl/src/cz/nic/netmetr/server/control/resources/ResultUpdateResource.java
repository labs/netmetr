/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.google.common.base.Strings;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.db.Client;
import cz.nic.netmetr.shared.db.GeoLocation;
import cz.nic.netmetr.shared.db.Test;
import cz.nic.netmetr.shared.db.fields.DoubleField;
import cz.nic.netmetr.shared.db.fields.IntField;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import cz.nic.netmetr.shared.exceptions.InvalidRequestException;
import io.swagger.client.model.Error;
import io.swagger.client.model.ResultUpdateRequest;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

public class ResultUpdateResource extends ServerResource {
    private final static String GEO_PROVIDER_MANUAL = "manual";
    private final static String GEO_PROVIDER_GEOCODER = "geocoder";

    @Post("json")
    public Error request(final ResultUpdateRequest request) throws ControlServerException, SQLException {
        try (var conn = getConnection()) {
            final UUID clientUUID = UUID.fromString(request.getUuid());
            final UUID testUUID = UUID.fromString(request.getTestUuid());
            final int zipCode = request.getZipCode() == null ? 0 : request.getZipCode();
            final double geoLat = request.getGeoLat() == null ? Double.NaN : request.getGeoLat();
            final double geoLong = request.getGeoLong() == null ? Double.NaN : request.getGeoLong();
            final float geoAccuracy = (float) (request.getAccuracy() == null ? 0 : request.getAccuracy());
            final String provider = Strings.nullToEmpty(request.getProvider()).toLowerCase();


            final Client client = new Client(conn);
            final long clientId = client.getClientByUuid(clientUUID);
            if (clientId < 0)
                throw new IllegalArgumentException("error while loading client");

            final Test test = new Test(conn);
            if (test.getTestByUuid(testUUID) < 0)
                throw new IllegalArgumentException("error while loading test");

            if (test.getField("client_id").longValue() != clientId)
                throw new IllegalArgumentException("client UUID does not match test");

            if (zipCode > 0) {
                ((IntField) test.getField("zip_code")).setValue(zipCode);
            }
            if (!Double.isNaN(geoLat) && !Double.isNaN(geoLong) &&
                    (provider.equals(GEO_PROVIDER_GEOCODER) || provider.equals(GEO_PROVIDER_MANUAL))) {
                final GeoLocation geoloc = new GeoLocation(conn);
                geoloc.setTest_id(test.getUid());

                final Timestamp tstamp = java.sql.Timestamp.valueOf(new Timestamp(
                        (new Date().getTime())).toString());
                geoloc.setTime(tstamp, TimeZone.getDefault().toString());
                geoloc.setAccuracy(geoAccuracy);
                geoloc.setGeo_lat(geoLat);
                geoloc.setGeo_long(geoLong);
                geoloc.setProvider(provider);
                geoloc.storeLocation();

                ((DoubleField) test.getField("geo_lat")).setValue(geoLat);
                ((DoubleField) test.getField("geo_long")).setValue(geoLong);
                ((DoubleField) test.getField("geo_accuracy")).setValue(geoAccuracy);
                test.getField("geo_provider").setString(provider);
            }

            test.storeTestResults(true);

            if (test.hasError())
                throw ControlServerException.fromMessageCode(test.getError());
        } catch (final IllegalArgumentException e) {
            throw InvalidRequestException.fromMessageCode("ERROR_REQUEST_JSON");
        }

        return postRequestHook(request, new Error());
    }

    @Get("json")
    public Error retrieve(final ResultUpdateRequest entity) throws ControlServerException, SQLException {
        return request(entity);
    }

}
