package cz.nic.netmetr.server.control.resources.openapi;

import cz.nic.netmetr.shared.resources.StaticResourceHelper;
import org.restlet.data.MediaType;
import org.restlet.representation.ByteArrayRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class OpenAPISpecResource extends ServerResource {

    /**
     * Serves OpenAPI specification loaded from the specification file
     */
    @Get
    public ByteArrayRepresentation getApiSpec() {
        try {
            return StaticResourceHelper.loadResource(this, "RMBTControlServer_openapi.yaml", MediaType.APPLICATION_YAML);
        } catch (StaticResourceHelper.ResourceUnavailableException e) {
            return e.getSupplementaryResponse();
        }
    }
}
