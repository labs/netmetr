/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.google.common.base.Strings;
import cz.nic.netmetr.model.NetworkType;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.Classification;
import cz.nic.netmetr.shared.HelperFunctions;
import cz.nic.netmetr.shared.SignificantFormat;
import cz.nic.netmetr.shared.db.Client;
import cz.nic.netmetr.shared.db.model.Test;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import cz.nic.netmetr.shared.exceptions.InvalidRequestException;
import io.swagger.client.model.TestResultMeasurementItem;
import io.swagger.client.model.TestResultRequest;
import io.swagger.client.model.TestResultResponse;
import io.swagger.client.model.TestResultResponseItem;
import org.json.JSONObject;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

public class TestResultResource extends ServerResource {

    private void addMeasurementItem(TestResultResponseItem response, String title, String value, Integer classification) {
        TestResultMeasurementItem item = new TestResultMeasurementItem();
        item.setTitle(title);
        item.setValue(value);
        item.setClassification(classification);
        response.addMeasurementItem(item);
    }

    private void addNetItem(TestResultResponseItem response, String title, String value) {
        TestResultMeasurementItem item = new TestResultMeasurementItem();
        item.setTitle(title);
        item.setValue(value);
        response.addNetItem(item);
    }

    @Post("json")
    public TestResultResponse request(final TestResultRequest request) throws ControlServerException, SQLException {
        try (var conn = getDbConnectionFromApp().getConnection()) {
            final TestResultResponse answer = new TestResultResponse();

            // Load Language Files for Client
            configureLanguage(request.getLanguage());

            if (request.getTestUuid() == null)
                throw InvalidRequestException.fromMessageCode("ERROR_REQUEST_NO_UUID");


            final Client client = new Client(conn);
            final Test test = Test.getTestByUuid(conn, UUID.fromString(request.getTestUuid()));

            if (!client.getClientByUid(test.client_id) || !"FINISHED".equals(test.status)) {
                throw InvalidRequestException.fromMessageCode("ERROR_REQUEST_NO_UUID");
            }

            final Locale locale = new Locale(request.getLanguage());
            final Format format = new SignificantFormat(2, locale);

            TestResultResponseItem testResultItem = new TestResultResponseItem();

            // RMBTClient Info
            testResultItem.setOpenUuid(test.getFormattedOpenUUID()); //also send open-uuid (starts with 'P')
            testResultItem.setOpenTestUuid(test.getFormattedOpenTestUUID()); //and open test-uuid (starts with 'O')

            // datetime + timezone
            final long time = test.client_time.toInstant().toEpochMilli();
            final TimeZone tz = TimeZone.getTimeZone(test.timezone);
            testResultItem.setTime(time);
            testResultItem.setTimezone(test.timezone);
            final DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, locale);
            dateFormat.setTimeZone(tz);
            final String timeString = dateFormat.format(Date.from(test.client_time.toInstant()));
            testResultItem.setTimeString(timeString);



            String downloadString = "-";
            if (test.getSpeed_download() != null) {
                downloadString = String.format("%s %s",
                        format.format(test.getSpeed_download() / 1000d),
                        labels.getString("RESULT_DOWNLOAD_UNIT"));
                addMeasurementItem(
                        testResultItem,
                        labels.getString("RESULT_DOWNLOAD"),
                        downloadString,
                        Classification.classify(classification.THRESHOLD_DOWNLOAD, test.getSpeed_download())
                );
            }


            String uploadString = "-";
            if (test.getSpeed_upload() != null) {
                uploadString = String.format("%s %s",
                        format.format(test.getSpeed_upload() / 1000d), labels.getString("RESULT_UPLOAD_UNIT"));
                addMeasurementItem(
                        testResultItem,
                        labels.getString("RESULT_UPLOAD"),
                        uploadString,
                        Classification.classify(classification.THRESHOLD_UPLOAD, test.getSpeed_upload())
                );
            }

            String pingString = "-";
            if (test.ping_median != null) {
                final double pingValue = test.getPing_median() / 1000000d;
                pingString = String.format("%s %s", format.format(pingValue),
                        labels.getString("RESULT_PING_UNIT"));
                addMeasurementItem(
                        testResultItem,
                        labels.getString("RESULT_PING"),
                        pingString,
                        Classification.classify(classification.THRESHOLD_PING, test.getPing_median())
                );
            }

            classifyAndAddJitterAndPacketLoss(test, testResultItem);

            // test.signal_strength  --  signal strength as RSSI (GSM, UMTS, Wifi, sometimes LTE)
            // test.lte_rsrp   --  signal strength as RSRP, used in LTE
            String signalString = null;
            if (test.getNetwork_type() == NetworkType.CELLULAR_NO_SIGNAL.id) {
                addMeasurementItem(
                        testResultItem,
                        labels.getString("RESULT_SIGNAL"),
                        labels.getString("RESULT_SIGNAL_NONE"),
                        1
                );
            } else {
                if (test.signal_strength != null || test.lte_rsrp != null) {
                    if (test.lte_rsrp == null) {  // only RSSI value, output RSSI to JSON
                        final int[] threshold = test.getNetwork_type() == NetworkType.WLAN.id || test.getNetwork_type() == 0 ? classification.THRESHOLD_SIGNAL_WIFI
                                : classification.THRESHOLD_SIGNAL_MOBILE;
                        signalString = test.signal_strength + " " + labels.getString("RESULT_SIGNAL_UNIT");
                        addMeasurementItem(
                                testResultItem,
                                labels.getString("RESULT_SIGNAL"),
                                signalString,
                                Classification.classify(threshold, test.signal_strength)
                        );
                    } else { // use RSRP value else (RSRP value has priority if both are available (e.g. 3G/4G-test))
                        signalString = test.lte_rsrp + " " + labels.getString("RESULT_SIGNAL_UNIT");
                        addMeasurementItem(
                                testResultItem,
                                labels.getString("RESULT_SIGNAL_RSRP"),
                                signalString,
                                Classification.classify(classification.THRESHOLD_SIGNAL_RSRP, test.lte_rsrp)
                        );
                    }
                }
            }

            final String networkTypeString = NetworkType.fromId(test.getNetwork_type()).printableName();
            addNetItem(testResultItem, labels.getString("RESULT_NETWORK_TYPE"), networkTypeString);

            if (test.getNetwork_type() == NetworkType.LAN.id || test.getNetwork_type() == NetworkType.WLAN.id) { // mobile wifi or browser
                if (test.provider_id_name != null) {
                    addNetItem(testResultItem,
                            labels.getString("RESULT_OPERATOR_NAME"),
                            test.provider_id_name
                    );
                }
                if (test.getNetwork_type() == NetworkType.WLAN.id) {// mobile wifi
                    if (test.wifi_ssid != null) {
                        addNetItem(
                                testResultItem,
                                labels.getString("RESULT_WIFI_SSID"),
                                test.wifi_ssid
                        );
                    }
                }
            } else { // mobile
                String operatorName = test.provider_id_name;
                if (operatorName != null) operatorName = operatorName.strip();
                if (Strings.isNullOrEmpty(operatorName) && test.getNetwork_type() != null && test.getNetwork_type() == NetworkType.CELLULAR_NO_SIGNAL.id) {
                    operatorName = test.network_sim_operator_name;
                }

                if (operatorName != null) {
                    addNetItem(
                            testResultItem,
                            labels.getString("RESULT_OPERATOR_NAME"),
                            operatorName
                    );
                }

                if (test.roaming_type != null && test.roaming_type > 0) {
                    addNetItem(
                            testResultItem,
                            labels.getString("RESULT_ROAMING"),
                            HelperFunctions.getRoamingType(labels, test.roaming_type)
                    );
                }
            }

            boolean includeLocation = false;
            if (!(test.geo_lat == null || test.geo_long == null || test.geo_accuracy == null)) {
                final double accuracy = test.geo_accuracy;
                if (accuracy < Double.parseDouble(getSetting("rmbt_geo_accuracy_button_limit", request.getLanguage()))) {
                    includeLocation = true;
                    testResultItem.setGeoLat(test.geo_lat);
                    testResultItem.setGeoLong(test.geo_long);
                }
            }

            //geo location
            JSONObject locationJson = TestResultDetailResource.getGeoLocation(this, test, conn, labels);
            if (locationJson != null) {
                if (locationJson.has("location")) {
                    testResultItem.setLocation(locationJson.getString("location"));
                }
                if (locationJson.has("motion")) {
                    testResultItem.setMotion(locationJson.getString("motion"));
                }
            }

            // both fields could be null, but we dont have to check
            testResultItem.setZipCode(test.zip_code);
            testResultItem.setZipCodeGeo(test.zip_code_geo);

            String providerString = Strings.nullToEmpty(test.provider_id_name);
            String platformString = Strings.nullToEmpty(test.plattform);
            String modelString = Strings.nullToEmpty(test.model_fullname);

            String mobileNetworkString = null;
            if (test.network_operator != null) {
                if (test.mobile_provider_name == null)
                    mobileNetworkString = test.network_operator;
                else
                    mobileNetworkString = String.format("%s (%s)", test.mobile_provider_name, test.network_operator);
            }

/*
RESULT_SHARE_TEXT = My Result:\nDate/time: {0}\nDownload: {1}\nUpload: {2}\nPing: {3}\n{4}Network type: {5}\n{6}{7}Platform: {8}\nModel: {9}\n{10}\n\n
RESULT_SHARE_TEXT_SIGNAL_ADD = Signal strength: {0}\n
RESULT_SHARE_TEXT_RSRP_ADD = Signal strength (RSRP): {0}\n
RESULT_SHARE_TEXT_MOBILE_ADD = Mobile network: {0}\n
RESULT_SHARE_TEXT_PROVIDER_ADD = Operator: {0}\n

*/
            String shareTextField4 = "";
            if (signalString != null)             //info on signal strength, field 4
                if (test.lte_rsrp == null) {      //add RSSI if RSRP is not available
                    shareTextField4 = MessageFormat.format(labels.getString("RESULT_SHARE_TEXT_SIGNAL_ADD"), signalString);
                } else {                            //add RSRP
                    shareTextField4 = MessageFormat.format(labels.getString("RESULT_SHARE_TEXT_RSRP_ADD"), signalString);
                }
            String shareLocation = "";
            if (includeLocation && locationJson != null) {

                shareLocation = MessageFormat.format(labels.getString("RESULT_SHARE_TEXT_LOCATION_ADD"),
                        locationJson.getString("location"));
            }

            final String shareText = MessageFormat.format(labels.getString("RESULT_SHARE_TEXT"),
                    timeString,                   //field 0
                    downloadString,               //field 1
                    uploadString,                 //field 2
                    pingString,                   //field 3
                    shareTextField4,              //contains field 4
                    networkTypeString,            //field 5
                    providerString.isEmpty() ? "" : MessageFormat.format(labels.getString("RESULT_SHARE_TEXT_PROVIDER_ADD"),
                            providerString),      //field 6
                    mobileNetworkString == null ? "" : MessageFormat.format(labels.getString("RESULT_SHARE_TEXT_MOBILE_ADD"),
                            mobileNetworkString), //field 7
                    platformString,               //field 8
                    modelString,                  //field 9
                    //dz add location
                    shareLocation,                  //field 10
                    getSetting("url_open_data_prefix", request.getLanguage()) + test.getFormattedOpenTestUUID());   //field 11
            testResultItem.setShareText(shareText);

            final String shareSubject = MessageFormat.format(labels.getString("RESULT_SHARE_SUBJECT"),
                    timeString                    //field 0
            );
            testResultItem.setShareSubject(shareSubject);
            testResultItem.setNetworkType(test.getNetwork_type());

            testResultItem.setTestTypeId(test.test_type_id);

            answer.addTestresultItem(testResultItem);

            return postRequestHook(request, answer);
        }
    }

    private void classifyAndAddJitterAndPacketLoss(Test test, TestResultResponseItem testResultItem) {
        switch (test.getJitter_packet_loss_status()) {
            case OK:
                if (test.isJitterResultPresentable()) {
                    addMeasurementItem(
                            testResultItem,
                            labels.getString("RESULT_JITTER"),
                            test.getAggregateMeanJitterMs() + " ms",
                            Classification.classify(Classification.THRESHOLD_JITTER, (long) test.getAggregateMeanJitterMs() * 1_000_000L)
                    );
                }

                // packet loss
                addMeasurementItem(
                        testResultItem,
                        labels.getString("RESULT_PACKET_LOSS"),
                        test.getAggregatePacketLossPercent() + "%",
                        Classification.classify(Classification.THRESHOLD_PACKET_LOSS, (long)test.getAggregatePacketLossPercent()*1000L)
                );
                break;
            case ERROR:
                // report error
                addMeasurementItem(
                        testResultItem,
                        labels.getString("RESULT_PACKET_LOSS"),
                        "error",
                        Classification.RED
                );
                addMeasurementItem(
                        testResultItem,
                        labels.getString("RESULT_JITTER"),
                        "error",
                        Classification.RED
                );
                break;
            case NOT_MEASURED:
                // don't do anything for backwards compatibility. Nothing sent -> nothing reported
                break;
        }
    }

    @Get("json")
    public TestResultResponse retrieve(final TestResultRequest entity) throws ControlServerException, SQLException {
        return request(entity);
    }

}
