/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.RevisionHelper;
import io.swagger.client.model.VersionResponse;
import org.restlet.resource.Get;

public class VersionResource extends ServerResource {
    @Get("json")
    public VersionResponse request(final String entity) {
        VersionResponse response = new VersionResponse();
        response.setVersion(RevisionHelper.getVerboseRevision());

        return postRequestHook(entity, response);
    }
}
