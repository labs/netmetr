/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.google.common.net.InetAddresses;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.db.QoSTestObjective;
import cz.nic.netmetr.shared.db.dao.QoSTestObjectiveDao;
import cz.nic.netmetr.shared.qos.testscript.TestScriptInterpreter;
import io.swagger.client.model.*;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.lang.reflect.Type;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lb
 */
public class QoSTestRequestResource extends ServerResource {

    @Post("json")
    public QoSTestRequestResponse request(final Empty entity) throws SQLException {
        try (Connection conn = getConnection()) {
            final QoSTestRequestResponse answer = new QoSTestRequestResponse();

            // set basic properties of the response
            final String clientIpRaw = getClientIP();
            final InetAddress clientAddress = InetAddresses.forString(clientIpRaw);
            final String clientIpString = InetAddresses.toAddrString(clientAddress);

            answer.setTestDuration(getSetting("rmbt_duration"));
            answer.setTestNumthreads(getSetting("rmbt_num_threads"));
            answer.setTestNumpings(getSetting("rmbt_num_pings"));
            answer.setClientRemoteIp(clientIpString);

            // prepare JSON parser for future use
            Gson gson = new Gson();
            final Type STRING_MAP_OBJECT = new TypeToken<HashMap<String, String>>() {
            }.getType();

            // in the database, there is a table called qos_test_objective. Every row of that table specifies one specific
            // QoS test. What we do here is, we read the whole table and group the results by test type.

            // database connection
            QoSTestObjectiveDao testObjectiveDao = new QoSTestObjectiveDao(conn);

            // result object (will be converted to JSON)
            QoSTestObjectives testObjectives = new QoSTestObjectives();

            // here we iterate fetch all row of the database with test class 1
            // (As of writing, there was nothing else then class 1. Not sure, what's its purpose)
            for (QoSTestObjective o : testObjectiveDao.getByTestClass(1)) {

                // for the row, we need to find appropriate group to which it belongs
                QoSTestObjectiveGroup testGroup;
                if (testObjectives.containsKey(o.getTestType())) {
                    testGroup = testObjectives.get(o.getTestType());
                } else {
                    testGroup = new QoSTestObjectiveGroup();
                    testObjectives.put(o.getTestType(), testGroup);
                }

                // we prepare the test resulting test detail object and parse the detains from DB (it's in JSON)
                QoSTestObjectiveItem testDetails = new QoSTestObjectiveItem();
                HashMap<String, String> testDefinition = gson.fromJson(o.getObjective(), STRING_MAP_OBJECT);

                //iterate through all properties and interpret their values if necessary
                boolean testInvalid = false;
                for (Map.Entry<String, String> testProperty : testDefinition.entrySet()) {
                    Object scriptResult = TestScriptInterpreter.interprete(testProperty.getValue(), null);
                    if (scriptResult != null) {
                        testDetails.put(testProperty.getKey(), String.valueOf(scriptResult));
                    } else {
                        testInvalid = true;
                        break;
                    }
                }

                //add always present properties to the testDetails object
                testDetails.put("qos_test_uid", String.valueOf(o.getUid()));
                testDetails.put("concurrency_group", String.valueOf(o.getConcurrencyGroup()));
                if (clientAddress instanceof Inet6Address) {
                    testDetails.put("server_addr", String.valueOf(o.getTestServerIpv6()));
                } else {
                    testDetails.put("server_addr", String.valueOf(o.getTestServerIpv4()));
                }
                testDetails.put("server_port", String.valueOf(o.getPort()));


                if (!testInvalid) {
                    testGroup.add(testDetails);
                }
            }

            answer.setObjectives(testObjectives);
            return postRequestHook(entity, answer);
        }
    }

    @Get("json")
    public QoSTestRequestResponse retrieve(final Empty entity) throws SQLException {
        return request(entity);
    }
}
