/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.google.common.base.Strings;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.Classification;
import cz.nic.netmetr.shared.LanguageManager;
import cz.nic.netmetr.shared.SignificantFormat;
import cz.nic.netmetr.shared.db.Client;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import cz.nic.netmetr.shared.exceptions.GeneralDatabaseException;
import cz.nic.netmetr.shared.exceptions.InvalidRequestException;
import io.swagger.client.model.HistoryItem;
import io.swagger.client.model.HistoryRequest;
import io.swagger.client.model.HistoryResponse;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.Format;
import java.util.*;
import java.util.stream.Collectors;

public class HistoryResource extends ServerResource {

    @Post("json")
    public HistoryResponse request(final HistoryRequest request) throws ControlServerException, SQLException {
        try (Connection conn = getConnection()) {

            // TODO quick fix for null pointer, more systematic solution would be definitely better
            if (request.getLanguage() == null) request.setLanguage("en_US");

            HistoryResponse answer = new HistoryResponse();

            // Load Language Files for Client
            configureLanguage(request.getLanguage());


            final Client client = new Client(conn);
            if (Strings.isNullOrEmpty(request.getUuid()) || client.getClientByUuid(UUID.fromString(request.getUuid())) <= 0) {
                throw InvalidRequestException.fromMessageCode("ERROR_REQUEST_NO_UUID");
            }

            final Locale locale = new Locale(request.getLanguage());
            final Format format = new SignificantFormat(2, locale);
            
            String limitRequest = "";
            if (request.getResultLimit() != null) {
                int limit = request.getResultLimit(); // make sure it's always int to avoid SQL injection
                limitRequest = " LIMIT " + limit;

                //get offset string if there is one
                if ((request.getResultOffset() != null) && (request.getResultOffset() >= 0)) {
                    int offset = request.getResultOffset();  // make sure it's always int to avoid SQL injection
                    limitRequest += " OFFSET " + offset;
                }
            }

            final ArrayList<String> deviceValues = new ArrayList<>();
            String deviceRequest = "";
            if (request.getDevices() != null) {
                boolean checkUnknown = request.getDevices().stream().anyMatch(s -> s.equals("Unknown Device"));
                request.getDevices().stream().filter(s -> !s.equals("Unknown Device")).forEach(deviceValues::add);
                String sb = request.getDevices().stream().map(s -> {
                    if (s.equals("Unknown Device")) {
                        return null;
                    } else {
                        return "?";
                    }
                }).filter(Objects::nonNull).collect(Collectors.joining(","));

                if (sb.length() > 0)
                    deviceRequest = " AND (COALESCE(adm.fullname, t.model) IN (" + sb + ")" + (checkUnknown ? " OR model IS NULL OR model = ''" : "") + ")";
            }

            final ArrayList<String> filterValues = new ArrayList<>();
            String networksRequest = "";
            if (request.getNetworks() != null && !request.getNetworks().isEmpty()) {
                networksRequest = "AND nt.group_name IN (" +
                        request.getNetworks().stream().map(s -> "?").collect(Collectors.joining(",")) +
                        ")";
                filterValues.addAll(request.getNetworks());
            }


            final String sql = client.getSync_group_id() == 0 ?
                    //use faster request ignoring sync-group as user is not synced (id=0)
                    String.format(
                            "SELECT DISTINCT"
                                    + " t.uuid, client_time, timezone, speed_upload, speed_download, ping_median, network_type, nt.group_name network_type_group_name,"
                                    + " COALESCE(adm.fullname, t.model) model, test_type_id"
                                    + " FROM test t"
                                    + " LEFT JOIN device_map adm ON adm.codename=t.model"
                                    + " LEFT JOIN network_type nt ON t.network_type=nt.uid"
                                    + " WHERE t.deleted = false AND t.implausible = false AND t.status = 'FINISHED'"
                                    + " AND client_id = ?"
                                    + " %s %s" + " ORDER BY client_time DESC" + " %s", deviceRequest,
                            networksRequest, limitRequest) :
                    //use slower request including sync-group if client is synced
                    String.format(
                            "SELECT DISTINCT"
                                    + " t.uuid, client_time, timezone, speed_upload, speed_download, ping_median, network_type, nt.group_name network_type_group_name,"
                                    + " COALESCE(adm.fullname, t.model) model, test_type_id"
                                    + " FROM (select * from test where client_id IN (SELECT ? UNION SELECT uid FROM client WHERE sync_group_id = ? )) t"
                                    + " LEFT JOIN device_map adm ON adm.codename=t.model"
                                    + " LEFT JOIN network_type nt ON t.network_type=nt.uid"
                                    + " WHERE t.deleted = false AND t.implausible = false AND t.status = 'FINISHED'"
                                    + " %s %s" + " ORDER BY client_time DESC" + " %s", deviceRequest,
                            networksRequest, limitRequest);

            try (PreparedStatement st = conn.prepareStatement(sql)) {

                int i = 1;
                st.setLong(i++, client.getUid());
                if (client.getSync_group_id() != 0)
                    st.setInt(i++, client.getSync_group_id());

                for (final String value : deviceValues)
                    st.setString(i++, value);

                for (final String filterValue : filterValues)
                    st.setString(i++, filterValue);


                answer.setHistory(new ArrayList<>()); //make sure it's not null for final emptiness check

                try (ResultSet rs = st.executeQuery()) {

                    while (rs.next()) {
                        HistoryItem historyItem = new HistoryItem();

                        historyItem.setTestUuid(rs.getString("uuid"));

                        final Date date = rs.getTimestamp("client_time");
                        final long time = date.getTime();
                        final String tzString = rs.getString("timezone");
                        final TimeZone tz = TimeZone.getTimeZone(tzString);
                        historyItem.setTime(time);
                        historyItem.setTimezone(tzString);
                        final DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
                                DateFormat.MEDIUM, locale);
                        dateFormat.setTimeZone(tz);
                        historyItem.setTimeString(dateFormat.format(date));

                        historyItem.setSpeedUpload(format.format(rs.getInt("speed_upload") / 1000d));
                        historyItem.setSpeedDownload(format.format(rs.getInt("speed_download") / 1000d));

                        if (rs.getString("ping_median") != null) {
                            final long ping = rs.getLong("ping_median");
                            historyItem.setPing(format.format(ping / 1000000d));
                            historyItem.setPingClassification(Classification.classify(classification.THRESHOLD_PING, rs.getLong("ping_median")));
                            // backwards compatibility for old clients
                            historyItem.setPingShortest(format.format(ping / 1000000d));
                            historyItem.setPingShortestClassification(Classification.classify(classification.THRESHOLD_PING, rs.getLong("ping_median")));
                        } else {
                            historyItem.setPingClassification(1); //RED
                            historyItem.setPingShortestClassification(1); //RED

                        }
                        historyItem.setModel(rs.getString("model"));
                        historyItem.setNetworkType(rs.getString("network_type_group_name"));

                        //for appscape-iPhone-Version: also add classification to the response
                        historyItem.setSpeedUploadClassification(Classification.classify(classification.THRESHOLD_UPLOAD, rs.getInt("speed_upload")));
                        historyItem.setSpeedDownloadClassification(Classification.classify(classification.THRESHOLD_DOWNLOAD, rs.getInt("speed_download")));

                        historyItem.setTestTypeId(rs.getInt("test_type_id"));

                        answer.addHistoryItem(historyItem);
                    }

                    if (answer.getHistory().isEmpty())
                        answer.addErrorItem(LanguageManager.resolve(locale, "ERROR_DB_GET_HISTORY"));
                }
            } catch (final SQLException e) {
                throw new GeneralDatabaseException(LanguageManager.resolve(locale, "ERROR_DB_GET_HISTORY_SQL"), e);
            }

            return postRequestHook(request, answer);
        }
    }

    @Get("json")
    public HistoryResponse retrieve(final HistoryRequest entity) throws ControlServerException, SQLException {
        return request(entity);
    }

}
