/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.google.common.base.Strings;
import com.google.common.net.InetAddresses;
import com.google.gson.Gson;
import cz.nic.netmetr.model.NetworkType;
import cz.nic.netmetr.server.control.ControlServer;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.HelperFunctions;
import cz.nic.netmetr.shared.config.GlobalConfig;
import cz.nic.netmetr.shared.db.Cell_location;
import cz.nic.netmetr.shared.db.GeoLocation;
import cz.nic.netmetr.shared.db.Signal;
import cz.nic.netmetr.shared.db.TestStat;
import cz.nic.netmetr.shared.db.dao.TestStatDao;
import cz.nic.netmetr.shared.db.model.AdvertisedSpeedOption;
import cz.nic.netmetr.shared.db.model.Test;
import cz.nic.netmetr.shared.db.repository.AdvertisedSpeedOptionRepository;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import cz.nic.netmetr.shared.exceptions.GeneralDatabaseException;
import cz.nic.netmetr.shared.exceptions.InvalidRequestException;
import cz.nic.netmetr.shared.reporting.AdvancedReporting;
import io.swagger.client.model.Error;
import io.swagger.client.model.*;
import org.json.JSONObject;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;

import static org.json.XMLTokener.entity;

public class SubmitResultsResource extends ServerResource {
    final static int UNKNOWN = Integer.MIN_VALUE;
    final static Pattern MCC_MNC_PATTERN = Pattern.compile("\\d{3}-\\d+");
    private Logger logger = LoggerFactory.getLogger(SubmitResultsResource.class);

    /**
     * Copy fields from request to test objects.
     * <p>
     * Implementation note:
     * Previously, it was implemented automagically and the assignment was kind of hidden in the implementation of
     * Test class. But that was rewritten so that it has single responsibility - mapping data to/from database. So now
     * it has to be done in other way. I decided, that this explicit copy operation would be the best, because it's not
     * hidden anywhere.
     *
     * @param request request data to copy from
     * @param test    test object to copy into
     */
    private static void fillTestFields(ResultRequest request, Test test) {
        /* expect, that any field can be null */

        test.client_version = request.getClientVersion().getValue();
        test.client_name = request.getClientName().getValue();
        test.client_language = request.getClientLanguage();
        test.client_ip_local_type = request.getClientLocalIp();
        test.speed_upload = request.getTestSpeedUpload() == null ? null : request.getTestSpeedUpload().intValue();
        test.speed_download = request.getTestSpeedDownload() == null ? null : request.getTestSpeedDownload().intValue();
        test.ping_shortest = request.getTestPingShortest();
        test.encryption = request.getTestEncryption();
        test.plattform = request.getPlattform();
        test.os_version = request.getOsVersion();
        test.api_level = request.getApiLevel();
        test.device = request.getDevice();
        test.model = request.getModel();
        test.product = request.getProduct();
        test.phone_type = request.getTelephonyPhoneType();
        test.data_state = request.getTelephonyDataState();
        test.network_country = request.getTelephonyNetworkCountry();
        test.network_operator_name = request.getTelephonyNetworkOperatorName();
        test.network_sim_country = request.getTelephonyNetworkSimCountry();
        test.network_sim_operator_name = request.getTelephonyNetworkSimOperatorName();
        test.wifi_ssid = request.getWifiSsid();
        test.wifi_bssid = request.getWifiBssid();
        test.wifi_network_id = request.getWifiNetworkId();
        test.num_threads = request.getTestNumThreads();
        test.bytes_download = request.getTestBytesDownload();
        test.bytes_upload = request.getTestBytesUpload();
        test.nsec_download = request.getTestNsecDownload();
        test.nsec_upload = request.getTestNsecUpload();
        test.client_software_version = request.getClientSoftwareVersion();
        test.geo_lat = request.getGeoLat();
        test.geo_long = request.getGeoLong();
        test.network_type = request.getNetworkType();
        test.total_bytes_download = request.getTestTotalBytesDownload();
        test.total_bytes_upload = request.getTestTotalBytesUpload();
        test.network_is_roaming = request.isTelephonyNetworkIsRoaming();
        test.zip_code = request.getZipCode();
        test.geo_provider = request.getProvider();
        test.geo_accuracy = request.getAccuracy();
        test.test_if_bytes_download = request.getTestIfBytesDownload();
        test.test_if_bytes_upload = request.getTestIfBytesUpload();
        test.testdl_if_bytes_download = request.getTestdlIfBytesDownload();
        test.testdl_if_bytes_upload = request.getTestdlIfBytesUpload();
        test.testul_if_bytes_download = request.getTestulIfBytesDownload();
        test.testul_if_bytes_upload = request.getTestulIfBytesUpload();
        test.time_dl_ns = request.getTimeDlNs();
        test.time_ul_ns = request.getTimeUlNs();
        test.num_threads_ul = request.getNumThreadsUl();
        test.publish_public_data = Objects.requireNonNullElse(request.isPublishPublicData(), Boolean.TRUE); // default value copied from dbinit
        test.tag = request.getTag();
        test.adv_spd_option_name = request.getAdvSpdOptionName();
        test.adv_spd_up_kbit = request.getAdvSpdUpKbit();
        test.adv_spd_down_kbit = request.getAdvSpdDownKbit();
        test.test_type_id = Objects.requireNonNullElse(request.getTestTypeId(), 1); // default value copied from dbinit

        final String networkOperator = Strings.nullToEmpty(request.getTelephonyNetworkOperator());
        test.network_operator = MCC_MNC_PATTERN.matcher(networkOperator).matches() ? networkOperator : null;

        final String networkSimOperator = Strings.nullToEmpty(request.getTelephonyNetworkSimOperator());
        test.network_sim_operator = MCC_MNC_PATTERN.matcher(networkSimOperator).matches() ? networkSimOperator : null;
    }

    /**
     * checks for submitted extended test stats
     *
     * @param stats
     * @param testUid
     * @return
     */
    public static TestStat checkForSubmittedTestStats(ExtendedTestStat stats, final long testUid) {
        if (stats != null && (stats.getCpuUsage() != null || stats.getMemUsage() != null)) {

            // backwards compatibility hack translating from one type of json object into another
            Gson gson = new Gson();
            final JSONObject cpuUsage = new JSONObject(gson.toJson(stats.getCpuUsage()));
            final JSONObject memUsage = new JSONObject(gson.toJson(stats.getMemUsage()));

            final TestStat ts = new TestStat();

            ts.setTestUid(testUid);
            ts.setCpuUsage(cpuUsage);
            ts.setMemUsage(memUsage);

            return ts;
        }

        return null;
    }

    @Post("json")
    public Error request(final ResultRequest request) throws ControlServerException {
        if (request == null)
            throw new InvalidRequestException("Missing request data");

        Test test = null;

        try (Connection conn = getConnection()) {
            try {
                conn.setAutoCommit(false);

                // Load Language Files for Client
                configureLanguage(request.getClientLanguage());

                final UUID testUuid = validateToken(conn, request);  // exception if invalid token
                validateClientName(request);  // exception if unknown name

                test = Test.getTestByUuid(conn, testUuid);
                fillTestFields(request, test);

                processClientIP(request, test); // anonymize, save and log IP address of the client

                saveExtendedTestStats(request, test, conn);
                saveAdvertisedSpeedOptions(request, test, conn);

                saveSpeedGraphData(request.getSpeedDetail(), test, conn);
                savePingData(request.getPings(), test, conn);
                saveGeoLocationData(request.getGeoLocations(), test, conn);
                saveCellLocations(request.getCellLocations(), test, conn);
                saveSignalData(request.getSignals(), test, conn);
                saveJitterPacketLoss(request.getJitterPacketLoss(), test);

                saveAdditionalReportFields(request, test, conn);

                performSanityChecks(test); // throws exception if something weird appears in the data

                test.status = "FINISHED"; // if we got here, everything went fine

                test.storeTestResults(conn, false);
                conn.commit();

            } catch (ControlServerException e) {
                // catches all exceptions, marks the data as erroneous and throws the exception again

                if (test == null) {
                    // the error was caused even before loading the test, so we just pass it further
                    throw e;
                }

                try {
                    test.status = "ERROR";
                    test.storeTestResults(conn, false);
                    conn.commit();
                } catch (Exception newException) {
                    // if anything fails at this point, we don't want to suppress the previous error
                    logger.error("Failed to mark test as failed in the database. Suppressing this error in order to allow handling of the original error.", newException);
                }

                throw e;

            }
        } catch (SQLException e) {
            throw new GeneralDatabaseException("Database error", e);
        }

        return postRequestHook(entity, new Error());
    }

    private void saveAdditionalReportFields(ResultRequest request, Test test, Connection conn) {
        try {
            final AdvancedReporting advancedReporting = getAdvancedReporting();
            final JSONObject reportField = advancedReporting.generateSpeedtestAdvancedReport(test, request, conn);
            if (reportField != null && reportField.length() > 0) {
                if (test.additional_report_fields != null) {
                    test.additional_report_fields = reportField.toString();
                }
            }
        } catch (InstantiationException | IllegalAccessException e) {
            logger.error("Unexpected exception caught..", e);
        }
    }

    private void saveExtendedTestStats(ResultRequest request, Test test, Connection conn) throws SQLException {
        //////////////////////////////////////////////////
        // extended test stats:
        //////////////////////////////////////////////////
        final TestStat extendedTestStat = checkForSubmittedTestStats(request.getExtendedTestStats(), test.uid);
        if (extendedTestStat != null) {
            final TestStatDao testStatDao = new TestStatDao(conn);
            testStatDao.save(extendedTestStat);
        }
    }

    private void saveAdvertisedSpeedOptions(ResultRequest request, Test test, Connection conn) throws SQLException {
        //////////////////////////////////////////////////
        // advertised speed option:
        //////////////////////////////////////////////////
        final Long advSpdOptionId = request.getAdvSpdOptionId();
        if (advSpdOptionId != null) {
            final AdvertisedSpeedOptionRepository repo = new AdvertisedSpeedOptionRepository(conn);
            final AdvertisedSpeedOption advSpd = repo.getByUid(advSpdOptionId);
            if (advSpd != null) {
                test.adv_spd_option_id = advSpdOptionId;
                test.adv_spd_option_name = advSpd.getName();
            }

            if (request.getAdvSpdUpKbit() != null) {
                test.adv_spd_up_kbit = request.getAdvSpdUpKbit();
            }
            if (request.getAdvSpdDownKbit() != null) {
                test.adv_spd_down_kbit = request.getAdvSpdDownKbit();
            }
        }
    }

    private void performSanityChecks(Test test) throws ControlServerException {
        try {
            var _unused = NetworkType.fromId(test.network_type);
        } catch (IllegalArgumentException e) {
            throw ControlServerException.fromMessageCode("ERROR_NETWORK_TYPE", e);
        }

        if (test.speed_upload != null && (test.speed_download < 0 || test.speed_download > 10000000)) // 10 gbit/s limit
            throw ControlServerException.fromMessageCode("ERROR_DOWNLOAD_INSANE");

        if (test.speed_upload != null && (test.speed_upload < 0 || test.speed_upload > 10000000)) // 10 gbit/s limit
            throw ControlServerException.fromMessageCode("ERROR_UPLOAD_INSANE");

        //clients still report eg: "test_ping_shortest":9195040 (note the 'test_' prefix there!)
        if (test.ping_shortest != null && (test.ping_shortest < 0 || test.ping_shortest > 60000000000L)) // 1 min limit
            throw ControlServerException.fromMessageCode("ERROR_PING_INSANE");
    }

    private void processClientIP(ResultRequest request, Test test) {
        // RMBTClient Info

        final String ipLocalRaw = request.getTestIpLocal();
        if (ipLocalRaw != null) {
            final InetAddress ipLocalAddress = InetAddresses.forString(ipLocalRaw);
            // original address (not filtered)
            test.client_ip_local = InetAddresses.toAddrString(ipLocalAddress);
            // anonymized local address
            final String ipLocalAnonymized = HelperFunctions.anonymizeIp(ipLocalAddress);
            test.client_ip_local_anonymized = ipLocalAnonymized;
            // type of local ip
            test.client_ip_local_type = HelperFunctions.IpType(ipLocalAddress);
            // public ip
            final InetAddress ipPublicAddress = InetAddresses.forString(test.client_public_ip);
            test.nat_type = HelperFunctions.getNatType(ipLocalAddress, ipPublicAddress);
        }

        final String ipServer = request.getTestIpServer();
        if (ipServer != null) {
            final InetAddress testServerInetAddress = InetAddresses.forString(ipServer);
            test.server_ip = InetAddresses.toAddrString(testServerInetAddress);
        }

        //log IP address
        final String ipSource = getClientIP();
        test.source_ip = ipSource;

        //log anonymized address
        try {
            final InetAddress ipSourceIP = InetAddress.getByName(ipSource);
            test.source_ip_anonymized = HelperFunctions.anonymizeIp(ipSourceIP);
        } catch (UnknownHostException e) {
            logger.error("Exception thrown while anonymizing IP:", e);
        }
    }

    private void validateClientName(ResultRequest request) throws ControlServerException {
        final List<String> clientNames = Arrays.asList(settings.getString("RMBT_CLIENT_NAME")
                .split(",\\s*"));
        final List<String> clientVersions = Arrays.asList(settings.getString(
                "RMBT_VERSION_NUMBER").split(",\\s*"));


        if (!clientNames.contains(request.getClientName().getValue())
                || !clientVersions.contains(request.getClientVersion().getValue())) {
            throw ControlServerException.fromMessageCode("ERROR_CLIENT_VERSION");
        }
    }

    private UUID validateToken(Connection conn, ResultRequest request) throws ControlServerException {
        if (request.getTestToken().length() <= 0) {
            throw ControlServerException.fromMessageCode("ERROR_TEST_TOKEN_MISSING");
        }

        final String fullToken = request.getTestToken();
        final String[] token = fullToken.split("_");

        final UUID testUuid;

        // Check if UUID is valid UUID
        try {
            testUuid = UUID.fromString(token[0]);
        } catch (IllegalArgumentException e) {
            throw ControlServerException.fromMessageCode("ERROR_TEST_TOKEN", e);
        }

        final String data = token[0] + "_" + token[1];

        String secret = selectSecretKeyByServerToken(conn, fullToken);
        if (secret == null || secret.isEmpty()) {
            secret = GlobalConfig.getInstance().getRmbtSecretKey();
        }

        final String hmac = HelperFunctions.calculateHMAC(secret, data);

        if (hmac.length() == 0)
            throw ControlServerException.fromMessageCode("ERROR_TEST_TOKEN");

        if (token[2].length() <= 0 || !hmac.equals(token[2])) {
            throw ControlServerException.fromMessageCode("ERROR_TEST_TOKEN_MALFORMED");
        }
        return testUuid;
    }

    private void saveJitterPacketLoss(JitterPacketLoss jpl, Test test) {
        if (jpl == null) {
            // Old client or a client, that does not measure this. Nothing to do.
            test.jitter_packet_loss_status = Test.JitterPacketLossStatus.NOT_MEASURED; // be carefull, this is part of an enum which is not checked
            // here. It's checked only during user input parsing
            return;
        }

        // check status
        test.jitter_packet_loss_status = Test.JitterPacketLossStatus.valueOf(jpl.getStatus().getValue());
        if (!test.isJitterPacketLossTestOK()) {
            // there is nothing to do, nothing was measured
            return;
        }

        // jitter down
        test.jitter_down_max_ns = jpl.getDown().getMaxJitter(); // can be null
        test.jitter_down_mean_ns = jpl.getDown().getMeanJitter(); // can be null

        // jitter up
        test.jitter_up_max_ns = jpl.getUp().getMaxJitter(); // can be null
        test.jitter_up_mean_ns = jpl.getUp().getMeanJitter(); // can be null

        // packet loss down
        test.packet_loss_down_packets_received = jpl.getDown().getNumPackets();
        test.packet_loss_down_packets_total = jpl.getDown().getNumPacketsTotal();

        // packet loss up
        test.packet_loss_up_packets_received = jpl.getUp().getNumPackets();
        test.packet_loss_up_packets_total = jpl.getUp().getNumPacketsTotal();
    }

    private void saveSignalData(final List<SignalsItem> signalData, Test test, Connection conn) throws ControlServerException, SQLException {
        int signalStrength = Integer.MAX_VALUE; //measured as RSSI (GSM,UMTS,Wifi)
        int lteRsrp = Integer.MAX_VALUE; // signal strength measured as RSRP
        int lteRsrq = Integer.MAX_VALUE; // signal quality of LTE measured as RSRQ
        int linkSpeed = UNKNOWN;
        final int networkType = test.network_type;

        if (signalData != null) {

            for (var signalDataItem : signalData) {

                final Signal signal = new Signal(conn);

                signal.setTest_id(test.uid);

                final Timestamp tstamp = Timestamp.valueOf(new Timestamp(
                        signalDataItem.getTime()).toString());

                signal.setTime(tstamp, test.timezone);

                final int thisNetworkType = Objects.requireNonNullElse(signalDataItem.getNetworkTypeId(), 0);
                signal.setNetwork_type_id(thisNetworkType);

                final int thisSignalStrength = Objects.requireNonNullElse(signalDataItem.getSignalStrength(), UNKNOWN);
                if (thisSignalStrength != UNKNOWN)
                    signal.setSignal_strength(thisSignalStrength);
                signal.setGsm_bit_error_rate(Objects.requireNonNullElse(signalDataItem.getGsmBitErrorRate(), 0));
                final int thisLinkSpeed = Objects.requireNonNullElse(signalDataItem.getWifiLinkSpeed(), 0);
                signal.setWifi_link_speed(thisLinkSpeed);
                final int rssi = Objects.requireNonNullElse(signalDataItem.getWifiRssi(), UNKNOWN);
                if (rssi != UNKNOWN)
                    signal.setWifi_rssi(rssi);

                lteRsrp = Objects.requireNonNullElse(signalDataItem.getLteRsrp(), UNKNOWN);
                lteRsrq = Objects.requireNonNullElse(signalDataItem.getLteRsrq(), UNKNOWN);
                final int lteRssnr = Objects.requireNonNullElse(signalDataItem.getLteRssnr(), UNKNOWN);
                final int lteCqi = Objects.requireNonNullElse(signalDataItem.getLteCqi(), UNKNOWN);
                final long timeNs = Objects.requireNonNullElse(signalDataItem.getTimeNs(), (long) UNKNOWN);
                signal.setLte_rsrp(lteRsrp);
                signal.setLte_rsrq(lteRsrq);
                signal.setLte_rssnr(lteRssnr);
                signal.setLte_cqi(lteCqi);
                signal.setTime_ns(timeNs);

                signal.storeSignal();

                if (networkType == 99) // wlan
                {
                    if (rssi < signalStrength && rssi != UNKNOWN)
                        signalStrength = rssi;
                } else if (thisSignalStrength < signalStrength && thisSignalStrength != UNKNOWN)
                    signalStrength = thisSignalStrength;

                if (thisLinkSpeed != 0 && (linkSpeed == UNKNOWN || thisLinkSpeed < linkSpeed))
                    linkSpeed = thisLinkSpeed;

                if (signal.hasError())
                    throw new ControlServerException(signal.getError());
            }
            // set rssi value (typically GSM,UMTS, but also old LTE-phones)
            if (signalStrength != Integer.MAX_VALUE
                    && signalStrength != UNKNOWN
                    && signalStrength != 0) // 0 dBm is out of range
                test.signal_strength = signalStrength;
            // set rsrp value (typically LTE)
            if (lteRsrp != Integer.MAX_VALUE
                    && lteRsrp != UNKNOWN
                    && lteRsrp != 0) // 0 dBm is out of range
                test.lte_rsrp = lteRsrp;
            // set rsrq value (LTE)
            if (lteRsrq != Integer.MAX_VALUE
                    && lteRsrq != UNKNOWN)
                test.lte_rsrq = lteRsrq;

            if (linkSpeed != Integer.MAX_VALUE && linkSpeed != UNKNOWN)
                test.wifi_link_speed = linkSpeed;
        }

        // use max network type

        final String sqlMaxNetworkType = "SELECT nt.uid"
                + " FROM signal s"
                + " JOIN network_type nt"
                + " ON s.network_type_id=nt.uid"
                + " WHERE test_id=?"
                + " ORDER BY nt.technology_order DESC"
                + " LIMIT 1";

        try (PreparedStatement psMaxNetworkType = conn.prepareStatement(sqlMaxNetworkType)) {
            psMaxNetworkType.setLong(1, test.uid);
            if (psMaxNetworkType.execute()) {
                final ResultSet rs = psMaxNetworkType.getResultSet();
                if (rs.next()) {
                    final int maxNetworkType = rs.getInt("uid");
                    if (maxNetworkType != 0)
                        test.network_type = maxNetworkType;
                }
            }
        }

        /*
         * check for different types (e.g.
         * 2G/3G)
         */
        final String sqlAggSignal = "WITH agg AS"
                + " (SELECT array_agg(DISTINCT nt.group_name ORDER BY nt.group_name) agg"
                + " FROM signal s"
                + " JOIN network_type nt ON s.network_type_id=nt.uid WHERE test_id=?)"
                + " SELECT uid FROM agg JOIN network_type nt ON nt.aggregate=agg";

        try (PreparedStatement psAgg = conn.prepareStatement(sqlAggSignal)) {
            psAgg.setLong(1, test.uid);
            if (psAgg.execute()) {
                final ResultSet rs = psAgg.getResultSet();
                if (rs.next()) {
                    final int newNetworkType = rs.getInt("uid");
                    if (newNetworkType != 0)
                        test.network_type = newNetworkType;
                }
            }
        }
    }

    private void saveCellLocations(final List<CellLocationsItem> cellData, Test test, Connection conn) throws ControlServerException {
        if (cellData != null)
            for (var cellDataItem : cellData) {


                final Cell_location cellloc = new Cell_location(conn);

                cellloc.setTest_id(test.uid);

                final Timestamp tstamp = Timestamp.valueOf(new Timestamp(
                        cellDataItem.getTime()).toString());

                cellloc.setTime(tstamp, test.timezone);

                cellloc.setTime_ns(Objects.<Long>requireNonNullElse(cellDataItem.getTimeNs(), 0L));
                cellloc.setLocation_id(Objects.requireNonNullElse(cellDataItem.getLocationId(), 0));
                cellloc.setArea_code(Objects.requireNonNullElse(cellDataItem.getAreaCode(), 0));
                cellloc.setPrimary_scrambling_code(Objects.requireNonNullElse(cellDataItem.getPrimaryScramblingCode(), 0));

                cellloc.storeLocation();

                if (cellloc.hasError())
                    throw new ControlServerException(cellloc.getError());

            }
    }

    private void saveGeoLocationData(List<GeoLocationsItem> geoData, Test test, Connection conn) throws ControlServerException {
        if (geoData != null)
            for (var geoDataItem : geoData) {
                if (geoDataItem.getTstamp() != null && geoDataItem.getGeoLat() != null && geoDataItem.getGeoLat() != null) {

                    final GeoLocation geoloc = new GeoLocation(conn);

                    geoloc.setTest_id(test.uid);

                    final long clientTime = geoDataItem.getTstamp();
                    final Timestamp tstamp = Timestamp.valueOf(new Timestamp(
                            clientTime).toString());

                    geoloc.setTime(tstamp, test.timezone);
                    geoloc.setAccuracy((float) (geoDataItem.getAccuracy() == null ? 0f : (double) geoDataItem.getAccuracy()));
                    geoloc.setAltitude((geoDataItem.getAltitude() == null ? 0f : (double) geoDataItem.getAltitude()));
                    geoloc.setBearing((float) (geoDataItem.getBearing() == null ? 0f : (double) geoDataItem.getBearing()));
                    geoloc.setSpeed((float) (geoDataItem.getSpeed() == null ? 0f : (double) geoDataItem.getSpeed()));
                    geoloc.setProvider(Strings.nullToEmpty(geoDataItem.getProvider()));
                    geoloc.setGeo_lat(Objects.requireNonNullElse(geoDataItem.getGeoLat(), 0d));
                    geoloc.setGeo_long(Objects.requireNonNullElse(geoDataItem.getGeoLong(), 0d));
                    geoloc.setTime_ns(Objects.requireNonNullElse(geoDataItem.getTimeNs(), 0L));

                    geoloc.storeLocation();

                    if (geoloc.hasError())
                        throw new ControlServerException(geoloc.getError());
                }

                if (geoData.size() > 0) {
                    geoDataItem = geoData.get(geoData.size() - 1);

                    // Store Last Geolocation as
                    // Testlocation
                    if (geoDataItem.getGeoLat() != null)
                        test.geo_lat = geoDataItem.getGeoLat();

                    if (geoDataItem.getGeoLong() != null)
                        test.geo_long = geoDataItem.getGeoLong();

                    if (geoDataItem.getAccuracy() != null)
                        test.geo_accuracy = geoDataItem.getAccuracy();

                    if (geoDataItem.getProvider() != null)
                        test.geo_provider = geoDataItem.getProvider();


                }
            }
    }

    private void saveSpeedGraphData(List<SpeedDetailItem> speedData, Test test, Connection conn) throws SQLException {
        if (speedData != null) {

            // Speed graph data are stored in custom type array. To simplify insertion,
            // we construct direct string representation of that array and insert it all
            // at once. It's not pretty, but it's probably the simplest possible. Feel free
            // to improve this if you find a better way. :)
            StringBuilder graphData = new StringBuilder();
            graphData.append("{");

            boolean first = true;
            for (var item : speedData) {
                final SpeedDetailItem.DirectionEnum direction = item.getDirection();
                if (direction != null) {
                    if (first) {
                        first = false;
                    } else {
                        graphData.append(",");
                    }
                    graphData.append("\"(");
                    graphData.append(direction == SpeedDetailItem.DirectionEnum.UPLOAD ? "t," : "f,");
                    graphData.append(item.getThread() == null ? 0 : item.getThread());
                    graphData.append(",");
                    graphData.append((long) item.getTime());
                    graphData.append(",");
                    graphData.append((long) item.getBytes());
                    graphData.append(")\"");
                }
            }
            graphData.append("}");

            try (final PreparedStatement psSpeed = conn.prepareStatement("UPDATE test SET speed_graph_data = ?::speed_graph_pt[] WHERE uid = ?")) {
                psSpeed.setString(1, graphData.toString());
                psSpeed.setLong(2, test.uid);
                psSpeed.executeUpdate();
            }
        }
    }

    private void savePingData(final List<PingsItem> pingData, Test test, Connection conn) throws SQLException {
        if (pingData != null) {
            // save the data to the database
            try (PreparedStatement psPing = conn.prepareStatement("INSERT INTO ping (test_id, value, value_server, time_ns) " + "VALUES(?,?,?,?)")) {
                psPing.setLong(1, test.uid);

                final List<Long> pingList = new ArrayList<>();

                for (var pingDataItem : pingData) {

                    Long valueClient = pingDataItem.getValue();
                    if (valueClient != null) {
                        psPing.setLong(2, valueClient);
                    } else {
                        psPing.setNull(2, Types.BIGINT);
                    }

                    Long valueServer = pingDataItem.getValueServer();
                    if (valueServer != null) {
                        pingList.add(valueServer); // use server value for ping variance calculation

                        psPing.setLong(3, valueServer);
                    } else {
                        psPing.setNull(3, Types.BIGINT);
                    }

                    BigDecimal timeNs = pingDataItem.getTimeNs();
                    if (timeNs != null)
                        psPing.setLong(4, timeNs.longValue());
                    else
                        psPing.setNull(4, Types.BIGINT);

                    psPing.addBatch();
                }
                psPing.executeBatch();


                // set ping variance
                Double variance = caluclatePingVariance(pingList);
                if (variance != null)
                    test.ping_variance = variance;

                // set ping shortest
                if (pingList.size() > 0)
                    test.ping_shortest = pingList.stream().min(Long::compareTo).orElseThrow();
            }
        }
    }

    @Get("json")
    public Error retrieve(final ResultRequest entity) throws ControlServerException {
        return request(entity);
    }

    private String selectSecretKeyByServerToken(Connection conn, String serverToken) {
        final String sql =
                "SELECT ts.secret FROM test_server ts, test t " +
                        "WHERE t.server_id = ts.uid " +
                        "and t.token = ?";

        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, serverToken);

            try (ResultSet rs = ps.executeQuery()) {
                if (!rs.next()) {
                    logger.error("Couldn't find secret for serverToken={}", serverToken);
                    return null;
                }

                String secret = rs.getString("secret");
                return secret;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Calculates the variance of the givens pings.
     *
     * @param pings
     * @return The variance as double value
     */
    private Double caluclatePingVariance(List<Long> pings) {
        double pingMean = 0;
        double pingCount = pings.size();

        if (pingCount < 1) {
            return null; // cannot be calculated if there are no pings
        }

        for (long p : pings) {
            pingMean += p;
        }

        pingMean /= pingCount;

        ///

        double pingVariance = 0;

        for (long p : pings) {
            pingVariance += Math.pow((p - pingMean), 2);
        }

        pingVariance /= pingCount;

        return pingVariance;
    }
}
