/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources.generic;

import cz.nic.netmetr.server.control.ControlServer;
import cz.nic.netmetr.shared.*;
import cz.nic.netmetr.shared.db.DbConnection;
import cz.nic.netmetr.shared.reporting.AdvancedReporting;
import io.swagger.client.model.Error;
import org.restlet.data.Header;
import org.restlet.data.Reference;
import org.restlet.representation.Representation;
import org.restlet.resource.Options;
import org.restlet.resource.ResourceException;
import org.restlet.util.Series;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class ServerResource extends org.restlet.resource.ServerResource implements Settings {
    protected ResourceBundle labels;
    protected ResourceBundle settings;
    protected Classification classification;
    protected JsonUtil jsonUtil;

    protected DbConnection getDbConnectionFromApp() {
        return ((ControlServer) getApplication()).getDbConnection();
    }

    protected Connection getConnection() throws SQLException {
        return getDbConnectionFromApp().getConnection();
    }

    @Override
    public void doInit() throws ResourceException {
        super.doInit();
        classification = Classification.getInstance(getDbConnectionFromApp());

        settings = ResourceManager.getCfgBundle();
        // Set default Language for System
        Locale.setDefault(new Locale(settings.getString("RMBT_DEFAULT_LANGUAGE")));
        labels = ResourceManager.getSysMsgBundle();

        jsonUtil = new JsonUtil(labels, false);
    }


    /**
     * Because the tests can be run Cross-Origin, browsers make OPTIONS request (Cross-Origin PreFlight Test) to
     * determine, whether they should allow the request or not. By having this here, we allow OPTIONS requests globally
     * everywhere. There is nothing to do in the body of the call, because all it is about are the headers and those are
     * handled by Restlet.
     *
     * @param entity
     */
    @Options
    public void doOptions(final Representation entity) {
    }

    @SuppressWarnings("unchecked")
    private Series<Header> getHeaders() {
        return (Series<Header>) getRequest().getAttributes().get("org.restlet.http.headers");
    }


    public String getClientIP() {
        final Series<Header> headers = getHeaders();

        final String ipBehindProxy = headers.getFirstValue("x-forwarded-for", true);
        if (ipBehindProxy != null && ipBehindProxy.length() > 0) {
            return ipBehindProxy;
        }

        final String realIp = headers.getFirstValue("X-Real-IP", true);
        if (realIp != null) {
            return realIp;
        } else {
            return getRequest().getClientInfo().getAddress();
        }
    }

    public Reference getURL() {
        final Series<Header> headers = getHeaders();
        String realURL = headers.getFirstValue("X-Real-URL", true);

        final String referrerURL = headers.getFirstValue("Referer", true);
        if (referrerURL != null && referrerURL.length() > 0) {
            realURL = referrerURL;
        }

        if (realURL != null)
            return new Reference(realURL);
        else
            return getRequest().getOriginalRef();
    }

    @Override
    public String getSetting(String key) {
        return getSetting(key, null);
    }

    // TODO: add caching!
    @Override
    public String getSetting(String key, String lang) {
        return SettingsHelper.getSetting(getDbConnectionFromApp(), key, lang);
    }

    public AdvancedReporting getAdvancedReporting() throws InstantiationException, IllegalAccessException {
        return AdvancedReporting.newInstance();
    }

    protected void configureLanguage(String language) {
        configureLanguage(language, null);
    }

    /**
     * For backwards compatibility, sets language also on the errorList
     *
     * @param language
     * @param errorList
     */
    protected void configureLanguage(String language, ErrorList errorList) {
        if (language == null) return;

        final List<String> langs = Arrays.asList(settings.getString("RMBT_SUPPORTED_LANGUAGES").split(",\\s*"));
        if (langs.contains(language)) {
            labels = ResourceManager.getSysMsgBundle(new Locale(language));
            if (errorList != null) errorList.setLanguage(language);
        }
    }

    /**
     * Post-request hook
     * <p>
     * I made it this way, because when using Restlet Filters, there is no access to the objects representing JSON,
     * only serialized version of it is available. Because of the design of Restlet, we can't do something like this
     * with inheritance due to different argument types. It would be possible only by rewriting whole Restlet converters
     * and implementing it all by ourselves.
     */
    protected <E, T> T postRequestHook(E request, T response) {
        makeErrorListNotNull(response);
        return response;
    }

    /**
     * Some clients crash, when error list is not present in the response. Therefore, we try to add it to all responses.
     *
     * @param response
     * @param <T>
     */
    @SuppressWarnings("unchecked")
    private <T> void makeErrorListNotNull(T response) {
        // old code returns strings, we don't care about that
        if (response instanceof String) {
            return;
        }

        // Code bellow attempts to call methods generated by Swagger codegen by reflection. There is a potential
        // problem, that the generated code will silently change and this code will fail. We want to make this fail fast,
        // best would be at compile time. So, in order to make the compilation error, this impossible if is placed here.
        // That's why the condition if false is here.
        if (false) {
            // test if Error object contains getError method
            List<String> error = new Error().getError();
            // test if Error object contains setError method
            new Error().setError(new ArrayList<String>());
        }

        try {
            List<String> errorList = (List<String>) response.getClass().getMethod("getError").invoke(response);
            if (errorList == null) {
                errorList = new ArrayList<>();
            }
            response.getClass().getMethod("setError", List.class).invoke(response, errorList);

        } catch (ReflectiveOperationException | ClassCastException e) {
            // we don't care about this. There's nothing to do, when errors are not part of the object.
            // it would be better to ignore this try-catch by using if. But sadly, the API just throws exceptions.
            // There is no way to query about method existence
        }
    }
}
