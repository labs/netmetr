/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import io.swagger.client.model.Time;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.math.BigDecimal;


public class TimeResource extends ServerResource {

    @Post("json")
    public Time request(final String entity) {
        Time t = new Time();
        t.setTime(BigDecimal.valueOf(System.currentTimeMillis()));

        return postRequestHook(entity, t);
    }

    @Get("json")
    public Time retrieve(final String entity) {
        return request(entity);
    }
}
