/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.ErrorList;
import cz.nic.netmetr.shared.hstoreparser.HstoreParseException;
import cz.nic.netmetr.shared.qos.QoSUtil;
import cz.nic.netmetr.shared.qos.QoSUtil.TestUuid;
import cz.nic.netmetr.shared.qos.QoSUtil.TestUuid.UuidType;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class QoSResultResource extends ServerResource
{
    private Logger logger = LoggerFactory.getLogger(QoSResultResource.class);

    @Post("json")
    public String request(final String entity) throws SQLException {
        try (Connection conn = getConnection()) {
            JSONObject request = null;

            final ErrorList errorList = new ErrorList();
            final JSONObject answer = new JSONObject();

            if (entity != null && !entity.isEmpty())
                // try parse the string to a JSON object
                try {
                    request = new JSONObject(entity);

                    String lang = request.optString("language");

                    // Load Language Files for Client

                    final List<String> langs = Arrays.asList(settings.getString("RMBT_SUPPORTED_LANGUAGES").split(",\\s*"));

                    if (langs.contains(lang)) {
                        errorList.setLanguage(lang);
                    } else
                        lang = settings.getString("RMBT_DEFAULT_LANGUAGE");


                    if (conn != null) {
                        final String testUuid = request.optString("test_uuid");
                        long ts = System.nanoTime();
                        QoSUtil.evaluate(settings, conn, new TestUuid(testUuid, UuidType.TEST_UUID), answer, lang, errorList);
                        long endTs = System.nanoTime() - ts;
                        answer.put("evaluation", "Time needed to evaluate test result: " + ((float) endTs / 1000000f) + " ms");
                    } else {
                        errorList.addError("ERROR_DB_CONNECTION");
                    }

                } catch (final JSONException | IllegalArgumentException e) {
                    errorList.addError("ERROR_REQUEST_JSON");
                    logger.error("JSON parsing error", e);
                } catch (SQLException e) {
                    errorList.addError("ERROR_DB_CONNECTION");
                    logger.error("SQL error", e);
                } catch (HstoreParseException e) {
                    errorList.addErrorString(e.getMessage());
                    logger.error("Hstore parsing error", e);
                } catch (IllegalAccessException e) {
                    logger.error("", e);
                } catch (UnsupportedOperationException e) {
                    logger.error("", e);
                    errorList.addError("ERROR_REQUEST_QOS_RESULT_DETAIL_NO_UUID");
                }

            else
                errorList.addErrorString("Expected request is missing.");

            try {
                answer.putOpt("error", errorList.getList());
            } catch (final JSONException e) {
                logger.error("Error saving ErrorList", e);
            }

            String answerString = answer.toString();
            return postRequestHook(entity, answerString);
        }
    }
    
    @Get("json")
    public String retrieve(final String entity) throws SQLException {
        return request(entity);
    }
    
}
