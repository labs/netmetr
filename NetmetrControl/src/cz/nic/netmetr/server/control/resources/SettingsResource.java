/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.base.Strings;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.google.gson.Gson;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.ErrorList;
import cz.nic.netmetr.shared.HelperFunctions;
import cz.nic.netmetr.shared.RevisionHelper;
import cz.nic.netmetr.shared.db.Client;
import cz.nic.netmetr.shared.db.QoSTestTypeDesc;
import cz.nic.netmetr.shared.db.dao.QoSTestTypeDescDao;
import cz.nic.netmetr.shared.db.model.AdvertisedSpeedOption;
import cz.nic.netmetr.shared.db.repository.AdvertisedSpeedOptionRepository;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import cz.nic.netmetr.shared.exceptions.DatabaseConnectionException;
import cz.nic.netmetr.shared.exceptions.GeneralDatabaseException;
import cz.nic.netmetr.shared.exceptions.InvalidRequestException;
import cz.nic.netmetr.shared.utils.ObjectHelpers;
import io.swagger.client.model.SettingsRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.data.Parameter;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.util.Series;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class SettingsResource extends ServerResource {


    private Logger logger = LoggerFactory.getLogger(SettingsResource.class);

    @Post
    public String request(final SettingsRequest request) throws ControlServerException {
        final JSONObject answer = new JSONObject();

        if (request == null) {
            throw new InvalidRequestException("Expected request body is missing!");
        }

        try (var conn = getConnection()) {
            // Load Language Files for Client
            configureLanguage(request.getLanguage());

            if (conn == null) {
                throw GeneralDatabaseException.fromMessageCode("ERROR_DB_CONNECTION");
            }

            final Client client = new Client(conn);
            int typeId = 0;
            if (request.getType().length() > 0)
                typeId = Client.getTypeId(request.getType(), conn);

            final JSONArray settingsList = new JSONArray();
            final JSONObject jsonItem = new JSONObject();

            // check for unknown client type
            if (!Arrays.asList(settings.getString("RMBT_CLIENT_NAME").split(",\\s*")).contains(request.getName()) || typeId <= 0) {
                throw InvalidRequestException.fromMessageCode("ERROR_CLIENT_VERSION");
            }

            UUID uuid = parseUUID(request);
            long clientUid = getClientUid(client, uuid);


            // register client if not already registered
            boolean tcAccepted = ObjectHelpers.nonNull(request.getTermsAndConditionsAcceptedVersion(), 0) > 0; // accept any version for now
            // allow old non-version parameter
            if (!tcAccepted)
                tcAccepted = ObjectHelpers.nonNull(request.isTermsAndConditionsAccepted(), false);

            if (uuid == null || clientUid == 0) {
                if (tcAccepted) {
                    final Timestamp tstamp = Timestamp.valueOf(new Timestamp(System
                            .currentTimeMillis()).toString());

                    final Calendar timeWithZone = HelperFunctions.getTimeWithTimeZone(HelperFunctions.getTimezoneId());
                    client.setTimeZone(timeWithZone);
                    client.setTime(tstamp);
                    client.setClient_type_id(typeId);
                    client.setTcAccepted(tcAccepted);
                    uuid = client.storeClient();

                    if (client.hasError())
                        throw ControlServerException.fromMessageCode(client.getError());
                    else
                        jsonItem.put("uuid", uuid.toString());
                } else {
                    throw new ControlServerException("You must accept our terms and conditions to proceed!");
                }
            }

            if (client.getUid() <= 0) {
                throw ControlServerException.fromMessageCode("ERROR_CLIENT_UUID");
            }

            /* map server */
            final Series<Parameter> ctxParams = getContext().getParameters();
            final String host = ctxParams.getFirstValue("RMBT_MAP_HOST");
            final String sslStr = ctxParams.getFirstValue("RMBT_MAP_SSL");
            final String portStr = ctxParams.getFirstValue("RMBT_MAP_PORT");
            if (host != null && sslStr != null && portStr != null) {
                JSONObject mapServer = new JSONObject();
                mapServer.put("host", host);
                mapServer.put("port", Integer.parseInt(portStr));
                mapServer.put("ssl", Boolean.parseBoolean(sslStr));
                jsonItem.put("map_server", mapServer);
            }

            // FIXME remove this manual hack for detecting Turris routers
            boolean isTurris = request.getName().equals("RMBT") &&
                    request.getType().equals("DESKTOP") &&
                    request.isTermsAndConditionsAccepted() != null &&
                    request.isTermsAndConditionsAccepted() &&
                    request.getVersionCode() != null &&
                    request.getVersionCode() == 1 &&
                    request.getVersionName() != null &&
                    request.getVersionName().equals("1.0") &&
                    request.getLanguage() != null &&
                    request.getLanguage().equals("en_US");

            if (!isTurris && !"true".equals(getQueryValue("skip_history"))) {
                // history (used by mobile application for filtering)
                try {
                    final JSONObject subItem = new JSONObject();
                    subItem.put("devices", getSyncGroupDeviceList(conn, client));
                    subItem.put("networks", getUsedNetworkTypeList(conn, client));
                    jsonItem.put("history", subItem);
                } catch (final SQLException e) {
                    throw DatabaseConnectionException.fromMessageCode("ERROR_DB_GET_SETTING_HISTORY_SQL", e);
                }
            }


            final String hasAdvertisedSpeedOption = getSetting("has_advertised_speed_option");
            if (hasAdvertisedSpeedOption != null && Boolean.parseBoolean(hasAdvertisedSpeedOption.trim())) {
                AdvertisedSpeedOptionRepository advSpdOptnRepo = new AdvertisedSpeedOptionRepository(conn);
                List<AdvertisedSpeedOption> advSpdOptnList = advSpdOptnRepo.getAllEnabled();
                Gson gson = new Gson();
                JSONArray jsonArray = new JSONArray(gson.toJson(advSpdOptnList));
                jsonItem.put("advertised_speed_option", jsonArray);
            }


            //also put there: basis-urls for all services
            final JSONObject jsonItemURLs = new JSONObject();
            jsonItemURLs.put("open_data_prefix", getSetting("url_open_data_prefix", request.getLanguage()));
            jsonItemURLs.put("statistics", getSetting("url_statistics", request.getLanguage()));
            jsonItemURLs.put("control_ipv4_only", getSetting("control_ipv4_only", request.getLanguage()));
            jsonItemURLs.put("control_ipv6_only", getSetting("control_ipv6_only", request.getLanguage()));
            jsonItemURLs.put("url_ipv4_check", getSetting("url_ipv4_check", request.getLanguage()));
            jsonItemURLs.put("url_ipv6_check", getSetting("url_ipv6_check", request.getLanguage()));

            jsonItem.put("urls", jsonItemURLs);

            final JSONObject jsonControlServerVersion = new JSONObject();
            jsonControlServerVersion.put("control_server_version", RevisionHelper.getVerboseRevision());
            jsonItem.put("versions", jsonControlServerVersion);

            //servers
            final boolean userServerSelection = ObjectHelpers.nonNull(request.isUserServerSelection(), false);

            if (userServerSelection) {
                final String serverType;

                switch (client.getClient_type_name()) {
                    case "RMBTws":
                        serverType = "RMBTws";
                        break;
                    case "fup":
                        serverType = "fup";
                        break;
                    default:
                        serverType = "RMBT";
                }
                jsonItem.put("servers", getServersForServerType(conn, serverType));
            }


            // qos
            try {
                final Locale locale = new Locale(request.getLanguage() == null ? "en_US" : request.getLanguage());
                final QoSTestTypeDescDao testTypeDao = new QoSTestTypeDescDao(conn, locale);
                final JSONArray testTypeDescArray = new JSONArray();
                for (QoSTestTypeDesc desc : testTypeDao.getAll()) {
                    JSONObject json = new JSONObject();
                    json.put("test_type", desc.getTestType().name());
                    json.put("name", desc.getName());
                    testTypeDescArray.put(json);
                }
                jsonItem.put("qostesttype_desc", testTypeDescArray);
            } catch (SQLException e) {
                throw DatabaseConnectionException.fromMessageCode("ERROR_DB_CONNECTION");
            }

            settingsList.put(jsonItem);
            answer.put("settings", settingsList);

        } catch (final JSONException e) {
            throw ControlServerException.fromMessageCode("ERROR_REQUEST_JSON", e);
        } catch (SQLException e) {
            throw DatabaseConnectionException.fromMessageCode("ERROR_DB_CONNECTION", e);
        }

        String answerString = answer.toString();
        return postRequestHook(request, answerString);
    }

    private JSONArray getUsedNetworkTypeList(Connection conn, Client client) throws SQLException, ControlServerException {
        final JSONArray netList = new JSONArray();
        try (PreparedStatement st = conn.prepareStatement(
                "select distinct group_name" +
                        " from (select distinct network_type from test t where (t.client_id in (select ? union select uid from client where sync_group_id = ? ))) as t" +
                        " left join network_type nt on t.network_type=nt.uid" +
                        " where group_name is not null order by group_name;")) {

            st.setLong(1, client.getUid());
            st.setInt(2, client.getSync_group_id());

            try (ResultSet rs = st.executeQuery()) {
                if (rs != null)
                    while (rs.next())
                        // netList.put(Helperfunctions.getNetworkTypeName(rs.getInt("network_type")));
                        netList.put(rs.getString("group_name"));
                else
                    throw DatabaseConnectionException.fromMessageCode("ERROR_DB_GET_SETTING_HISTORY_NETWORKS_SQL");

            }
        }
        return netList;
    }

    private long getClientUid(Client client, UUID uuid) throws ControlServerException {
        if (uuid == null) {
            return 0;
        }

        long clientUid = client.getClientByUuid(uuid);
        if (client.hasError())
            throw ControlServerException.fromMessageCode(client.getError());
        return clientUid;
    }

    private UUID parseUUID(SettingsRequest request) throws InvalidRequestException {
        UUID uuid = null;
        try {
            if (!Strings.isNullOrEmpty(request.getUuid()))
                uuid = UUID.fromString(request.getUuid());
        } catch (IllegalArgumentException e) {
            throw new InvalidRequestException("Invalid UUID format.", e);
        }
        return uuid;
    }

    Cache<String, JSONArray> serversCache = Caffeine.newBuilder()
            .maximumSize(1000000)
            .expireAfterWrite(Duration.ofHours(1))
            .build();

    private JSONArray getServersForServerTypeRaw(Connection conn, String serverType) {
        if (conn == null)
            throw new RuntimeException("Database connection was not provided!");

        //empty
        if (serverType == null)
            return new JSONArray();

        final JSONArray result = new JSONArray();

        final String sql = "SELECT * FROM test_server WHERE active AND selectable AND server_type = ?";
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, serverType);

            try (ResultSet rs = ps.executeQuery()) {

                while (rs.next()) {
                    final JSONObject obj = new JSONObject();

                    obj.put("uuid", rs.getString("uuid"));
                    obj.put("name", rs.getString("name"));
                    obj.put("city", rs.getString("city"));
                    obj.put("country", rs.getString("country"));

                    result.put(obj);
                }
            }
        } catch (SQLException throwables) {
            throw new RuntimeException(throwables);
        }
        return result;
    }

    private JSONArray getServersForServerType(Connection conn, String serverType) throws ControlServerException {
        try {
            return serversCache.get(serverType, (key) -> getServersForServerTypeRaw(conn, key));
        } catch (RuntimeException e) {
            throw new ControlServerException(e);
        }
    }

    /**
     * @param entity
     * @return
     */
    @Get("json")
    public String retrieve(final SettingsRequest entity) throws ControlServerException {
        return request(entity);
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private JSONArray getSyncGroupDeviceList(Connection conn, final Client client) throws SQLException, ControlServerException {

        JSONArray ownDeviceList = null;

        try (PreparedStatement st = conn
                .prepareStatement(
                        "SELECT DISTINCT coalesce(adm.fullname, t.model) model FROM" +
                                " (SELECT DISTINCT model FROM test t WHERE (t.client_id IN (SELECT ? UNION SELECT uid FROM client WHERE sync_group_id = ? ))) AS t" +
                                " LEFT JOIN device_map adm ON adm.codename = t.model"
                                + " ORDER BY model ASC")) {

            st.setLong(1, client.getUid());
            st.setInt(2, client.getSync_group_id());


            try (final ResultSet rs = st.executeQuery()) {
                if (rs != null) {

                    ownDeviceList = new JSONArray();

                    while (rs.next()) {
                        final String model = rs.getString("model");
                        if (model == null || model.isEmpty())
                            ownDeviceList.put("Unknown Device");
                        else
                            ownDeviceList.put(model);
                    }
                } else
                    throw DatabaseConnectionException.fromMessageCode("ERROR_DB_GET_SETTING_HISTORY_DEVICES_SQL");
            }
        }

        return ownDeviceList;
    }

}
