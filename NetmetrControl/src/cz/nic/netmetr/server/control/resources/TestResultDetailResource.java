/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.google.common.base.Strings;
import cz.nic.netmetr.model.NetworkType;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.*;
import cz.nic.netmetr.shared.db.Client;
import cz.nic.netmetr.shared.db.TestNdt;
import cz.nic.netmetr.shared.db.model.Test;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.*;
import java.util.*;

public class TestResultDetailResource extends ServerResource {
    protected final String OPTION_WITH_KEYS = "WITH_KEYS";
    protected boolean optionWithKeys = false;
    private Logger logger = LoggerFactory.getLogger(TestResultDetailResource.class);

    /**
     * @param test
     * @param conn
     * @return
     * @throws JSONException
     */
    public static JSONObject getGeoLocation(Settings sett, Test test, Connection conn, ResourceBundle labels) throws JSONException {
        JSONObject json = new JSONObject();
        // geo-location
        if (!(test.getGeo_lat() == null || test.getGeo_long() == null || test.getGeo_accuracy() == null)) {
            final double accuracy = test.getGeo_accuracy();
            if (accuracy < Double.parseDouble(sett.getSetting("rmbt_geo_accuracy_detail_limit"))) {
                final StringBuilder geoString = new StringBuilder(HelperFunctions.geoToString(test.getGeo_lat(),
                        test.getGeo_long()));

                geoString.append(" (");
                if (test.getGeo_provider() != null) {
                    String provider = test.getGeo_provider().toUpperCase(Locale.US);

                    switch (provider) {
                        case "NETWORK":
                            provider = labels.getString("key_geo_source_network");
                            break;
                        case "GPS":
                            provider = labels.getString("key_geo_source_gps");
                            break;
                    }

                    geoString.append(provider);
                    geoString.append(", ");
                }
                geoString.append(String.format(Locale.US, "+/- %.0f m", accuracy));
                geoString.append(")");
                json.put("location", geoString.toString());

                //also try getting the distance during the test
                final long clientTime = test.getTime().toInstant().toEpochMilli();
                try {
                    OpenTestResource.LocationGraph locGraph = new OpenTestResource.LocationGraph(test.getUid(), clientTime, conn);
                    if ((locGraph.getTotalDistance() > 0) &&
                            locGraph.getTotalDistance() <= Double.parseDouble(sett.getSetting("rmbt_geo_distance_detail_limit"))) {
                        json.put("motion", Math.round(locGraph.getTotalDistance()) + " m");
                    }

                } catch (SQLException ex) {
                    //cannot happen since the test uid has to exist in here
                    ex.printStackTrace();
                }
            }

            // country derived from location
            if (test.getCountry_location() != null) {
                json.put("country_location", test.getCountry_location());
            }
        }

        return json;
    }

    @Post("json")
    public String request(final String entity) throws ControlServerException, SQLException {
        // sanity check that the input is present
        if (entity == null || entity.isEmpty()) {
            throw new ControlServerException("Expected request is missing.");
        }

        final JSONObject answer = new JSONObject();
        final ErrorList errorList = new ErrorList();

        try (Connection conn = getDbConnectionFromApp().getConnection()) {
            JSONObject request = null;

            // try parse the string to a JSON object
            request = new JSONObject(entity);

            JSONArray options = request.optJSONArray("options");
            if (options != null) {
                for (int i = 0; i < options.length(); i++) {
                    final String op = options.optString(i, null);
                    if (op != null) {
                        if (OPTION_WITH_KEYS.equals(op.toUpperCase(Locale.US))) {
                            jsonUtil.setOptionWithKeys(true);
                        }
                    }
                }
            }

            // Load Language Files for Client
            String lang = request.optString("language");
            configureLanguage(lang, errorList);

            final Client client = new Client(conn);
            TestNdt ndt = new TestNdt(conn);

            final String testUuid = request.optString("test_uuid");
            if (testUuid == null)
                throw new ControlServerException("Test UUID was not provided. Can't show test result details.");

            Test test = Test.getTestByUuid(conn, UUID.fromString(testUuid));

            if (!client.getClientByUid(test.getClient_id())) {
                throw new ControlServerException("No client was registered that matches this test result. There is probably an inconsistency in the server code. Please contact the server administrators.");
            }

            if (!ndt.loadByTestId(test.getUid()))
                ndt = null;

            final Locale locale = new Locale(lang);
            final Format format = new SignificantFormat(2);

            final JSONArray resultList = new JSONArray();

            final JSONObject singleItem = jsonUtil.addObject(resultList, "time");
            if (test.getClient_time() != null) {
                final long time = test.getClient_time().toInstant().toEpochMilli();
                singleItem.put("time", time); //csv 3

                if (test.getTimezone() != null) {
                    final TimeZone tz = TimeZone.getTimeZone(test.getTimezone());
                    singleItem.put("timezone", test.getTimezone());

                    final DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, locale);
                    dateFormat.setTimeZone(tz);
                    singleItem.put("value", dateFormat.format(Date.from(test.getClient_time().toInstant())));

                    final Format tzFormat = new DecimalFormat("+0.##;-0.##", new DecimalFormatSymbols(locale));

                    final float offset = tz.getOffset(time) / 1000f / 60f / 60f;
                    jsonUtil.addString(resultList, "timezone", String.format("UTC%sh", tzFormat.format(offset)));
                }
            }

            // speed download in Mbit/s (converted from kbit/s) - csv 10 (in kbit/s)
            if (test.speed_download != null) {
                final String download = format.format(test.speed_download.doubleValue() / 1000d);
                jsonUtil.addString(resultList, "speed_download",
                        String.format("%s %s", download, labels.getString("RESULT_DOWNLOAD_UNIT")));
            }

            // speed upload in MBit/s (converted from kbit/s) - csv 11 (in kbit/s)
            if (test.speed_upload != null) {
                final String upload = format.format(test.speed_upload.doubleValue() / 1000d);
                jsonUtil.addString(resultList, "speed_upload",
                        String.format("%s %s", upload, labels.getString("RESULT_UPLOAD_UNIT")));
            }

            // median ping in ms
            if (test.getPing_median() != null) {
                final String pingMedian = format.format(test.getPing_median().doubleValue() / 1000000d);
                jsonUtil.addString(resultList, "ping_median",
                        String.format("%s %s", pingMedian, labels.getString("RESULT_PING_UNIT")));
            }

            // variance ping in ms
            if (test.getPing_variance() != null) {
                final String pingVariance = format.format(test.getPing_variance() / 1000000000000d); // E^12 because variance is ^2 and stored as ns
                jsonUtil.addString(resultList, "ping_variance",
                        String.format("%s %s", pingVariance, labels.getString("RESULT_PING_UNIT")));
            }

            // jitter
            if (test.isJitterResultPresentable()) {
                jsonUtil.addString(resultList, labels.getString("RESULT_JITTER_DOWN"), test.getMeanJitterMsDown() + " ms");
                jsonUtil.addString(resultList, labels.getString("RESULT_JITTER_UP"), test.getMeanJitterMsUp() + " ms");
            }

            // packet loss
            if (test.isJitterPacketLossTestOK()) {
                jsonUtil.addString(resultList, labels.getString("RESULT_PACKET_LOSS_DOWN"), test.getPacketLossPercentDownlink() + "% " + MessageFormat.format(labels.getString("RESULT_PACKET_LOSS_N_PACKETS"), test.getPacket_loss_down_packets_total()));
                jsonUtil.addString(resultList, labels.getString("RESULT_PACKET_LOSS_UP"), test.getPacketLossPercentUplink() + "% " + MessageFormat.format(labels.getString("RESULT_PACKET_LOSS_N_PACKETS"), test.getPacket_loss_up_packets_total()));
            }

            // signal strength RSSI in dBm - csv 13
            if (test.getSignal_strength() != null)
                jsonUtil.addString(
                        resultList,
                        "signal_strength",
                        String.format("%d %s", test.getSignal_strength(),
                                labels.getString("RESULT_SIGNAL_UNIT")));

            //signal strength RSRP in dBm (LTE) - csv 29
            if (test.getLte_rsrp() != null)
                jsonUtil.addString(
                        resultList,
                        "signal_rsrp",
                        String.format("%d %s", test.getLte_rsrp(),
                                labels.getString("RESULT_SIGNAL_UNIT")));

            //signal quality in LTE, RSRQ in dB
            if (test.getLte_rsrq() != null)
                jsonUtil.addString(
                        resultList,
                        "signal_rsrq",
                        String.format("%d %s", test.getLte_rsrq(),
                                labels.getString("RESULT_DB_UNIT")));

            // network, eg. "3G (HSPA+)
            if (test.getNetwork_type() != null)
                jsonUtil.addString(resultList, "network_type",
                        NetworkType.fromId(test.getNetwork_type()).printableName());

            // geo-location
            JSONObject locationJson = getGeoLocation(this, test, conn, labels);
            if (locationJson.has("location")) {
                jsonUtil.addString(resultList, "location", locationJson.getString("location"));
            }
            if (locationJson.has("country_location")) {
                jsonUtil.addString(resultList, "country_location", locationJson.getString("country_location"));
            }
            if (locationJson.has("motion")) {
                jsonUtil.addString(resultList, "motion", locationJson.getString("motion"));
            }

            // country derived from AS registry
            if (test.getCountry_asn() != null)
                jsonUtil.addString(resultList, "country_asn", test.getCountry_asn());

            // country derived from geo-IP database
            if (test.getCountry_geoip() != null)
                jsonUtil.addString(resultList, "country_geoip", test.getCountry_geoip());

            if (test.getZip_code() != null) {
                final String zipCode = test.getZip_code().toString();
                final int zipCodeInt = test.getZip_code();
                if (zipCodeInt > 999 || zipCodeInt <= 9999)  // plausibility of zip code (must be 4 digits in Austria)
                    jsonUtil.addString(resultList, "zip_code", zipCode);
            }

            if (!Strings.isNullOrEmpty(test.getData())) {
                final JSONObject data = new JSONObject(test.getData());

                if (data.has("region"))
                    jsonUtil.addString(resultList, "region", data.getString("region"));
                if (data.has("municipality"))
                    jsonUtil.addString(resultList, "municipality", data.getString("municipality"));
                if (data.has("settlement"))
                    jsonUtil.addString(resultList, "settlement", data.getString("settlement"));
                if (data.has("whitespace"))
                    jsonUtil.addString(resultList, "whitespace", data.getString("whitespace"));

                if (data.has("cell_id"))
                    jsonUtil.addString(resultList, "cell_id", data.getString("cell_id"));
                if (data.has("cell_name"))
                    jsonUtil.addString(resultList, "cell_name", data.getString("cell_name"));
                if (data.has("cell_id_multiple") && data.getBoolean("cell_id_multiple"))
                    jsonUtil.addString(resultList, "cell_id_multiple", jsonUtil.getTranslation("value", "cell_id_multiple"));
            }

            if (test.getSpeed_test_duration() != null) {
                final String speedTestDuration = format.format(test.getSpeed_test_duration() / 1000d);
                jsonUtil.addString(resultList, "speed_test_duration",
                        String.format("%s %s", speedTestDuration, labels.getString("RESULT_DURATION_UNIT")));
            }

            // public client ip (private)
            jsonUtil.addString(resultList, "client_public_ip", test.getClient_public_ip());

            // AS number - csv 24
            jsonUtil.addString(resultList, "client_public_ip_as", test.getPublic_ip_asn() == null ? "detection failure" : test.getPublic_ip_asn().toString());

            // name of AS
            jsonUtil.addString(resultList, "client_public_ip_as_name", test.getPublic_ip_as_name());

            // reverse hostname (from ip) - (private)
            jsonUtil.addString(resultList, "client_public_ip_rdns", test.getPublic_ip_rdns());

            // operator - derived from provider_id (only for pre-defined operators)
            //TODO replace provider-information by more generic information
            jsonUtil.addString(resultList, "provider", test.getProvider_id_name());

            // type of client local ip (private)
            jsonUtil.addString(resultList, "client_local_ip", test.getClient_ip_local_type());

            // nat-translation of client - csv 23
            jsonUtil.addString(resultList, "nat_type", test.getNat_type());

            // wifi base station id - SSID (text) eg 'my hotspot'
            jsonUtil.addString(resultList, "wifi_ssid", test.getWifi_ssid());
            // wifi base station id BSSID (numberic) eg 01:2c:3d..
            jsonUtil.addString(resultList, "wifi_bssid", test.getWifi_bssid());

            // nominal link speed of wifi connection in MBit/s
            if (test.getWifi_link_speed() != null)
                jsonUtil.addString(
                        resultList,
                        "wifi_link_speed",
                        String.format("%d %s", test.getWifi_link_speed(),
                                labels.getString("RESULT_WIFI_LINK_SPEED_UNIT")));
            // name of mobile network operator (eg. 'T-Mobile AT')
            jsonUtil.addString(resultList, "network_operator_name", test.network_operator_name);

            // network_operator = mobile network name derived from MCC/MNC of network, eg. '232-01'
            // mobile_provider_name = mobile provider name, eg. 'Hutchison Drei' (derived from mobile_provider_id)
            if (test.getMobile_provider_name() == null) // eg. '248-02'
                jsonUtil.addString(resultList, "network_operator", test.getNetwork_operator());
            else {
                if (test.getNetwork_operator() == null)
                    jsonUtil.addString(resultList, "network_operator", test.getMobile_provider_name());
                else // eg. 'Hutchison Drei (232-10)'
                    jsonUtil.addString(resultList, "network_operator",
                            String.format("%s (%s)", test.getMobile_provider_name(), test.getNetwork_operator()));
            }

            jsonUtil.addString(resultList, "network_sim_operator_name", test.getNetwork_sim_operator_name());

            if (test.getNetwork_sim_operator_mcc_mnc_text() == null)
                jsonUtil.addString(resultList, "network_sim_operator", test.getNetwork_sim_operator());
            else
                jsonUtil.addString(resultList, "network_sim_operator",
                        String.format("%s (%s)", test.getNetwork_sim_operator_mcc_mnc_text(), test.getNetwork_sim_operator()));

            if (test.getRoaming_type() != null)
                jsonUtil.addString(resultList, "roaming", HelperFunctions.getRoamingType(labels, test.getRoaming_type()));

            final long totalDownload = Objects.requireNonNullElse(test.getTotal_bytes_download(), 0L);
            final long totalUpload = Objects.requireNonNullElse(test.getTotal_bytes_upload(), 0L);
            final long totalBytes = totalDownload + totalUpload;
            if (totalBytes > 0) {
                final String totalBytesString = format.format(totalBytes / (1000d * 1000d));
                jsonUtil.addString(
                        resultList,
                        "total_bytes",
                        String.format("%s %s", totalBytesString,
                                labels.getString("RESULT_TOTAL_BYTES_UNIT")));
            }

            // interface volumes - total including control-server and pre-tests (and other tests)
            final long totalIfDownload = Objects.requireNonNullElse(test.getTest_if_bytes_download(), 0L);
            final long totalIfUpload = Objects.requireNonNullElse(test.getTest_if_bytes_upload(), 0L);
            // output only total of down- and upload
            final long totalIfBytes = totalIfDownload + totalIfUpload;
            if (totalIfBytes > 0) {
                final String totalIfBytesString = format.format(totalIfBytes / (1000d * 1000d));
                jsonUtil.addString(
                        resultList,
                        "total_if_bytes",
                        String.format("%s %s", totalIfBytesString,
                                labels.getString("RESULT_TOTAL_BYTES_UNIT")));
            }
            // interface volumes during test
            // download test - volume in download direction
            if (test.getTestdl_if_bytes_download() != null && test.getTestdl_if_bytes_download() > 0) {
                final String testDlIfBytesDownloadString = format.format(test.getTestdl_if_bytes_download() / (1000d * 1000d));
                jsonUtil.addString(
                        resultList,
                        "testdl_if_bytes_download",
                        String.format("%s %s", testDlIfBytesDownloadString,
                                labels.getString("RESULT_TOTAL_BYTES_UNIT")));
            }
            // download test - volume in upload direction
            if (test.getTestdl_if_bytes_upload() != null && test.getTestdl_if_bytes_upload() > 0L) {
                final String testDlIfBytesUploadString = format.format(test.getTestdl_if_bytes_upload() / (1000d * 1000d));
                jsonUtil.addString(
                        resultList,
                        "testdl_if_bytes_upload",
                        String.format("%s %s", testDlIfBytesUploadString,
                                labels.getString("RESULT_TOTAL_BYTES_UNIT")));
            }
            // upload test - volume in upload direction
            if (test.getTestul_if_bytes_upload() != null && test.getTestul_if_bytes_upload() > 0) {
                final String testUlIfBytesUploadString = format.format(test.getTestul_if_bytes_upload() / (1000d * 1000d));
                jsonUtil.addString(
                        resultList,
                        "testul_if_bytes_upload",
                        String.format("%s %s", testUlIfBytesUploadString,
                                labels.getString("RESULT_TOTAL_BYTES_UNIT")));
            }
            // upload test - volume in download direction
            if (test.getTestul_if_bytes_download() != null && test.getTestul_if_bytes_download() > 0L) {
                final String testUlIfBytesDownloadString = format.format(test.getTestul_if_bytes_download() / (1000d * 1000d));
                jsonUtil.addString(
                        resultList,
                        "testul_if_bytes_download",
                        String.format("%s %s", testUlIfBytesDownloadString,
                                labels.getString("RESULT_TOTAL_BYTES_UNIT")));
            }

            //start time download-test
            if (test.getTime_dl_ns() != null) {
                jsonUtil.addString(resultList, "time_dl",
                        String.format("%s %s", format.format(test.getTime_dl_ns().doubleValue() / 1000000000d),  //convert ns to s
                                labels.getString("RESULT_DURATION_UNIT")));
            }

            //duration download-test
            if (test.getNsec_download() != null) {
                jsonUtil.addString(resultList, "duration_dl",
                        String.format("%s %s", format.format(test.getNsec_download().doubleValue() / 1000000000d),  //convert ns to s
                                labels.getString("RESULT_DURATION_UNIT")));
            }

            //start time upload-test
            if (test.getTime_ul_ns() != null) {
                jsonUtil.addString(resultList, "time_ul",
                        String.format("%s %s", format.format(test.getTime_ul_ns().doubleValue() / 1000000000d),  //convert ns to s
                                labels.getString("RESULT_DURATION_UNIT")));
            }

            //duration upload-test
            if (test.getNsec_upload() != null) {
                jsonUtil.addString(resultList, "duration_ul",
                        String.format("%s %s", format.format(test.getNsec_upload().doubleValue() / 1000000000d),  //convert ns to s
                                labels.getString("RESULT_DURATION_UNIT")));
            }

            if (ndt != null) {
                final String downloadNdt = format.format(ndt.getField("s2cspd").doubleValue());
                jsonUtil.addString(resultList, "speed_download_ndt",
                        String.format("%s %s", downloadNdt, labels.getString("RESULT_DOWNLOAD_UNIT")));

                final String uploaddNdt = format.format(ndt.getField("c2sspd").doubleValue());
                jsonUtil.addString(resultList, "speed_upload_ndt",
                        String.format("%s %s", uploaddNdt, labels.getString("RESULT_UPLOAD_UNIT")));

                // final String pingNdt =
                // format.format(ndt.getField("avgrtt").doubleValue());
                // jsonUtil.addString(resultList, "ping_ndt",
                // String.format("%s %s", pingNdt,
                // labels.getString("RESULT_PING_UNIT")));
            }

            jsonUtil.addString(resultList, "server_name", test.server_name);
            jsonUtil.addString(resultList, "plattform", test.plattform);
            jsonUtil.addString(resultList, "os_version", test.os_version);
            jsonUtil.addString(resultList, "model", test.model_fullname);
            jsonUtil.addString(resultList, "client_name", test.client_name);
            jsonUtil.addString(resultList, "client_software_version", test.client_software_version);
            final String encryption = test.encryption;

            if (encryption != null) {
                jsonUtil.addString(
                        resultList,
                        "encryption",
                        "NONE".equals(encryption) ? labels
                                .getString("key_encryption_false") : labels.getString("key_encryption_true"));
            }

            jsonUtil.addString(resultList, "client_version", test.client_version);

            jsonUtil.addString(
                    resultList,
                    "duration",
                    String.format("%d %s", test.duration,
                            labels.getString("RESULT_DURATION_UNIT")));

            // number of threads for download-test
            if (test.num_threads != null) {
                jsonUtil.addInt(resultList, "num_threads", test.num_threads);
            }

            //number of threads for upload-test
            if (test.num_threads_ul != null) {
                jsonUtil.addInt(resultList, "num_threads_ul", test.getNum_threads_ul());
            } else if (test.getNum_threads() != null) {
                jsonUtil.addInt(resultList, "num_threads_ul", test.getNum_threads());
            }

            //dz 2013-11-09 removed UUID from details as users might get confused by two
            //              ids;
            //jsonUtil.addString(resultList, "uuid", String.format("T%s", test.uuid));

            if (test.getOpen_test_uuid() != null)
                jsonUtil.addString(resultList, "open_test_uuid", test.getFormattedOpenTestUUID());

            //number of threads for upload-test
            if (test.getTag() != null) {
                jsonUtil.addString(resultList, "tag", test.getTag());
            }

            if (ndt != null) {
                jsonUtil.addString(resultList, "ndt_details_main", ndt.getField("main"));
                jsonUtil.addString(resultList, "ndt_details_stat", ndt.getField("stat"));
                jsonUtil.addString(resultList, "ndt_details_diag", ndt.getField("diag"));
            }

            AdvertisedSpeedOptionUtil.expandWithAdvertisedSpeedStatus(jsonUtil, this, test, resultList, format);


            if (test.getAdditional_report_fields() != null) {
                final JSONObject reportJson = new JSONObject(test.getAdditional_report_fields());
                final JSONObject resultReportJson = new JSONObject();
                final DecimalFormat fmt = (DecimalFormat) DecimalFormat.getInstance(locale);
                fmt.setMaximumFractionDigits(2);

                double peak = reportJson.optDouble("peak_up_kbit");
                if (peak > 0) {
                    resultReportJson.put("peak_up_kbit", String.format("%s %s", fmt.format(peak / 1000d),
                            labels.getString("RESULT_UPLOAD_UNIT")));
                }

                peak = reportJson.optDouble("peak_down_kbit");
                if (peak > 0) {
                    resultReportJson.put("peak_down_kbit", String.format("%s %s", fmt.format(peak / 1000d),
                            labels.getString("RESULT_DOWNLOAD_UNIT")));
                }

                jsonUtil.addJSONObject(resultList, "additional_report_fields", resultReportJson);
            }

            if (resultList.length() == 0)
                errorList.addError("ERROR_DB_GET_TESTRESULT_DETAIL");

            answer.put("testresultdetail", resultList);

        } catch (final JSONException e) {
            logger.error("Error parsing JSON Data " + e.toString());
            throw ControlServerException.fromMessageCode("ERROR_REQUEST_JSON");
        }

        try {
            answer.putOpt("error", errorList.getList());
        } catch (final JSONException e) {
            logger.error("Error saving ErrorList: " + e.toString());
        }

        String answerString = answer.toString();
        return postRequestHook(entity, answerString);
    }

    @Get("json")
    public String retrieve(final String entity) throws SQLException, ControlServerException {
        return request(entity);
    }
}
