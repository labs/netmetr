/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.base.Strings;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.GeoIPHelper;
import cz.nic.netmetr.shared.HelperFunctions;
import cz.nic.netmetr.shared.config.GlobalConfig;
import cz.nic.netmetr.shared.db.Client;
import cz.nic.netmetr.shared.db.GeoLocation;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import cz.nic.netmetr.shared.exceptions.GeneralDatabaseException;
import cz.nic.netmetr.shared.exceptions.InvalidRequestException;
import io.swagger.client.model.Location;
import io.swagger.client.model.RegistrationRequest;
import io.swagger.client.model.RegistrationResponse;
import org.json.JSONArray;
import org.restlet.data.Reference;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.sql.*;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class RegistrationResource extends ServerResource {
    static class TestServer {
        int id;
        String name;
        int port;
        String address;
        String secret;
        String type;
    }


    @Post("json")
    public RegistrationResponse request(final RegistrationRequest request) throws ControlServerException, SQLException {
        try (Connection conn = getConnection()) {

            if (request == null)
                throw InvalidRequestException.fromMessageCode("ERROR_REQUEST_JSON");

            // parameter defaults
            if (request.getLocation() == null)
                request.setLocation(new Location());

            final RegistrationResponse answer = new RegistrationResponse();
            int typeId = 0;
            String answerString;

            // geoIP lookup
            GeoIPHelper.GeoIPLookupResult geoIP = GeoIPHelper.GeoIPLookupResult.fullLookup(getClientIP());

            // Load Language Files for Client
            configureLanguage(request.getLanguage());

            final Client clientDb = new Client(conn);
            if (conn == null) {
                throw new GeneralDatabaseException();
            }

            if (!Strings.isNullOrEmpty(request.getType())) {
                typeId = Client.getTypeId(request.getType(), conn);
            }

            final List<String> clientNames = Arrays.asList(settings.getString("RMBT_CLIENT_NAME")
                    .split(",\\s*"));
            final List<String> clientVersions = Arrays.asList(settings.getString("RMBT_VERSION_NUMBER").split(
                    ",\\s*"));

            if (!clientNames.contains(request.getClient().getValue()) || !clientVersions.contains(request.getVersion().getValue()) || typeId <= 0)
                throw InvalidRequestException.fromMessageCode("ERROR_CLIENT_VERSION");

            UUID uuid = null;
            final String uuidString = request.getUuid();
            if (uuidString.length() != 0)
                uuid = UUID.fromString(uuidString);

            final String clientName = request.getClient().getValue();
            final String clientRMBTProtocolVersion = request.getVersion().getValue();

            final long clientTime = request.getTime();
            final Timestamp clientTstamp = Timestamp.valueOf(new Timestamp(clientTime).toString());


            Calendar timeWithZone = null;
            if (Strings.isNullOrEmpty(request.getTimezone())) {
                timeWithZone = HelperFunctions.getTimeWithTimeZone(HelperFunctions.getTimezoneId());
            } else {
                timeWithZone = HelperFunctions.getTimeWithTimeZone(request.getTimezone());
            }


            long clientUid = 0;
            /*
             * if (uuid == null) {
             * clientDb.setTimeZone(timeWithZone);
             * clientDb.setTime(tstamp);
             * clientDb.setClient_type_id(typeId); uuid =
             * clientDb.storeClient(); if (clientDb.hasError()) {
             * errorList.addError(clientDb.getError()); } else {
             * answer.put("uuid", uuid.toString()); } }
             */

            if (uuid != null) {
                clientUid = clientDb.getClientByUuid(uuid);
                if (clientDb.hasError())
                    throw new GeneralDatabaseException();
            }

            if (clientUid <= 0) throw InvalidRequestException.fromMessageCode("ERROR_CLIENT_UUID");

            final String testUuid = UUID.randomUUID().toString();
            final String testOpenUuid = UUID.randomUUID().toString();

            boolean hasNetwork = !"no_signal".equals(request.getCondition());
            boolean testServerEncryption = true; // default is true

            // hack for android api <= 10 (2.3.x)
            // using encryption with test doesn't work
            if (Strings.nullToEmpty(request.getPlattform()).equals("Android") && request.getApiLevel() != null) {
                try {
                    final int apiLevel = Integer.parseInt(request.getApiLevel());
                    if (apiLevel <= 10)
                        testServerEncryption = false;
                } catch (final NumberFormatException e) {
                }
            }


            final String serverType;
            switch (request.getClient()) {
                case RMBTWS:
                    serverType = RegistrationRequest.ClientEnum.RMBTWS.getValue();
                    break;
                case FUP:
                    serverType = RegistrationRequest.ClientEnum.FUP.getValue();
                    break;
                case HW_PROBE:
                    serverType = RegistrationRequest.ClientEnum.RMBT.getValue();
                    testServerEncryption = false;
                    break;
                default:
                    serverType = RegistrationRequest.ClientEnum.RMBT.getValue();
            }

            final Boolean ipv6;
            if (geoIP.clientAddress instanceof Inet6Address)
                ipv6 = true;
            else if (geoIP.clientAddress instanceof Inet4Address)
                ipv6 = false;
            else // should never happen, unless ipv > 6 is available
                ipv6 = null;

            TestServer server = null;

            boolean serverSelectionResult = false;

            if (request.isUserServerSelection()) {
                if (!Strings.isNullOrEmpty(request.getPreferServer())) {
                    serverSelectionResult = true;
                    server = getPreferredServer(conn, request.getPreferServer(), testServerEncryption, ipv6);
                }
            }

            if (server == null) {
                serverSelectionResult = false;
                server = getNearestServer(conn, request.getLocation().getLat(), request.getLocation().getLong(), request.getLocation().getTime(), geoIP.clientAddressString,
                        geoIP.asCountry, geoIP.geoIpCountry, serverType, testServerEncryption, ipv6);
            }

            if (server == null)
                throw new ControlServerException("could not find server");

            if (request.getTimezone().isEmpty()) {
                timeWithZone = HelperFunctions.getTimeWithTimeZone(HelperFunctions.getTimezoneId());
            } else
                timeWithZone = HelperFunctions.getTimeWithTimeZone(request.getTimezone());

            answer.setUserServerSelection(serverSelectionResult);
            answer.setTestServerAddress(server.address);
            answer.setTestServerPort(server.port);
            answer.setTestServerName(server.name);
            answer.setTestServerEncryption(testServerEncryption);
            answer.setTestServerType(server.type);

            answer.setTestDuration(getSetting("rmbt_duration"));
            answer.setTestNumthreads(getSetting("rmbt_num_threads"));
            answer.setTestNumpings(getSetting("rmbt_num_pings"));

            if (hasNetwork) {
                answer.setClientRemoteIp(geoIP.clientAddressString);
            }

            final String resultUrl = new Reference(getURL(), settings.getString("RMBT_RESULT_PATH"))
                    .getTargetRef().toString();

            answer.setResultUrl(resultUrl);

            final String resultQoSUrl = new Reference(getURL(), settings.getString("RMBT_QOS_RESULT_PATH")).getTargetRef().toString();

            answer.resultQosUrl(resultQoSUrl);


            final String query = "INSERT INTO test(time, uuid, open_test_uuid, client_id, client_name, client_version, client_software_version, client_language, client_public_ip, client_public_ip_anonymized, country_geoip, server_id, port, use_ssl, timezone, client_time, duration, num_threads_requested, status, software_revision, client_test_counter, client_previous_test_status, public_ip_asn, public_ip_as_name, country_asn, public_ip_rdns, run_ndt)"
                    + "VALUES(NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            try (PreparedStatement st = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

                int i = 1;
                // uuid
                st.setObject(i++, UUID.fromString(testUuid));
                // open_test_uuid
                st.setObject(i++, UUID.fromString(testOpenUuid));
                // client_id
                st.setLong(i++, clientUid);
                // client_name
                st.setString(i++, clientName);
                // client_version
                st.setString(i++, clientRMBTProtocolVersion);
                // client_software_version
                st.setString(i++, request.getSoftwareVersion());
                // client_language
                st.setString(i++, request.getLanguage());
                // client_public_ip
                st.setString(i++, hasNetwork ? geoIP.clientAddressString : "");
                // client_public_ip_anonymized
                st.setString(i++, hasNetwork ? HelperFunctions.anonymizeIp(geoIP.clientAddress) : "");
                // country_geoip (2digit country code derived from public IP of client)
                st.setString(i++, hasNetwork ? geoIP.geoIpCountry : "");
                // server_id
                st.setInt(i++, server.id);
                // port
                st.setInt(i++, server.port);
                // use_ssl
                st.setBoolean(i++, testServerEncryption);
                // timezone (of client)
                st.setString(i++, request.getTimezone());
                // client_time (local time of client)
                st.setTimestamp(i++, clientTstamp, timeWithZone);
                // duration (requested)
                st.setInt(i++, Integer.parseInt(getSetting("rmbt_duration")));
                // num_threads_requested
                st.setInt(i++, Integer.parseInt(getSetting("rmbt_num_threads")));
                // status (of test)
                st.setString(i++, "STARTED"); //was "RUNNING" before
                // software_revision (of client)
                st.setString(i++, request.getSoftwareRevision());
                // client_test_counter (number of tests the client has performed)
                if (request.getTestCounter() == null) // older clients did not support testCounter
                    st.setNull(i++, Types.INTEGER);
                else
                    st.setLong(i++, request.getTestCounter());
                // client_previous_test_status (outcome of previous test)
                st.setString(i++, request.getPreviousTestStatus());
                // AS name
                if (geoIP.asn == null)
                    st.setNull(i++, Types.BIGINT);
                else
                    st.setLong(i++, geoIP.asn);
                if (geoIP.asName == null)
                    st.setNull(i++, Types.VARCHAR);
                else
                    st.setString(i++, geoIP.asName);
                // AS country
                if (geoIP.asCountry == null)
                    st.setNull(i++, Types.VARCHAR);
                else
                    st.setString(i++, geoIP.asCountry);
                //public_ip_rdns
                String reverseDNS = hasNetwork ? HelperFunctions.reverseDNSLookup(geoIP.clientAddress) : null;
                if (reverseDNS == null || reverseDNS.isEmpty())
                    st.setNull(i++, Types.VARCHAR);
                else {
                    reverseDNS = reverseDNS.replaceFirst("\\.$", "");
                    st.setString(i++, reverseDNS); // cut off last dot (#332)
                }
                // run_ndt
                if (request.isNdt() != null)
                    st.setBoolean(i++, request.isNdt());
                else
                    st.setNull(i++, Types.BOOLEAN);

                final int affectedRows = st.executeUpdate();

                if (affectedRows == 0) throw ControlServerException.fromMessageCode("ERROR_DB_STORE_TEST");

                long key = 0;
                final ResultSet rs = st.getGeneratedKeys();
                if (rs.next())
                    // Retrieve the auto generated
                    // key(s).
                    key = rs.getLong(1);
                rs.close();

                try (PreparedStatement getProviderSt = conn
                        .prepareStatement("SELECT rmbt_set_provider_from_as(?)")) {
                    getProviderSt.setLong(1, key);
                    String provider = null;
                    if (getProviderSt.execute()) {
                        final ResultSet rs2 = getProviderSt.getResultSet();
                        if (rs2.next())
                            provider = rs2.getString(1);
                    }

                    if (provider != null)
                        answer.setProvider(provider);
                }

                try (PreparedStatement testSlotStatement = conn
                        .prepareStatement("SELECT rmbt_get_next_test_slot(?)")) {
                    testSlotStatement.setLong(1, key);
                    int testSlot = -1;
                    if (testSlotStatement.execute()) {
                        final ResultSet rs2 = testSlotStatement.getResultSet();
                        if (rs2.next())
                            testSlot = rs2.getInt(1);
                    }

                    if (testSlot < 0)
                        throw new GeneralDatabaseException();

                    final String data = testUuid + "_" + testSlot;
                    final String hmac = HelperFunctions.calculateHMAC(server.secret, data);
                    if (hmac.length() == 0)
                        throw InvalidRequestException.fromMessageCode("ERROR_TEST_TOKEN");

                    final String token = data + "_" + hmac;

                    try (final PreparedStatement updateSt = conn
                            .prepareStatement("UPDATE test SET token = ? WHERE uid = ?")) {
                        updateSt.setString(1, token);
                        updateSt.setLong(2, key);
                        updateSt.executeUpdate();
                    }

                    answer.setTestToken(token);

                    answer.setTestUuid(testUuid);
                    answer.setTestId(key);

                    final long now = System.currentTimeMillis();
                    int wait = testSlot - (int) (now / 1000);
                    if (wait < 0)
                        wait = 0;

                    answer.setTestWait(wait);

                    if (request.getLocation().getTime() != 0 && request.getLocation().getLat() != 0 && request.getLocation().getLong() != 0) {

                        final GeoLocation clientLocation = new GeoLocation(conn);

                        clientLocation.setTest_id(key);

                        final Timestamp geotstamp = Timestamp.valueOf(new Timestamp(
                                request.getTime()).toString());
                        clientLocation.setTime(geotstamp, request.getTimezone());

                        clientLocation.setAccuracy(request.getLocation().getAccuracy());
                        clientLocation.setAltitude(request.getLocation().getAltitude());
                        clientLocation.setBearing(request.getLocation().getBearing());
                        clientLocation.setSpeed(request.getLocation().getSpeed());
                        clientLocation.setProvider(request.getLocation().getProvider());
                        clientLocation.setGeo_lat(request.getLocation().getLat());
                        clientLocation.setGeo_long(request.getLocation().getLong());

                        clientLocation.storeLocation();

                        if (clientLocation.hasError())
                            throw ControlServerException.fromMessageCode(clientLocation.getError());
                    }
                }

            } catch (final SQLException e) {
                throw new GeneralDatabaseException(e);
            }

            return postRequestHook(request, answer);
        }
    }

    @Get("json")
    public RegistrationResponse retrieve(final RegistrationRequest entity) throws ControlServerException, SQLException {
        return request(entity);
    }

    Cache<String, Optional<TestServer>> preferredServerCache = Caffeine.newBuilder()
            .maximumSize(1000000)
            .expireAfterWrite(Duration.ofHours(1))
            .build();
    private Optional<TestServer> getPreferredServerRaw(Connection conn, final String uuid, final boolean ssl, final Boolean ipv6) {

        final String sql = "SELECT * FROM test_server"
                + " WHERE active"
                + " AND uuid = ?::uuid"
                + " LIMIT 1";

        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, uuid);
            try (ResultSet rs = ps.executeQuery()) {
                if (!rs.next())
                    return Optional.empty();
                return Optional.of(toTestServer(rs, ssl, ipv6));
            }
        } catch (SQLException throwables) {
            throw new RuntimeException(throwables);
        }
    }

    private TestServer getPreferredServer(Connection conn, final String uuid, final boolean ssl, final Boolean ipv6) throws ControlServerException {
        try {
            return preferredServerCache.get(String.format("%s %b %b", uuid, ssl, ipv6), (_key) -> getPreferredServerRaw(conn, uuid, ssl, ipv6)).orElse(null);
        } catch (RuntimeException e) {
            throw new ControlServerException("Failed to get preferred test server!", e);
        }
    }

    private TestServer getNearestServer(Connection conn, final double geolat, final double geolong,
                                        final long geotime, final String clientIp, final String asCountry, final String geoIpCountry, final String serverType,
                                        final boolean ssl, final Boolean ipv6) {

        // TODO find nearest Server to GeoLocation or IP address

        final String sql = "SELECT * FROM test_server"
                + " WHERE active"
                + " AND server_type = ?"
                + " AND (country = ? OR country = 'any' OR country IS NULL)"
                + " ORDER BY"
                + " (country != 'any' AND country IS NOT NULL) DESC,"
                + " priority,"
                + " random() * weight DESC"
                + " LIMIT 1";

        //+ " ST_Distance(ST_TRANSFORM(ST_SetSRID(ST_Point(?, ?), 4326), 900913))";??

        try (PreparedStatement ps = conn.prepareStatement(sql)) {

            // use geoIP with fallback to AS
            String country = asCountry;
            if (!Strings.isNullOrEmpty(geoIpCountry))
                country = geoIpCountry;

            int i = 1;
            ps.setString(i++, serverType);

            ps.setString(i++, country);
            try (ResultSet rs = ps.executeQuery()) {
                if (!rs.next())
                    return null;
                return toTestServer(rs, ssl, ipv6);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private TestServer toTestServer(final ResultSet rs, final boolean ssl, final Boolean ipv6) throws SQLException {
        final String address;
        if (ipv6 == null)
            address = rs.getString("web_address");
        else if (ipv6)
            address = rs.getString("web_address_ipv6");
        else
            address = rs.getString("web_address_ipv4");

        final TestServer result = new TestServer();

        result.id = rs.getInt("uid");
        result.address = address;
        result.port = rs.getInt(ssl ? "port_ssl" : "port");
        result.name = rs.getString("name") + " (" + rs.getString("city") + ")";
        result.type = rs.getString("server_type");

        String secret = rs.getString("secret");

        //Fallback secret key
        if (secret == null || secret.isEmpty()) {
            secret = GlobalConfig.getInstance().getRmbtSecretKey();
        }
        result.secret = secret;

        return result;
    }

}
