/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.google.common.net.InetAddresses;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.GeoIPHelper;
import io.swagger.client.model.Empty;
import io.swagger.client.model.RequestData;
import net.sf.uadetector.ReadableDeviceCategory;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.json.JSONException;
import org.restlet.Request;
import org.restlet.data.ClientInfo;
import org.restlet.data.Header;
import org.restlet.resource.Get;
import org.restlet.util.Series;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RequestDataCollector extends ServerResource {
    @Get("json")
    public RequestData request(final Empty entity) throws JSONException {
        final RequestData answer = new RequestData();

        final Request request = getRequest();
        final ClientInfo clientInfo = request.getClientInfo();
        final String uaString = clientInfo.getAgent();
        final String ip = getClientIP();
        answer.setIp(ip);
        answer.setCountryGeoip(GeoIPHelper.lookupCountry(InetAddresses.forString(ip)));
        answer.setPort(clientInfo.getPort());

        UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();
        ReadableUserAgent agent = parser.parse(uaString);

        answer.setProduct(agent.getName());
        answer.setVersion(agent.getVersionNumber().getMajor());
        answer.setVersionMinor(agent.getVersionNumber().getMinor());
        answer.setCategory(translateCategory(agent.getDeviceCategory().getCategory()));
        answer.setOs(agent.getOperatingSystem().getFamilyName());
        answer.setFamily(agent.getFamily().toString());
        answer.setAgent(uaString);

        final List<String> acceptedLanguages = clientInfo.getAcceptedLanguages().stream()
                .map(languagePreference -> languagePreference.getMetadata().getName())
                .collect(Collectors.toList());
        answer.setLanguages(acceptedLanguages);

        @SuppressWarnings("unchecked") final Series<Header> headers = (Series<Header>) request.getAttributes().get("org.restlet.http.headers");
        Map<String, String> headersObj = headers.stream()
                .collect(Collectors.toMap(Header::getName, Header::getValue));
        answer.setHeaders(headersObj);

        return postRequestHook(entity, answer);
    }

    private RequestData.CategoryEnum translateCategory(ReadableDeviceCategory.Category category) {
        return RequestData.CategoryEnum.valueOf(category.toString());
    }
}
