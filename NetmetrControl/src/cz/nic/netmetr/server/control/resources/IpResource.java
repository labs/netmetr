/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.google.common.base.Strings;
import com.google.common.net.InetAddresses;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.exceptions.GeneralDatabaseException;
import cz.nic.netmetr.shared.exceptions.InvalidRequestException;
import io.swagger.client.model.IpRequest;
import io.swagger.client.model.IpResponse;
import io.swagger.client.model.IpResponseData;
import io.swagger.client.model.LocationForIpRequest;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

public class IpResource extends ServerResource {
    @Post("json")
    public IpResponse request(final IpRequest request) throws InvalidRequestException, GeneralDatabaseException, SQLException {
        try (Connection conn = getConnection()) {
            if (request == null)
                throw new InvalidRequestException("Missing request body...");

            IpResponse answer = new IpResponse();

        /* sample request data
            {
                "api_level": "21",
                "device": "hammerhead",
                "language": "en",
                "model": "Nexus 5",
                "os_version": "5.0(1570415)",
                "plattform": "Android",
                "product": "hammerhead",
                "softwareRevision": "master_initial-2413-gf89049d",
                "softwareVersionCode": 20046,
                "softwareVersionName": "2.0.46",
                "timezone": "Europe/Vienna",
                "type": "MOBILE",
                "uuid": "........(uuid)........"
                "location": {
                    "accuracy": 20,
                    "age": 7740,
                    "lat": 51.1053539,
                    "long": 17.4921002,
                    "provider": "network"
                },
            }
        */
            UUID uuid = null;
            final String uuidString = Strings.nullToEmpty(request.getUuid());
            if (uuidString.length() != 0)
                uuid = UUID.fromString(uuidString);
            else
                throw new InvalidRequestException("Missing UUID parameter...");

            final String clientPlattform = request.getPlattform();
            final String clientModel = request.getModel();
            final String clientProduct = Strings.emptyToNull(request.getProduct());
            final String clientDevice = request.getDevice();
            final String clientSoftwareVersionCode;

            clientSoftwareVersionCode = request.getSoftwareVersionCode();

            final String clientApiLevel = Strings.emptyToNull(request.getApiLevel());

            final LocationForIpRequest location = request.getLocation();

            long geoage = 0; // age in ms
            double geolat = 0;
            double geolong = 0;
            float geoaccuracy = 0; // in m
            double geoaltitude = 0;
            float geospeed = 0; // in m/s
            String geoprovider = "";

            if (location != null) {
                geoage = location.getAge();
                geolat = location.getLat();
                geolong = location.getLong();
                geoaccuracy = location.getAccuracy();
                geoaltitude = location.getAltitude();
                geospeed = location.getSpeed();
                geoprovider = location.getProvider();
            }


            final String query =
                    "INSERT INTO status(client_uuid,time,plattform,model,product,device,software_version_code,api_level,ip,"
                            + "age,lat,long,accuracy,altitude,speed,provider)"
                            + "VALUES(?, NOW(),?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            try (PreparedStatement st = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                int i = 1;
                st.setObject(i++, uuid);
                st.setObject(i++, clientPlattform);
                st.setObject(i++, clientModel);
                st.setObject(i++, clientProduct);
                st.setObject(i++, clientDevice);
                st.setObject(i++, clientSoftwareVersionCode);
                st.setObject(i++, clientApiLevel);
                st.setObject(i++, getClientIP());
                // location information
                st.setObject(i++, geoage);
                st.setObject(i++, geolat);
                st.setObject(i++, geolong);
                st.setObject(i++, geoaccuracy);
                st.setObject(i++, geoaltitude);
                st.setObject(i++, geospeed);
                st.setObject(i++, geoprovider);

                final int affectedRows = st.executeUpdate();
                if (affectedRows == 0)
                    throw new GeneralDatabaseException();
            } catch (final SQLException e) {
                e.printStackTrace();
                throw new GeneralDatabaseException(e);
            }

            answer.setIp(getClientIP());

            InetAddress clientAddress = InetAddresses.forString(getClientIP());
            if (clientAddress instanceof Inet4Address) {
                answer.setV(IpResponseData.VEnum._4);
            } else if (clientAddress instanceof Inet6Address) {
                answer.setV(IpResponseData.VEnum._6);
            }

            return postRequestHook(request, answer);
        }
    }

    @Get("json")
    public IpResponse retrieve(final IpRequest entity) throws InvalidRequestException, GeneralDatabaseException, SQLException {
        return request(entity);
    }

}