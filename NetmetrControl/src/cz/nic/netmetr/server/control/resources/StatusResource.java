/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.google.common.base.Strings;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.exceptions.GeneralDatabaseException;
import io.swagger.client.model.StatusRequest;
import io.swagger.client.model.StatusResponse;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StatusResource extends ServerResource {
    @Post("json")
    public StatusResponse request(final StatusRequest request) throws GeneralDatabaseException, SQLException {
        try (Connection conn = getConnection()) {

            StatusResponse answer = new StatusResponse();

            boolean homeCountry = true;
            double geolat = request.getLat();
            double geolong = request.getLong();
            String telephonyNetworkSimCountry = Strings.nullToEmpty(request.getTelephonyNetworkSimCountry());


            /*
             *
             * wb_a2 = 'AT' (2 digit country code)
             * ne_50m_admin_0_countries (SRID 900913 Mercator)
             * lat/long (SRID 4326 WGS84)
             *
             * SELECT st_contains(the_geom, ST_transform(ST_GeomFromText('POINT(56.391944 48.218056)',4326),900913)) home_country
             * FROM  ne_50m_admin_0_countries
             * WHERE wb_a2='AT';
             *
             *
             * */

            String sql = " SELECT st_contains(the_geom, ST_TRANSFORM(ST_GeomFromText( ? ,4326),900913)) home_country " +
                    " FROM  ne_50m_admin_0_countries " +
                    " WHERE wb_a2 = ? ";
            try (PreparedStatement st = conn.prepareStatement(sql)) {

                int i = 1;
                final String point = "POINT(" + geolong + " " + geolat + ")";
                st.setObject(i++, point);
                st.setObject(i++, telephonyNetworkSimCountry.toUpperCase());

                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) // result only available if country (wb_a2) is found in ne_50_admmin_0_countries
                        homeCountry = rs.getBoolean("home_country");
                }

            } catch (final SQLException e) {
                throw new GeneralDatabaseException(e);
            }

            answer.setHomeCountry(homeCountry);
            return postRequestHook(request, answer);
        }
    }

    @Get("json")
    public StatusResponse retrieve(final StatusRequest entity) throws GeneralDatabaseException, SQLException {
        return request(entity);
    }

}