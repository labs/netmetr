/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import cz.nic.netmetr.shared.exceptions.GeneralDatabaseException;
import io.swagger.client.model.NewsItem;
import io.swagger.client.model.NewsRequest;
import io.swagger.client.model.NewsResponse;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NewsResource extends ServerResource {
    @Post("json")
    public NewsResponse request(final NewsRequest request) throws ControlServerException, SQLException {
        try(Connection conn = getConnection()) {

            final NewsResponse answer = new NewsResponse();

            // Load Language Files for Client
            String lang = request.getLanguage();
            configureLanguage(lang);

            String sqlLang = lang;
            if (!sqlLang.equals("de") && !sqlLang.equals("cs"))
                sqlLang = "en";

            final Long lastNewsUid = request.getLastNewsUid();
            final String plattform = request.getPlattform();
            final int softwareVersionCode = request.getSoftwareVersionCode() == null ? -1 : request.getSoftwareVersionCode();
            String uuid = request.getUuid();

            String query = "SELECT uid,title_" + sqlLang +
                    " AS title, text_" + sqlLang +
                    " AS text FROM news " +
                    " WHERE" +
                    " (uid > ? OR force = true)" +
                    " AND active = true" +
                    " AND (plattform IS NULL OR plattform = ?)" +
                    " AND (max_software_version_code IS NULL OR ? <= max_software_version_code)" +
                    " AND (min_software_version_code IS NULL OR ? >= min_software_version_code)" +
                    " AND (uuid IS NULL OR uuid::TEXT = ?)" + //convert to text so that empty uuid-strings are tolerated
                    " ORDER BY time ASC";
            try (final PreparedStatement st = conn.prepareStatement(query)) {

                st.setLong(1, lastNewsUid);
                st.setString(2, plattform);
                st.setInt(3, softwareVersionCode);
                st.setInt(4, softwareVersionCode);
                st.setString(5, uuid);

                try (final ResultSet rs = st.executeQuery()) {
                    List<NewsItem> news = new ArrayList<NewsItem>();
                    while (rs.next()) {
                        NewsItem item = new NewsItem();

                        item.setUid(rs.getInt("uid"));
                        item.setTitle(rs.getString("title"));
                        item.setText(rs.getString("text"));

                        news.add(item);
                    }
                    answer.news(news);
                }

            } catch (final SQLException e) {
                throw GeneralDatabaseException.fromMessageCode("ERROR_DB_GET_NEWS_SQL");
            }

            return postRequestHook(request, answer);
        }
    }

    @Get("json")
    public NewsResponse retrieve(final NewsRequest entity) throws ControlServerException, SQLException {
        return request(entity);
    }

}
