/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control.resources;

import com.google.common.base.Strings;
import cz.nic.netmetr.server.control.resources.generic.ServerResource;
import cz.nic.netmetr.shared.LanguageManager;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import cz.nic.netmetr.shared.exceptions.DatabaseConnectionException;
import cz.nic.netmetr.shared.exceptions.GeneralDatabaseException;
import io.swagger.client.model.SyncRequest;
import io.swagger.client.model.SyncResponse;
import io.swagger.client.model.SyncResponseItem;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.UUID;

public class SyncResource extends ServerResource {
    @Post("json")
    public SyncResponse request(final SyncRequest request) throws ControlServerException, SQLException {
        try (var conn = getConnection()) {
            configureLanguage(request.getLanguage());
            SyncResponse answer = new SyncResponse();

            if (conn == null) {
                throw new DatabaseConnectionException();
            }


            UUID uuid = null;
            if (!Strings.isNullOrEmpty(request.getUuid()))
                uuid = UUID.fromString(request.getUuid());

            if (uuid != null && Strings.isNullOrEmpty(request.getSyncCode())) {
                String syncCode;

                try (PreparedStatement st = conn.prepareStatement("SELECT rmbt_get_sync_code(CAST (? AS UUID)) AS code")) {
                    st.setString(1, uuid.toString());

                    try (ResultSet rs = st.executeQuery()) {
                        if (rs.next())
                            syncCode = rs.getString("code");
                        else
                            throw GeneralDatabaseException.fromMessageCode("ERROR_DB_GET_SYNC_SQL");
                    }
                } catch (final SQLException e) {
                    throw GeneralDatabaseException.fromMessageCode("ERROR_DB_GET_SYNC_SQL");
                }

                SyncResponseItem item = new SyncResponseItem();
                //lower case code is easier to enter on mobile devices
                item.setSyncCode(syncCode.toLowerCase(Locale.US));
                answer.addSyncItem(item);

            } else if (uuid != null && !Strings.isNullOrEmpty(request.getSyncCode())) {

                final String syncCode = request.getSyncCode().toUpperCase(Locale.US);
                int syncGroup1 = 0;
                int uid1 = 0;
                int syncGroup2 = 0;
                int uid2 = 0;

                String msgTitle = labels.getString("SYNC_SUCCESS_TITLE");
                String msgText = labels.getString("SYNC_SUCCESS_TEXT");

                boolean error = false;

                try {
                    PreparedStatement st = conn.prepareStatement("SELECT * FROM client WHERE sync_code = ? AND sync_code_timestamp + INTERVAL '1 month' > NOW()");
                    st.setString(1, syncCode);

                    ResultSet rs = st.executeQuery();

                    if (rs.next()) {
                        syncGroup1 = rs.getInt("sync_group_id");
                        uid1 = rs.getInt("uid");
                    } else {
                        msgTitle = labels.getString("SYNC_CODE_TITLE");
                        msgText = labels.getString("SYNC_CODE_TEXT");
                        error = true;
                        // errorList.addError(MessageFormat.format(labels.getString("ERROR_DB_GET_CLIENT"),
                        // new Object[] {uuid}));
                    }
                    rs.close();
                    st.close();

                    st = conn.prepareStatement("SELECT * FROM client WHERE uuid = CAST(? AS UUID)");
                    st.setString(1, uuid.toString());

                    rs = st.executeQuery();

                    if (rs.next()) {
                        syncGroup2 = rs.getInt("sync_group_id");
                        uid2 = rs.getInt("uid");
                    } else {
                        msgTitle = labels.getString("SYNC_UUID_TITLE");
                        msgText = labels.getString("SYNC_UUID_TEXT");
                        error = true;
                        // errorList.addError(MessageFormat.format(labels.getString("ERROR_DB_GET_CLIENT"),
                        // new Object[] {uuid}));
                    }
                    rs.close();
                    st.close();

                    if (syncGroup1 > 0 && syncGroup1 == syncGroup2) {
                        msgTitle = labels.getString("SYNC_GROUP_TITLE");
                        msgText = labels.getString("SYNC_GROUP_TEXT");
                        error = true;
                    }

                    if (uid1 > 0 && uid1 == uid2) {
                        msgTitle = labels.getString("SYNC_CLIENT_TITLE");
                        msgText = labels.getString("SYNC_CLIENT_TEXT");
                        error = true;
                    }

                    if (!error)
                        if (syncGroup1 == 0 && syncGroup2 == 0) {

                            int key = 0;

                            // create new group
                            st = conn.prepareStatement("INSERT INTO sync_group(tstamp) " + "VALUES(now())",
                                    Statement.RETURN_GENERATED_KEYS);

                            int affectedRows = st.executeUpdate();
                            if (affectedRows == 0)
                                throw GeneralDatabaseException.fromMessageCode("ERROR_DB_STORE_SYNC_GROUP");
                            else {

                                rs = st.getGeneratedKeys();
                                if (rs.next())
                                    // Retrieve the auto generated
                                    // key(s).
                                    key = rs.getInt(1);
                                rs.close();
                            }
                            st.close();

                            if (key > 0) {
                                st = conn
                                        .prepareStatement("UPDATE client SET sync_group_id = ? WHERE uid = ? OR uid = ?");
                                st.setInt(1, key);
                                st.setInt(2, uid1);
                                st.setInt(3, uid2);

                                affectedRows = st.executeUpdate();

                                if (affectedRows == 0)
                                    throw GeneralDatabaseException.fromMessageCode("ERROR_DB_UPDATE_SYNC_GROUP");

                                st.close();
                            }

                        } else if (syncGroup1 == 0 && syncGroup2 > 0) {

                            // add 1 to 2

                            st = conn.prepareStatement("UPDATE client SET sync_group_id = ? WHERE uid = ?");
                            st.setInt(1, syncGroup2);
                            st.setInt(2, uid1);

                            final int affectedRows = st.executeUpdate();

                            if (affectedRows == 0)
                                throw GeneralDatabaseException.fromMessageCode("ERROR_DB_UPDATE_SYNC_GROUP");

                            st.close();

                        } else if (syncGroup1 > 0 && syncGroup2 == 0) {

                            // add 2 to 1

                            st = conn.prepareStatement("UPDATE client SET sync_group_id = ? WHERE uid = ? ");
                            st.setInt(1, syncGroup1);
                            st.setInt(2, uid2);

                            final int affectedRows = st.executeUpdate();

                            if (affectedRows == 0)
                                throw GeneralDatabaseException.fromMessageCode("ERROR_DB_UPDATE_SYNC_GROUP");

                            st.close();

                        } else if (syncGroup1 > 0 && syncGroup2 > 0) {

                            // add all of 2 to 1

                            st = conn
                                    .prepareStatement("UPDATE client SET sync_group_id = ? WHERE sync_group_id = ?");
                            st.setInt(1, syncGroup1);
                            st.setInt(2, syncGroup2);

                            int affectedRows = st.executeUpdate();

                            if (affectedRows == 0)
                                throw GeneralDatabaseException.fromMessageCode("ERROR_DB_UPDATE_SYNC_GROUP");
                            else {

                                // Delete empty group
                                st = conn.prepareStatement("DELETE FROM sync_group WHERE uid = ?");
                                st.setInt(1, syncGroup2);

                                affectedRows = st.executeUpdate();

                                if (affectedRows == 0)
                                    throw GeneralDatabaseException.fromMessageCode("ERROR_DB_DELETE_SYNC_GROUP");
                            }

                            st.close();

                        }

                } catch (final SQLException e) {
                    throw new GeneralDatabaseException(LanguageManager.resolve("ERROR_DB_GET_SYNC_SQL"), e);
                }

                SyncResponseItem item = new SyncResponseItem();
                item.setMsgTitle(msgTitle);
                item.setMsgText(msgText);
                item.setSuccess(!error);
                answer.addSyncItem(item);
            }


            return postRequestHook(request, answer);
        }
    }

    @Get("json")
    public SyncResponse retrieve(final SyncRequest entity) throws ControlServerException, SQLException {
        return request(entity);
    }

}
