package cz.nic.netmetr.server.control;

import com.google.gson.Gson;
import cz.nic.netmetr.shared.LanguageManager;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import cz.nic.netmetr.shared.exceptions.GeneralDatabaseException;
import cz.nic.netmetr.shared.exceptions.InvalidRequestException;
import io.swagger.client.model.Error;
import org.postgresql.util.PSQLException;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ControlServerStatusService extends org.restlet.service.StatusService {
    private Logger logger = LoggerFactory.getLogger(ControlServerStatusService.class);

    @Override
    public Status toStatus(Throwable throwable, Request request, Response response) {
        return new Status(Status.CLIENT_ERROR_BAD_REQUEST, throwable);
    }

    @Override
    public Representation toRepresentation(Status status, Request request, Response response) {
        Error answer = new Error();
        Throwable cause = status.getThrowable().getCause();  // jetty specific way to extract the underlying exception

        if (cause == null) {
            answer.addErrorItem("Something caused an error in the server. I have no idea, what it is, because even the exception is null.");
        } else if (cause instanceof NullPointerException) {
            answer.addErrorItem(LanguageManager.resolve("ERROR_REQUEST_JSON"));
            cause.printStackTrace();
        } else if (cause instanceof ControlServerException) {
            answer.addErrorItem(cause.getMessage());
        } else if (cause instanceof PSQLException) {
            // not nice, but ensures the message is only in one place
            answer.addErrorItem(new GeneralDatabaseException().getMessage());
        } else {
            // WARNING: I'm not exactly sure, if this is a good idea. It will let attacker know a bit of internal details
            // about error caused by some malformed input. However, the project is open source so keeping the message
            // secret would not help much and this might actually help to catch more errors.
            answer.addErrorItem("Unknown error! [" + cause.getMessage() + "]");
        }


        if (cause instanceof InvalidRequestException) {
            // do not log full stack traces of errors caused by the user
            logger.warn("Error occured during request processing! - " + cause.toString());
        } else {
            // log the error
            logger.warn("Error occurred during request processing!", cause);
        }

        Gson gson = new Gson();
        return new StringRepresentation(gson.toJson(answer, Error.class));
    }
}
