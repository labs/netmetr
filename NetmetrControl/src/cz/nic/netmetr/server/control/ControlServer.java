/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.control;

import cz.nic.netmetr.boot.NetmetrBackgroundTasks;
import cz.nic.netmetr.server.control.resources.*;
import cz.nic.netmetr.server.control.resources.openapi.OpenAPISpecResource;
import cz.nic.netmetr.shared.db.DbConnection;
import cz.nic.netmetr.shared.filter.LoggingRequestIdFilter;
import cz.nic.netmetr.shared.reporting.AdvancedReporting;
import cz.nic.netmetr.shared.reporting.PeakReport;
import cz.nic.netmetr.shared.reporting.TcpUdpPortsReport;
import cz.nic.netmetr.shared.reporting.VoipPortsReport;
import cz.nic.netmetr.shared.resources.openapi.OpenAPIUIResource;
import cz.nic.netmetr.shared.restlet.filter.DefaultHeadersFilter;
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;


public class ControlServer extends Application {

    private final DbConnection dbConnection;
    private Logger logger = LoggerFactory.getLogger(ControlServer.class);

    public ControlServer(DbConnection dbConnection) {
        this.dbConnection = dbConnection;
        setStatusService(new ControlServerStatusService());
        initServerServices();
    }

    private void initServerServices() {
        // content of this method was moved from ContextListener, because I am removing dependency on Tomcat

        //init AdvancedReporting:
        AdvancedReporting.init(TcpUdpPortsReport.class, VoipPortsReport.class, PeakReport.class);

        // Remove IP addresses every day from the database
        NetmetrBackgroundTasks.getInstance().runDaily(this::runDatabaseIpAddressCleanup);
    }

    private void runDatabaseIpAddressCleanup() {
        logger.info("Running periodic IP address cleanup...");
        PreparedStatement ps;
        try (Connection conn = getDbConnection().getConnection()) {
            //purge test table
            ps = conn.prepareStatement(
                    "UPDATE test SET client_public_ip = NULL, public_ip_rdns = NULL, source_ip = NULL, client_ip_local = NULL "
                            + "WHERE time < NOW() - CAST('4 months' AS INTERVAL) "
                            + "AND (client_public_ip IS NOT NULL OR public_ip_rdns IS NOT NULL OR source_ip IS NOT NULL OR client_ip_local IS NOT NULL)");
            ps.executeUpdate();
            ps.close();
            //purge ndt table
            ps = conn.prepareStatement("UPDATE test_ndt n SET main = NULL, stat = NULL, diag = NULL FROM test t "
                    + "WHERE t.uid = n.test_id AND t.time < NOW() - CAST('4 months' AS INTERVAL) "
                    + "AND (n.main IS NOT NULL OR n.stat IS NOT NULL OR n.diag IS NOT NULL)");
            ps.executeUpdate();
            ps.close();
            //purge status table
            ps = conn.prepareStatement("UPDATE status SET ip = NULL "
                    + "WHERE time < NOW() - CAST('4 months' AS INTERVAL) "
                    + "AND (ip IS NOT NULL)");
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            logger.error("Periodic IP address cleanup failed", e);
        }
        logger.info("Periodic IP address cleanup completed.");
    }

    public DbConnection getDbConnection() {
        return dbConnection;
    }

    /**
     * The Restlet instance that will call the correct resource depending up on
     * URL mapped to it.
     *
     * @return -- The resource Restlet mapped to the URL.
     */
    @Override
    public Restlet createInboundRoot() {

        final Router router = new Router(getContext());

        router.attach("/version", VersionResource.class);
        router.attach("/time", TimeResource.class);

        // test request
        router.attach("/", RegistrationResource.class); // old URL, for backwards compatibility
        router.attach("/testRequest", RegistrationResource.class);

        // test result is submitted, will be called once only
        router.attach("/result", SubmitResultsResource.class);

        router.attach("/resultQoS", QualityOfServiceResultResource.class);

        // plz is submitted (optional additional resource for browser)
        router.attach("/resultUpdate", ResultUpdateResource.class);

        // ndt test results are submitted (optional, after /result)
        router.attach("/ndtResult", NdtResultResource.class);

        router.attach("/news", NewsResource.class);

        router.attach("/ip", IpResource.class);

        router.attach("/status", StatusResource.class);


        // send history list to client
        router.attach("/history", HistoryResource.class);

        // send brief summary of test results to client
        router.attach("/testresult", TestResultResource.class);

        // send detailed test results to client
        router.attach("/testresultdetail", TestResultDetailResource.class);

        router.attach("/sync", SyncResource.class);

        router.attach("/settings", SettingsResource.class);
        // collection of UserAgent etc.for IE (via server)  
        router.attach("/requestDataCollector", RequestDataCollector.class);

        router.attach("/opentests/O{open_test_uuid}&sender={sender}", OpenTestResource.class);
        router.attach("/opentests/O{open_test_uuid}", OpenTestResource.class);

        router.attach("/qos/O{open_test_uuid}", OpenTestQoSResource.class);
        router.attach("/qos/O{open_test_uuid}/{lang}", OpenTestQoSResource.class);

        router.attach("/qosTestRequest", QoSTestRequestResource.class);
        router.attach("/qosTestResult", QoSResultResource.class);

        // administrative resources (access restrictions might be applied to /admin/ 
        router.attach("/admin/qosObjectives", QualityOfServiceExportResource.class);

        // openapi docs
        router.attach("/api/v1/openapi", OpenAPIUIResource.class);
        router.attach("/api/v1/openapi.yaml", OpenAPISpecResource.class);


        // adds cors headers etc
        DefaultHeadersFilter defaultHeadersFilter = new DefaultHeadersFilter();
        defaultHeadersFilter.setNext(router);

        // configure filter which adds IDs to logs
        LoggingRequestIdFilter loggingRequestIdFilter = new LoggingRequestIdFilter();
        loggingRequestIdFilter.setNext(defaultHeadersFilter);

        return loggingRequestIdFilter;
    }

}
