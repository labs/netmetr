/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.map;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import cz.nic.netmetr.server.map.MapServerOptions.MapFilter;
import cz.nic.netmetr.server.map.MapServerOptions.MapOption;
import cz.nic.netmetr.server.map.MapServerOptions.SQLFilter;
import cz.nic.netmetr.server.map.parameters.TileParameters;
import cz.nic.netmetr.shared.db.DbConnection;
import io.prometheus.client.Counter;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.Form;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public abstract class TileRestlet<Params extends TileParameters> extends Restlet {

    protected static final int[] TILE_SIZES = new int[]{256, 512, 768};
    protected static final byte[][] EMPTY_IMAGES = new byte[TILE_SIZES.length][];
    protected static final byte[] EMPTY_MARKER = "EMPTY".getBytes();
    static final Counter cacheLoads = Counter.build()
            .name("control_map_tile_cache_reloads")
            .help("Number of times the cache regenerated its content.")
            .register();
    static final Counter requests = Counter.build()
            .name("control_map_tile_requests")
            .help("Number of requests for a tile.")
            .register();
    private static final int CACHE_STALE = 3600;
    private static final int CACHE_EXPIRE = 3600 * 24;
    private static final Logger logger = LoggerFactory.getLogger(TileRestlet.class);

    static {
        for (int i = 0; i < TILE_SIZES.length; i++) {
            final BufferedImage img = new BufferedImage(TILE_SIZES[i], TILE_SIZES[i], BufferedImage.TYPE_INT_ARGB);
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                ImageIO.write(img, "png", baos);
            } catch (final IOException e) {
                e.printStackTrace();
            }
            EMPTY_IMAGES[i] = baos.toByteArray();
        }
    }

    protected final MapServerOptions mso;
    @SuppressWarnings("unchecked")
    protected final ThreadLocal<Image>[] images = new ThreadLocal[TILE_SIZES.length];
    private final DbConnection dbConnection;
    private final LoadingCache<CacheKey, byte[]> cache = Caffeine.newBuilder()
            .refreshAfterWrite(CACHE_STALE, TimeUnit.SECONDS)
            .expireAfterWrite(CACHE_EXPIRE, TimeUnit.SECONDS)
            .maximumSize(1_000)
            .buildAsync((CacheKey key) -> {
                cacheLoads.inc();
                var data = generateTile(key.path, key.params, getTileSizeIdx(key.params));
                if (data == null)
                    data = EMPTY_IMAGES[getTileSizeIdx(key.params)];
                return data;
            })
            .synchronous();
    public TileRestlet(DbConnection dbConnection) {
        this.dbConnection = dbConnection;

        for (int i = 0; i < TILE_SIZES.length; i++) {
            final int tileSize = TILE_SIZES[i];
            images[i] = new ThreadLocal<Image>() {
                @Override
                protected Image initialValue() {
                    final Image image = new Image();
                    image.bi = new BufferedImage(tileSize, tileSize, BufferedImage.TYPE_INT_ARGB);
                    image.g = image.bi.createGraphics();
                    image.g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                    image.g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
                    image.g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                    image.g.setStroke(new BasicStroke(1f));
                    image.width = image.bi.getWidth();
                    image.height = image.bi.getHeight();
                    return image;
                }
            };
        }

        mso = MapServerOptions.load(getDbConnection());

    }

    protected static int valueToColor(final int[] colors, final double[] intervals, final double value) {
        int idx = -1;
        for (int i = 0; i < intervals.length; i++)
            if (value < intervals[i]) {
                idx = i;
                break;
            }
        if (idx == 0)
            return colors[0];
        if (idx == -1)
            return colors[colors.length - 1];

        final double factor = (value - intervals[idx - 1]) / (intervals[idx] - intervals[idx - 1]);

        final int c0 = colors[idx - 1];
        final int c1 = colors[idx];

        final int c0r = c0 >> 16;
        final int c0g = c0 >> 8 & 0xff;
        final int c0b = c0 & 0xff;

        final int r = (int) (c0r + ((c1 >> 16) - c0r) * factor);
        final int g = (int) (c0g + ((c1 >> 8 & 0xff) - c0g) * factor);
        final int b = (int) (c0b + ((c1 & 0xff) - c0b) * factor);
        return r << 16 | g << 8 | b;
    }

    protected DbConnection getDbConnection() {
        return dbConnection;
    }

    @Override
    public void handle(final Request req, final Response res) {
        final String zoomStr = (String) req.getAttributes().get("zoom");
        final String xStr = (String) req.getAttributes().get("x");
        final String yStr = (String) req.getAttributes().get("y");

        final TileParameters.Path path = new TileParameters.Path(zoomStr, xStr, yStr, req.getResourceRef().getQueryAsForm().getFirstValue("path", true));
        final Params p = getTileParameters(path, req.getResourceRef().getQueryAsForm());

        requests.inc();
        var key = new CacheKey(p, path);
        var data = cache.get(key);
        res.setEntity(new PngOutputRepresentation(data));
    }

    private int getTileSizeIdx(final Params p) {
        int tileSizeIdx = 0;
        final int size = p.getSize();
        for (int i = 0; i < TILE_SIZES.length; i++) {
            if (size == TILE_SIZES[i]) {
                tileSizeIdx = i;
                break;
            }
        }
        return tileSizeIdx;
    }

    private byte[] generateTile(final TileParameters.Path path, final Params p, int tileSizeIdx) {
        final MapOption mo = mso.getMapOptionMap().get(p.getMapOption());
        if (mo == null)
            throw new IllegalArgumentException();

        final List<SQLFilter> filters = new ArrayList<>(mso.getDefaultMapFilters());
        for (final Map.Entry<String, String> entry : p.getFilterMap().entrySet()) {
            final MapFilter mapFilter = mso.getMapFilterMap().get(entry.getKey());
            if (mapFilter != null) {
                final SQLFilter filter = mapFilter.getFilter(entry.getValue());
                if (filter != null)
                    filters.add(filter);
            }
        }

        final DBox box = GeoCalc.xyToMeters(TILE_SIZES[tileSizeIdx], path.getX(), path.getY(), path.getZoom());

        float quantile = p.getQuantile();
        if (mo.reverseScale)
            quantile = 1 - quantile;

        final byte[] data = generateTile(p, tileSizeIdx, path.getZoom(), box, mo, filters, quantile);
        return data;
    }

    protected abstract Params getTileParameters(TileParameters.Path path, Form params);

    protected abstract byte[] generateTile(Params params, int tileSizeIdx, int zoom, DBox box, MapOption mo,
                                           List<SQLFilter> filters, float quantile);

    protected static class Image {
        protected BufferedImage bi;
        protected Graphics2D g;
        protected int width;
        protected int height;
    }

    static class DPoint {
        double x;
        double y;
    }

    static class DBox {
        double x1;
        double y1;
        double x2;
        double y2;
        double res;
    }

    private class CacheKey {
        final Params params;
        final TileParameters.Path path;

        public CacheKey(Params params, TileParameters.Path path) {
            this.params = params;
            this.path = path;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CacheKey cacheKey = (CacheKey) o;
            return Objects.equals(params, cacheKey.params) &&
                    Objects.equals(path, cacheKey.path);
        }

        @Override
        public int hashCode() {
            return Objects.hash(params, path);
        }
    }
}
