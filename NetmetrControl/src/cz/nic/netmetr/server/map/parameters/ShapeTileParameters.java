/*******************************************************************************
 * Copyright 2015 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.nic.netmetr.server.map.parameters;

import org.restlet.data.Form;

import java.util.Objects;

public class ShapeTileParameters extends TileParameters {
    protected final String shapeType;

    public ShapeTileParameters(Path path, Form params) {
        super(path, params, 0.4);
        shapeType = params.getFirstValue("shapetype", true, "");
    }

    /**
     * Returns argument shapetype.
     * @return never null, default is an empty string
     */
    public String getShapeType() {
        return shapeType;
    }

    @Override
    public boolean isNoCache() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ShapeTileParameters that = (ShapeTileParameters) o;
        return Objects.equals(shapeType, that.shapeType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), shapeType);
    }
}
