/*******************************************************************************
 * Copyright 2015 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.nic.netmetr.server.map.parameters;

import cz.nic.netmetr.server.map.MapServerOptions;
import org.restlet.data.Form;
import org.restlet.data.Parameter;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class TileParameters {
    protected static final Pattern PATH_PATTERN = Pattern.compile("(\\d+)/(\\d+)/(\\d+)");
    protected static final int MAX_ZOOM = 21;

    protected final Path path;
    protected final int size;
    protected final String mapOption;
    protected final float quantile;
    protected final Map<String, String> filterMap;
    protected final double transparency;

    protected TileParameters(Path path, Form params, double defaultTransparency) {
        this.path = path;

        int _size = 0;
        final String sizeStr = params.getFirstValue("size", true);
        if (sizeStr != null) {
            try {
                _size = Integer.parseInt(sizeStr);
            } catch (NumberFormatException ignored) {
            }
        }
        size = _size;

        mapOption = params.getFirstValue("map_options", true, "mobile/download");

        float _quantile = 0.5f; //median is default quantile
        final String statisticalMethod = params.getFirstValue("statistical_method", true);
        if (statisticalMethod != null) {
            try {
                float __quantile = Float.parseFloat(statisticalMethod);
                if (__quantile >= 0f && __quantile <= 1f)
                    _quantile = __quantile;
            } catch (NumberFormatException ignored) {
            }
        }
        quantile = _quantile;

        final String transparencyString = params.getFirstValue("transparency");
        double _transparency = defaultTransparency;
        if (transparencyString != null)
            try {
                _transparency = Double.parseDouble(transparencyString);
            } catch (final NumberFormatException e) {
            }
        if (_transparency < 0)
            _transparency = 0;
        if (_transparency > 1)
            _transparency = 1;
        transparency = _transparency;

        final TreeMap<String, String> _filterMap = new TreeMap<>();

        MapServerOptions mapServerOptions = MapServerOptions.loadCached().orElseThrow(() -> new AssertionError("Options should have been already initialized!"));
        for (final Parameter param : params) {
            if (mapServerOptions.isValidFilter(param.getName()))
                _filterMap.put(param.getName(), param.getValue());
        }
        filterMap = Collections.unmodifiableMap(_filterMap);
    }

    public Path getPath() {
        return path;
    }

    public int getSize() {
        return size;
    }

    public String getMapOption() {
        return mapOption;
    }

    public float getQuantile() {
        return quantile;
    }

    public Map<String, String> getFilterMap() {
        return filterMap;
    }

    public double getTransparency() {
        return transparency;
    }

    public abstract boolean isNoCache();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TileParameters that = (TileParameters) o;
        return size == that.size &&
                Float.compare(that.quantile, quantile) == 0 &&
                Double.compare(that.transparency, transparency) == 0 &&
                Objects.equals(path, that.path) &&
                Objects.equals(mapOption, that.mapOption) &&
                Objects.equals(filterMap, that.filterMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, size, mapOption, quantile, filterMap, transparency);
    }

    public final static class Path {
        protected final int zoom, x, y;

        public Path(String zoomStr, String xStr, String yStr, String path) {
            if (zoomStr != null && xStr != null && yStr != null) {
                zoom = Integer.parseInt(zoomStr);
                x = Integer.parseInt(xStr);
                y = Integer.parseInt(yStr);
            } else {
                if (path == null)
                    throw new IllegalArgumentException();
                final Matcher m = PATH_PATTERN.matcher(path);
                if (!m.matches())
                    throw new IllegalArgumentException();
                zoom = Integer.parseInt(m.group(1));
                x = Integer.parseInt(m.group(2));
                y = Integer.parseInt(m.group(3));
            }
            if (zoom < 0 || zoom > MAX_ZOOM)
                throw new IllegalArgumentException();
            if (x < 0 || y < 0)
                throw new IllegalArgumentException();
            int pow = 1 << zoom;
            if (x >= pow || y >= pow)
                throw new IllegalArgumentException();
        }

        public Path(int zoom, int x, int y) {
            this.zoom = zoom;
            this.x = x;
            this.y = y;
        }

        public int getZoom() {
            return zoom;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Path path = (Path) o;
            return zoom == path.zoom &&
                    x == path.x &&
                    y == path.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(zoom, x, y);
        }
    }
}
