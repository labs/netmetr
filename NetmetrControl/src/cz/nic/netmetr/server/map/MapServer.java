/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.map;

import cz.nic.netmetr.shared.db.DbConnection;
import cz.nic.netmetr.shared.filter.LoggingRequestIdFilter;
import cz.nic.netmetr.shared.restlet.filter.DefaultHeadersFilter;
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class MapServer extends Application {
    private final DbConnection dbConnection;

    public MapServer(DbConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public DbConnection getDbConnection() {
        return dbConnection;
    }

    @Override
    public Restlet createInboundRoot()
    {
        final Router router = new Router(getContext());
        
        router.attach("/version", VersionResource.class);
        
        final PointTiles pointTiles = new PointTiles(getDbConnection());
        router.attach("/tiles/points/{zoom}/{x}/{y}.png", pointTiles);
        router.attach("/tiles/points", pointTiles);
        
        final HeatmapTiles heatmapTiles = new HeatmapTiles(getDbConnection());
        router.attach("/tiles/heatmap/{zoom}/{x}/{y}.png", heatmapTiles);
        router.attach("/tiles/heatmap", heatmapTiles);
        
        final ShapeTiles shapeTiles = new ShapeTiles(getDbConnection());
        router.attach("/tiles/shapes/{zoom}/{x}/{y}.png", shapeTiles);
        router.attach("/tiles/shapes", shapeTiles);
        
        router.attach("/tiles/markers", MarkerResource.class);
        
        router.attach("/tiles/info", InfoResource.class);

        DefaultHeadersFilter defaultHeadersFilter = new DefaultHeadersFilter();
        defaultHeadersFilter.setNext(router);

        // configure filter which adds IDs to logs
        LoggingRequestIdFilter loggingRequestIdFilter = new LoggingRequestIdFilter();
        loggingRequestIdFilter.setNext(defaultHeadersFilter);

        return loggingRequestIdFilter;
    }
}
