/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.server.map;

import cz.nic.netmetr.shared.ResourceManager;
import cz.nic.netmetr.shared.db.DbConnection;
import org.restlet.representation.Representation;
import org.restlet.resource.Options;
import org.restlet.resource.ResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.ResourceBundle;

public class ServerResource extends org.restlet.resource.ServerResource {
    protected ResourceBundle labels;
    protected ResourceBundle settings;
    private Logger logger = LoggerFactory.getLogger(ServerResource.class);

    protected DbConnection getDbConnectionFromApp() {
        return ((MapServer) getApplication()).getDbConnection();
    }

    @Override
    public void doInit() throws ResourceException {
        super.doInit();

        settings = ResourceManager.getCfgBundle();
        // Set default Language for System
        Locale.setDefault(new Locale(settings.getString("RMBT_DEFAULT_LANGUAGE")));
        labels = ResourceManager.getSysMsgBundle();
    }

    @Options
    public void doOptions(final Representation entity) {
    }
}
