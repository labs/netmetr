package cz.nic.netmetr.shared.config;

public class GlobalConfig {
    private static GlobalConfig instance = null;

    public static void init(String rmbtSecretKey) {
        if (instance != null)
            throw new AssertionError("Global config initialization can be called only once!");

        instance = new GlobalConfig(rmbtSecretKey);
    }

    public static GlobalConfig getInstance() {
        if (instance == null)
            throw new AssertionError("Global config must be initialized before use!");

        return instance;
    }

    private GlobalConfig(String rmbtSecretKey) {
        this.rmbtSecretKey = rmbtSecretKey;
    }

    private final String rmbtSecretKey;

    public String getRmbtSecretKey() {
        return rmbtSecretKey;
    }
}
