package cz.nic.netmetr.shared.filter;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.routing.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.UUID;

public class LoggingRequestIdFilter extends Filter {

    private Logger logger = LoggerFactory.getLogger(LoggingRequestIdFilter.class);
    private final static String REQUEST_ID = "request_id";

    @Override
    protected void afterHandle(Request request, Response response) {
        super.afterHandle(request, response);
        // we could remove the REQUEST_ID value here, but we don't do that, because LogService runs after this.
        // and we want it to see the value.
        // so in fact, new request sets new UUID and it stays that way until a new request arrives
    }

    @Override
    protected int beforeHandle(Request request, Response response) {
        MDC.put(REQUEST_ID, UUID.randomUUID().toString());
        return super.beforeHandle(request, response);
    }
}
