/*******************************************************************************
 * Copyright 2016 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.nic.netmetr.shared;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SpeedGraph {
    private ArrayList<SpeedGraphItem> upload = new ArrayList<>();
    private ArrayList<SpeedGraphItem> download = new ArrayList<>();


    private List<SpeedGraphFromDb> getDataFromDb(long testUID, boolean upload, Connection conn) throws SQLException {
        String query = "SELECT thread, bytes, (time/1000/1000) as time_ms FROM unnest((SELECT speed_graph_data FROM test WHERE uid = ?)) WHERE upload = ? ORDER BY time ASC";
        try (PreparedStatement psSpeed = conn.prepareStatement(query)) {
            psSpeed.setLong(1, testUID);
            psSpeed.setBoolean(2, upload);

            try (ResultSet queryResult = psSpeed.executeQuery()) {
                ArrayList<SpeedGraphFromDb> data = new ArrayList<>();

                while (queryResult.next()) {
                    int thread = queryResult.getInt("thread");
                    long ms = queryResult.getLong("time_ms");
                    long bytes = queryResult.getLong("bytes");

                    data.add(new SpeedGraphFromDb(thread, ms, bytes));
                }
                return data;
            }
        }
    }

    /**
     * Transforms bytes transferred from cumulative to amount since last data point. Does so in every thread separately.
     * The list is updated inplace.
     *
     * @param data     List of data points from database
     * @param nThreads number of threads
     */
    private void transformFromCumulative(List<SpeedGraphFromDb> data, int nThreads) {
        // for each thread prepare a list of items
        List<List<SpeedGraphFromDb>> threads = Stream
                .generate((Supplier<ArrayList<SpeedGraphFromDb>>) ArrayList::new)
                .limit(nThreads)
                .collect(Collectors.toList());

        // sort data point into corresponding thread buckets
        data.forEach(sg -> threads.get(sg.getThreadId()).add(sg));

        // for each thread, update transferredBytes to represent bytes transferred since last data point
        for (List<SpeedGraphFromDb> thr : threads) {
            for (int i = thr.size() - 1; i > 0; i--) {
                SpeedGraphFromDb sg = thr.get(i);
                sg.setTransferredBytes(sg.getTransferredBytes() - thr.get(i - 1).getTransferredBytes());
            }
        }

        // because we only copied references from data parameter to other objects,
        // the list is intact with changed data

    }

    /**
     * Load download and upload speed details
     *
     * @param testUID the test uid
     * @param threads the max number of threads used in the test
     * @throws SQLException
     */
    public SpeedGraph(long testUID, int threads, java.sql.Connection conn) throws SQLException {
        // upload
        List<SpeedGraphFromDb> uploadData = getDataFromDb(testUID, true, conn);
        transformFromCumulative(uploadData, threads);

        long bytesAccumulatorUpload = 0;
        long lastMs = -1;
        for (SpeedGraphFromDb sg : uploadData) {
            bytesAccumulatorUpload += sg.getTransferredBytes();
            if (lastMs == sg.getMs())
                this.upload.set(this.upload.size() - 1, new SpeedGraphItem(sg.getMs(), bytesAccumulatorUpload));
            else
                this.upload.add(new SpeedGraphItem(sg.getMs(), bytesAccumulatorUpload));
            lastMs = sg.getMs();
        }

        // download
        List<SpeedGraphFromDb> downloadData = getDataFromDb(testUID, false, conn);
        transformFromCumulative(downloadData, threads);

        long bytesAccumulatorDownload = 0;
        lastMs = -1;
        for (SpeedGraphFromDb sg : downloadData) {
            bytesAccumulatorDownload += sg.getTransferredBytes();
            if (lastMs == sg.getMs())
                this.download.set(this.download.size() - 1, new SpeedGraphItem(sg.getMs(), bytesAccumulatorDownload));
            else
                this.download.add(new SpeedGraphItem(sg.getMs(), bytesAccumulatorDownload));
            lastMs = sg.getMs();
        }
    }

    public ArrayList<SpeedGraphItem> getUpload() {
        return this.upload;
    }

    public ArrayList<SpeedGraphItem> getDownload() {
        return this.download;
    }

    public class SpeedGraphItem {
        private long timeElapsed;
        private long bytesTotal;

        public SpeedGraphItem(long timeElapsed, long bytesTotal) {
            this.timeElapsed = timeElapsed;
            this.bytesTotal = bytesTotal;
        }

        /**
         * @return The time elapsed since the start of the test in millis
         */
        public long getTimeElapsed() {
            return this.timeElapsed;
        }

        /**
         * @return The total bytes transmitted in all threads since the start of the test
         */
        public long getBytesTotal() {
            return this.bytesTotal;
        }
    }

    private static class SpeedGraphFromDb {
        private int threadId;
        private long ms;

        public void setTransferredBytes(long transferredBytes) {
            this.transferredBytes = transferredBytes;
        }

        private long transferredBytes;

        public SpeedGraphFromDb(int threadId, long ms, long transferredBytes) {
            this.threadId = threadId;
            this.ms = ms;
            this.transferredBytes = transferredBytes;
        }

        public int getThreadId() {
            return threadId;
        }

        public long getMs() {
            return ms;
        }

        public long getTransferredBytes() {
            return transferredBytes;
        }
    }

}