/*******************************************************************************
 * Copyright 2016 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.nic.netmetr.shared.reporting;

import cz.nic.netmetr.shared.db.QoSTestResult;
import cz.nic.netmetr.shared.db.model.Test;
import cz.nic.netmetr.shared.qos.VoipResult;
import io.swagger.client.model.ResultRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.util.List;

public class VoipPortsReport implements AdvancedReport {

	public final static class PacketStats {
		public long total = 0;
		public long arrived = 0;
	}

	@Override
	public void addSpeedtestReport(final Test test, final ResultRequest testResultJson, final JSONObject reportJson, Connection connection) throws JSONException {
		//nothing to do with speed-test data
	}

	@Override
	public void addQoSReport(List<QoSTestResult> qosResultList, JSONObject reportJson) throws JSONException {
		final PacketStats outPackets = new PacketStats();
		final PacketStats inPackets = new PacketStats();

		try {
			for (final QoSTestResult result : qosResultList) {
				if (result.getResult() instanceof VoipResult) {
					VoipResult voipResult = (VoipResult) result.getResult();

					// because someone used Object everywhere instead of normal types, functions bellow return
					// just anything but the type you want. That's why I am converting it to String and back.
					// This way, just about any type of number will get converted properly.

					// please FIXME, this is garbage

					outPackets.total += (Long.parseLong(voipResult.getCallDuration().toString()) / Long.parseLong(voipResult.getDelay().toString()));
					outPackets.arrived = Long.parseLong(voipResult.getNumPacketsOut().toString());
					inPackets.total += outPackets.total;
					inPackets.arrived = Long.parseLong(voipResult.getNumPacketsIn().toString());
				}
			}

			reportJson.put("packet_loss_down", (int)(100f * ((float)(inPackets.total - inPackets.arrived) / (float)inPackets.total)));
			reportJson.put("packet_loss_up", (int)(100f * ((float)(outPackets.total - outPackets.arrived) / (float)outPackets.total)));
		}
		catch (final Exception e) {
			e.printStackTrace();
		}
	}
}
