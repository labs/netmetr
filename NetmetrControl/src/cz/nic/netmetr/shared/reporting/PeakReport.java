/*******************************************************************************
 * Copyright 2016 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.nic.netmetr.shared.reporting;

import cz.nic.netmetr.shared.SpeedGraph;
import cz.nic.netmetr.shared.SpeedGraph.SpeedGraphItem;
import cz.nic.netmetr.shared.db.QoSTestResult;
import cz.nic.netmetr.shared.db.model.Test;
import io.swagger.client.model.ResultRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class PeakReport implements AdvancedReport {

	@Override
	public void addSpeedtestReport(final Test test, final ResultRequest testResultJson, final JSONObject reportJson, Connection connection) throws JSONException {
        try {
        	final int numThreads = test.num_threads != null ? test.num_threads : 0;
        	final int numThreadsUl = test.num_threads_ul != null ? test.num_threads_ul : 0;
        	
			SpeedGraph speedGraph = new SpeedGraph(test.uid, Math.max(numThreads, numThreadsUl), connection);
			
			double peak = 0d;
			
			for (final SpeedGraphItem i : speedGraph.getDownload()) {
				peak = Math.max(peak, ((double)i.getBytesTotal() / (double)i.getTimeElapsed()));
			}
			
			reportJson.put("peak_down_kbit", (int)(8 * peak));

			peak = 0d;
			for (final SpeedGraphItem i : speedGraph.getUpload()) {
				peak = Math.max(peak, ((double)i.getBytesTotal() / (double)i.getTimeElapsed()));
			}

			reportJson.put("peak_up_kbit", (int)(8 * peak));
			
		} catch (SQLException e) {
			//some error while generating report = ignore
		}
	}

	@Override
	public void addQoSReport(List<QoSTestResult> qosResultList, JSONObject reportJson) throws JSONException {
		//nothing to do with qos-test data
	}

}
