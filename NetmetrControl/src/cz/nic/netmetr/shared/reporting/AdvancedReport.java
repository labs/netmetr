/*******************************************************************************
 * Copyright 2016 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.nic.netmetr.shared.reporting;

import cz.nic.netmetr.shared.db.QoSTestResult;
import cz.nic.netmetr.shared.db.model.Test;
import io.swagger.client.model.ResultRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.util.List;

public interface AdvancedReport {

	void addSpeedtestReport(final Test test, final ResultRequest testResultJson, final JSONObject reportJson, Connection connection) throws JSONException;
	
	void addQoSReport(final List<QoSTestResult> qosResultList, final JSONObject reportJson) throws JSONException;
}
