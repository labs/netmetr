package cz.nic.netmetr.shared.resources;

import org.apache.commons.io.IOUtils;
import org.restlet.data.MediaType;
import org.restlet.representation.ByteArrayRepresentation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Class for serving static resources from within the war file.
 *
 * This method of serving resources should not be used for anything performance critical. Full-featured web server
 * is much better for that. But for simple and not often used things, this is OK. Main usecase of this is serving content,
 * which is closely coupled to current version of the server. For example API specification (this class was written for
 * that use case).
 */
public class StaticResourceHelper {

    /**
     * Load resource with supplied name by using class loader's build-in resource loading. Wraps it into an Restlet
     * compatible response and returns it.
     *
     * @param caller Object whose class loader will be used to load the resource
     * @param name name of the resource to load
     * @param mimeType MIME type to attach to the response
     * @return Restlet compatible response with the loaded payload and supplied MIME type
     * @throws ResourceUnavailableException when something goes wrong.
     */
    public static ByteArrayRepresentation loadResource(Object caller, String name, MediaType mimeType) throws ResourceUnavailableException {
        InputStream in = caller.getClass().getClassLoader().getResourceAsStream(name);
        if (in == null)
            throw new ResourceUnavailableException();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            IOUtils.copy(in, baos);
        } catch (IOException e) {
            throw new ResourceUnavailableException(e);
        }

        return new ByteArrayRepresentation(baos.toByteArray(), mimeType);
    }

    public static class ResourceUnavailableException extends Exception {
        ResourceUnavailableException() {
            super("The resource, you are trying to load, is unavailable.");
        }

        ResourceUnavailableException(Throwable throwable) {
            super("The resource, you are trying to load, is unavailable.", throwable);
        }

        /**
         * Returns Restlet compatible response that can be used to handle errors in any resource loading method.
         *
         * @return error message in simple HTML encoded into raw UTF-8 bytes
         */
        public ByteArrayRepresentation getSupplementaryResponse() {
            String msg = String.format("<h1>Error</h1><p>%s</p>", this.getMessage());
            return new ByteArrayRepresentation(msg.getBytes(StandardCharsets.UTF_8), MediaType.TEXT_HTML);
        }
    }
}
