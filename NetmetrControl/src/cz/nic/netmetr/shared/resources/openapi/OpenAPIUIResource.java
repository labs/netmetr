package cz.nic.netmetr.shared.resources.openapi;

import org.restlet.data.MediaType;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;

public class OpenAPIUIResource extends org.restlet.resource.ServerResource {

    /**
     * Renders OpenAPI web UI documentation...
     *
     * @return HTML page with OpenAPI docs
     */
    @Get
    public StringRepresentation renderOpenAPIUI(String openapiYamlURL) {
        /*
         * NOTE:
         *
         * This method would likely be better implemented using some template engine. But that's pointless overkill for
         * a server, that does normally nothing like that and whose main purpose is REST API. That's why this method is
         * creating HTML by concatenation of strings. Not pretty, but serves it purpose. If you happen to find a better
         * way, feel free to update it. ;)
         */


        String s = "<!doctype html>" +
                "<html>" +
                "<head>" +
                "<meta charset=\"UTF-8\">" +
                "<title>NetMetr OpenAPI UI</title>" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"//unpkg.com/swagger-ui-dist@3/swagger-ui.css\">" +
                "<script src=\"//unpkg.com/swagger-ui-dist@3.12.1/swagger-ui-standalone-preset.js\"></script>" +
                "<script src=\"//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js\"></script>" +
                "</head>" +
                "<body>" +
                "<div id=\"swagger-ui\"><h1>Requires JS and external resources...</h1></div>" +
                "<script>\n" +
                "    window.onload = function() {\n" +
                "        // Build a system\n" +
                "        const ui = SwaggerUIBundle({\n" +
                "            url: \"openapi.yaml\",\n" +
                "            dom_id: '#swagger-ui',\n" +
                "            deepLinking: true,\n" +
                "            presets: [\n" +
                "                SwaggerUIBundle.presets.apis,\n" +
                "                SwaggerUIStandalonePreset\n" +
                "            ],\n" +
                "            plugins: [\n" +
                "                SwaggerUIBundle.plugins.DownloadUrl\n" +
                "            ],\n" +
                "            layout: \"StandaloneLayout\",\n" +
                "        })\n" +
                "        window.ui = ui\n" +
                "    }\n" +
                "</script>" +
                "</body>" +
                "</html>";
        return new StringRepresentation(s, MediaType.TEXT_HTML);
    }
}
