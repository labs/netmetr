/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package cz.nic.netmetr.shared.db.model;

import cz.nic.netmetr.shared.db.ReflexiveUpdateQueryMapping;
import cz.nic.netmetr.shared.exceptions.ControlServerException;
import cz.nic.netmetr.shared.exceptions.GeneralDatabaseException;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

public class Test {

    private final static String SELECT = "SELECT" +
            " t.*," +
            " ((EXTRACT (EPOCH FROM (t.timestamp - t.time))) * 1000) speed_test_duration, " +
            " pMob.shortname mobile_provider_name," +
            " pSim.shortname network_sim_operator_mcc_mnc_text," +
            " pPro.shortname provider_id_name," +
            " COALESCE(adm.fullname, t.model) model_fullname," +
            " pServ.name server_name" +
            " FROM test t" +
            " LEFT JOIN provider pSim" +
            " ON t.network_sim_operator=pSim.mcc_mnc" +
            " LEFT JOIN provider pPro" +
            " ON t.provider_id=pPro.uid" +
            " LEFT JOIN provider pMob" +
            " ON t.mobile_provider_id=pMob.uid" +
            " LEFT JOIN device_map adm ON adm.codename=t.model" +
            " LEFT JOIN test_server pServ ON t.server_id=pServ.uid";

    // Mapping from database to this object. It's not great, that the database schema forces this structure, but it's
    // as of writing this the simplest way to achieve some improvement. (This is way better that how it was before).
    //
    // Fields have the same name as in the database, transient fields are read-only, they are never saved

    public transient Long uid;
    public UUID uuid;
    public Long client_id;
    public String client_version; //"client_version"),
    public String client_name; //"client_name"),
    public String client_language; //"client_language"),
    public String client_ip_local;
    public String client_ip_local_anonymized;
    public String client_ip_local_type; //"client_local_ip"),
    public String token;
    public Integer server_id;
    public Integer port; // "test_port_remote"
    public Boolean use_ssl;
    public Timestamp time;
    public Timestamp client_time;
    public Integer speed_upload; //"test_speed_upload"), // note the '_test' prefix!
    public Integer speed_download; //"test_speed_download"), // note the '_test' prefix!
    public Long ping_shortest; //"test_ping_shortest"), // note the '_test' prefix!
    public Long ping_median;
    public Double ping_variance;
    public String encryption; //"test_encryption"), // note the '_test' prefix!
    public String client_public_ip;
    public String client_public_ip_anonymized;
    public String plattform; //"plattform"),
    public transient String server_name; //null, true),
    public String os_version; //"os_version"),
    public String api_level; //"api_level"),
    public String device; //"device"),
    public String model; //"model"),
    public transient String model_fullname; //null, true),
    public String product; //"product"),
    public Integer phone_type; //"telephony_phone_type"),
    public Integer data_state; //"telephony_data_state"),
    public String network_country; //"telephony_network_country"),
    public String network_operator;
    public transient String mobile_provider_name; //null, true),
    public String network_operator_name; //"telephony_network_operator_name"),
    public String network_sim_country; //"telephony_network_sim_country"),
    public String network_sim_operator;
    public transient String network_sim_operator_mcc_mnc_text; //null, true),
    public String network_sim_operator_name; //"telephony_network_sim_operator_name"),
    public Integer roaming_type;
    public String wifi_ssid; //"wifi_ssid"),
    public String wifi_bssid; //"wifi_bssid"),
    public Integer wifi_network_id; //"wifi_network_id"),
    public Integer duration;
    public Integer num_threads; //"test_num_threads"),
    public String status;
    public String timezone;
    public Long bytes_download; //"test_bytes_download"),
    public Long bytes_upload; //"test_bytes_upload"),
    public Long nsec_download; //"test_nsec_download"),
    public Long nsec_upload; //"test_nsec_upload"),
    public String server_ip;
    public String source_ip;
    public String source_ip_anonymized;
    public String client_software_version; //"client_software_version"),
    public Double geo_lat; //"geo_lat"),
    public Double geo_long; //"geo_long"),
    public Integer network_type; //"network_type"),
    public Integer signal_strength; // signal strength as RSSI value
    public Integer lte_rsrp;        // signal strength as RSRP value
    public Integer lte_rsrq;        // signal quality as RSRQ value
    public String software_revision;
    public Long client_test_counter;
    public String nat_type;
    public String client_previous_test_status;
    public Long public_ip_asn;
    public String public_ip_rdns;
    public String public_ip_as_name;
    public String country_geoip;
    public String country_location;
    public String country_asn;
    public Long total_bytes_download; //"test_total_bytes_download"),
    public Long total_bytes_upload; //"test_total_bytes_upload"),
    public Integer wifi_link_speed;
    public Boolean network_is_roaming; //"telephony_network_is_roaming"),
    public Integer zip_code; //"zip_code"),
    public Integer zip_code_geo;
    public transient String provider_id_name; //null, true),
    public String geo_provider; //"provider"),
    public Double geo_accuracy; //"accuracy"),
    public UUID open_uuid;
    public UUID open_test_uuid;
    public Long test_if_bytes_download; //"test_if_bytes_download"),
    public Long test_if_bytes_upload; //"test_if_bytes_upload"),
    public Long testdl_if_bytes_download; //"testdl_if_bytes_download"),
    public Long testdl_if_bytes_upload; //"testdl_if_bytes_upload"),
    public Long testul_if_bytes_download; //"testul_if_bytes_download"),
    public Long testul_if_bytes_upload; //"testul_if_bytes_upload"),
    public Long time_dl_ns; //"time_dl_ns"),
    public Long time_ul_ns; //"time_ul_ns"),
    public Integer num_threads_ul; //"num_threads_ul"),
    public transient String data; //null, true),
    public Boolean publish_public_data; //"publish_public_data"),
    public String tag; //"tag"),
    public transient Double speed_test_duration; //null, true),
    public Long adv_spd_option_id;
    public String adv_spd_option_name; //"adv_spd_option_name"),
    public Long adv_spd_up_kbit; //"adv_spd_up_kbit"),
    public Long adv_spd_down_kbit; //"adv_spd_down_kbit"),
    public String additional_report_fields;
    public Integer test_type_id; //"test_type_id")
    public JitterPacketLossStatus jitter_packet_loss_status; // This is an db enum with values NOT_MEASURED (default), OK, ERROR, TIMEOUT. Can't be null.
    public Integer packet_loss_up_packets_total;
    public Integer packet_loss_up_packets_received;
    public Integer packet_loss_down_packets_total;
    public Integer packet_loss_down_packets_received;
    public Long jitter_up_max_ns;
    public Long jitter_up_mean_ns;
    public Long jitter_down_max_ns;
    public Long jitter_down_mean_ns;

    private static Test loadTest(Connection conn, final String query, final Object[] values) throws SQLException {
        QueryRunner runner = new QueryRunner();
        return runner.query(conn, query, new BeanHandler<>(Test.class), values);
    }

    public static Test getTestByUuid(Connection conn, final UUID uuid) throws GeneralDatabaseException {
        try {
            String query = SELECT + " WHERE t.deleted = false AND t.implausible = false AND t.uuid = ?";

            return loadTest(conn, query, new Object[]{uuid});

        } catch (final SQLException e) {
            throw new GeneralDatabaseException("Failed to get test with specified UUID", e);
        }
    }

    public static Test getTestByOpenTestUuid(Connection conn, final UUID openTestUuid) throws GeneralDatabaseException {
        try {
            String query = SELECT + " WHERE t.deleted = false AND t.implausible = false AND t.open_test_uuid = ?";

            return loadTest(conn, query, new Object[]{openTestUuid});

        } catch (final SQLException e) {
            throw new GeneralDatabaseException("Failed to get test with specified UUID", e);
        }
    }

    public static Test getTestByUid(Connection conn, final long uid) throws GeneralDatabaseException {
        try {
            String query = SELECT + " WHERE t.deleted = false AND  t.implausible = false AND t.uid = ?";

            return loadTest(conn, query, new Object[]{uid});

        } catch (final SQLException e) {
            throw new GeneralDatabaseException("Failed to get test with specified UUID", e);
        }
    }

    public String getFormattedOpenTestUUID() {
        return "O" + this.open_test_uuid.toString();
    }

    public String getFormattedOpenUUID() {
        return "P" + this.open_uuid.toString();
    }

    public void storeTestResults(Connection conn, boolean update) throws ControlServerException {
        QueryRunner runner = new QueryRunner();

        // enforce two minute limit (previously it was in the query bellow, but the error message was completely wrong.
        if (Duration.between(Instant.now(), this.time.toInstant()).abs().toSeconds() > 2*60) {
            throw new ControlServerException("Saving an old test is not allowed. Too much time has elapsed since the test has been performed.");
        }

        // prepare the query
        String query = "UPDATE test SET " + ReflexiveUpdateQueryMapping.listFieldsAsSql(this) +
                ", location = ST_TRANSFORM(ST_SetSRID(ST_Point(?, ?), 4326), 900913) WHERE uid = ? " +
                (update ? "" : " AND status = 'STARTED'");


        // prepare the values that will be inserted/updated
        List<Object> values;
        try {
            values = ReflexiveUpdateQueryMapping.listFieldValues(this);
        } catch (IllegalAccessException e) {
            throw new GeneralDatabaseException("Failed to access fields of the Test object", e);
        }
        values.add(this.geo_long);
        values.add(this.geo_lat);
        values.add(this.uid);

        // run the query
        final int updated;
        try {
            updated = runner.update(conn, query, values.toArray());
        } catch (SQLException e) {
            throw new GeneralDatabaseException("Failed to save Test object into database.", e);
        }

        if (updated == 0)
            throw new GeneralDatabaseException("There is no test in database which would map onto the current one. Unable to update data.");
    }

    public Long getUid() {
        return uid;
    }


    /*
     * Getters and setters to be a proper java bean
     */

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Long getClient_id() {
        return client_id;
    }

    public void setClient_id(Long client_id) {
        this.client_id = client_id;
    }

    public String getClient_version() {
        return client_version;
    }

    public void setClient_version(String client_version) {
        this.client_version = client_version;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getClient_language() {
        return client_language;
    }

    public void setClient_language(String client_language) {
        this.client_language = client_language;
    }

    public String getClient_ip_local() {
        return client_ip_local;
    }

    public void setClient_ip_local(String client_ip_local) {
        this.client_ip_local = client_ip_local;
    }

    public String getClient_ip_local_anonymized() {
        return client_ip_local_anonymized;
    }

    public void setClient_ip_local_anonymized(String client_ip_local_anonymized) {
        this.client_ip_local_anonymized = client_ip_local_anonymized;
    }

    public String getClient_ip_local_type() {
        return client_ip_local_type;
    }

    public void setClient_ip_local_type(String client_ip_local_type) {
        this.client_ip_local_type = client_ip_local_type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getServer_id() {
        return server_id;
    }

    public void setServer_id(Integer server_id) {
        this.server_id = server_id;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Boolean getUse_ssl() {
        return use_ssl;
    }

    public void setUse_ssl(Boolean use_ssl) {
        this.use_ssl = use_ssl;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public Timestamp getClient_time() {
        return client_time;
    }

    public void setClient_time(Timestamp client_time) {
        this.client_time = client_time;
    }

    public Integer getSpeed_upload() {
        return speed_upload;
    }

    public void setSpeed_upload(Integer speed_upload) {
        this.speed_upload = speed_upload;
    }

    public Integer getSpeed_download() {
        return speed_download;
    }

    public void setSpeed_download(Integer speed_download) {
        this.speed_download = speed_download;
    }

    public Long getPing_shortest() {
        return ping_shortest;
    }

    public void setPing_shortest(Long ping_shortest) {
        this.ping_shortest = ping_shortest;
    }

    public Long getPing_median() {
        return ping_median;
    }

    public void setPing_median(Long ping_median) {
        this.ping_median = ping_median;
    }

    public Double getPing_variance() {
        return ping_variance;
    }

    public void setPing_variance(Double ping_variance) {
        this.ping_variance = ping_variance;
    }

    public String getEncryption() {
        return encryption;
    }

    public void setEncryption(String encryption) {
        this.encryption = encryption;
    }

    public String getClient_public_ip() {
        return client_public_ip;
    }

    public void setClient_public_ip(String client_public_ip) {
        this.client_public_ip = client_public_ip;
    }

    public String getClient_public_ip_anonymized() {
        return client_public_ip_anonymized;
    }

    public void setClient_public_ip_anonymized(String client_public_ip_anonymized) {
        this.client_public_ip_anonymized = client_public_ip_anonymized;
    }

    public String getPlattform() {
        return plattform;
    }

    public void setPlattform(String plattform) {
        this.plattform = plattform;
    }

    public String getServer_name() {
        return server_name;
    }

    public void setServer_name(String server_name) {
        this.server_name = server_name;
    }

    public String getOs_version() {
        return os_version;
    }

    public void setOs_version(String os_version) {
        this.os_version = os_version;
    }

    public String getApi_level() {
        return api_level;
    }

    public void setApi_level(String api_level) {
        this.api_level = api_level;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel_fullname() {
        return model_fullname;
    }

    public void setModel_fullname(String model_fullname) {
        this.model_fullname = model_fullname;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getPhone_type() {
        return phone_type;
    }

    public void setPhone_type(Integer phone_type) {
        this.phone_type = phone_type;
    }

    public Integer getData_state() {
        return data_state;
    }

    public void setData_state(Integer data_state) {
        this.data_state = data_state;
    }

    public String getNetwork_country() {
        return network_country;
    }

    public void setNetwork_country(String network_country) {
        this.network_country = network_country;
    }

    public String getNetwork_operator() {
        return network_operator;
    }

    public void setNetwork_operator(String network_operator) {
        this.network_operator = network_operator;
    }

    public String getMobile_provider_name() {
        return mobile_provider_name;
    }

    public void setMobile_provider_name(String mobile_provider_name) {
        this.mobile_provider_name = mobile_provider_name;
    }

    public String getNetwork_operator_name() {
        return network_operator_name;
    }

    public void setNetwork_operator_name(String network_operator_name) {
        this.network_operator_name = network_operator_name;
    }

    public String getNetwork_sim_country() {
        return network_sim_country;
    }

    public void setNetwork_sim_country(String network_sim_country) {
        this.network_sim_country = network_sim_country;
    }

    public String getNetwork_sim_operator() {
        return network_sim_operator;
    }

    public void setNetwork_sim_operator(String network_sim_operator) {
        this.network_sim_operator = network_sim_operator;
    }

    public String getNetwork_sim_operator_mcc_mnc_text() {
        return network_sim_operator_mcc_mnc_text;
    }

    public void setNetwork_sim_operator_mcc_mnc_text(String network_sim_operator_mcc_mnc_text) {
        this.network_sim_operator_mcc_mnc_text = network_sim_operator_mcc_mnc_text;
    }

    public String getNetwork_sim_operator_name() {
        return network_sim_operator_name;
    }

    public void setNetwork_sim_operator_name(String network_sim_operator_name) {
        this.network_sim_operator_name = network_sim_operator_name;
    }

    public Integer getRoaming_type() {
        return roaming_type;
    }

    public void setRoaming_type(Integer roaming_type) {
        this.roaming_type = roaming_type;
    }

    public String getWifi_ssid() {
        return wifi_ssid;
    }

    public void setWifi_ssid(String wifi_ssid) {
        this.wifi_ssid = wifi_ssid;
    }

    public String getWifi_bssid() {
        return wifi_bssid;
    }

    public void setWifi_bssid(String wifi_bssid) {
        this.wifi_bssid = wifi_bssid;
    }

    public Integer getWifi_network_id() {
        return wifi_network_id;
    }

    public void setWifi_network_id(Integer wifi_network_id) {
        this.wifi_network_id = wifi_network_id;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getNum_threads() {
        return num_threads;
    }

    public void setNum_threads(Integer num_threads) {
        this.num_threads = num_threads;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Long getBytes_download() {
        return bytes_download;
    }

    public void setBytes_download(Long bytes_download) {
        this.bytes_download = bytes_download;
    }

    public Long getBytes_upload() {
        return bytes_upload;
    }

    public void setBytes_upload(Long bytes_upload) {
        this.bytes_upload = bytes_upload;
    }

    public Long getNsec_download() {
        return nsec_download;
    }

    public void setNsec_download(Long nsec_download) {
        this.nsec_download = nsec_download;
    }

    public Long getNsec_upload() {
        return nsec_upload;
    }

    public void setNsec_upload(Long nsec_upload) {
        this.nsec_upload = nsec_upload;
    }

    public String getServer_ip() {
        return server_ip;
    }

    public void setServer_ip(String server_ip) {
        this.server_ip = server_ip;
    }

    public String getSource_ip() {
        return source_ip;
    }

    public void setSource_ip(String source_ip) {
        this.source_ip = source_ip;
    }

    public String getSource_ip_anonymized() {
        return source_ip_anonymized;
    }

    public void setSource_ip_anonymized(String source_ip_anonymized) {
        this.source_ip_anonymized = source_ip_anonymized;
    }

    public String getClient_software_version() {
        return client_software_version;
    }

    public void setClient_software_version(String client_software_version) {
        this.client_software_version = client_software_version;
    }

    public Double getGeo_lat() {
        return geo_lat;
    }

    public void setGeo_lat(Double geo_lat) {
        this.geo_lat = geo_lat;
    }

    public Double getGeo_long() {
        return geo_long;
    }

    public void setGeo_long(Double geo_long) {
        this.geo_long = geo_long;
    }

    public Integer getNetwork_type() {
        return network_type;
    }

    public void setNetwork_type(Integer network_type) {
        this.network_type = network_type;
    }

    public Integer getSignal_strength() {
        return signal_strength;
    }

    public void setSignal_strength(Integer signal_strength) {
        this.signal_strength = signal_strength;
    }

    public Integer getLte_rsrp() {
        return lte_rsrp;
    }

    public void setLte_rsrp(Integer lte_rsrp) {
        this.lte_rsrp = lte_rsrp;
    }

    public Integer getLte_rsrq() {
        return lte_rsrq;
    }

    public void setLte_rsrq(Integer lte_rsrq) {
        this.lte_rsrq = lte_rsrq;
    }

    public String getSoftware_revision() {
        return software_revision;
    }

    public void setSoftware_revision(String software_revision) {
        this.software_revision = software_revision;
    }

    public Long getClient_test_counter() {
        return client_test_counter;
    }

    public void setClient_test_counter(Long client_test_counter) {
        this.client_test_counter = client_test_counter;
    }

    public String getNat_type() {
        return nat_type;
    }

    public void setNat_type(String nat_type) {
        this.nat_type = nat_type;
    }

    public String getClient_previous_test_status() {
        return client_previous_test_status;
    }

    public void setClient_previous_test_status(String client_previous_test_status) {
        this.client_previous_test_status = client_previous_test_status;
    }

    public Long getPublic_ip_asn() {
        return public_ip_asn;
    }

    public void setPublic_ip_asn(Long public_ip_asn) {
        this.public_ip_asn = public_ip_asn;
    }

    public String getPublic_ip_rdns() {
        return public_ip_rdns;
    }

    public void setPublic_ip_rdns(String public_ip_rdns) {
        this.public_ip_rdns = public_ip_rdns;
    }

    public String getPublic_ip_as_name() {
        return public_ip_as_name;
    }

    public void setPublic_ip_as_name(String public_ip_as_name) {
        this.public_ip_as_name = public_ip_as_name;
    }

    public String getCountry_geoip() {
        return country_geoip;
    }

    public void setCountry_geoip(String country_geoip) {
        this.country_geoip = country_geoip;
    }

    public String getCountry_location() {
        return country_location;
    }

    public void setCountry_location(String country_location) {
        this.country_location = country_location;
    }

    public String getCountry_asn() {
        return country_asn;
    }

    public void setCountry_asn(String country_asn) {
        this.country_asn = country_asn;
    }

    public Long getTotal_bytes_download() {
        return total_bytes_download;
    }

    public void setTotal_bytes_download(Long total_bytes_download) {
        this.total_bytes_download = total_bytes_download;
    }

    public Long getTotal_bytes_upload() {
        return total_bytes_upload;
    }

    public void setTotal_bytes_upload(Long total_bytes_upload) {
        this.total_bytes_upload = total_bytes_upload;
    }

    public Integer getWifi_link_speed() {
        return wifi_link_speed;
    }

    public void setWifi_link_speed(Integer wifi_link_speed) {
        this.wifi_link_speed = wifi_link_speed;
    }

    public Boolean getNetwork_is_roaming() {
        return network_is_roaming;
    }

    public void setNetwork_is_roaming(Boolean network_is_roaming) {
        this.network_is_roaming = network_is_roaming;
    }

    public Integer getZip_code() {
        return zip_code;
    }

    public void setZip_code(Integer zip_code) {
        this.zip_code = zip_code;
    }

    public Integer getZip_code_geo() {
        return zip_code_geo;
    }

    public void setZip_code_geo(Integer zip_code_geo) {
        this.zip_code_geo = zip_code_geo;
    }

    public String getProvider_id_name() {
        return provider_id_name;
    }

    public void setProvider_id_name(String provider_id_name) {
        this.provider_id_name = provider_id_name;
    }

    public String getGeo_provider() {
        return geo_provider;
    }

    public void setGeo_provider(String geo_provider) {
        this.geo_provider = geo_provider;
    }

    public Double getGeo_accuracy() {
        return geo_accuracy;
    }

    public void setGeo_accuracy(Double geo_accuracy) {
        this.geo_accuracy = geo_accuracy;
    }

    public UUID getOpen_uuid() {
        return open_uuid;
    }

    public void setOpen_uuid(UUID open_uuid) {
        this.open_uuid = open_uuid;
    }

    public UUID getOpen_test_uuid() {
        return open_test_uuid;
    }

    public void setOpen_test_uuid(UUID open_test_uuid) {
        this.open_test_uuid = open_test_uuid;
    }

    public Long getTest_if_bytes_download() {
        return test_if_bytes_download;
    }

    public void setTest_if_bytes_download(Long test_if_bytes_download) {
        this.test_if_bytes_download = test_if_bytes_download;
    }

    public Long getTest_if_bytes_upload() {
        return test_if_bytes_upload;
    }

    public void setTest_if_bytes_upload(Long test_if_bytes_upload) {
        this.test_if_bytes_upload = test_if_bytes_upload;
    }

    public Long getTestdl_if_bytes_download() {
        return testdl_if_bytes_download;
    }

    public void setTestdl_if_bytes_download(Long testdl_if_bytes_download) {
        this.testdl_if_bytes_download = testdl_if_bytes_download;
    }

    public Long getTestdl_if_bytes_upload() {
        return testdl_if_bytes_upload;
    }

    public void setTestdl_if_bytes_upload(Long testdl_if_bytes_upload) {
        this.testdl_if_bytes_upload = testdl_if_bytes_upload;
    }

    public Long getTestul_if_bytes_download() {
        return testul_if_bytes_download;
    }

    public void setTestul_if_bytes_download(Long testul_if_bytes_download) {
        this.testul_if_bytes_download = testul_if_bytes_download;
    }

    public Long getTestul_if_bytes_upload() {
        return testul_if_bytes_upload;
    }

    public void setTestul_if_bytes_upload(Long testul_if_bytes_upload) {
        this.testul_if_bytes_upload = testul_if_bytes_upload;
    }

    public Long getTime_dl_ns() {
        return time_dl_ns;
    }

    public void setTime_dl_ns(Long time_dl_ns) {
        this.time_dl_ns = time_dl_ns;
    }

    public Long getTime_ul_ns() {
        return time_ul_ns;
    }

    public void setTime_ul_ns(Long time_ul_ns) {
        this.time_ul_ns = time_ul_ns;
    }

    public Integer getNum_threads_ul() {
        return num_threads_ul;
    }

    public void setNum_threads_ul(Integer num_threads_ul) {
        this.num_threads_ul = num_threads_ul;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Boolean getPublish_public_data() {
        return publish_public_data;
    }

    public void setPublish_public_data(Boolean publish_public_data) {
        this.publish_public_data = publish_public_data;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Double getSpeed_test_duration() {
        return speed_test_duration;
    }

    public void setSpeed_test_duration(Double speed_test_duration) {
        this.speed_test_duration = speed_test_duration;
    }

    public Long getAdv_spd_option_id() {
        return adv_spd_option_id;
    }

    public void setAdv_spd_option_id(Long adv_spd_option_id) {
        this.adv_spd_option_id = adv_spd_option_id;
    }

    public String getAdv_spd_option_name() {
        return adv_spd_option_name;
    }

    public void setAdv_spd_option_name(String adv_spd_option_name) {
        this.adv_spd_option_name = adv_spd_option_name;
    }

    public Long getAdv_spd_up_kbit() {
        return adv_spd_up_kbit;
    }

    public void setAdv_spd_up_kbit(Long adv_spd_up_kbit) {
        this.adv_spd_up_kbit = adv_spd_up_kbit;
    }

    public Long getAdv_spd_down_kbit() {
        return adv_spd_down_kbit;
    }

    public void setAdv_spd_down_kbit(Long adv_spd_down_kbit) {
        this.adv_spd_down_kbit = adv_spd_down_kbit;
    }

    public String getAdditional_report_fields() {
        return additional_report_fields;
    }

    public void setAdditional_report_fields(String additional_report_fields) {
        this.additional_report_fields = additional_report_fields;
    }

    public Integer getTest_type_id() {
        return test_type_id;
    }

    public void setTest_type_id(Integer test_type_id) {
        this.test_type_id = test_type_id;
    }

    public JitterPacketLossStatus getJitter_packet_loss_status() {
        return jitter_packet_loss_status;
    }

    public void setJitter_packet_loss_status(JitterPacketLossStatus jitter_packet_loss_status) {
        this.jitter_packet_loss_status = jitter_packet_loss_status;
    }

    public Integer getPacket_loss_up_packets_total() {
        return packet_loss_up_packets_total;
    }

    public void setPacket_loss_up_packets_total(Integer packet_loss_up_packets_total) {
        this.packet_loss_up_packets_total = packet_loss_up_packets_total;
    }

    public Integer getPacket_loss_up_packets_received() {
        return packet_loss_up_packets_received;
    }

    public void setPacket_loss_up_packets_received(Integer packet_loss_up_packets_received) {
        this.packet_loss_up_packets_received = packet_loss_up_packets_received;
    }

    public Integer getPacket_loss_down_packets_total() {
        return packet_loss_down_packets_total;
    }

    public void setPacket_loss_down_packets_total(Integer packet_loss_down_packets_total) {
        this.packet_loss_down_packets_total = packet_loss_down_packets_total;
    }

    public Integer getPacket_loss_down_packets_received() {
        return packet_loss_down_packets_received;
    }

    public void setPacket_loss_down_packets_received(Integer packet_loss_down_packets_received) {
        this.packet_loss_down_packets_received = packet_loss_down_packets_received;
    }

    public Long getJitter_up_max_ns() {
        return jitter_up_max_ns;
    }

    public void setJitter_up_max_ns(Long jitter_up_max_ns) {
        this.jitter_up_max_ns = jitter_up_max_ns;
    }

    public Long getJitter_up_mean_ns() {
        return jitter_up_mean_ns;
    }

    public void setJitter_up_mean_ns(Long jitter_up_mean_ns) {
        this.jitter_up_mean_ns = jitter_up_mean_ns;
    }

    public Long getJitter_down_max_ns() {
        return jitter_down_max_ns;
    }

    public void setJitter_down_max_ns(Long jitter_down_max_ns) {
        this.jitter_down_max_ns = jitter_down_max_ns;
    }

    public Long getJitter_down_mean_ns() {
        return jitter_down_mean_ns;
    }

    public void setJitter_down_mean_ns(Long jitter_down_mean_ns) {
        this.jitter_down_mean_ns = jitter_down_mean_ns;
    }

    public boolean isJitterPacketLossTestOK() {
        return getJitter_packet_loss_status() == JitterPacketLossStatus.OK;
    }

    public boolean isJitterResultPresentable() {
        return ! (jitter_down_max_ns == null || jitter_down_mean_ns == null || jitter_up_max_ns == null || jitter_up_mean_ns == null);
    }

    public boolean isJitterPacketLossTestError() {
        return getJitter_packet_loss_status() == JitterPacketLossStatus.ERROR;
    }

    /**
     * Method used for calculating results to display to users. Returns number with 2 digit precision.
     * @return packet loss of this test in percent
     */
    public double getAggregatePacketLossPercent() {
        int packetLossMeanPercent = (int) Math.round((1000d * (this.packet_loss_up_packets_received + this.packet_loss_down_packets_received)) /
                ((double) (this.packet_loss_up_packets_total + this.packet_loss_down_packets_total)));  // percent * 1000
        return 100d - (packetLossMeanPercent / 10d);  // percentage of packet lost, not received :) That's the reason for the inversion
    }

    public float getPacketLossPercentDownlink() {
        int packetLossPercent = Math.round((1000f * this.packet_loss_down_packets_received) / (float)this.packet_loss_down_packets_total);
        return 100f - (packetLossPercent / 10f);
    }

    public float getPacketLossPercentUplink() {
        int packetLossPercent = Math.round((1000f * this.packet_loss_up_packets_received) / (float)this.packet_loss_up_packets_total);
        return 100f - (packetLossPercent / 10f);
    }

    /**
     * Method used for calculating results to display to users. Returns number with 2 digit precision.
     * @return mean jitter in ms
     */
    public double getAggregateMeanJitterMs() {
        long jitterMean = Math.round((double) (this.jitter_up_mean_ns + this.jitter_down_mean_ns) / 2d / 10_000d);
        return (double) jitterMean / 100d;
    }

    public float getMeanJitterMsDown() {
        int jitter = (int) Math.round((double)this.jitter_down_mean_ns / 10_000d);
        return (float) jitter / 100f;
    }

    public float getMeanJitterMsUp() {
        int jitter = (int) Math.round((double)this.jitter_up_mean_ns / 10_000d);
        return (float) jitter / 100f;
    }

    /**
     * Enum for representing jitter packet loss states. Must be named in the same manner as in the database, because the
     * enum names are converted to snake case when using {@link ReflexiveUpdateQueryMapping}
     */
    public enum JitterPacketLossStatus {
        OK,
        ERROR,
        NOT_MEASURED;
    }
}
