package cz.nic.netmetr.shared.db;

import com.google.common.base.CaseFormat;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class generates SQL from Java beans that can be used with UPDATE SQL query.
 */
public class ReflexiveUpdateQueryMapping {

    private static Stream<Field> streamFields(Object o) {
        return Arrays.stream(o.getClass().getDeclaredFields())
                .sorted(Comparator.comparing(Field::getName))
                .filter(field -> Modifier.isPublic(field.getModifiers()))
                .filter(field -> !Modifier.isTransient(field.getModifiers()));
    }

    public static String listFieldsAsSql(Object o) {
        StringBuilder sb = new StringBuilder();
        return streamFields(o)
                .map(field -> {
                    if (field.getType().isEnum())
                        return field.getName() + "=?::" + CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, field.getType().getSimpleName());
                    else
                        return field.getName() + "=?";

                })
                .collect(Collectors.joining(", "));
    }

    public static List<Object> listFieldValues(Object o) throws IllegalAccessException {
        try {   // ugly hack to propagate exception out of the stream

            return streamFields(o)
                    .map(field -> {
                        try {
                            if (field.getType().isEnum())
                                return field.get(o).toString();
                            else
                                return field.get(o);
                        } catch (IllegalAccessException e) {
                            throw new RuntimeException(e);
                        }
                    }).collect(Collectors.toList());

        } catch (RuntimeException e) {
            if (e.getCause() instanceof IllegalAccessException)
                throw (IllegalAccessException) e.getCause();
            throw e;
        }

    }
}
