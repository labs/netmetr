package cz.nic.netmetr.shared.utils;

public class ObjectHelpers {
    public static <T> T  nonNull(T object, T defaul) {
        return object == null ? defaul : object;
    }
}
