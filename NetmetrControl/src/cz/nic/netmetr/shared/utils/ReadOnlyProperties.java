package cz.nic.netmetr.shared.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public final class ReadOnlyProperties {
    private static Map<String, ReadOnlyProperties> propertiesCache = new ConcurrentHashMap<>();

    private final Properties props;

    private ReadOnlyProperties(Properties props) {
        this.props = props;
    }

    public String get(String propertyName) {
        return props.getProperty(propertyName);
    }

    public String getOrDefault(String propertyName, String defaul) {
        String res = get(propertyName);
        return res == null ? defaul : res;
    }

    public static ReadOnlyProperties from(String filename) {
        if (!propertiesCache.containsKey(filename)) {
            synchronized (ReadOnlyProperties.class) {
                try {
                    if (!propertiesCache.containsKey(filename)) {
                        Properties props = new Properties();
                        props.load(new FileInputStream(filename));
                        propertiesCache.put(filename, new ReadOnlyProperties(props));
                    }
                } catch (IOException e) {
                    throw new RuntimeException("Tried to read from non-existent properties file - \"" + new File(filename).getAbsolutePath() + "\".");
                }
            }
        }

        return propertiesCache.get(filename);
    }
}
