package cz.nic.netmetr.shared.utils;

import java.util.function.Consumer;

public class FuncTools {

    public static <T> void doIfNotNull(T obj, Consumer<T> consumer) {
        if (obj != null) consumer.accept(obj);
    }
}
