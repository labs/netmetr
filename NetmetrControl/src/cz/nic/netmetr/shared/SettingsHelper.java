/*******************************************************************************
 * Copyright 2015 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.nic.netmetr.shared;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import cz.nic.netmetr.shared.db.DbConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.util.Optional;

public class SettingsHelper {
    static Logger logger = LoggerFactory.getLogger(SettingsHelper.class);

    // we are caching even that a value is not present...
    static Cache<String, Optional<String>> cache = Caffeine.newBuilder()
            .maximumSize(1000000)
            .expireAfterWrite(Duration.ofHours(1))
            .build();

    private static Optional<String> getSettingNoCache(DbConnection dbConn, String key, String defaultLang) {
        if (dbConn == null)
            throw new RuntimeException("No database connection!");

        String query = "SELECT value"
                + " FROM settings"
                + " WHERE key=? AND (lang IS NULL OR lang = '' OR lang = ?)"
                + " ORDER BY lang DESC NULLS LAST LIMIT 1";

        try (Connection conn = dbConn.getConnection();
             final PreparedStatement st = conn.prepareStatement(query)) {

            st.setString(1, key);
            st.setString(2, defaultLang);

            try (final ResultSet rs = st.executeQuery()) {
                // obtain the value if it is present
                if (rs != null && rs.next())
                    return Optional.of(rs.getString("value"));

                    // try a fallback to the default english locale
                else if (!"en".equals(defaultLang)) {
                    return getSettingNoCache(dbConn, key, "en");
                }
            }

            return Optional.empty();
        } catch (SQLException throwables) {
            throw new RuntimeException(throwables);
        }
    }

    public static String getSetting(DbConnection conn, String key) {
        return getSetting(conn, key, null);
    }

    public static String getSetting(DbConnection dbConn, String key, String defaultLang) {
        try {
            return cache.get(key, (_key) -> getSettingNoCache(dbConn, key, defaultLang)).orElse(null);
        } catch (RuntimeException e) {
            logger.warn("Failed to read settings", e);
            return null;
        }
    }
}
