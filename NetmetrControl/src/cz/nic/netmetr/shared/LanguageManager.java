package cz.nic.netmetr.shared;


import java.util.Locale;

public class LanguageManager {

    public static String resolve(Locale locale, String messageCode) {
        return ResourceManager.getSysMsgBundle(locale).getString(messageCode);
    }

    public static String resolve(String locale, String messageCode) {
        return resolve(new Locale(locale), messageCode);
    }

    public static String resolve(String messageCode) {
        return resolve(Locale.getDefault(), messageCode);
    }
}
