package cz.nic.netmetr.shared.restlet.filter;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Method;
import org.restlet.routing.Filter;

import java.util.Set;

public class DefaultHeadersFilter extends Filter {
    @Override
    protected int beforeHandle(Request request, Response response) {
        response.setAccessControlAllowOrigin("*");

        Set<Method> allowedMethods = response.getAllowedMethods();
        allowedMethods.clear();
        allowedMethods.add(Method.GET);
        allowedMethods.add(Method.POST);
        allowedMethods.add(Method.OPTIONS);

        response.getAccessControlAllowHeaders().clear();
        response.getAccessControlAllowHeaders().add("Content-Type");

        response.setAccessControlAllowCredentials(false);

        response.setAccessControlMaxAge(60);

        return super.beforeHandle(request, response);
    }
}
