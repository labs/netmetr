package cz.nic.netmetr.shared.exceptions;

import cz.nic.netmetr.shared.LanguageManager;

public class InvalidRequestException extends ControlServerException {
    public InvalidRequestException() {
        super();
    }

    public InvalidRequestException(String s) {
        super(s);
    }

    public InvalidRequestException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public InvalidRequestException(Throwable throwable) {
        super(throwable);
    }

    protected InvalidRequestException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

    public static InvalidRequestException fromMessageCode(String code) {
        return new InvalidRequestException(LanguageManager.resolve(code));
    }
}
