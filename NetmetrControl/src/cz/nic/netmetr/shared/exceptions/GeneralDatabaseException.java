package cz.nic.netmetr.shared.exceptions;

import cz.nic.netmetr.shared.LanguageManager;

public class GeneralDatabaseException extends ControlServerException{
    public GeneralDatabaseException() {
        super(LanguageManager.resolve("ERROR_DB_STORE_GENERAL"));
    }

    public GeneralDatabaseException(String s) {
        super(s);
    }

    public GeneralDatabaseException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public GeneralDatabaseException(Throwable throwable) {
        super(LanguageManager.resolve("ERROR_DB_STORE_GENERAL"), throwable);
    }

    protected GeneralDatabaseException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
