package cz.nic.netmetr.shared.exceptions;

import cz.nic.netmetr.shared.LanguageManager;

public class ControlServerException extends Exception{
    public ControlServerException() {
        super();
    }

    public ControlServerException(String s) {
        super(s);
    }

    public ControlServerException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ControlServerException(Throwable throwable) {
        super(throwable);
    }

    protected ControlServerException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

    public static ControlServerException fromMessageCode(String code) {
        return new ControlServerException(LanguageManager.resolve(code));
    }

    public static ControlServerException fromMessageCode(String code, Throwable t) {
        return new ControlServerException(LanguageManager.resolve(code), t);
    }
}
