package cz.nic.netmetr.shared.exceptions;

import cz.nic.netmetr.shared.LanguageManager;

public class DatabaseConnectionException extends GeneralDatabaseException{
    public DatabaseConnectionException() {
        super(LanguageManager.resolve("ERROR_DB_CONNECTION"));
    }
}
