/*******************************************************************************
 * Copyright 2013-2014 alladin-IT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package cz.nic.netmetr.boot;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import cz.nic.netmetr.boot.db.MeasuredConnection;
import cz.nic.netmetr.shared.utils.ReadOnlyProperties;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class DbConnection implements cz.nic.netmetr.shared.db.DbConnection {

    private static final String DATABASE_URL = "server.db.url";
    private static final String DATABASE_USER = "server.db.username";
    private static final String DATABASE_PASSWORD = "server.db.password";
    private static final String DATABASE_POOL_SIZE = "server.db.pool_size";
    private static final String DATABASE_CONNECTION_TIMEOUT = "server.db.connection_timeout_ms";

    private static final HikariDataSource dataSource;

    private static DbConnection instance = null;

    public static DbConnection getInstance() {
        if (instance == null) {
            synchronized (DbConnection.class) {
                if (instance == null) {
                    instance = new DbConnection();
                }
            }
        }

        return instance;
    }

    static {
        ReadOnlyProperties props = ReadOnlyProperties.from("config.properties");

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(props.get(DATABASE_URL));
        config.setUsername(props.get(DATABASE_USER));
        config.setPassword(props.get(DATABASE_PASSWORD));

        config.setMaximumPoolSize(Integer.parseInt(props.getOrDefault(DATABASE_POOL_SIZE, "20")));
        config.setConnectionTimeout(Long.parseLong(props.getOrDefault(DATABASE_CONNECTION_TIMEOUT, "1000"))); // aggressively drop requests for database connections when no are available
        config.setLeakDetectionThreshold(30_000); // try to detect connection leaks

        LoggerFactory.getLogger(DbConnection.class).info("Initializing DB connection pool of size={} and connection_timeout_ms={}", config.getMaximumPoolSize(), config.getConnectionTimeout());

        dataSource = new HikariDataSource(config);
    }


    public Connection getConnection() throws SQLException {
        final Connection connection = dataSource.getConnection();
        connection.setAutoCommit(true);

        return new MeasuredConnection(connection);
    }
}
