package cz.nic.netmetr.boot;

import cz.nic.netmetr.server.control.ControlServer;
import cz.nic.netmetr.server.map.MapServer;
import cz.nic.netmetr.server.statistic.StatisticServer;
import org.restlet.Component;

public class NetmetrComponent extends Component {
    public NetmetrComponent() {
        this.getDefaultHost().attach("/RMBTControlServer", new ControlServer(DbConnection.getInstance()));
        this.getDefaultHost().attach("/RMBTMapServer", new MapServer(DbConnection.getInstance()));
        this.getDefaultHost().attach("/RMBTStatisticServer", new StatisticServer(DbConnection.getInstance()));

        this.setLogService(new NetmetrLogService());
    }
}
