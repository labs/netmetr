package cz.nic.netmetr.boot.migrations;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.SQLException;

public class AddBasicGroupNetworkTypes implements Migration {
    @Override
    public boolean shouldRun(Connection connection) throws SQLException {
        // test, whether 2G UID == 11 is present in the table

        QueryRunner runner = new QueryRunner();
        Integer uid2G = runner.query(
                connection,
                "SELECT uid FROM network_type where uid = 111",
                new ScalarHandler<>()
        );

        // could be as well done with not null check, but this is stronger and works as well
        return !(Integer.valueOf(111).equals(uid2G));
    }

    @Override
    public void run(Connection connection) throws SQLException {
        try (var statement = connection.createStatement()) {
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (111, '2G', '2G', '{2G}', 'MOBILE', 0)");
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (112, '3G', '3G', '{3G}', 'MOBILE', 0)");
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (113, '4G', '4G', '{4G}', 'MOBILE', 0)");
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (114, '5G', '5G', '{5G}', 'MOBILE', 0)");
        }
    }
}
