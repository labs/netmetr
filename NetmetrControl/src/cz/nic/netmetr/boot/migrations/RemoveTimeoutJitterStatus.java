package cz.nic.netmetr.boot.migrations;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.SQLException;

public class RemoveTimeoutJitterStatus implements Migration {

    @Override
    public boolean shouldRun(Connection connection) throws SQLException {
        String query = "SELECT 'TIMEOUT' = any(enum_range(null::jitter_packet_loss_status)::text[]) as q";
        QueryRunner runner = new QueryRunner();
        Boolean res = runner.query(connection, query, new ScalarHandler<>());

        return Boolean.TRUE == res;
    }

    @Override
    public void run(Connection connection) throws SQLException {
        /*
        Removing value from enum:
        Source: https://blog.yo1.dog/updating-enum-values-in-postgresql-the-safe-and-easy-way/

        # rename the existing type
        ALTER TYPE status_enum RENAME TO status_enum_old;

        # create the new type
        CREATE TYPE status_enum AS ENUM('queued', 'running', 'done');

        # update the columns to use the new type
        ALTER TABLE job ALTER COLUMN job_status TYPE status_enum USING job_status::text::status_enum;
        # if you get an error, see bottom of post

        # remove the old type
        DROP TYPE status_enum_old;
         */

        try (var statement = connection.createStatement()) {
            statement.execute("ALTER TYPE jitter_packet_loss_status RENAME TO jitter_packet_loss_status_old");
            statement.execute("CREATE TYPE jitter_packet_loss_status AS ENUM ('OK', 'ERROR', 'NOT_MEASURED')");
            statement.executeUpdate("DELETE FROM test WHERE jitter_packet_loss_status = 'TIMEOUT'::jitter_packet_loss_status_old");
            statement.executeUpdate("ALTER TABLE test ALTER COLUMN jitter_packet_loss_status DROP DEFAULT");
            statement.execute("ALTER TABLE test ALTER COLUMN jitter_packet_loss_status TYPE jitter_packet_loss_status USING jitter_packet_loss_status::text::jitter_packet_loss_status");
            statement.executeUpdate("ALTER TABLE test ALTER COLUMN jitter_packet_loss_status SET DEFAULT 'NOT_MEASURED'::jitter_packet_loss_status");
            statement.execute("DROP TYPE jitter_packet_loss_status_old");
        }
    }
}
