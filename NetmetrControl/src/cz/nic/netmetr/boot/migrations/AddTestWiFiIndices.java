package cz.nic.netmetr.boot.migrations;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.SQLException;

public class AddTestWiFiIndices implements Migration {
    @Override
    public boolean shouldRun(Connection connection) throws SQLException {
        QueryRunner runner = new QueryRunner();
        String indexName = runner.query(
                connection,
                "SELECT indexname FROM pg_indexes where tablename = 'test' and indexname = 'test_ssid_idx'",
                new ScalarHandler<>()
        );

        // could be as well done with not null check, but this is stronger and works as well
        return !("test_ssid_idx".equals(indexName));
    }

    @Override
    public void run(Connection connection) throws SQLException {
        try (var statement = connection.createStatement()) {
            String sql = "CREATE INDEX test_ssid_idx ON test USING btree (wifi_ssid)";
            statement.executeUpdate(sql);
        }

        try (var statement = connection.createStatement()) {
            String sql = "CREATE INDEX test_bssid_idx ON test USING btree (wifi_bssid)";
            statement.executeUpdate(sql);
        }

    }
}
