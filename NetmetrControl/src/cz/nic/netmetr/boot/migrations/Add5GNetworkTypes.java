package cz.nic.netmetr.boot.migrations;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.SQLException;

public class Add5GNetworkTypes implements Migration {
    @Override
    public boolean shouldRun(Connection connection) throws SQLException {
        // test, whether 5G UID == 20 is present in the table

        QueryRunner runner = new QueryRunner();
        Integer uid5G = runner.query(
                connection,
                "SELECT uid FROM network_type where uid = 20",
                new ScalarHandler<>()
        );

        // could be as well done with not null check, but this is stronger and works as well
        return !(Integer.valueOf(20).equals(uid5G));
    }

    @Override
    public void run(Connection connection) throws SQLException {
        try (var statement = connection.createStatement()) {
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (19, 'LTE CA', '4G', NULL, 'MOBILE', 41000)");
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (50, 'LTE Advanced', '4G', NULL, 'MOBILE', 42000)");
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (51, 'LTE Advanced Pro', '4G', NULL, 'MOBILE', 43000)");

            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (20, '5G NR SA', '5G', NULL, 'MOBILE', 51000)");
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (21, '5G NR NSA', '5G', NULL, 'MOBILE', 50000)");
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (22, '5G NR NSA mmWave', '5G', NULL, 'MOBILE', 52000)");
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (23, '5G NR SA mmWave', '5G', NULL, 'MOBILE', 53000)");

            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (108, '2G/3G/4G/5G', '2G/3G/4G/5G', '{2G,3G,4G,5G}', 'MOBILE', 0)");
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (109, '3G/4G/5G', '3G/4G/5G', '{3G,4G,5G}', 'MOBILE', 0)");
            statement.executeUpdate("INSERT INTO network_type(uid, name, group_name, aggregate, type, technology_order) VALUES (110, '4G/5G', '4G/5G', '{4G,5G}', 'MOBILE', 0)");
        }
    }
}
