package cz.nic.netmetr.boot.migrations;


import cz.nic.netmetr.shared.db.DbConnection;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

public interface Migration {
    /**
     * List of migrations that will be run. Order from top to bottom.
     */
    List<? extends Migration> MIGRATIONS = List.of(
            new AddJitterPacketLossColumns(),
            new RemoveJitterMinColumns(),
            new RemoveTimeoutJitterStatus(),
            new AddTestWiFiIndices(),
            new Add5GNetworkTypes(),
            new AddBasicGroupNetworkTypes()
    );

    /**
     * Run all migrations from MIGRATIONS list in a single transaction, skipping the ones that are not necessary.
     *
     * @param connection Database connection to use.
     * @throws SQLException
     */
    static void runMigrations(DbConnection connection) throws SQLException {
        Logger logger = LoggerFactory.getLogger(Migration.class);
        logger.info("Checking for migrations");


        try (Connection conn = connection.getConnection()) {
            conn.setAutoCommit(false);

            for (var migration : MIGRATIONS) {
                if (migration.shouldRun(conn)) {
                    logger.info("Running migration " + migration.getClass().getName());
                    var start = Instant.now();
                    migration.run(conn);
                    var difference = Duration.between(start, Instant.now());
                    logger.info("Migration completed in {} ms", difference.toMillis());
                }
            }


            conn.commit();
            DbUtils.close(conn);
            logger.info("Migrations successfully completed.");
        }
    }

    /**
     * <p>Helper method to determine, if a specified column is present in specified table. Is dependent on PostgreSQL.</p>
     *
     * <p><b>WARNING:</b> table name and column names are not sanitized against SQL injection. Do not use with user provided input. Use only with string literals.</p>
     *
     * @param connection Database connection to use. Will not be closed. Must not be null.
     * @param tableName  Table to check for the column in. Must not be null.
     * @param columnName Column name to check for. Must not be null.
     * @return Whether the column is present in the given table.
     */
    static boolean isColumnPresentInTable(Connection connection, String tableName, String columnName) throws SQLException {
        QueryRunner runner = new QueryRunner();
        String resultColumnName = runner.query(
                connection,
                String.format("SELECT column_name FROM information_schema.columns WHERE table_name='%s' and column_name='%s'", tableName, columnName),
                new ScalarHandler<>()
        );

        // could be as well done with not null check, but this is stronger and works as well
        return columnName.equals(resultColumnName);
    }

    /**
     * Determines, whether the migration is useful and whether it should be run against the current database state.
     *
     * @param connection Database connection to use.
     * @return true if the migration should be run
     * @throws SQLException
     */
    boolean shouldRun(Connection connection) throws SQLException;

    /**
     * Run the actual migration. Change the database schema.
     *
     * @param connection Database connection to use
     * @throws SQLException
     */
    void run(Connection connection) throws SQLException;
}
