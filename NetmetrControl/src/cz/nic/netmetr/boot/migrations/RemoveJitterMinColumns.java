package cz.nic.netmetr.boot.migrations;

import java.sql.Connection;
import java.sql.SQLException;

public class RemoveJitterMinColumns implements Migration {
    @Override
    public boolean shouldRun(Connection connection) throws SQLException {
        return Migration.isColumnPresentInTable(connection, "test", "jitter_up_min_ns");
    }

    @Override
    public void run(Connection connection) throws SQLException {
        try (var statement = connection.createStatement()) {
            String sql = "ALTER TABLE test " +
                    "DROP COLUMN IF EXISTS jitter_down_min_ns, " +
                    "DROP COLUMN IF EXISTS jitter_up_min_ns";
            statement.executeUpdate(sql);
        }
    }
}
