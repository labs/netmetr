package cz.nic.netmetr.boot.migrations;

import java.sql.Connection;
import java.sql.SQLException;

public class AddJitterPacketLossColumns implements Migration {
    @Override
    public boolean shouldRun(Connection connection) throws SQLException {
        return !Migration.isColumnPresentInTable(connection, "test", "jitter_packet_loss_status");
    }

    @Override
    public void run(Connection connection) throws SQLException {
        try (var statement = connection.createStatement()) {
            statement.executeUpdate("CREATE TYPE jitter_packet_loss_status AS ENUM ('OK', 'ERROR', 'TIMEOUT', 'NOT_MEASURED')");

            String sql = "ALTER TABLE test " +
                    "ADD COLUMN jitter_packet_loss_status jitter_packet_loss_status DEFAULT 'NOT_MEASURED'," +
                    "ADD COLUMN packet_loss_down_packets_total INTEGER DEFAULT NULL CHECK (packet_loss_down_packets_total is NULL or packet_loss_down_packets_total >= 0)," +
                    "ADD COLUMN packet_loss_down_packets_received INTEGER DEFAULT NULL CHECK (packet_loss_down_packets_received is NULL or packet_loss_down_packets_received >= 0)," +
                    "ADD COLUMN jitter_down_min_ns BIGINT DEFAULT NULL CHECK (jitter_down_min_ns is NULL or jitter_down_min_ns >= 0)," +
                    "ADD COLUMN jitter_down_max_ns BIGINT DEFAULT NULL CHECK (jitter_down_max_ns is NULL or jitter_down_max_ns >= 0)," +
                    "ADD COLUMN jitter_down_mean_ns BIGINT DEFAULT NULL CHECK (jitter_down_mean_ns is NULL or jitter_down_mean_ns >= 0)," +
                    "ADD COLUMN packet_loss_up_packets_total INTEGER DEFAULT NULL CHECK (packet_loss_up_packets_total is NULL or packet_loss_up_packets_total >= 0)," +
                    "ADD COLUMN packet_loss_up_packets_received INTEGER DEFAULT NULL CHECK (packet_loss_up_packets_received is NULL or packet_loss_up_packets_received >= 0)," +
                    "ADD COLUMN jitter_up_min_ns BIGINT DEFAULT NULL CHECK (jitter_up_min_ns is NULL or jitter_up_min_ns >= 0)," +
                    "ADD COLUMN jitter_up_max_ns BIGINT DEFAULT NULL CHECK (jitter_up_max_ns is NULL or jitter_up_max_ns >= 0)," +
                    "ADD COLUMN jitter_up_mean_ns BIGINT DEFAULT NULL CHECK (jitter_up_mean_ns is NULL or jitter_up_mean_ns >= 0)";
            statement.executeUpdate(sql);
        }
    }
}
