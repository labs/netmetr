/**
 * This package contains database migrations for NetMetr. If you need to make a new migration, implement Migration
 * interface and register an instance of your class in {@link cz.nic.netmetr.boot.migrations.Migration.MIGRATIONS}.
 * It will be then run automatically.
 */
package cz.nic.netmetr.boot.migrations;