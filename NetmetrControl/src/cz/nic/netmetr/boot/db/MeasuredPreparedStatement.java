package cz.nic.netmetr.boot.db;

import com.google.common.base.Stopwatch;
import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import io.prometheus.client.Histogram;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;

/**
 * Wrapper for PreparedStatement interface, collects metrics for Prometheus.
 */
public class MeasuredPreparedStatement implements PreparedStatement {
    static final Counter requests = Counter.build()
            .name("control_prepared_statements_total")
            .help("Total number of prepared statements created.")
            .register();
    static final Gauge maxExecutionTime = Gauge.build()
            .name("control_prepared_statements_execution_seconds_max")
            .help("Maximum query execution time by prepared statements")
            .labelNames("sql")
            .register();
    static final Histogram executionTimeHistogram = Histogram.build()
            .name("control_prepared_statements_execution_seconds")
            .help("Execution time histogram, labels per query")
            .buckets(0.1d, 0.5d, 1d, 3d, 10d)
            .labelNames("sql")
            .register();

    private static String queryToLabel(String query) {
        return query
                .strip()
                .toLowerCase()
                .replace('\n', ' ')
                .replaceAll("\\s+", " ");
    }

    private interface ExecuteSQL<T> {
        T execute() throws SQLException;
    }

    private <T> T measureQueryExecution(ExecuteSQL<T> supplier) throws SQLException {
        Stopwatch s = Stopwatch.createStarted();
        var result = supplier.execute();
        s.stop();

        var time = (double) s.elapsed().toMillis() / 1000d;

        // store elapsed time in the histogram
        executionTimeHistogram.labels(queryToLabel(this.query)).observe(time);

        // this might be prone to race conditions, but we do not care
        // synchronization would be too costly
        var c = maxExecutionTime.labels(queryToLabel(this.query));
        if (c.get() < time) c.set(time);

        return result;
    }

    private final PreparedStatement wrapped;
    private final String query;

    public MeasuredPreparedStatement(PreparedStatement wrapped, String query) {
        // log prepared statement creation
        requests.inc();

        // save fields
        this.wrapped = wrapped;
        this.query = query;
    }

    @Override
    public ResultSet executeQuery() throws SQLException {
        return measureQueryExecution(wrapped::executeQuery);
    }

    @Override
    public int executeUpdate() throws SQLException {
        return measureQueryExecution(wrapped::executeUpdate);
    }

    @Override
    public void setNull(int i, int i1) throws SQLException {
        wrapped.setNull(i, i1);
    }

    @Override
    public void setBoolean(int i, boolean b) throws SQLException {
        wrapped.setBoolean(i, b);
    }

    @Override
    public void setByte(int i, byte b) throws SQLException {
        wrapped.setByte(i, b);
    }

    @Override
    public void setShort(int i, short i1) throws SQLException {
        wrapped.setShort(i, i1);
    }

    @Override
    public void setInt(int i, int i1) throws SQLException {
        wrapped.setInt(i, i1);
    }

    @Override
    public void setLong(int i, long l) throws SQLException {
        wrapped.setLong(i, l);
    }

    @Override
    public void setFloat(int i, float v) throws SQLException {
        wrapped.setFloat(i, v);
    }

    @Override
    public void setDouble(int i, double v) throws SQLException {
        wrapped.setDouble(i, v);
    }

    @Override
    public void setBigDecimal(int i, BigDecimal bigDecimal) throws SQLException {
        wrapped.setBigDecimal(i, bigDecimal);
    }

    @Override
    public void setString(int i, String s) throws SQLException {
        wrapped.setString(i, s);
    }

    @Override
    public void setBytes(int i, byte[] bytes) throws SQLException {
        wrapped.setBytes(i, bytes);
    }

    @Override
    public void setDate(int i, Date date) throws SQLException {
        wrapped.setDate(i, date);
    }

    @Override
    public void setTime(int i, Time time) throws SQLException {
        wrapped.setTime(i, time);
    }

    @Override
    public void setTimestamp(int i, Timestamp timestamp) throws SQLException {
        wrapped.setTimestamp(i, timestamp);
    }

    @Override
    public void setAsciiStream(int i, InputStream inputStream, int i1) throws SQLException {
        wrapped.setAsciiStream(i, inputStream, i1);
    }

    @Override
    @Deprecated(since = "1.2")
    public void setUnicodeStream(int i, InputStream inputStream, int i1) throws SQLException {
        wrapped.setUnicodeStream(i, inputStream, i1);
    }

    @Override
    public void setBinaryStream(int i, InputStream inputStream, int i1) throws SQLException {
        wrapped.setBinaryStream(i, inputStream, i1);
    }

    @Override
    public void clearParameters() throws SQLException {
        wrapped.clearParameters();
    }

    @Override
    public void setObject(int i, Object o, int i1) throws SQLException {
        wrapped.setObject(i, o, i1);
    }

    @Override
    public void setObject(int i, Object o) throws SQLException {
        wrapped.setObject(i, o);
    }

    @Override
    public boolean execute() throws SQLException {
        return measureQueryExecution(wrapped::execute);
    }

    @Override
    public void addBatch() throws SQLException {
        wrapped.addBatch();
    }

    @Override
    public void setCharacterStream(int i, Reader reader, int i1) throws SQLException {
        wrapped.setCharacterStream(i, reader, i1);
    }

    @Override
    public void setRef(int i, Ref ref) throws SQLException {
        wrapped.setRef(i, ref);
    }

    @Override
    public void setBlob(int i, Blob blob) throws SQLException {
        wrapped.setBlob(i, blob);
    }

    @Override
    public void setClob(int i, Clob clob) throws SQLException {
        wrapped.setClob(i, clob);
    }

    @Override
    public void setArray(int i, Array array) throws SQLException {
        wrapped.setArray(i, array);
    }

    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        return wrapped.getMetaData();
    }

    @Override
    public void setDate(int i, Date date, Calendar calendar) throws SQLException {
        wrapped.setDate(i, date, calendar);
    }

    @Override
    public void setTime(int i, Time time, Calendar calendar) throws SQLException {
        wrapped.setTime(i, time, calendar);
    }

    @Override
    public void setTimestamp(int i, Timestamp timestamp, Calendar calendar) throws SQLException {
        wrapped.setTimestamp(i, timestamp, calendar);
    }

    @Override
    public void setNull(int i, int i1, String s) throws SQLException {
        wrapped.setNull(i, i1, s);
    }

    @Override
    public void setURL(int i, URL url) throws SQLException {
        wrapped.setURL(i, url);
    }

    @Override
    public ParameterMetaData getParameterMetaData() throws SQLException {
        return wrapped.getParameterMetaData();
    }

    @Override
    public void setRowId(int i, RowId rowId) throws SQLException {
        wrapped.setRowId(i, rowId);
    }

    @Override
    public void setNString(int i, String s) throws SQLException {
        wrapped.setNString(i, s);
    }

    @Override
    public void setNCharacterStream(int i, Reader reader, long l) throws SQLException {
        wrapped.setNCharacterStream(i, reader, l);
    }

    @Override
    public void setNClob(int i, NClob nClob) throws SQLException {
        wrapped.setNClob(i, nClob);
    }

    @Override
    public void setClob(int i, Reader reader, long l) throws SQLException {
        wrapped.setClob(i, reader, l);
    }

    @Override
    public void setBlob(int i, InputStream inputStream, long l) throws SQLException {
        wrapped.setBlob(i, inputStream, l);
    }

    @Override
    public void setNClob(int i, Reader reader, long l) throws SQLException {
        wrapped.setNClob(i, reader, l);
    }

    @Override
    public void setSQLXML(int i, SQLXML sqlxml) throws SQLException {
        wrapped.setSQLXML(i, sqlxml);
    }

    @Override
    public void setObject(int i, Object o, int i1, int i2) throws SQLException {
        wrapped.setObject(i, o, i1, i2);
    }

    @Override
    public void setAsciiStream(int i, InputStream inputStream, long l) throws SQLException {
        wrapped.setAsciiStream(i, inputStream, l);
    }

    @Override
    public void setBinaryStream(int i, InputStream inputStream, long l) throws SQLException {
        wrapped.setBinaryStream(i, inputStream, l);
    }

    @Override
    public void setCharacterStream(int i, Reader reader, long l) throws SQLException {
        wrapped.setCharacterStream(i, reader, l);
    }

    @Override
    public void setAsciiStream(int i, InputStream inputStream) throws SQLException {
        wrapped.setAsciiStream(i, inputStream);
    }

    @Override
    public void setBinaryStream(int i, InputStream inputStream) throws SQLException {
        wrapped.setBinaryStream(i, inputStream);
    }

    @Override
    public void setCharacterStream(int i, Reader reader) throws SQLException {
        wrapped.setCharacterStream(i, reader);
    }

    @Override
    public void setNCharacterStream(int i, Reader reader) throws SQLException {
        wrapped.setNCharacterStream(i, reader);
    }

    @Override
    public void setClob(int i, Reader reader) throws SQLException {
        wrapped.setClob(i, reader);
    }

    @Override
    public void setBlob(int i, InputStream inputStream) throws SQLException {
        wrapped.setBlob(i, inputStream);
    }

    @Override
    public void setNClob(int i, Reader reader) throws SQLException {
        wrapped.setNClob(i, reader);
    }

    @Override
    public void setObject(int parameterIndex, Object x, SQLType targetSqlType, int scaleOrLength) throws SQLException {
        wrapped.setObject(parameterIndex, x, targetSqlType, scaleOrLength);
    }

    @Override
    public void setObject(int parameterIndex, Object x, SQLType targetSqlType) throws SQLException {
        wrapped.setObject(parameterIndex, x, targetSqlType);
    }

    @Override
    public long executeLargeUpdate() throws SQLException {
        return measureQueryExecution(wrapped::executeLargeUpdate);
    }

    @Override
    public ResultSet executeQuery(String q) throws SQLException {
        return measureQueryExecution(() -> wrapped.executeQuery(q));
    }

    @Override
    public int executeUpdate(String q) throws SQLException {
        return measureQueryExecution(() -> wrapped.executeUpdate(q));
    }

    @Override
    public void close() throws SQLException {
        wrapped.close();
    }

    @Override
    public int getMaxFieldSize() throws SQLException {
        return wrapped.getMaxFieldSize();
    }

    @Override
    public void setMaxFieldSize(int i) throws SQLException {
        wrapped.setMaxFieldSize(i);
    }

    @Override
    public int getMaxRows() throws SQLException {
        return wrapped.getMaxRows();
    }

    @Override
    public void setMaxRows(int i) throws SQLException {
        wrapped.setMaxRows(i);
    }

    @Override
    public void setEscapeProcessing(boolean b) throws SQLException {
        wrapped.setEscapeProcessing(b);
    }

    @Override
    public int getQueryTimeout() throws SQLException {
        return wrapped.getQueryTimeout();
    }

    @Override
    public void setQueryTimeout(int i) throws SQLException {
        wrapped.setQueryTimeout(i);
    }

    @Override
    public void cancel() throws SQLException {
        wrapped.cancel();
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        return wrapped.getWarnings();
    }

    @Override
    public void clearWarnings() throws SQLException {
        wrapped.clearWarnings();
    }

    @Override
    public void setCursorName(String s) throws SQLException {
        wrapped.setCursorName(s);
    }

    @Override
    public boolean execute(String q) throws SQLException {
        return measureQueryExecution(() -> wrapped.execute(q));
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        return wrapped.getResultSet();
    }

    @Override
    public int getUpdateCount() throws SQLException {
        return wrapped.getUpdateCount();
    }

    @Override
    public boolean getMoreResults() throws SQLException {
        return wrapped.getMoreResults();
    }

    @Override
    public void setFetchDirection(int i) throws SQLException {
        wrapped.setFetchDirection(i);
    }

    @Override
    public int getFetchDirection() throws SQLException {
        return wrapped.getFetchDirection();
    }

    @Override
    public void setFetchSize(int i) throws SQLException {
        wrapped.setFetchSize(i);
    }

    @Override
    public int getFetchSize() throws SQLException {
        return wrapped.getFetchSize();
    }

    @Override
    public int getResultSetConcurrency() throws SQLException {
        return wrapped.getResultSetConcurrency();
    }

    @Override
    public int getResultSetType() throws SQLException {
        return wrapped.getResultSetType();
    }

    @Override
    public void addBatch(String s) throws SQLException {
        wrapped.addBatch(s);
    }

    @Override
    public void clearBatch() throws SQLException {
        wrapped.clearBatch();
    }

    @Override
    public int[] executeBatch() throws SQLException {
        return measureQueryExecution(() -> wrapped.executeBatch());
    }

    @Override
    public Connection getConnection() throws SQLException {
        return wrapped.getConnection();
    }

    @Override
    public boolean getMoreResults(int i) throws SQLException {
        return wrapped.getMoreResults(i);
    }

    @Override
    public ResultSet getGeneratedKeys() throws SQLException {
        return wrapped.getGeneratedKeys();
    }

    @Override
    public int executeUpdate(String q, int i) throws SQLException {
        return measureQueryExecution(() -> wrapped.executeUpdate(q, i));
    }

    @Override
    public int executeUpdate(String q, int[] ints) throws SQLException {
        return measureQueryExecution(() -> wrapped.executeUpdate(q, ints));
    }

    @Override
    public int executeUpdate(String q, String[] strings) throws SQLException {
        return measureQueryExecution(() -> wrapped.executeUpdate(q, strings));
    }

    @Override
    public boolean execute(String q, int i) throws SQLException {
        return measureQueryExecution(() -> wrapped.execute(q, i));
    }

    @Override
    public boolean execute(String q, int[] ints) throws SQLException {
        return measureQueryExecution(() -> wrapped.execute(q, ints));
    }

    @Override
    public boolean execute(String q, String[] strings) throws SQLException {
        return measureQueryExecution(() -> wrapped.execute(q, strings));
    }

    @Override
    public int getResultSetHoldability() throws SQLException {
        return wrapped.getResultSetHoldability();
    }

    @Override
    public boolean isClosed() throws SQLException {
        return wrapped.isClosed();
    }

    @Override
    public void setPoolable(boolean b) throws SQLException {
        wrapped.setPoolable(b);
    }

    @Override
    public boolean isPoolable() throws SQLException {
        return wrapped.isPoolable();
    }

    @Override
    public void closeOnCompletion() throws SQLException {
        wrapped.closeOnCompletion();
    }

    @Override
    public boolean isCloseOnCompletion() throws SQLException {
        return wrapped.isCloseOnCompletion();
    }

    @Override
    public long getLargeUpdateCount() throws SQLException {
        return wrapped.getLargeUpdateCount();
    }

    @Override
    public void setLargeMaxRows(long max) throws SQLException {
        wrapped.setLargeMaxRows(max);
    }

    @Override
    public long getLargeMaxRows() throws SQLException {
        return wrapped.getLargeMaxRows();
    }

    @Override
    public long[] executeLargeBatch() throws SQLException {
        return measureQueryExecution(wrapped::executeLargeBatch);
    }

    @Override
    public long executeLargeUpdate(String sql) throws SQLException {
        return measureQueryExecution(() -> wrapped.executeLargeUpdate(sql));
    }

    @Override
    public long executeLargeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
        return measureQueryExecution(() -> wrapped.executeLargeUpdate(sql, autoGeneratedKeys));
    }

    @Override
    public long executeLargeUpdate(String sql, int[] columnIndexes) throws SQLException {
        return measureQueryExecution(() -> wrapped.executeLargeUpdate(sql, columnIndexes));
    }

    @Override
    public long executeLargeUpdate(String sql, String[] columnNames) throws SQLException {
        return measureQueryExecution(() -> wrapped.executeLargeUpdate(sql, columnNames));
    }

    @Override
    public String enquoteLiteral(String val) throws SQLException {
        return wrapped.enquoteLiteral(val);
    }

    @Override
    public String enquoteIdentifier(String identifier, boolean alwaysQuote) throws SQLException {
        return wrapped.enquoteIdentifier(identifier, alwaysQuote);
    }

    @Override
    public boolean isSimpleIdentifier(String identifier) throws SQLException {
        return wrapped.isSimpleIdentifier(identifier);
    }

    @Override
    public String enquoteNCharLiteral(String val) throws SQLException {
        return wrapped.enquoteNCharLiteral(val);
    }

    @Override
    public <T> T unwrap(Class<T> aClass) throws SQLException {
        return wrapped.unwrap(aClass);
    }

    @Override
    public boolean isWrapperFor(Class<?> aClass) throws SQLException {
        return wrapped.isWrapperFor(aClass);
    }
}
