package cz.nic.netmetr.boot.db;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * Wrapper for Connection interface returning MeasuredPreparedStatements instead of normal PreparedStatements.
 *
 * Used for metrics collection into Prometheus
 */
public class MeasuredConnection implements Connection {

    private final Connection wrapped;

    public MeasuredConnection(Connection wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public Statement createStatement() throws SQLException {
        return wrapped.createStatement();
    }

    @Override
    public PreparedStatement prepareStatement(String s) throws SQLException {
        return new MeasuredPreparedStatement(wrapped.prepareStatement(s), s);
    }

    @Override
    public CallableStatement prepareCall(String s) throws SQLException {
        return wrapped.prepareCall(s);
    }

    @Override
    public String nativeSQL(String s) throws SQLException {
        return wrapped.nativeSQL(s);
    }

    @Override
    public void setAutoCommit(boolean b) throws SQLException {
        wrapped.setAutoCommit(b);
    }

    @Override
    public boolean getAutoCommit() throws SQLException {
        return wrapped.getAutoCommit();
    }

    @Override
    public void commit() throws SQLException {
        wrapped.commit();
    }

    @Override
    public void rollback() throws SQLException {
        wrapped.rollback();
    }

    @Override
    public void close() throws SQLException {
        wrapped.close();
    }

    @Override
    public boolean isClosed() throws SQLException {
        return wrapped.isClosed();
    }

    @Override
    public DatabaseMetaData getMetaData() throws SQLException {
        return wrapped.getMetaData();
    }

    @Override
    public void setReadOnly(boolean b) throws SQLException {
        wrapped.setReadOnly(b);
    }

    @Override
    public boolean isReadOnly() throws SQLException {
        return wrapped.isReadOnly();
    }

    @Override
    public void setCatalog(String s) throws SQLException {
        wrapped.setCatalog(s);
    }

    @Override
    public String getCatalog() throws SQLException {
        return wrapped.getCatalog();
    }

    @Override
    public void setTransactionIsolation(int i) throws SQLException {
        wrapped.setTransactionIsolation(i);
    }

    @Override
    public int getTransactionIsolation() throws SQLException {
        return wrapped.getTransactionIsolation();
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        return wrapped.getWarnings();
    }

    @Override
    public void clearWarnings() throws SQLException {
        wrapped.clearWarnings();
    }

    @Override
    public Statement createStatement(int i, int i1) throws SQLException {
        return wrapped.createStatement(i, i1);
    }

    @Override
    public PreparedStatement prepareStatement(String s, int i, int i1) throws SQLException {
        return new MeasuredPreparedStatement(wrapped.prepareStatement(s, i, i1), s);
    }

    @Override
    public CallableStatement prepareCall(String s, int i, int i1) throws SQLException {
        return wrapped.prepareCall(s, i, i1);
    }

    @Override
    public Map<String, Class<?>> getTypeMap() throws SQLException {
        return wrapped.getTypeMap();
    }

    @Override
    public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
        wrapped.setTypeMap(map);
    }

    @Override
    public void setHoldability(int i) throws SQLException {
        wrapped.setHoldability(i);
    }

    @Override
    public int getHoldability() throws SQLException {
        return wrapped.getHoldability();
    }

    @Override
    public Savepoint setSavepoint() throws SQLException {
        return wrapped.setSavepoint();
    }

    @Override
    public Savepoint setSavepoint(String s) throws SQLException {
        return wrapped.setSavepoint(s);
    }

    @Override
    public void rollback(Savepoint savepoint) throws SQLException {
        wrapped.rollback(savepoint);
    }

    @Override
    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
        wrapped.releaseSavepoint(savepoint);
    }

    @Override
    public Statement createStatement(int i, int i1, int i2) throws SQLException {
        return wrapped.createStatement(i, i1, i2);
    }

    @Override
    public PreparedStatement prepareStatement(String s, int i, int i1, int i2) throws SQLException {
        return new MeasuredPreparedStatement(wrapped.prepareStatement(s, i, i1, i2), s);
    }

    @Override
    public CallableStatement prepareCall(String s, int i, int i1, int i2) throws SQLException {
        return wrapped.prepareCall(s, i, i1, i2);
    }

    @Override
    public PreparedStatement prepareStatement(String s, int i) throws SQLException {
        return new MeasuredPreparedStatement(wrapped.prepareStatement(s, i), s);
    }

    @Override
    public PreparedStatement prepareStatement(String s, int[] ints) throws SQLException {
        return new MeasuredPreparedStatement(wrapped.prepareStatement(s, ints), s);
    }

    @Override
    public PreparedStatement prepareStatement(String s, String[] strings) throws SQLException {
        return new MeasuredPreparedStatement(wrapped.prepareStatement(s, strings), s);
    }

    @Override
    public Clob createClob() throws SQLException {
        return wrapped.createClob();
    }

    @Override
    public Blob createBlob() throws SQLException {
        return wrapped.createBlob();
    }

    @Override
    public NClob createNClob() throws SQLException {
        return wrapped.createNClob();
    }

    @Override
    public SQLXML createSQLXML() throws SQLException {
        return wrapped.createSQLXML();
    }

    @Override
    public boolean isValid(int i) throws SQLException {
        return wrapped.isValid(i);
    }

    @Override
    public void setClientInfo(String s, String s1) throws SQLClientInfoException {
        wrapped.setClientInfo(s, s1);
    }

    @Override
    public void setClientInfo(Properties properties) throws SQLClientInfoException {
        wrapped.setClientInfo(properties);
    }

    @Override
    public String getClientInfo(String s) throws SQLException {
        return wrapped.getClientInfo(s);
    }

    @Override
    public Properties getClientInfo() throws SQLException {
        return wrapped.getClientInfo();
    }

    @Override
    public Array createArrayOf(String s, Object[] objects) throws SQLException {
        return wrapped.createArrayOf(s, objects);
    }

    @Override
    public Struct createStruct(String s, Object[] objects) throws SQLException {
        return wrapped.createStruct(s, objects);
    }

    @Override
    public void setSchema(String s) throws SQLException {
        wrapped.setSchema(s);
    }

    @Override
    public String getSchema() throws SQLException {
        return wrapped.getSchema();
    }

    @Override
    public void abort(Executor executor) throws SQLException {
        wrapped.abort(executor);
    }

    @Override
    public void setNetworkTimeout(Executor executor, int i) throws SQLException {
        wrapped.setNetworkTimeout(executor, i);
    }

    @Override
    public int getNetworkTimeout() throws SQLException {
        return wrapped.getNetworkTimeout();
    }

    @Override
    public void beginRequest() throws SQLException {
        wrapped.beginRequest();
    }

    @Override
    public void endRequest() throws SQLException {
        wrapped.endRequest();
    }

    @Override
    public boolean setShardingKeyIfValid(ShardingKey shardingKey, ShardingKey superShardingKey, int timeout) throws SQLException {
        return wrapped.setShardingKeyIfValid(shardingKey, superShardingKey, timeout);
    }

    @Override
    public boolean setShardingKeyIfValid(ShardingKey shardingKey, int timeout) throws SQLException {
        return wrapped.setShardingKeyIfValid(shardingKey, timeout);
    }

    @Override
    public void setShardingKey(ShardingKey shardingKey, ShardingKey superShardingKey) throws SQLException {
        wrapped.setShardingKey(shardingKey, superShardingKey);
    }

    @Override
    public void setShardingKey(ShardingKey shardingKey) throws SQLException {
        wrapped.setShardingKey(shardingKey);
    }

    @Override
    public <T> T unwrap(Class<T> aClass) throws SQLException {
        return wrapped.unwrap(aClass);
    }

    @Override
    public boolean isWrapperFor(Class<?> aClass) throws SQLException {
        return wrapped.isWrapperFor(aClass);
    }
}
