package cz.nic.netmetr.boot;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class NetmetrBackgroundTasks {
    private static final int THREAD_POOL_SIZE = 1;

    private static NetmetrBackgroundTasks instance = null;

    public static NetmetrBackgroundTasks getInstance() {
        if (instance == null) {
            synchronized (NetmetrBackgroundTasks.class) {
                if (instance == null) {
                    instance = new NetmetrBackgroundTasks();
                }
            }
        }

        return instance;
    }


    private ScheduledExecutorService scheduler;

    private NetmetrBackgroundTasks() {
        scheduler = Executors.newScheduledThreadPool(THREAD_POOL_SIZE);
    }

    public void runDaily(Runnable task) {
        // generate random delay for the first run of the task
        long delay = new Random().nextInt(24);

        // schedule the task without allowing cancellation
        this.scheduler.scheduleAtFixedRate(task, delay, 24, TimeUnit.HOURS);
    }

    public void runAtNight(Runnable task) {
        // get time delay to start between 2am and 3am
        int waitHours;
        var now = LocalDateTime.now();
        if (now.getHour() < 3) {
            waitHours = 3 - now.getHour();
        } else {
            waitHours = 24 - now.getHour() + 3;
        }

        // schedule the task without allowing cancellation
        this.scheduler.scheduleAtFixedRate(task, waitHours, 24, TimeUnit.HOURS);
    }

    private void runFirstDayNextMonth(Runnable task) {
        // get time delay until first day of next month in hours
        var newTime = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS).plusMonths(1).plusHours(2);
        var waitSeconds = Duration.between(LocalDateTime.now(), newTime).abs().getSeconds();

        // schedule
        this.scheduler.schedule(task, waitSeconds, TimeUnit.SECONDS);
    }

    public void runFirstDayEveryMonth(Runnable task) {
        // the problem with this is that not all months are the same length, so with regular interval we would drift
        // if we schedule only the next run every time, than we can handle the drift dynamically
        runFirstDayNextMonth(() -> {
            task.run();
            runFirstDayNextMonth(task);
        });
    }
    
    public void runSoon(Runnable task) {
        this.scheduler.execute(task);
    }
}
