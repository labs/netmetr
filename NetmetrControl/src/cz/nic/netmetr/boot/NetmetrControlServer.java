package cz.nic.netmetr.boot;

import cz.nic.netmetr.boot.migrations.Migration;
import cz.nic.netmetr.maintenance.NetmetrMaintenance;
import cz.nic.netmetr.model.ModelInitializer;
import cz.nic.netmetr.shared.config.GlobalConfig;
import cz.nic.netmetr.shared.utils.ReadOnlyProperties;
import io.prometheus.client.exporter.HTTPServer;
import io.prometheus.client.hotspot.DefaultExports;
import io.prometheus.client.jetty.JettyStatisticsCollector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.StatisticsHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.restlet.ext.servlet.ServerServlet;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.io.IOException;

public class NetmetrControlServer {

    public static void main(String[] args) throws Exception {
        initializeLogger();

        // initialize global configuration
        GlobalConfig.init(ReadOnlyProperties.from("config.properties").get("server.rmbt.secret_key"));

        // run migrations
        Migration.runMigrations(DbConnection.getInstance());

        // once database is using the latest schema, initialise model
        ModelInitializer.init(DbConnection.getInstance());

        // setup maintenance tasks
        NetmetrMaintenance.setupMaintenanceTasks();

        // configure server
        Server server = initializeJetty();

        // setup monitoring
        initializePrometheusMonitoring(server);

        // run the server
        try {
            server.start();
            server.join();
        } finally {
            server.stop();
        }
    }

    private static Server initializeJetty() {
        // configure Servlet-Restlet mapping
        var restletHandler = new ServletContextHandler();
        restletHandler.addServlet(ServerServlet.class, "/*");
        restletHandler.setInitParameter("org.restlet.component", NetmetrComponent.class.getCanonicalName());

        // configure server
        int port = Integer.parseInt(ReadOnlyProperties.from("config.properties").get("server.net.port"));
        Server server = new Server(port);
        server.setHandler(restletHandler);

        return server;
    }

    private static void initializeLogger() {
        // Optionally remove existing handlers attached to j.u.l root logger
        SLF4JBridgeHandler.removeHandlersForRootLogger();  // (since SLF4J 1.6.5)

        // add SLF4JBridgeHandler to j.u.l's root logger, should be done once during
        // the initialization phase of your application
        SLF4JBridgeHandler.install();

        // Reconfigure Restlet logging to use SLF4J
        System.getProperties().put("org.restlet.engine.loggerFacadeClass", "org.restlet.ext.slf4j.Slf4jLoggerFacade");
    }

    private static void initializePrometheusMonitoring(Server jetty) throws IOException {
        // start collecting generic information about JVM
        DefaultExports.initialize();

        // configure Jetty statistics collection
        StatisticsHandler stats = new StatisticsHandler();
        stats.setHandler(jetty.getHandler());
        jetty.setHandler(stats);
        new JettyStatisticsCollector(stats).register();

        // We could expose metrics over Jetty, but then we could not prevent people from all over the internet from
        // reading them. Using a separate web server on a port that's not exposed to the whole internet solves
        // the issue.
        //
        // Hard-coded port, referenced only from within the Ansible scripts
        HTTPServer server = new HTTPServer(19400);
    }

}
