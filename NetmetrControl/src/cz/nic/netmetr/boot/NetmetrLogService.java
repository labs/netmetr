package cz.nic.netmetr.boot;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.service.LogService;

public class NetmetrLogService extends LogService {
    public NetmetrLogService() {
        super();
    }

    /**
     * Code copy-pasted and modified from Restlet's {@link LogService} implementation.
     */
    @Override
    protected String getDefaultResponseLogMessage(Response response, int duration) {


        StringBuilder sb = new StringBuilder();
        Request request = response.getRequest();

        // Append the client IP address
        String clientAddress = request.getClientInfo().getUpstreamAddress();
        sb.append((clientAddress == null) ? "-" : clientAddress);
        sb.append('\t');

        // Append the method name
        String methodName = (request.getMethod() == null) ? "-" : request
                .getMethod().getName();
        sb.append((methodName == null) ? "-" : methodName);

        // Append the resource path
        sb.append('\t');
        String resourcePath = (request.getResourceRef() == null) ? "-"
                : request.getResourceRef().getPath();
        sb.append((resourcePath == null) ? "-" : resourcePath);

        // Append the resource query
        sb.append('\t');
        String resourceQuery = (request.getResourceRef() == null) ? "-"
                : request.getResourceRef().getQuery();
        sb.append((resourceQuery == null) ? "-" : resourceQuery);

        // Append the status code
        sb.append('\t');
        sb.append((response.getStatus() == null) ? "-" : Integer
                .toString(response.getStatus().getCode()));

        // Append the duration
        sb.append('\t');
        sb.append(duration);
        sb.append("ms");

        // Append the host reference
        sb.append('\t');
        sb.append((request.getHostRef() == null) ? "-" : request.getHostRef()
                .toString());

        // Append the agent name
        sb.append('\t');
        String agentName = request.getClientInfo().getAgent();
        sb.append((agentName == null) ? "-" : agentName);

        return sb.toString();
    }
}
