package cz.nic.netmetr.maintenance;

import cz.nic.netmetr.boot.NetmetrBackgroundTasks;

public final class NetmetrMaintenance {
    public static void setupMaintenanceTasks() {
        NetmetrBackgroundTasks.getInstance().runAtNight(new OldSpeedGraphDropper());
        NetmetrBackgroundTasks.getInstance().runFirstDayEveryMonth(new OpenDataPublication());

        // check that open-data have been published this month. If not, run the task manually right now
        if (!OpenDataPublication.isThePreviousMonthPublished()) {
            NetmetrBackgroundTasks.getInstance().runSoon(new OpenDataPublication());
        }
    }
}
