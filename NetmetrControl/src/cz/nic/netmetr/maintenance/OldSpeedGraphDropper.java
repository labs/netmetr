package cz.nic.netmetr.maintenance;

import cz.nic.netmetr.boot.DbConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;

/**
 * Drops old speed graph data from the database. Should be used with
 * {@link cz.nic.netmetr.boot.NetmetrBackgroundTasks} and run daily.
 *
 * Configured in {@link NetmetrMaintenance}
 */
public final class OldSpeedGraphDropper implements Runnable {

    private static final String INTERVAL_TO_KEEP = "2 months";

    Logger logger = LoggerFactory.getLogger(OldSpeedGraphDropper.class);

    @Override
    public void run() {
        logger.info("Maintenance - dropping old speed_graph_data from table test");
        Instant start = Instant.now();

        try (Connection conn = DbConnection.getInstance().getConnection()) {
            final String qry = "UPDATE test SET speed_graph_data = DEFAULT WHERE test.time < NOW() - INTERVAL '" + INTERVAL_TO_KEEP + "' and cardinality(speed_graph_data) <> 0";
            try (var stmt = conn.prepareStatement(qry)) {
                stmt.execute();
            }

            long durationMs = Duration.between(start, Instant.now()).toMillis();
            logger.info("Maintenance - speed_graph_data from table test older than " + INTERVAL_TO_KEEP + " dropped. It took {}ms.", durationMs);
        } catch (SQLException e) {
            logger.error("Dropping old speed graphs failed...", e);
        }
    }
}
