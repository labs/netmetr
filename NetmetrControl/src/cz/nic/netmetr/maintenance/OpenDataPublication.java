package cz.nic.netmetr.maintenance;

import cz.nic.netmetr.boot.DbConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;

/**
 * Tags monthly data for publication. Can perform any validation as we please.
 *
 * Should be used with {@link cz.nic.netmetr.boot.NetmetrBackgroundTasks} and can be run daily (at least monthly).
 * Configured in {@link NetmetrMaintenance}
 */
public final class OpenDataPublication implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(OpenDataPublication.class);

    public static boolean isThePreviousMonthPublished() {
        try (Connection conn = DbConnection.getInstance().getConnection()) {
            int year = LocalDateTime.now().minus(1, ChronoUnit.MONTHS).getYear();
            int month = LocalDateTime.now().minus(1, ChronoUnit.MONTHS).getMonthValue();
            final String qry = String.format("SELECT timestamp_month from audit_results WHERE timestamp_month = '%04d-%02d-01 00:00:00';", year, month);
            try (var stmt = conn.prepareStatement(qry); var o = stmt.executeQuery()) {
                return o.next();
            }
        } catch (SQLException throwables) {
            logger.error("Failed to check whether last month has been published...", throwables);
        }

        return true; // assume true, because that means nothing more gets done (=> it is safer state to be in)
    }

    @Override
    public void run() {
        logger.info("Maintenance - starting open data publication");
        Instant start = Instant.now();

        try (Connection conn = DbConnection.getInstance().getConnection()) {
            conn.setAutoCommit(false);

            // fill audit table

            // NOTE: Uncommenting the following function call enables monthly validation, that the results make sense
            // and that they can be exported in open-data. Starting in spring 2022, the NetMetr project has only
            // the Turris routers as clients and for their use-case, we don't really care about validity. It's the problem
            // of the user, who performs invalid tests. Not ours. Therefore, the validation has been disabled, because
            // it seems that it marks all tests from Turris routers as invalid (codes 341 and 342). I don't currently
            // know, what the root cause is and I am not going to investigate further. The open-data will not be
            // validated starting on 2022-06-01, but it will still be published.
            //
            // The discussed function:
            // runQueryTagging(conn);

            propagateAuditInformationIntoTests(conn);

            // Mark the previous month as ready for publication
            int year = LocalDateTime.now().minus(1, ChronoUnit.MONTHS).getYear();
            int month = LocalDateTime.now().minus(1, ChronoUnit.MONTHS).getMonthValue();
            final String qry = String.format("INSERT INTO audit_results(timestamp_month, checked) SELECT '%04d-%02d-01 00:00:00', true WHERE NOT EXISTS (SELECT timestamp_month from audit_results WHERE timestamp_month = '%04d-%02d-01 00:00:00');", year, month, year, month);
            try (var stmt = conn.prepareStatement(qry)) {
                stmt.execute();
            }

            // commit it all
            conn.commit();

            long durationMs = Duration.between(start, Instant.now()).toMillis();
            logger.info("Maintenance - open data publication process ended. It took {}ms.", durationMs);
        } catch (SQLException e) {
            logger.error("Publishing open data failed...", e);
        }
    }

    private static void runQueryTagging(Connection conn) throws SQLException {
        // ensure that audit table is synchronized with test
        try(var stmt = conn.prepareStatement("INSERT INTO audit (uid) SELECT x.uid FROM ( SELECT uid FROM test ) x WHERE NOT EXISTS (SELECT 1 FROM audit t WHERE t.uid = x.uid);")) {
            stmt.execute();
        }

        // code 200, status is not FINISHED
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 200) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status NOT LIKE 'FINISHED';")) {
            stmt.execute();
        }

        // code 311, 2G download speed too high
        // this check lacks missing information in device table
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 311) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND t.network_group_type LIKE 'MOBILE' AND t.network_group_name LIKE '2G' AND t.speed_download>(4*59.2);")) {
            stmt.execute();
        }

        // code 312, 2G upload speed too high
        // this check lacks missing information in device table
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 312) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND t.network_group_type LIKE 'MOBILE' AND t.network_group_name LIKE '2G' AND t.speed_upload>(2*59.2);")) {
                stmt.execute();
        }

        // code 313, 3G speed too high
        // this check lacks missing information in device table
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 313) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND t.network_group_type LIKE 'MOBILE' AND t.network_group_name LIKE '3G' AND t.speed_download>(42200);")) {
            stmt.execute();
        }

        // code 314, 3G speed too high
        // this check lacks missing information in device table
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 314) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND t.network_group_type LIKE 'MOBILE' AND t.network_group_name LIKE '3G' AND t.speed_upload>(5760);")) {
            stmt.execute();
        }

        // code 315, 4G speed too high
        // mind: all values seem to be correct since all mobile are CAT6 (=300Mbit/s)
        // this information IS currently missing in db and should be added to table devices
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 315) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND t.network_group_type LIKE 'MOBILE' AND t.network_group_name LIKE '4G' AND t.speed_download>(150000);")) {
            stmt.execute();
        }

        // code 316, 4G upload speed too high
        // mind: all values seem to be correct since all mobile are CAT6 (=300Mbit/s)
        // this information IS currently missing in db and should be added to table devices
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 316) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND t.network_group_type LIKE 'MOBILE' AND t.network_group_name LIKE '4G' AND t.speed_upload>(50000);")) {
            stmt.execute();
        }

        // code 34x, speed information doesn't match transferred bytes
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 341) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND nsec_download <> 0 AND speed_download <> 0 AND abs(round(100*(round(1000000.0*8*bytes_download/nsec_download)-speed_download)/speed_download)) > 5 AND abs(round(1000000.0*8*bytes_download/nsec_download) - speed_download) > 1;")) {
            stmt.execute();
        }
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 342) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND nsec_upload <> 0 AND speed_upload <> 0 AND abs(round(100*(round(1000000.0*8*bytes_upload/nsec_upload)-speed_upload)/speed_upload)) > 5 AND abs(round(1000000.0*8*bytes_upload/nsec_upload) - speed_upload) > 1;")) {
            stmt.execute();
        }

        // code 35x, transferred bytes are irregular
        // total_bytes_xx includes background traffic, so should be bigger !
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 351) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND total_bytes_download < bytes_download;")) {
            stmt.execute();
        }
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 352) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND total_bytes_upload < bytes_upload;")) {
            stmt.execute();
        }

        // code 36x, measurement duration not reached out
        // mark everything with 50% deviation or more
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 361) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND ABS(( nsec_download::float/1000000000 - duration::float ) / duration::float*100.0 ) > 50;")) {
            stmt.execute();
        }
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 362) as a) FROM test t WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND ABS(( nsec_upload::float/1000000000 - duration::float ) / duration::float*100.0 ) > 50;")) {
            stmt.execute();
        }

        // code 110, measurement with missing network_group_type caused by missing entry in table network_type
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 110) as a) FROM test t LEFT JOIN network_type n ON (t.network_type = n.uid) WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND n.type IS NULL;")) {
            stmt.execute();
        }

        // code 111, measurement with missing network_group_type but existing lookup in table network_type
        try(var stmt = conn.prepareStatement("UPDATE audit a SET tags = (select a from ary_sortuniq(tags || 111) as a) FROM test t LEFT JOIN network_type n ON (t.network_type = n.uid) WHERE a.uid=t.uid AND NOT a.checked AND t.status='FINISHED' AND t.network_group_type IS NULL AND n.type IS NOT NULL;")) {
            stmt.execute();
        }
    }

    private static void propagateAuditInformationIntoTests(Connection conn) throws SQLException {
        // update all checked values
        try(var stmt = conn.prepareStatement("UPDATE audit SET checked=true WHERE checked=false;")) {
            stmt.execute();
        }

        // update implausible field
        try(var stmt = conn.prepareStatement("UPDATE test t SET implausible=true FROM audit a WHERE a.uid=t.uid AND array_length(a.tags, 1) >= 1;")) {
            stmt.execute();
        }
    }


    private static void heavyUsers() {
        // Note: This function does nothing! In the ideal world, it should perform the following check written as a Rscript.
        // However, there I the script has been recovered from an old installation of the production server and nobody
        // knows what exactly it does. I placed it here for archival purposes.
        //
        // I think that it tags all tests performed by a mobile application over cellular connection, where a single
        // user made more than 20 tests. But I am not really sure...
        //
        // The Rscript follows:

        /*
        ################################################################################
        # Export from Postgres DB, analyse and store results (back in DB)
        #
        # Fritz Diener, Wolfgang Trutschnig
        # correlate.at
        # 2015-08-27 added nw_area tagging (W)
        # 2015-08-08 initial
        #
        #
        #
        ################################################################################
        # ------------------------------------------------------------------------------
        # 01 configuration
        # ------------------------------------------------------------------------------
        # set database access variables accordingly
        db.user = "rmbt"
        db.pass = "..."
        db.db = "rmbt"
        db.host = "localhost"
        db.port = 5432


        # ------------------------------------------------------------------------------
        # 02 load necessary R packages
        # ------------------------------------------------------------------------------
        library(DBI)
        library(RPostgreSQL)
        library(doBy)
        #library(rgdal)
        # ------------------------------------------------------------------------------
        # 03 access database
        # ------------------------------------------------------------------------------
        CON <- dbConnect("PostgreSQL", user=db.user, password=db.pass, dbname=db.db, host=db.host, port=db.port)

        qry <- "SELECT
              a.uid, a.client_id, a.time, a.public_ip_as_name, a.network_group_type,a.network_group_name, a.network_operator,
              a.speed_download, a.speed_upload, a.ping_shortest,a.client_version, a.model, a.network_country,
              a.geo_long, a.geo_lat,a.public_ip_asn,f.tags
        FROM test a
        LEFT JOIN audit f ON a.uid=f.uid
        WHERE a.status LIKE 'FINISHED'"

        TEST <- dbGetQuery(CON, qry)


        # qry all GPS positions for
        # LOC$test_id is a foreign key to TEST$uid
        #qry <- "
        #SELECT uid, test_id, time, time_ns, accuracy, provider, geo_lat, geo_long FROM geo_location
        #";
        #LOC <- dbGetQuery(CON, qry)


        # ------------------------------------------------------------------------------
        # 04 filter to implausible combinations nw_area_code versus GPS
        # ------------------------------------------------------------------------------


        #filter to data with existing nw_area_codes
        #A<-TEST
        #A<-subset(A,select=c(uid,client_id,time,geo_long,geo_lat,country_location,nw_area_codes))  #all columns needed
        #A<-subset(A,A$nw_area_codes!="")
        #A<-subset(A,is.na(A$geo_lat)==0 & A$country_location=="CZ")

        #add cartesian coordinates to TEST data -> allow exact distance calculation
        #S<-cbind(A$geo_long,A$geo_lat)
        #SLAM<-project(S, proj="+proj=lcc +lat_1=47 +lat_2=52 +lon_0=15 +lat_0=49.5", inv = FALSE,  use_ob_tran=FALSE)
        #A$XCOOR<-SLAM[,1]
        #A$YCOOR<-SLAM[,2]

        #filter LOC to relevant data and add cartesian coordinates
        #LOC<-subset(LOC,select=c(uid, test_id,time,provider,geo_lat,geo_long)) # all colums needed
        #LOC<-subset(LOC,LOC$test_id %in% A$uid)
        #S<-cbind(LOC$geo_long,LOC$geo_lat)
        #SLAM<-project(S,proj="+proj=lcc +lat_1=47 +lat_2=52 +lon_0=15 +lat_0=49.5", inv = FALSE, use_ob_tran=FALSE)
        #LOC$XCOOR<-SLAM[,1]
        #LOC$YCOOR<-SLAM[,2]

        #in case of several nw_area_codes take first one
        #A$first_nw_area_code<-A$nw_area_codes
        #temp<-grepl(";",A$nw_area_codes)
        #indices<-which(temp==TRUE)
        #for(i in indices){
        #  x<-strsplit(A$nw_area_codes[i],split=";")[[1]]
        #  A$first_nw_area_code[i]<-x[1]
        #}

        #add nw_area_codes to LOC data and calculate median per nw_area_code
        #A_mini<-subset(A,select=c(uid,client_id,first_nw_area_code,time))
        #A_mini<-A_mini[order(A_mini$uid),]
        #L1<-merge(LOC,A_mini,by.x = "test_id", by.y="uid")

        #AC<-summaryBy(data=L1,geo_long+geo_lat+XCOOR+YCOOR~first_nw_area_code,FUN=c(length,median))
        #AC<-
        #subset(AC,select=c(first_nw_area_code,XCOOR.length,geo_long.median,geo_lat.median,XCOOR.median,YCOOR.median))
        #names(AC)[2:6]<-c("nw_area_count","nw_area_geo_long","nw_area_geo_lat","nw_area_XCOOR","nw_area_YCOOR")

        #add median_coords to A and calculate distances
        #tempnam<-c(names(A),names(AC)[2:ncol(AC)])
        #A1<-merge(A,AC)
        #A1<-subset(A1,select=tempnam)
        #A1$dist_nw_area<-sqrt((A1$XCOOR-A1$nw_area_XCOOR)^2 + (A1$YCOOR-A1$nw_area_YCOOR)^2)

        #set tag if distance is more than 50km and there are at least 20 recorded GPS coordinates
        #Bad<-subset(A1,A1$dist_nw_area>50000 & A1$nw_area_count>=20)
        #bad_uids<-Bad$uid



        #test heavy users
        A<- TEST
        A<-A[ order ( A$uid ),]
        A<- subset (A, A$tags =="{}")
        A$temp <-as.numeric ( substr ( A$time ,12 ,13))
        A$temp2 <- ifelse ( A$temp <15 ,"V","N")
        A$ymdh <- paste ( substr (A$time ,1 ,10) , A$temp2 , sep ="")
        A<- subset (A, select =-c(temp , temp2 ))
        A$hexnam <-NA
        A$hexnam <- ifelse (is.na( A$hexnam ),"HNA", A$hexnam )
        M<- subset (A, A$network_group_type == "MOBILE")
        M$operator <-NA
        # add operator info by hand whenever available
        M$operator <- ifelse ( M$public_ip_asn ==5610 & (is.na( M$network_operator )==1 |
        M$network_operator =="230-02") ,"O2", M$operator )
        M$operator <- ifelse ( M$public_ip_asn ==13036 & (is.na( M$network_operator )==1 |
        M$network_operator =="230-01") ,"TM", M$operator )
        M$operator <- ifelse ( M$public_ip_asn ==16019 & (is.na( M$network_operator )==1 |
        M$network_operator =="230-03") ,"VF", M$operator )
        M<- subset (M,is.na( M$operator )==0)
        HH<-summaryBy(data=M, uid~ymdh+hexnam+client_id+network_group_type+
        network_group_name+public_ip_as_name,FUN=c(length))
        names(HH)[7] <- "count"
        namen <- names (M)
        M<- merge (M,HH)
        M<- subset (M, select =c(namen ,"count"))
        M<-M[ order ( -M$count ),]
        # heavy user aggregation - calculate median of all relevant quantities
        #AC <- summaryBy ( data =M, speed_download + speed_upload + ping_shortest + count + geo_long +
        #geo_lat ~ ymdh + hexnam + client_id + network_group_type +
        #network_group_name + client_version + platform + model +
        #operator + network_country , FUN =c( median ), keep.names = TRUE )
        Bad<-subset(M,M$count>=10)
        bad_uids<-Bad$uid
        #print(Bad)

        # ------------------------------------------------------------------------------
        # 05 save results per uid back into database
        # ------------------------------------------------------------------------------

        ## looping tags back to database
        for (i in 1:length(bad_uids)){
          upd <- paste(sep="","UPDATE audit a
          SET tags = (select a from ary_sortuniq(tags || 444) as a)
          WHERE a.uid=",bad_uids[i]," AND NOT a.checked;")
          rs <- dbSendQuery(CON,upd)
        }
        dbDisconnect(CON)
         */
    }

    public static void nwaTagging() {
        // Same as the function heavy users. This does nothing. It's here for archival purposes, because nobody
        // understands as of now, if this is not important. It's a sad state of reality, but what else can we do... :)
        //
        // The Rscript

        /*
        ################################################################################
        # Export from Postgres DB, analyse and store results (back in DB)
        #
        # Fritz Diener, Wolfgang Trutschnig
        # correlate.at
        # 2015-08-27 added nw_area tagging (W)
        # 2015-08-08 initial
        #
        #
        #
        ################################################################################
        # ------------------------------------------------------------------------------
        # 01 configuration
        # ------------------------------------------------------------------------------
        # set database access variables accordingly
        db.user = "rmbt"
        db.pass = "..."
        db.db = "rmbt"
        db.host = "localhost"
        db.port = 5432


        # ------------------------------------------------------------------------------
        # 02 load necessary R packages
        # ------------------------------------------------------------------------------
        library(DBI)
        library(RPostgreSQL)
        library(doBy)
        library(rgdal)
        # ------------------------------------------------------------------------------
        # 03 access database
        # ------------------------------------------------------------------------------
        CON <- dbConnect("PostgreSQL", user=db.user, password=db.pass, dbname=db.db, host=db.host, port=db.port)

        qry <- "SELECT
          a.uid, a.client_id, a.time, a.geo_long, a.geo_lat, a.country_location
          ,translate( ARRAY(SELECT DISTINCT a.network_operator||'-'||c.area_code FROM cell_location c WHERE
        c.test_id=a.uid AND a.network_operator LIKE '230%')::text, ',{}', ';') as nw_area_codes
        FROM test a
        LEFT JOIN audit f ON a.uid=f.uid
        WHERE a.status LIKE 'FINISHED'
        AND a.network_operator LIKE '230%'"

        TEST <- dbGetQuery(CON, qry)


        # qry all GPS positions for
        # LOC$test_id is a foreign key to TEST$uid
        qry <- "
        SELECT uid, test_id, time, time_ns, accuracy, provider, geo_lat, geo_long FROM geo_location
        ";
        LOC <- dbGetQuery(CON, qry)


        # ------------------------------------------------------------------------------
        # 04 filter to implausible combinations nw_area_code versus GPS
        # ------------------------------------------------------------------------------


        #filter to data with existing nw_area_codes
        A<-TEST
        A<-subset(A,select=c(uid,client_id,time,geo_long,geo_lat,country_location,nw_area_codes))  #all columns needed
        A<-subset(A,A$nw_area_codes!="")
        A<-subset(A,is.na(A$geo_lat)==0 & A$country_location=="CZ")

        #add cartesian coordinates to TEST data -> allow exact distance calculation
        S<-cbind(A$geo_long,A$geo_lat)
        SLAM<-project(S, proj="+proj=lcc +lat_1=47 +lat_2=52 +lon_0=15 +lat_0=49.5", inv = FALSE,  use_ob_tran=FALSE)
        A$XCOOR<-SLAM[,1]
        A$YCOOR<-SLAM[,2]

        #filter LOC to relevant data and add cartesian coordinates
        LOC<-subset(LOC,select=c(uid, test_id,time,provider,geo_lat,geo_long)) # all colums needed
        LOC<-subset(LOC,LOC$test_id %in% A$uid)
        S<-cbind(LOC$geo_long,LOC$geo_lat)
        SLAM<-project(S,proj="+proj=lcc +lat_1=47 +lat_2=52 +lon_0=15 +lat_0=49.5", inv = FALSE, use_ob_tran=FALSE)
        LOC$XCOOR<-SLAM[,1]
        LOC$YCOOR<-SLAM[,2]

        #in case of several nw_area_codes take first one
        A$first_nw_area_code<-A$nw_area_codes
        temp<-grepl(";",A$nw_area_codes)
        indices<-which(temp==TRUE)
        for(i in indices){
          x<-strsplit(A$nw_area_codes[i],split=";")[[1]]
          A$first_nw_area_code[i]<-x[1]
        }

        #add nw_area_codes to LOC data and calculate median per nw_area_code
        A_mini<-subset(A,select=c(uid,client_id,first_nw_area_code,time))
        A_mini<-A_mini[order(A_mini$uid),]
        L1<-merge(LOC,A_mini,by.x = "test_id", by.y="uid")

        AC<-summaryBy(data=L1,geo_long+geo_lat+XCOOR+YCOOR~first_nw_area_code,FUN=c(length,median))
        AC<-
        subset(AC,select=c(first_nw_area_code,XCOOR.length,geo_long.median,geo_lat.median,XCOOR.median,YCOOR.median))
        names(AC)[2:6]<-c("nw_area_count","nw_area_geo_long","nw_area_geo_lat","nw_area_XCOOR","nw_area_YCOOR")

        #add median_coords to A and calculate distances
        tempnam<-c(names(A),names(AC)[2:ncol(AC)])
        A1<-merge(A,AC)
        A1<-subset(A1,select=tempnam)
        A1$dist_nw_area<-sqrt((A1$XCOOR-A1$nw_area_XCOOR)^2 + (A1$YCOOR-A1$nw_area_YCOOR)^2)

        #set tag if distance is more than 50km and there are at least 20 recorded GPS coordinates
        Bad<-subset(A1,A1$dist_nw_area>50000 & A1$nw_area_count>=20)
        bad_uids<-Bad$uid


        # ------------------------------------------------------------------------------
        # 05 save results per uid back into database
        # ------------------------------------------------------------------------------

        ## looping tags back to database
        for (i in 1:length(bad_uids)){
          upd <- paste(sep="","UPDATE audit a
          SET tags = (select a from ary_sortuniq(tags || 411) as a)
          WHERE a.uid=",bad_uids[i]," AND NOT a.checked;")
          rs <- dbSendQuery(CON,upd)
        }
        dbDisconnect(CON)
         */
    }
}
