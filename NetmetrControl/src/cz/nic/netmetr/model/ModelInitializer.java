package cz.nic.netmetr.model;

import cz.nic.netmetr.boot.DbConnection;

public class ModelInitializer {
    public static void init(DbConnection dbConnection) {
        NetworkType.loadAndValidateEnumConsistencyWithDB(dbConnection);
    }
}
