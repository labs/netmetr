package cz.nic.netmetr.model;

import com.google.common.graph.Network;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.nic.netmetr.boot.DbConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class corresponds to table public.network_type located in the database. It's implemented as an ordinary table,
 * but in practice we use it as an enum. This code therefore loads the table only once and keeps it in memory.
 * <p>
 * The values listed here are the same values located in the database. To ensure consistency, we check that they match
 * exactly.
 */
public enum NetworkType {
    _2G_GSM(1),
    _2G_EDGE(2),
    _2G_CDMA(4),
    _2G_EVDO_0(5),
    _2G_EVDO_A(6),
    _2G_1xRTT(7),
    _2G_EHRPD(14),
    _2G_IDEN(11),
    _2G_EVDO_B(12),

    _3G_UMTS(3),
    _3G_HSDPA(8),
    _3G_HSUPA(9),
    _3G_HSPA(10),
    _3G_HSPA_PLUS(15),

    _4G_LTE(13),
    _4G_LTE_CA(19),
    _4G_LTE_ADVANCED(50),
    _4G_LTE_ADVANCED_PRO(51),

    _5G_NR_SA(20),
    _5G_NR_NSA(21),
    _5G_NR_NSA_MMWAVE(22),
    _5G_NR_SA_MMWAVE(23),

    CLI(97),
    LAN(98),
    WLAN(99),

    // groups
    _2G_3G(101),
    _3G_4G(102),
    _2G_4G(103),
    _2G_3G_4G(104),
    _2G_3G_4G_5G(108),
    _3G_4G_5G(109),
    _4G_5G(110),
    _2G(111),
    _3G(112),
    _4G(113),
    _5G(114),
    
    CELLULAR_ANY(105), // should be the same as all cellular groups at once, only stable with new technologies appearing
    CELLULAR_NO_SIGNAL(200),  // this is mobile network but with no data about signal
    //Ethernet(106),   --- FIXME was in the Java code previously, but not in the DB? Is it really used?
    // Bluetooth(107), --- FIXME was in the Java code previously, but not in the DB? Is it really used?
    ;

    public int id;
    
    private static Map<Integer, DbNetworkType> valueMap = null;
    private static Map<Integer, DbNetworkType> loadDataFromDB(DbConnection dbConnection) {
        HashMap<Integer, DbNetworkType> valueMap = new HashMap<>(NetworkType.values().length);
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        try (Connection conn = dbConnection.getConnection();
             PreparedStatement statement = conn.prepareStatement("select row_to_json(network_type.*) val from network_type;");
             ResultSet rs = statement.executeQuery()) {
        
            while (rs.next()) {
                DbNetworkType dbtype = gson.fromJson(rs.getString(1), DbNetworkType.class);
                valueMap.put(dbtype.uid, dbtype);
            }
        } catch (SQLException throwables) {
            throw new IllegalStateException("Failed to query the database.", throwables);
        }
        return Collections.unmodifiableMap(valueMap);
    }

    public static void loadAndValidateEnumConsistencyWithDB(DbConnection dbConnection) throws IllegalStateException {
        // check for conflicting IDs
        for (NetworkType nt1 : NetworkType.values()) {
            for (NetworkType nt2 : NetworkType.values()) {
                if (nt1.id == nt2.id && nt1 != nt2) {
                    throw new IllegalStateException("There are two NetworkTypes with conflicting id " + nt1.id);
                }
            }
        }

        if (valueMap == null) valueMap = loadDataFromDB(dbConnection);

        for (NetworkType nt : NetworkType.values()) {
            if (!valueMap.containsKey(nt.id)) {
                throw new IllegalStateException("NetworkType enum uid=" + nt.id + " could not be found in the database!");
            }
        }
        
        for (DbNetworkType dbnt : valueMap.values()) {
            // will crash if the value does not exist
            NetworkType _ignored = NetworkType.fromId(dbnt.uid);
        }
    }

    public String printableName() {
        assert valueMap != null;
        return valueMap.get(this.id).name;
    }

    public String groupName() {
        assert valueMap != null;
        return valueMap.get(this.id).groupName;
    }

    public boolean isGroup() {
        assert valueMap != null;
        return valueMap.get(this.id).aggregate != null && !valueMap.get(this.id).aggregate.isEmpty();
    }

    private static Map<NetworkType, List<NetworkType>> groupExpansionIndex = null;
    public List<NetworkType> expandGroup() {
        assert valueMap != null;
        assert this.isGroup();

        // technically, there is nothing preventing the groups from being recursive. Let's just resolve the first level
        // of recursion and ignore it completely.

        // build lookup index
        if (groupExpansionIndex == null) {
            synchronized (NetworkType.class) {
                if (groupExpansionIndex == null) {
                    Set<String> emptySet = Set.of();
                    groupExpansionIndex = valueMap.entrySet().stream().collect(Collectors.toUnmodifiableMap(
                            entry -> NetworkType.fromId(entry.getKey()),
                            entry -> valueMap.values().stream()
                                    .filter(o -> o.groupName != null && (entry.getValue().aggregate == null ? emptySet : entry.getValue().aggregate).contains(o.groupName))
                                    .map(o -> NetworkType.fromId(o.uid))
                                    .collect(Collectors.toUnmodifiableList())
                    ));
                }
            }
        }

        return groupExpansionIndex.get(this);
    }

    private static Map<Integer, NetworkType> idLookupIndex = null;
    public static NetworkType fromId(int uid) {
        // construct index if it does not exist
        if (idLookupIndex == null) {
            synchronized (NetworkType.class) {
                if (idLookupIndex == null) {
                    idLookupIndex = Arrays.stream(NetworkType.values()).collect(Collectors.toUnmodifiableMap(
                            nt -> nt.id,
                            nt -> nt
                    ));
                }
            }
        }

        if (idLookupIndex.containsKey(uid))
            return idLookupIndex.get(uid);
        else
            throw new IllegalArgumentException("There is no NetworkType with uid=" + uid);
    }

    private static Map<String, NetworkType> nameLookupIndex = null;
    public static NetworkType fromName(String name) throws NoSuchElementException {
        assert valueMap != null;
        assert name != null;

        // construct index if it did not exist previously
        if (nameLookupIndex == null) {
            synchronized (NetworkType.class) {
                if (nameLookupIndex == null) {
                    nameLookupIndex = valueMap.entrySet().stream()
                            .collect(Collectors.toUnmodifiableMap(
                                    o -> o.getValue().name,
                                    o -> NetworkType.fromId(o.getKey())
                            ));
                }
            }
        }

        if (nameLookupIndex.containsKey(name))
            return nameLookupIndex.get(name);
        else
            throw new NoSuchElementException("The network type with name " + name + " does not exist!");
    }

    NetworkType(int id) {
        this.id = id;
    }

    /**
     * Class for representing data read from the database.
     */
    private static class DbNetworkType {
        int uid;
        String name;
        String groupName;
        Set<String> aggregate;
        String type;
        long technologyOrder;
    }
}
