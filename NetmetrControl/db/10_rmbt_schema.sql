--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 10.5

-- Started on 2018-11-06 11:12:21 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2283 (class 1247 OID 73101)
-- Name: mobiletech; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.mobiletech AS ENUM (
    'unknown',
    '2G',
    '3G',
    '4G',
    'mixed'
);

ALTER TYPE public.mobiletech OWNER TO rmbt;


CREATE TYPE public.speed_graph_pt AS (
  upload boolean,
  thread smallint,
  "time" bigint,
  bytes bigint
);

ALTER TYPE public.speed_graph_pt OWNER TO rmbt;
--
-- TOC entry 2286 (class 1247 OID 73112)
-- Name: qostest; Type: TYPE; Schema: public; Owner: rmbt
--

CREATE TYPE public.qostest AS ENUM (
    'website',
    'http_proxy',
    'non_transparent_proxy',
    'dns',
    'tcp',
    'udp',
    'traceroute',
    'voip',
    'jitter'
);


ALTER TYPE public.qostest OWNER TO rmbt;

--
-- TOC entry 1818 (class 1255 OID 73316)
-- Name: rmbt_fill_open_uuid(); Type: FUNCTION; Schema: public; Owner: rmbt
--

CREATE FUNCTION public.rmbt_fill_open_uuid() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
 _t RECORD;
 _uuid uuid;
BEGIN

FOR _t IN SELECT uid,client_id,time FROM test WHERE open_uuid IS NULL ORDER BY uid LOOP
    SELECT INTO _uuid open_uuid FROM test WHERE client_id=_t.client_id AND (_t.time - INTERVAL '4 hours' < time) AND uid<_t.uid ORDER BY uid DESC LIMIT 1;
    IF (_uuid IS NULL) THEN
        _uuid = uuid_generate_v4();
    END IF;
    UPDATE test SET open_uuid=_uuid WHERE uid=_t.uid;
END LOOP;

END;$$;


ALTER FUNCTION public.rmbt_fill_open_uuid() OWNER TO rmbt;

--
-- TOC entry 1819 (class 1255 OID 73317)
-- Name: rmbt_get_next_test_slot(bigint); Type: FUNCTION; Schema: public; Owner: rmbt
--

CREATE FUNCTION public.rmbt_get_next_test_slot(_test_id bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  _slot integer;
  _count integer;
  _server_id integer;
BEGIN
SELECT server_id FROM test WHERE uid = _test_id INTO _server_id;
_slot := EXTRACT(EPOCH FROM NOW())::int - 2;
_count := 100;
WHILE _count >= 5 LOOP
  _slot := _slot + 1;
  SELECT COUNT(uid) FROM test WHERE test_slot = _slot AND server_id=_server_id INTO _count;
END LOOP;
  UPDATE test SET test_slot = _slot WHERE uid = _test_id;
RETURN _slot;
END;
$$;


ALTER FUNCTION public.rmbt_get_next_test_slot(_test_id bigint) OWNER TO rmbt;

--
-- TOC entry 1820 (class 1255 OID 73318)
-- Name: rmbt_get_sync_code(uuid); Type: FUNCTION; Schema: public; Owner: rmbt
--

CREATE FUNCTION public.rmbt_get_sync_code(client_uuid uuid) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE 
	return_code VARCHAR;
	count integer;
	
BEGIN
count := 0;
SELECT sync_code INTO return_code FROM client WHERE client.uuid = CAST(client_uuid AS UUID) AND sync_code_timestamp + INTERVAL '1 month' > NOW();

if (return_code ISNULL OR char_length(return_code) < 1) then
	LOOP
		return_code := random_sync_code(12);
		BEGIN
			UPDATE client
			SET sync_code = return_code,
			sync_code_timestamp = NOW()
			WHERE client.uuid = CAST(client_uuid AS UUID);
			return return_code;
		EXCEPTION WHEN unique_violation THEN
			-- return NULL when tried 10 times;
			if (count > 10) then
				return NULL;
			end if;
			count := count + 1;
		END;
	END LOOP;
else 
	return return_code;
end if;
END;
$$;


ALTER FUNCTION public.rmbt_get_sync_code(client_uuid uuid) OWNER TO rmbt;

--
-- TOC entry 1821 (class 1255 OID 73319)
-- Name: rmbt_random_sync_code(integer); Type: FUNCTION; Schema: public; Owner: rmbt
--

CREATE FUNCTION public.rmbt_random_sync_code(integer) RETURNS text
    LANGUAGE sql
    AS $_$

    select upper(
        substring(
            (
                SELECT string_agg(md5(random()::TEXT), '')
                FROM generate_series(1, CEIL($1 / 32.)::integer)
                ),
        (33-$1))
    );

$_$;


ALTER FUNCTION public.rmbt_random_sync_code(integer) OWNER TO rmbt;

--
-- TOC entry 1822 (class 1255 OID 73320)
-- Name: rmbt_set_provider_from_as(bigint); Type: FUNCTION; Schema: public; Owner: rmbt
--

CREATE FUNCTION public.rmbt_set_provider_from_as(_test_id bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
  _asn bigint;
  _rdns character varying;
  _provider_id integer;
  _provider_name character varying;
BEGIN

SELECT
  ap.provider_id,
  p.shortname
  FROM test t
  JOIN as2provider ap
  ON t.public_ip_asn=ap.asn 
  AND (ap.dns_part IS NULL OR t.public_ip_rdns ILIKE ap.dns_part /*Case insensitive regexp, DJ per #235:*/ OR t.public_ip_rdns ~* ap.dns_part)
  JOIN provider p
  ON p.uid = ap.provider_id
  WHERE t.uid = _test_id
  ORDER BY dns_part IS NOT NULL DESC
  LIMIT 1
  INTO _provider_id, _provider_name;

IF _provider_id IS NOT NULL THEN
  UPDATE test SET provider_id = _provider_id WHERE uid = _test_id;
  RETURN _provider_name;
ELSE
  RETURN NULL;
END IF;

END;
$$;


ALTER FUNCTION public.rmbt_set_provider_from_as(_test_id bigint) OWNER TO rmbt;

--
-- Name: random_sync_code(integer); Type: FUNCTION; Schema: public; Owner: rmbt
--

CREATE FUNCTION public.random_sync_code(integer) RETURNS text
LANGUAGE sql
AS $_$

select upper(
         substring(
           (
           SELECT string_agg(md5(random()::TEXT), '')
           FROM generate_series(1, CEIL($1 / 32.)::integer)
           ),
           (33-$1))
           );

$_$;


ALTER FUNCTION public.random_sync_code(integer) OWNER TO rmbt;

--
-- Name: _final_median(anyarray); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public._final_median(anyarray) RETURNS double precision
LANGUAGE sql IMMUTABLE
AS $_$
WITH q AS
    (
    SELECT val
    FROM unnest($1) val
    WHERE VAL IS NOT NULL
    ORDER BY 1
    ),
     cnt AS
    (
    SELECT COUNT(*) AS c FROM q
    )
SELECT AVG(val)::float8
FROM
     (
     SELECT val FROM q
     LIMIT  2 - MOD((SELECT c FROM cnt), 2)
     OFFSET GREATEST(CEIL((SELECT c FROM cnt) / 2.0) - 1,0)
     ) q2;
$_$;


ALTER FUNCTION public._final_median(anyarray) OWNER TO rmbt;


CREATE AGGREGATE public.median(anyelement) (
  SFUNC = array_append,
  STYPE = anyarray,
  INITCOND = '{}',
  FINALFUNC = public._final_median
);


ALTER AGGREGATE public.median(anyelement) OWNER TO rmbt;


--
-- TOC entry 1275 (class 1255 OID 73382)
-- Name: trigger_test(); Type: FUNCTION; Schema: public; Owner: rmbt
--

CREATE FUNCTION public.trigger_test() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    _tmp_uuid uuid;
    _tmp_uid integer;
    _tmp_time timestamp;
    _tmp_network_group_name VARCHAR;
    _mcc_sim VARCHAR;
    _mcc_net VARCHAR;
    _min_accuracy CONSTANT integer := 3000;

    v_old_data TEXT;
    v_new_data TEXT;

BEGIN

    IF ((TG_OP = 'INSERT' OR NEW.speed_download IS DISTINCT FROM OLD.speed_download) AND NEW.speed_download > 0) THEN
        NEW.speed_download_log=(log(NEW.speed_download::double precision/10))/4;
    END IF;
    IF ((TG_OP = 'INSERT' OR NEW.speed_upload IS DISTINCT FROM OLD.speed_upload) AND NEW.speed_upload > 0) THEN
        NEW.speed_upload_log=(log(NEW.speed_upload::double precision/10))/4;
    END IF;
    IF ((TG_OP = 'INSERT' OR NEW.ping_shortest IS DISTINCT FROM OLD.ping_shortest) AND NEW.ping_shortest > 0) THEN
        NEW.ping_shortest_log=(log(NEW.ping_shortest::double precision/1000000))/3;
        SELECT INTO NEW.ping_median floor(median(coalesce(value_server,value))) FROM ping WHERE NEW.uid = test_id;
        NEW.ping_median_log=(log(NEW.ping_median::double precision/1000000))/3;
        IF (NEW.ping_median IS NULL) THEN
             NEW.ping_median = NEW.ping_shortest;
        END IF;
    END IF;

    IF (TG_OP = 'INSERT' OR NEW.location IS DISTINCT FROM OLD.location) THEN
        IF (NEW.location IS NULL OR NEW.geo_accuracy > _min_accuracy) THEN
            NEW.zip_code_geo = NULL;
            NEW.country_location = NULL;
        ELSE
            SELECT INTO NEW.zip_code_geo plz_4
            FROM plz2001
            WHERE NEW.location && the_geom AND Within(NEW.location, the_geom)
            LIMIT 1;
            IF (NEW.zip_code IS NULL) THEN
        NEW.zip_code = NEW.zip_code_geo;
            END IF;

            IF (NEW.zip_code_geo IS NOT NULL) THEN
                NEW.country_location = 'AT'; -- plz2001 is more accurate for AT than ne_50m_admin_0_countries
            ELSE
                SELECT INTO NEW.country_location iso_a2
                FROM ne_50m_admin_0_countries
                WHERE NEW.location && the_geom AND Within(NEW.location, the_geom) AND char_length(iso_a2)=2
                LIMIT 1;
            END IF;
        END IF;
    END IF;

    IF (TG_OP = 'INSERT'
        OR NEW.network_sim_operator IS DISTINCT FROM OLD.network_sim_operator
        OR NEW.network_operator IS DISTINCT FROM OLD.network_operator
        OR NEW.time IS DISTINCT FROM OLD.time
        ) THEN

            IF (NEW.network_sim_operator IS NULL OR NEW.network_operator IS NULL) THEN
                NEW.roaming_type = NULL;
            ELSE
		IF (NEW.network_sim_operator = NEW.network_operator) THEN
			NEW.roaming_type = 0; -- no roaming
		ELSE
                    _mcc_sim := split_part(NEW.network_sim_operator, '-', 1);
                    _mcc_net := split_part(NEW.network_operator, '-', 1);
                    IF (_mcc_sim = _mcc_net) THEN
                        NEW.roaming_type = 1;  -- national roaming
                    ELSE
			NEW.roaming_type = 2;  -- international roaming
		    END IF;
                END IF;
            END IF;

            IF ((NEW.roaming_type IS NULL AND NEW.country_location != 'AT') OR NEW.roaming_type = 2) THEN -- not for foreign networks
                NEW.mobile_provider_id = NULL;
            ELSE
                SELECT INTO NEW.mobile_provider_id provider_id FROM mccmnc2provider
                    WHERE mcc_mnc_sim = NEW.network_sim_operator
                    AND (valid_from IS NULL OR valid_from <= NEW.time) AND (valid_to IS NULL OR valid_to >= NEW.time)
                    AND (mcc_mnc_network IS NULL OR mcc_mnc_network = NEW.network_operator)
                    ORDER BY mcc_mnc_network NULLS LAST
                    LIMIT 1;
            END IF;
    END IF;

     IF ((TG_OP = 'UPDATE' AND OLD.STATUS='STARTED' AND NEW.STATUS='FINISHED') 
          AND (NEW.time > (now() - INTERVAL '5 minutes'))) THEN -- update only new entries, skip old entries
          IF (NEW.network_operator is not NULL) THEN
            SELECT INTO NEW.mobile_network_id COALESCE(n.mapped_uid,n.uid) 
                FROM mccmnc2name n
                WHERE NEW.network_operator=n.mccmnc 
                AND (n.valid_from is null OR n.valid_from <= NEW.time)
                AND (n.valid_to is null or n.valid_to  >= NEW.time)
                AND use_for_network=TRUE
                ORDER BY n.uid NULLS LAST
                LIMIT 1;
          END IF;
          
          IF (NEW.network_sim_operator is not NULL) THEN
          SELECT INTO NEW.mobile_sim_id COALESCE(n.mapped_uid,n.uid) 
                FROM mccmnc2name n
                WHERE NEW.network_sim_operator=n.mccmnc 
                AND (n.valid_from is null OR n.valid_from <= NEW.time)
                AND (n.valid_to is null or n.valid_to  >= NEW.time)
                AND (NEW.network_sim_operator=n.mcc_mnc_network_mapping OR n.mcc_mnc_network_mapping is NULL)
                AND use_for_sim=TRUE
                ORDER BY n.uid NULLS LAST
                LIMIT 1;
          END IF;
          
     END IF;


    IF ((TG_OP = 'UPDATE')  AND (NEW.time > (now() - INTERVAL '5 minutes')) AND NEW.network_type=97/*CLI*/ AND NEW.deleted=FALSE) THEN
        NEW.deleted=TRUE;
        NEW.comment='Exclude CLI per #211';
    END IF;    

    IF ((TG_OP = 'UPDATE' AND OLD.STATUS='STARTED' AND NEW.STATUS='FINISHED')
      AND (NEW.time > (now() - INTERVAL '5 minutes')) 
      AND NEW.model='SM-N9005' 
      AND NEW.geo_provider='network') THEN
         NEW.geo_accuracy = 99999;
    END IF;
 
    IF ((TG_OP = 'UPDATE' AND OLD.STATUS='STARTED' AND NEW.STATUS='FINISHED' )  
      AND (NEW.time > (now() - INTERVAL '5 minutes')) 
      AND NEW.geo_accuracy is not null
      AND NEW.geo_accuracy <= 10000 ) THEN
      
     SELECT INTO _tmp_uid uid FROM test
        WHERE client_id=NEW.client_id
        AND (NEW.time - INTERVAL '24 hours' < time)
        AND geo_accuracy is not null 
        AND geo_accuracy <= 10000
        ORDER BY uid DESC LIMIT 1;

      IF _tmp_uid is not null THEN
        SELECT INTO NEW.dist_prev ST_Distance(t.location,NEW.location) 
        FROM test t WHERE uid=_tmp_uid;
        IF NEW.dist_prev is not null THEN
            SELECT INTO _tmp_time time FROM test t
            WHERE uid=_tmp_uid;
            NEW.speed_prev = NEW.dist_prev/EXTRACT(EPOCH FROM (NEW.time - _tmp_time));
        END IF;
      END IF;
    END IF;

    IF ((NEW.network_type > 0) AND (NEW.time > (now() - INTERVAL '5 minutes'))) THEN
       SELECT INTO NEW.network_group_name group_name FROM network_type 
          WHERE uid = NEW.network_type
          LIMIT 1;
       SELECT INTO NEW.network_group_type type FROM network_type 
          WHERE uid = NEW.network_type
          LIMIT 1;
    END IF;

    IF (TG_OP = 'INSERT') THEN
        SELECT INTO _tmp_uuid open_uuid FROM test
        WHERE client_id=NEW.client_id
        AND (now() - INTERVAL '4 hours' < time)
        AND (now()::date = time::date)
        ORDER BY uid DESC LIMIT 1;
        IF (_tmp_uuid IS NULL) THEN
            _tmp_uuid = uuid_generate_v4();
        END IF;
        NEW.open_uuid = _tmp_uuid;
    END IF;

     IF (TG_OP = 'UPDATE' AND OLD.STATUS='STARTED' AND NEW.STATUS='FINISHED') THEN
       SELECT INTO _tmp_network_group_name network_group_name FROM test
          WHERE OLD.open_uuid = open_uuid AND
          OLD.uid != uid AND
          status = 'FINISHED' 
          ORDER BY uid DESC LIMIT 1;
       IF (_tmp_network_group_name IS NOT NULL AND
          ((_tmp_network_group_name = 'WLAN' AND NEW.network_group_name != 'WLAN') OR  
          (_tmp_network_group_name != 'WLAN' AND NEW.network_group_name = 'WLAN'))) THEN
             NEW.open_uuid=uuid_generate_v4();
        END IF;
         
    END IF;

  


    IF (TG_OP = 'UPDATE' AND OLD.STATUS='STARTED' AND NEW.STATUS='FINISHED') THEN
        NEW.timestamp = now();
        
        SELECT INTO NEW.location_max_distance
          round(|/((st_xmax(st_extent(location))-st_xmin(st_extent(location)))^2+(st_ymax(st_extent(location))-st_ymin(st_extent(location)))^2))
          FROM geo_location
          WHERE test_id=NEW.uid;
    END IF;

    IF ((NEW.time > (now() - INTERVAL '5 minutes')) -- update only new entries, skip old entries
       AND ((NEW.geo_lat>49.1 or NEW.geo_lat<46.3) OR (NEW.geo_long>17.2 or NEW.geo_long<9.5)) -- location outside of the Austria grid
       AND ( 
           (NEW.network_operator ILIKE '232%') -- test with Austrian mobile network operator
           OR
           ((NEW.network_type BETWEEN 98/*LAN*/ AND 99/*WLAN*/) AND (NEW.country_geoip ILIKE 'AT')) -- WLAN or LAN test with an Austrian IP-Address
           )
       AND FALSE)
    THEN NEW.status='UPDATE ERROR'; NEW.comment='Automatic update error due to invalid location per #272';
    END IF;

    IF ((NEW.time > (now() - INTERVAL '5 minutes')) -- update only new entries, skip old entries
       AND (NEW.model='unknown') -- model is 'unknown'
       ) 
    THEN NEW.status='UPDATE ERROR'; NEW.comment='Automatic update error due to unknown model per #356';
    END IF;


    RETURN NEW;

  

END;$$;


ALTER FUNCTION public.trigger_test() OWNER TO rmbt;

--
-- TOC entry 210 (class 1259 OID 73405)
-- Name: advertised_speed_option; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.advertised_speed_option (
    uid integer NOT NULL,
    name character varying(200) NOT NULL,
    min_speed_down_kbps integer,
    max_speed_down_kbps integer,
    min_speed_up_kbps integer,
    max_speed_up_kbps integer,
    enabled boolean DEFAULT true
);


ALTER TABLE public.advertised_speed_option OWNER TO rmbt;

--
-- TOC entry 211 (class 1259 OID 73409)
-- Name: advertised_speed_option_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.advertised_speed_option_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.advertised_speed_option_uid_seq OWNER TO rmbt;

--
-- TOC entry 4442 (class 0 OID 0)
-- Dependencies: 211
-- Name: advertised_speed_option_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.advertised_speed_option_uid_seq OWNED BY public.advertised_speed_option.uid;


--
-- TOC entry 212 (class 1259 OID 73411)
-- Name: device_map; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.device_map (
    uid integer NOT NULL,
    codename character varying(200),
    fullname character varying(200),
    source character varying(200),
    "timestamp" timestamp with time zone
);


ALTER TABLE public.device_map OWNER TO rmbt;

--
-- TOC entry 213 (class 1259 OID 73417)
-- Name: android_device_map_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.android_device_map_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.android_device_map_uid_seq OWNER TO rmbt;

--
-- TOC entry 4444 (class 0 OID 0)
-- Dependencies: 213
-- Name: android_device_map_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.android_device_map_uid_seq OWNED BY public.device_map.uid;


--
-- TOC entry 214 (class 1259 OID 73419)
-- Name: as2provider; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.as2provider (
    uid integer NOT NULL,
    asn bigint,
    dns_part character varying(200),
    provider_id integer
);


ALTER TABLE public.as2provider OWNER TO rmbt;

--
-- TOC entry 215 (class 1259 OID 73422)
-- Name: as2provider_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.as2provider_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.as2provider_uid_seq OWNER TO rmbt;

--
-- TOC entry 4446 (class 0 OID 0)
-- Dependencies: 215
-- Name: as2provider_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.as2provider_uid_seq OWNED BY public.as2provider.uid;


--
-- TOC entry 216 (class 1259 OID 73424)
-- Name: audit; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.audit (
    uid bigint NOT NULL,
    checked boolean DEFAULT false NOT NULL,
    tags integer[] DEFAULT '{}'::integer[] NOT NULL
);


ALTER TABLE public.audit OWNER TO rmbt;

--
-- TOC entry 217 (class 1259 OID 73432)
-- Name: audit_results; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.audit_results (
    timestamp_month timestamp without time zone,
    checked boolean DEFAULT false NOT NULL
);


ALTER TABLE public.audit_results OWNER TO rmbt;

--
-- TOC entry 218 (class 1259 OID 73436)
-- Name: audit_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.audit_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.audit_uid_seq OWNER TO rmbt;

--
-- TOC entry 4447 (class 0 OID 0)
-- Dependencies: 218
-- Name: audit_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.audit_uid_seq OWNED BY public.audit.uid;


--
-- TOC entry 219 (class 1259 OID 73438)
-- Name: cell_location; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.cell_location (
    uid bigint NOT NULL,
    test_id bigint,
    location_id integer,
    area_code integer,
    "time" timestamp with time zone,
    primary_scrambling_code integer,
    time_ns bigint
);


ALTER TABLE public.cell_location OWNER TO rmbt;

--
-- TOC entry 220 (class 1259 OID 73441)
-- Name: cell; Type: VIEW; Schema: public; Owner: rmbt
--

CREATE VIEW public.cell AS
 SELECT DISTINCT cell_location.test_id,
    cell_location.location_id,
    cell_location.area_code
   FROM public.cell_location
  ORDER BY cell_location.test_id DESC;


ALTER TABLE public.cell OWNER TO rmbt;

--
-- TOC entry 221 (class 1259 OID 73445)
-- Name: geo_location; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.geo_location (
    uid bigint NOT NULL,
    test_id bigint NOT NULL,
    "time" timestamp with time zone,
    accuracy double precision,
    altitude double precision,
    bearing double precision,
    speed double precision,
    provider character varying(200),
    geo_lat double precision,
    geo_long double precision,
    location public.geometry,
    time_ns bigint,
    CONSTRAINT enforce_dims_location CHECK ((public.st_ndims(location) = 2)),
    CONSTRAINT enforce_geotype_location CHECK (((public.geometrytype(location) = 'POINT'::text) OR (location IS NULL))),
    CONSTRAINT enforce_srid_location CHECK ((public.st_srid(location) = 900913))
);


ALTER TABLE public.geo_location OWNER TO rmbt;

--
-- TOC entry 222 (class 1259 OID 73454)
-- Name: test; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.test (
    uid bigint NOT NULL,
    uuid uuid,
    client_id bigint,
    client_version character varying(10),
    client_name character varying,
    client_language character varying(10),
    client_local_ip character varying(100),
    token character varying(500),
    server_id integer,
    port integer,
    use_ssl boolean DEFAULT false NOT NULL,
    "time" timestamp with time zone,
    speed_upload integer,
    speed_download integer,
    ping_shortest bigint,
    encryption character varying(50),
    client_public_ip character varying(100),
    plattform character varying(200),
    os_version character varying(100),
    api_level character varying(10),
    device character varying(200),
    model character varying(200),
    product character varying(200),
    phone_type integer,
    data_state integer,
    network_country character varying(10),
    network_operator character varying(10),
    network_operator_name character varying(200),
    network_sim_country character varying(10),
    network_sim_operator character varying(10),
    network_sim_operator_name character varying(200),
    wifi_ssid character varying(200),
    wifi_bssid character varying(200),
    wifi_network_id character varying(200),
    duration integer,
    num_threads integer,
    status character varying(100),
    timezone character varying(200),
    bytes_download bigint,
    bytes_upload bigint,
    nsec_download bigint,
    nsec_upload bigint,
    server_ip character varying(100),
    client_software_version character varying(100),
    geo_lat double precision,
    geo_long double precision,
    network_type integer,
    location public.geometry,
    signal_strength integer,
    software_revision character varying(200),
    client_test_counter bigint,
    nat_type character varying(200),
    client_previous_test_status character varying(200),
    public_ip_asn bigint,
    speed_upload_log double precision,
    speed_download_log double precision,
    total_bytes_download bigint,
    total_bytes_upload bigint,
    wifi_link_speed integer,
    public_ip_rdns character varying(200),
    public_ip_as_name character varying(200),
    test_slot integer,
    provider_id integer,
    network_is_roaming boolean,
    ping_shortest_log double precision,
    run_ndt boolean,
    num_threads_requested integer,
    client_public_ip_anonymized character varying(100),
    zip_code integer,
    geo_provider character varying(200),
    geo_accuracy double precision,
    deleted boolean DEFAULT false NOT NULL,
    comment text,
    open_uuid uuid,
    client_time timestamp with time zone,
    zip_code_geo integer,
    mobile_provider_id integer,
    roaming_type integer,
    open_test_uuid uuid,
    country_asn character(2),
    country_location character(2),
    test_if_bytes_download bigint,
    test_if_bytes_upload bigint,
    implausible boolean DEFAULT false NOT NULL,
    testdl_if_bytes_download bigint,
    testdl_if_bytes_upload bigint,
    testul_if_bytes_download bigint,
    testul_if_bytes_upload bigint,
    country_geoip character(2),
    location_max_distance integer,
    location_max_distance_gps integer,
    network_group_name character varying(200),
    network_group_type character varying(200),
    time_dl_ns bigint,
    time_ul_ns bigint,
    num_threads_ul integer,
    "timestamp" timestamp without time zone DEFAULT now(),
    source_ip character varying(50),
    lte_rsrp integer,
    lte_rsrq integer,
    mobile_network_id integer,
    mobile_sim_id integer,
    dist_prev double precision,
    speed_prev double precision,
    tag character varying(512),
    ping_median integer,
    ping_median_log double precision,
    real_geo_lat double precision,
    real_geo_long double precision,
    real_location public.geometry,
    publish_public_data boolean DEFAULT true NOT NULL,
    adv_spd_option_id integer,
    adv_spd_option_name character varying(100),
    adv_spd_up_kbit integer,
    adv_spd_down_kbit integer,
    additional_report_fields json,
    ping_variance numeric,
    client_ip_local character varying(50),
    client_ip_local_anonymized character varying(50),
    client_ip_local_type character varying(50),
    opendata_source character varying,
    source_ip_anonymized character varying(50),
    hidden_code character varying(8),
    data json,
    gkz integer,
    test_type_id integer DEFAULT 1 NOT NULL,
    speed_graph_data public.speed_graph_pt[] DEFAULT array[]::public.speed_graph_pt[] NOT NULL,
    CONSTRAINT enforce_dims_location CHECK ((public.st_ndims(location) = 2)),
    CONSTRAINT enforce_geotype_location CHECK (((public.geometrytype(location) = 'POINT'::text) OR (location IS NULL))),
    CONSTRAINT enforce_geotype_real_location CHECK (((public.geometrytype(real_location) = 'POINT'::text) OR (real_location IS NULL))),
    CONSTRAINT enforce_srid_location CHECK ((public.st_srid(location) = 900913)),
    CONSTRAINT enforce_srid_real_location CHECK ((public.st_srid(real_location) = 900913)),
    CONSTRAINT test_speed_download_noneg CHECK ((speed_download >= 0)),
    CONSTRAINT test_speed_upload_noneg CHECK ((speed_upload >= 0))
);


ALTER TABLE public.test OWNER TO rmbt;

--
-- TOC entry 4450 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN test.server_id; Type: COMMENT; Schema: public; Owner: rmbt
--

COMMENT ON COLUMN public.test.server_id IS 'id of test server used';


--
-- TOC entry 223 (class 1259 OID 73472)
-- Name: cell2earth; Type: VIEW; Schema: public; Owner: rmbt
--

CREATE VIEW public.cell2earth AS
 SELECT cell_location.location_id,
    cell_location.area_code,
    test.network_operator,
    geo_location.provider,
    test.network_group_name,
    count(*) AS count,
    round(avg(geo_location.accuracy)) AS avg_accuracy,
    round(stddev(geo_location.accuracy)) AS sd_accuracy,
    round(min(geo_location.accuracy)) AS min_accuracy,
    round(max(geo_location.accuracy)) AS max_accuracy,
    avg(geo_location.geo_lat) AS avg_geo_lat,
    stddev(geo_location.geo_lat) AS sd_geo_lat,
    min(geo_location.geo_lat) AS min_geo_lat,
    max(geo_location.geo_lat) AS max_geo_lat,
    avg(geo_location.geo_long) AS avg_geo_long,
    stddev(geo_location.geo_long) AS sd_geo_long,
    min(geo_location.geo_long) AS min_geo_long,
    max(geo_location.geo_long) AS max_geo_long
   FROM public.cell_location,
    public.geo_location,
    public.test
  WHERE ((cell_location.test_id = geo_location.test_id) AND (cell_location.test_id = test.uid) AND (geo_location.geo_lat IS NOT NULL) AND (geo_location.geo_long IS NOT NULL))
  GROUP BY cell_location.location_id, cell_location.area_code, test.network_operator, geo_location.provider, test.network_group_name
  ORDER BY cell_location.location_id, cell_location.area_code, test.network_operator, geo_location.provider, test.network_group_name;


ALTER TABLE public.cell2earth OWNER TO rmbt;

--
-- TOC entry 4452 (class 0 OID 0)
-- Dependencies: 223
-- Name: VIEW cell2earth; Type: COMMENT; Schema: public; Owner: rmbt
--

COMMENT ON VIEW public.cell2earth IS 'Used in display of cells for Google Earth';


--
-- TOC entry 224 (class 1259 OID 73477)
-- Name: geo; Type: VIEW; Schema: public; Owner: rmbt
--

CREATE VIEW public.geo AS
 SELECT DISTINCT geo_location.test_id,
    geo_location.provider,
    (round((geo_location.accuracy / (10.0)::double precision)) * (10)::double precision) AS accuracy_rd,
    round((geo_location.geo_lat)::numeric, 3) AS lat_rd,
    round((geo_location.geo_long)::numeric, 3) AS long_rd
   FROM public.geo_location
  ORDER BY geo_location.test_id DESC;


ALTER TABLE public.geo OWNER TO rmbt;

--
-- TOC entry 225 (class 1259 OID 73481)
-- Name: cell_geo_test; Type: VIEW; Schema: public; Owner: rmbt
--

CREATE VIEW public.cell_geo_test AS
 SELECT cell.test_id,
    cell.location_id,
    cell.area_code,
    geo.test_id AS test_id2,
    geo.provider,
    geo.accuracy_rd,
    geo.lat_rd,
    geo.long_rd,
    test.uid,
    test.uuid,
    test.client_id,
    test.client_version,
    test.client_name,
    test.client_language,
    test.client_local_ip,
    test.token,
    test.server_id,
    test.port,
    test.use_ssl,
    test."time",
    test.speed_upload,
    test.speed_download,
    test.ping_shortest,
    test.encryption,
    test.client_public_ip,
    test.plattform,
    test.os_version,
    test.api_level,
    test.device,
    test.model,
    test.product,
    test.phone_type,
    test.data_state,
    test.network_country,
    test.network_operator,
    test.network_operator_name,
    test.network_sim_country,
    test.network_sim_operator,
    test.network_sim_operator_name,
    test.wifi_ssid,
    test.wifi_bssid,
    test.wifi_network_id,
    test.duration,
    test.num_threads,
    test.status,
    test.timezone,
    test.bytes_download,
    test.bytes_upload,
    test.nsec_download,
    test.nsec_upload,
    test.server_ip,
    test.client_software_version,
    test.geo_lat,
    test.geo_long,
    test.network_type,
    test.location,
    test.signal_strength,
    test.software_revision,
    test.client_test_counter,
    test.nat_type,
    test.client_previous_test_status,
    test.public_ip_asn,
    test.speed_upload_log,
    test.speed_download_log,
    test.total_bytes_download,
    test.total_bytes_upload,
    test.wifi_link_speed,
    test.public_ip_rdns,
    test.public_ip_as_name,
    test.test_slot,
    test.provider_id,
    test.network_is_roaming,
    test.ping_shortest_log,
    test.run_ndt,
    test.num_threads_requested,
    test.client_public_ip_anonymized,
    test.zip_code,
    test.geo_provider,
    test.geo_accuracy,
    test.deleted,
    test.comment,
    test.open_uuid,
    test.client_time,
    test.zip_code_geo,
    test.mobile_provider_id,
    test.roaming_type,
    test.open_test_uuid,
    test.country_asn,
    test.country_location,
    test.test_if_bytes_download,
    test.test_if_bytes_upload,
    test.implausible,
    test.testdl_if_bytes_download,
    test.testdl_if_bytes_upload,
    test.testul_if_bytes_download,
    test.testul_if_bytes_upload,
    test.country_geoip,
    test.location_max_distance,
    test.location_max_distance_gps,
    test.network_group_name,
    test.network_group_type,
    test.time_dl_ns,
    test.time_ul_ns,
    test.num_threads_ul,
    test."timestamp",
    test.source_ip,
    test.lte_rsrp,
    test.lte_rsrq,
    test.mobile_network_id,
    test.mobile_sim_id
   FROM public.cell,
    public.geo,
    public.test
  WHERE ((cell.test_id = geo.test_id) AND (cell.test_id = test.uid))
  ORDER BY cell.location_id, cell.area_code, cell.test_id DESC, geo.accuracy_rd, geo.lat_rd, geo.long_rd;


ALTER TABLE public.cell_geo_test OWNER TO rmbt;

--
-- TOC entry 226 (class 1259 OID 73486)
-- Name: cell_location_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.cell_location_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cell_location_uid_seq OWNER TO rmbt;

--
-- TOC entry 4453 (class 0 OID 0)
-- Dependencies: 226
-- Name: cell_location_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.cell_location_uid_seq OWNED BY public.cell_location.uid;


--
-- TOC entry 227 (class 1259 OID 73488)
-- Name: client; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.client (
    uid bigint NOT NULL,
    uuid uuid NOT NULL,
    client_type_id integer,
    "time" timestamp with time zone,
    sync_group_id integer,
    sync_code character varying(12),
    terms_and_conditions_accepted boolean DEFAULT false NOT NULL,
    sync_code_timestamp timestamp with time zone
);


ALTER TABLE public.client OWNER TO rmbt;

--
-- TOC entry 228 (class 1259 OID 73492)
-- Name: client_type; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.client_type (
    uid integer NOT NULL,
    name character varying(200)
);


ALTER TABLE public.client_type OWNER TO rmbt;

--
-- TOC entry 229 (class 1259 OID 73495)
-- Name: client_type_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.client_type_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_type_uid_seq OWNER TO rmbt;

--
-- TOC entry 4457 (class 0 OID 0)
-- Dependencies: 229
-- Name: client_type_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.client_type_uid_seq OWNED BY public.client_type.uid;


--
-- TOC entry 230 (class 1259 OID 73497)
-- Name: client_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.client_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_uid_seq OWNER TO rmbt;

--
-- TOC entry 4458 (class 0 OID 0)
-- Dependencies: 230
-- Name: client_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.client_uid_seq OWNED BY public.client.uid;


--
-- TOC entry 231 (class 1259 OID 73499)
-- Name: location_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.location_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.location_uid_seq OWNER TO rmbt;

--
-- TOC entry 4460 (class 0 OID 0)
-- Dependencies: 231
-- Name: location_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.location_uid_seq OWNED BY public.geo_location.uid;


--
-- TOC entry 232 (class 1259 OID 73501)
-- Name: logged_actions; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.logged_actions (
    schema_name text NOT NULL,
    table_name text NOT NULL,
    user_name text,
    action_tstamp timestamp with time zone DEFAULT now() NOT NULL,
    action text NOT NULL,
    original_data text,
    new_data text,
    query text,
    CONSTRAINT logged_actions_action_check CHECK ((action = ANY (ARRAY['I'::text, 'D'::text, 'U'::text])))
)
WITH (fillfactor='100');


ALTER TABLE public.logged_actions OWNER TO rmbt;

--
-- TOC entry 233 (class 1259 OID 73509)
-- Name: mcc2country; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.mcc2country (
    mcc character varying(3) NOT NULL,
    country character varying(2) NOT NULL
);


ALTER TABLE public.mcc2country OWNER TO rmbt;

--
-- TOC entry 234 (class 1259 OID 73512)
-- Name: mccmnc2name; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.mccmnc2name (
    uid integer NOT NULL,
    mccmnc character varying(7) NOT NULL,
    valid_from date DEFAULT '0001-01-01'::date,
    valid_to date DEFAULT '9999-12-31'::date,
    country character varying(2),
    name character varying(200) NOT NULL,
    shortname character varying(100),
    use_for_sim boolean DEFAULT true,
    use_for_network boolean DEFAULT true,
    mcc_mnc_network_mapping character varying(10),
    comment character varying(200),
    mapped_uid integer
);


ALTER TABLE public.mccmnc2name OWNER TO rmbt;

--
-- TOC entry 235 (class 1259 OID 73522)
-- Name: mccmnc2name_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.mccmnc2name_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mccmnc2name_uid_seq OWNER TO rmbt;

--
-- TOC entry 4465 (class 0 OID 0)
-- Dependencies: 235
-- Name: mccmnc2name_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.mccmnc2name_uid_seq OWNED BY public.mccmnc2name.uid;


--
-- TOC entry 236 (class 1259 OID 73524)
-- Name: mccmnc2provider; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.mccmnc2provider (
    uid integer NOT NULL,
    mcc_mnc_sim character varying(10),
    provider_id integer NOT NULL,
    mcc_mnc_network character varying(10),
    valid_from date,
    valid_to date
);


ALTER TABLE public.mccmnc2provider OWNER TO rmbt;

--
-- TOC entry 237 (class 1259 OID 73527)
-- Name: mccmnc2provider_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.mccmnc2provider_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mccmnc2provider_uid_seq OWNER TO rmbt;

--
-- TOC entry 4467 (class 0 OID 0)
-- Dependencies: 237
-- Name: mccmnc2provider_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.mccmnc2provider_uid_seq OWNED BY public.mccmnc2provider.uid;


--
-- TOC entry 238 (class 1259 OID 73529)
-- Name: ne_50m_admin_0_countries; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.ne_50m_admin_0_countries (
    gid integer NOT NULL,
    scalerank smallint,
    featurecla character varying(30),
    labelrank double precision,
    sovereignt character varying(254),
    sov_a3 character varying(254),
    adm0_dif double precision,
    level double precision,
    type character varying(254),
    admin character varying(254),
    adm0_a3 character varying(254),
    geou_dif double precision,
    geounit character varying(254),
    gu_a3 character varying(254),
    su_dif double precision,
    subunit character varying(254),
    su_a3 character varying(254),
    brk_diff double precision,
    name character varying(254),
    name_long character varying(254),
    brk_a3 character varying(254),
    brk_name character varying(254),
    brk_group character varying(254),
    abbrev character varying(254),
    postal character varying(254),
    formal_en character varying(254),
    formal_fr character varying(254),
    note_adm0 character varying(254),
    note_brk character varying(254),
    name_sort character varying(254),
    name_alt character varying(254),
    mapcolor7 double precision,
    mapcolor8 double precision,
    mapcolor9 double precision,
    mapcolor13 double precision,
    pop_est double precision,
    gdp_md_est double precision,
    pop_year double precision,
    lastcensus double precision,
    gdp_year double precision,
    economy character varying(254),
    income_grp character varying(254),
    wikipedia double precision,
    fips_10 character varying(254),
    iso_a2 character varying(254),
    iso_a3 character varying(254),
    iso_n3 character varying(254),
    un_a3 character varying(254),
    wb_a2 character varying(254),
    wb_a3 character varying(254),
    woe_id double precision,
    adm0_a3_is character varying(254),
    adm0_a3_us character varying(254),
    adm0_a3_un double precision,
    adm0_a3_wb double precision,
    continent character varying(254),
    region_un character varying(254),
    subregion character varying(254),
    region_wb character varying(254),
    name_len double precision,
    long_len double precision,
    abbrev_len double precision,
    tiny double precision,
    homepart double precision,
    the_geom public.geometry,
    CONSTRAINT enforce_dims_the_geom CHECK ((public.st_ndims(the_geom) = 2)),
    CONSTRAINT enforce_geotype_the_geom CHECK (((public.geometrytype(the_geom) = 'MULTIPOLYGON'::text) OR (the_geom IS NULL))),
    CONSTRAINT enforce_srid_the_geom CHECK ((public.st_srid(the_geom) = 900913))
);


ALTER TABLE public.ne_50m_admin_0_countries OWNER TO rmbt;

--
-- TOC entry 4468 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE ne_50m_admin_0_countries; Type: COMMENT; Schema: public; Owner: rmbt
--

COMMENT ON TABLE public.ne_50m_admin_0_countries IS 'shp2pgsql -d -W LATIN1 -c -D -s 4326 -I ne_50m_admin_0_countries.shp  > ne_50m_admin_0_countries.sql
ALTER TABLE ne_50m_admin_0_countries DROP CONSTRAINT enforce_srid_the_geom;
update ne_50m_admin_0_countries set the_geom=(ST_TRANSFORM(the_geom, 900913));
alter table ne_50m_admin_0_countries add CONSTRAINT enforce_srid_the_geom CHECK (st_srid(the_geom) = 900913);';


--
-- TOC entry 239 (class 1259 OID 73538)
-- Name: ne_50m_admin_0_countries_gid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.ne_50m_admin_0_countries_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ne_50m_admin_0_countries_gid_seq OWNER TO rmbt;

--
-- TOC entry 4470 (class 0 OID 0)
-- Dependencies: 239
-- Name: ne_50m_admin_0_countries_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.ne_50m_admin_0_countries_gid_seq OWNED BY public.ne_50m_admin_0_countries.gid;


--
-- TOC entry 240 (class 1259 OID 73540)
-- Name: network_type; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.network_type (
    uid integer NOT NULL,
    name character varying(200) NOT NULL,
    group_name character varying NOT NULL,
    aggregate character varying[],
    type character varying NOT NULL,
    technology_order integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.network_type OWNER TO rmbt;

--
-- TOC entry 241 (class 1259 OID 73547)
-- Name: network_type_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.network_type_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.network_type_uid_seq OWNER TO rmbt;

--
-- TOC entry 4472 (class 0 OID 0)
-- Dependencies: 241
-- Name: network_type_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.network_type_uid_seq OWNED BY public.network_type.uid;


--
-- TOC entry 242 (class 1259 OID 73549)
-- Name: news; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.news (
    uid integer NOT NULL,
    "time" timestamp with time zone NOT NULL,
    title_en text,
    title_de text,
    text_en text,
    text_de text,
    active boolean DEFAULT false NOT NULL,
    force boolean DEFAULT false NOT NULL,
    plattform text,
    max_software_version_code integer,
    min_software_version_code integer,
    uuid uuid,
    text_cs text,
    title_cs text
);


ALTER TABLE public.news OWNER TO rmbt;

--
-- TOC entry 243 (class 1259 OID 73557)
-- Name: news_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.news_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_uid_seq OWNER TO rmbt;

--
-- TOC entry 4474 (class 0 OID 0)
-- Dependencies: 243
-- Name: news_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.news_uid_seq OWNED BY public.news.uid;


--
-- TOC entry 244 (class 1259 OID 73559)
-- Name: ping; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.ping (
    uid bigint NOT NULL,
    test_id bigint,
    value bigint,
    value_server bigint,
    time_ns bigint
);


ALTER TABLE public.ping OWNER TO rmbt;

--
-- TOC entry 245 (class 1259 OID 73562)
-- Name: ping_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.ping_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ping_uid_seq OWNER TO rmbt;

--
-- TOC entry 4476 (class 0 OID 0)
-- Dependencies: 245
-- Name: ping_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.ping_uid_seq OWNED BY public.ping.uid;


--
-- TOC entry 246 (class 1259 OID 73564)
-- Name: plz2001; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.plz2001 (
    gid integer NOT NULL,
    objectid integer,
    plz_4 integer,
    "fläche" numeric,
    plz_3 integer,
    shape_leng numeric,
    shape_area numeric,
    the_geom public.geometry,
    CONSTRAINT enforce_dims_the_geom CHECK ((public.st_ndims(the_geom) = 2)),
    CONSTRAINT enforce_geotype_the_geom CHECK (((public.geometrytype(the_geom) = 'MULTIPOLYGON'::text) OR (the_geom IS NULL))),
    CONSTRAINT enforce_srid_the_geom CHECK ((public.st_srid(the_geom) = 900913))
);


ALTER TABLE public.plz2001 OWNER TO rmbt;

--
-- TOC entry 4478 (class 0 OID 0)
-- Dependencies: 246
-- Name: TABLE plz2001; Type: COMMENT; Schema: public; Owner: rmbt
--

COMMENT ON TABLE public.plz2001 IS 'shp2pgsql -d -W LATIN1 -c -D -s 97064 -I PLZ2001.shp
update plz2001 set the_geom=(ST_TRANSFORM(the_geom, 900913));';


--
-- TOC entry 247 (class 1259 OID 73573)
-- Name: plz2001_gid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.plz2001_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plz2001_gid_seq OWNER TO rmbt;

--
-- TOC entry 4480 (class 0 OID 0)
-- Dependencies: 247
-- Name: plz2001_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.plz2001_gid_seq OWNED BY public.plz2001.gid;


--
-- TOC entry 248 (class 1259 OID 73575)
-- Name: provider; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.provider (
    uid integer NOT NULL,
    name character varying(200),
    mcc_mnc character varying(10),
    shortname character varying(100),
    map_filter boolean NOT NULL
);


ALTER TABLE public.provider OWNER TO rmbt;

--
-- TOC entry 249 (class 1259 OID 73578)
-- Name: provider_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.provider_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.provider_uid_seq OWNER TO rmbt;

--
-- TOC entry 4482 (class 0 OID 0)
-- Dependencies: 249
-- Name: provider_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.provider_uid_seq OWNED BY public.provider.uid;


--
-- TOC entry 250 (class 1259 OID 73580)
-- Name: qos_test_desc; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.qos_test_desc (
    uid integer NOT NULL,
    desc_key text,
    value text,
    lang text
);


ALTER TABLE public.qos_test_desc OWNER TO rmbt;

--
-- TOC entry 251 (class 1259 OID 73586)
-- Name: qos_test_desc_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.qos_test_desc_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qos_test_desc_uid_seq OWNER TO rmbt;

--
-- TOC entry 4484 (class 0 OID 0)
-- Dependencies: 251
-- Name: qos_test_desc_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.qos_test_desc_uid_seq OWNED BY public.qos_test_desc.uid;


--
-- TOC entry 252 (class 1259 OID 73588)
-- Name: qos_test_objective; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.qos_test_objective (
    uid integer NOT NULL,
    test public.qostest NOT NULL,
    test_class integer,
    test_server integer,
    concurrency_group integer DEFAULT 0 NOT NULL,
    test_desc text,
    test_summary text,
    param json DEFAULT '{}'::json NOT NULL,
    results json
);


ALTER TABLE public.qos_test_objective OWNER TO rmbt;

--
-- TOC entry 253 (class 1259 OID 73596)
-- Name: qos_test_objective_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.qos_test_objective_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qos_test_objective_uid_seq OWNER TO rmbt;

--
-- TOC entry 4486 (class 0 OID 0)
-- Dependencies: 253
-- Name: qos_test_objective_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.qos_test_objective_uid_seq OWNED BY public.qos_test_objective.uid;


--
-- TOC entry 254 (class 1259 OID 73598)
-- Name: qos_test_result; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.qos_test_result (
    uid integer NOT NULL,
    test_uid bigint,
    qos_test_uid bigint,
    success_count integer DEFAULT 0 NOT NULL,
    failure_count integer DEFAULT 0 NOT NULL,
    result json DEFAULT '{}'::json NOT NULL,
    implausible boolean DEFAULT false,
    deleted boolean DEFAULT false
);


ALTER TABLE public.qos_test_result OWNER TO rmbt;

--
-- TOC entry 255 (class 1259 OID 73609)
-- Name: qos_test_result_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.qos_test_result_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qos_test_result_uid_seq OWNER TO rmbt;

--
-- TOC entry 4488 (class 0 OID 0)
-- Dependencies: 255
-- Name: qos_test_result_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.qos_test_result_uid_seq OWNED BY public.qos_test_result.uid;


--
-- TOC entry 256 (class 1259 OID 73611)
-- Name: qos_test_type_desc; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.qos_test_type_desc (
    uid integer NOT NULL,
    test public.qostest,
    test_desc text,
    test_name text
);


ALTER TABLE public.qos_test_type_desc OWNER TO rmbt;

--
-- TOC entry 257 (class 1259 OID 73617)
-- Name: qos_test_type_desc_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.qos_test_type_desc_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qos_test_type_desc_uid_seq OWNER TO rmbt;

--
-- TOC entry 4491 (class 0 OID 0)
-- Dependencies: 257
-- Name: qos_test_type_desc_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.qos_test_type_desc_uid_seq OWNED BY public.qos_test_type_desc.uid;


--
-- TOC entry 258 (class 1259 OID 73619)
-- Name: settings; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.settings (
    uid integer NOT NULL,
    key character varying NOT NULL,
    lang character(2),
    value character varying NOT NULL
);


ALTER TABLE public.settings OWNER TO rmbt;

--
-- TOC entry 259 (class 1259 OID 73625)
-- Name: settings_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.settings_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_uid_seq OWNER TO rmbt;

--
-- TOC entry 4493 (class 0 OID 0)
-- Dependencies: 259
-- Name: settings_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.settings_uid_seq OWNED BY public.settings.uid;


--
-- TOC entry 260 (class 1259 OID 73627)
-- Name: signal; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.signal (
    uid bigint NOT NULL,
    test_id bigint,
    "time" timestamp with time zone,
    signal_strength integer,
    network_type_id integer,
    wifi_link_speed integer,
    gsm_bit_error_rate integer,
    wifi_rssi integer,
    time_ns bigint,
    lte_rsrp integer,
    lte_rsrq integer,
    lte_rssnr integer,
    lte_cqi integer
);


ALTER TABLE public.signal OWNER TO rmbt;

--
-- TOC entry 261 (class 1259 OID 73630)
-- Name: signal_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.signal_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.signal_uid_seq OWNER TO rmbt;

--
-- TOC entry 4495 (class 0 OID 0)
-- Dependencies: 261
-- Name: signal_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.signal_uid_seq OWNED BY public.signal.uid;


--
-- TOC entry 262 (class 1259 OID 73632)
-- Name: status; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.status (
    uid integer NOT NULL,
    client_uuid uuid NOT NULL,
    "time" timestamp with time zone,
    plattform character varying(50),
    model character varying(50),
    product character varying(50),
    device character varying(50),
    software_version_code character varying(50),
    api_level character varying(10),
    ip character varying(50),
    age bigint,
    lat double precision,
    long double precision,
    accuracy double precision,
    altitude double precision,
    speed double precision,
    provider character varying(50)
);


ALTER TABLE public.status OWNER TO rmbt;

--
-- TOC entry 263 (class 1259 OID 73635)
-- Name: status_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.status_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_uid_seq OWNER TO rmbt;

--
-- TOC entry 4498 (class 0 OID 0)
-- Dependencies: 263
-- Name: status_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.status_uid_seq OWNED BY public.status.uid;


--
-- TOC entry 264 (class 1259 OID 73637)
-- Name: sync_group; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.sync_group (
    uid integer NOT NULL,
    tstamp timestamp with time zone NOT NULL
);


ALTER TABLE public.sync_group OWNER TO rmbt;

--
-- TOC entry 265 (class 1259 OID 73640)
-- Name: sync_group_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.sync_group_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sync_group_uid_seq OWNER TO rmbt;

--
-- TOC entry 4500 (class 0 OID 0)
-- Dependencies: 265
-- Name: sync_group_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.sync_group_uid_seq OWNED BY public.sync_group.uid;


--
-- TOC entry 266 (class 1259 OID 73642)
-- Name: test_ndt; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.test_ndt (
    uid integer NOT NULL,
    test_id bigint,
    s2cspd double precision,
    c2sspd double precision,
    avgrtt double precision,
    main text,
    stat text,
    diag text,
    time_ns bigint,
    time_end_ns bigint
);


ALTER TABLE public.test_ndt OWNER TO rmbt;

--
-- TOC entry 267 (class 1259 OID 73648)
-- Name: test_ndt_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.test_ndt_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_ndt_uid_seq OWNER TO rmbt;

--
-- TOC entry 4503 (class 0 OID 0)
-- Dependencies: 267
-- Name: test_ndt_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.test_ndt_uid_seq OWNED BY public.test_ndt.uid;


--
-- TOC entry 268 (class 1259 OID 73650)
-- Name: test_server; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.test_server (
    uid integer NOT NULL,
    name character varying(200),
    web_address character varying(500),
    port integer,
    port_ssl integer,
    city character varying,
    country character varying,
    geo_lat double precision,
    geo_long double precision,
    location public.geometry,
    web_address_ipv4 character varying(200),
    web_address_ipv6 character varying(200),
    priority integer DEFAULT 0 NOT NULL,
    weight integer DEFAULT 1 NOT NULL,
    active boolean DEFAULT true NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    key character varying,
    selectable boolean DEFAULT false NOT NULL,
    server_type character varying(10),
    secret text,
    CONSTRAINT enforce_dims_location CHECK ((public.st_ndims(location) = 2)),
    CONSTRAINT enforce_geotype_location CHECK (((public.geometrytype(location) = 'POINT'::text) OR (location IS NULL))),
    CONSTRAINT enforce_srid_location CHECK ((public.st_srid(location) = 900913))
);


ALTER TABLE public.test_server OWNER TO rmbt;

--
-- TOC entry 269 (class 1259 OID 73664)
-- Name: test_server_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.test_server_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_server_uid_seq OWNER TO rmbt;

--
-- TOC entry 4506 (class 0 OID 0)
-- Dependencies: 269
-- Name: test_server_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.test_server_uid_seq OWNED BY public.test_server.uid;

--
-- TOC entry 272 (class 1259 OID 73671)
-- Name: test_stat; Type: TABLE; Schema: public; Owner: rmbt
--

CREATE TABLE public.test_stat (
    test_uid bigint NOT NULL,
    cpu_usage json,
    mem_usage json
);


ALTER TABLE public.test_stat OWNER TO rmbt;

--
-- TOC entry 275 (class 1259 OID 81064)
-- Name: test_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_type (
    id integer NOT NULL,
    description character varying(25) NOT NULL
);


ALTER TABLE public.test_type OWNER TO rmbt;

--
-- TOC entry 273 (class 1259 OID 73677)
-- Name: test_uid_seq; Type: SEQUENCE; Schema: public; Owner: rmbt
--

CREATE SEQUENCE public.test_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_uid_seq OWNER TO rmbt;

--
-- TOC entry 4513 (class 0 OID 0)
-- Dependencies: 273
-- Name: test_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rmbt
--

ALTER SEQUENCE public.test_uid_seq OWNED BY public.test.uid;


--
-- TOC entry 274 (class 1259 OID 73679)
-- Name: v_test; Type: VIEW; Schema: public; Owner: rmbt
--

CREATE VIEW public.v_test AS
 SELECT test.uid,
    test.uuid,
    test.client_id,
    test.client_version,
    test.client_name,
    test.client_language,
    test.client_local_ip,
    test.token,
    test.server_id,
    test.port,
    test.use_ssl,
    test."time",
    test.speed_upload,
    test.speed_download,
    test.ping_shortest,
    test.encryption,
    test.client_public_ip,
    test.plattform,
    test.os_version,
    test.api_level,
    test.device,
    test.model,
    test.product,
    test.phone_type,
    test.data_state,
    test.network_country,
    test.network_operator,
    test.network_operator_name,
    test.network_sim_country,
    test.network_sim_operator,
    test.network_sim_operator_name,
    test.wifi_ssid,
    test.wifi_bssid,
    test.wifi_network_id,
    test.duration,
    test.num_threads,
    test.status,
    test.timezone,
    test.bytes_download,
    test.bytes_upload,
    test.nsec_download,
    test.nsec_upload,
    test.server_ip,
    test.client_software_version,
    test.geo_lat,
    test.geo_long,
    test.network_type,
    test.location,
    test.signal_strength,
    test.software_revision,
    test.client_test_counter,
    test.nat_type,
    test.client_previous_test_status,
    test.public_ip_asn,
    test.speed_upload_log,
    test.speed_download_log,
    test.total_bytes_download,
    test.total_bytes_upload,
    test.wifi_link_speed,
    test.public_ip_rdns,
    test.public_ip_as_name,
    test.test_slot,
    test.provider_id,
    test.network_is_roaming,
    test.ping_shortest_log,
    test.run_ndt,
    test.num_threads_requested,
    test.client_public_ip_anonymized,
    test.zip_code,
    test.geo_provider,
    test.geo_accuracy,
    test.deleted,
    test.comment,
    test.open_uuid,
    test.client_time,
    test.zip_code_geo,
    test.mobile_provider_id,
    test.roaming_type,
    test.open_test_uuid,
    test.country_asn,
    test.country_location,
    test.test_if_bytes_download,
    test.test_if_bytes_upload,
    test.implausible,
    test.testdl_if_bytes_download,
    test.testdl_if_bytes_upload,
    test.testul_if_bytes_download,
    test.testul_if_bytes_upload,
    test.country_geoip,
    test.location_max_distance,
    test.location_max_distance_gps,
    test.network_group_name,
    test.network_group_type,
    test.time_dl_ns,
    test.time_ul_ns,
    test.num_threads_ul,
    test."timestamp",
    test.source_ip,
    test.lte_rsrp,
    test.lte_rsrq,
    test.mobile_network_id,
    test.mobile_sim_id,
    test.dist_prev,
    test.speed_prev,
    test.tag,
    test.client_ip_local,
    test.client_ip_local_anonymized,
    test.client_ip_local_type,
    test.ping_median,
    test.ping_median_log,
    test.source_ip_anonymized,
    test.hidden_code,
    test.data,
    test.real_geo_lat,
    test.real_geo_long,
    test.real_location,
    test.publish_public_data,
    test.gkz,
    test.opendata_source,
    test.adv_spd_option_id,
    test.adv_spd_up_kbit,
    test.adv_spd_down_kbit,
    test.adv_spd_option_name,
    COALESCE((test.lte_rsrp + 10), test.signal_strength) AS merged_signal
   FROM public.test;


ALTER TABLE public.v_test OWNER TO rmbt;

--
-- TOC entry 4074 (class 2604 OID 73684)
-- Name: advertised_speed_option uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.advertised_speed_option ALTER COLUMN uid SET DEFAULT nextval('public.advertised_speed_option_uid_seq'::regclass);


--
-- TOC entry 4076 (class 2604 OID 73685)
-- Name: as2provider uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.as2provider ALTER COLUMN uid SET DEFAULT nextval('public.as2provider_uid_seq'::regclass);


--
-- TOC entry 4079 (class 2604 OID 73686)
-- Name: audit uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.audit ALTER COLUMN uid SET DEFAULT nextval('public.audit_uid_seq'::regclass);


--
-- TOC entry 4081 (class 2604 OID 73687)
-- Name: cell_location uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.cell_location ALTER COLUMN uid SET DEFAULT nextval('public.cell_location_uid_seq'::regclass);


--
-- TOC entry 4101 (class 2604 OID 73688)
-- Name: client uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.client ALTER COLUMN uid SET DEFAULT nextval('public.client_uid_seq'::regclass);


--
-- TOC entry 4102 (class 2604 OID 73689)
-- Name: client_type uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.client_type ALTER COLUMN uid SET DEFAULT nextval('public.client_type_uid_seq'::regclass);


--
-- TOC entry 4075 (class 2604 OID 73690)
-- Name: device_map uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.device_map ALTER COLUMN uid SET DEFAULT nextval('public.android_device_map_uid_seq'::regclass);


--
-- TOC entry 4082 (class 2604 OID 73691)
-- Name: geo_location uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.geo_location ALTER COLUMN uid SET DEFAULT nextval('public.location_uid_seq'::regclass);


--
-- TOC entry 4109 (class 2604 OID 73692)
-- Name: mccmnc2name uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.mccmnc2name ALTER COLUMN uid SET DEFAULT nextval('public.mccmnc2name_uid_seq'::regclass);


--
-- TOC entry 4110 (class 2604 OID 73693)
-- Name: mccmnc2provider uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.mccmnc2provider ALTER COLUMN uid SET DEFAULT nextval('public.mccmnc2provider_uid_seq'::regclass);


--
-- TOC entry 4111 (class 2604 OID 73694)
-- Name: ne_50m_admin_0_countries gid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.ne_50m_admin_0_countries ALTER COLUMN gid SET DEFAULT nextval('public.ne_50m_admin_0_countries_gid_seq'::regclass);


--
-- TOC entry 4116 (class 2604 OID 73695)
-- Name: network_type uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.network_type ALTER COLUMN uid SET DEFAULT nextval('public.network_type_uid_seq'::regclass);


--
-- TOC entry 4119 (class 2604 OID 73696)
-- Name: news uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.news ALTER COLUMN uid SET DEFAULT nextval('public.news_uid_seq'::regclass);


--
-- TOC entry 4120 (class 2604 OID 73697)
-- Name: ping uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.ping ALTER COLUMN uid SET DEFAULT nextval('public.ping_uid_seq'::regclass);


--
-- TOC entry 4121 (class 2604 OID 73698)
-- Name: plz2001 gid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.plz2001 ALTER COLUMN gid SET DEFAULT nextval('public.plz2001_gid_seq'::regclass);


--
-- TOC entry 4125 (class 2604 OID 73699)
-- Name: provider uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.provider ALTER COLUMN uid SET DEFAULT nextval('public.provider_uid_seq'::regclass);


--
-- TOC entry 4126 (class 2604 OID 73700)
-- Name: qos_test_desc uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_desc ALTER COLUMN uid SET DEFAULT nextval('public.qos_test_desc_uid_seq'::regclass);


--
-- TOC entry 4129 (class 2604 OID 73701)
-- Name: qos_test_objective uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_objective ALTER COLUMN uid SET DEFAULT nextval('public.qos_test_objective_uid_seq'::regclass);


--
-- TOC entry 4135 (class 2604 OID 73702)
-- Name: qos_test_result uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_result ALTER COLUMN uid SET DEFAULT nextval('public.qos_test_result_uid_seq'::regclass);


--
-- TOC entry 4136 (class 2604 OID 73703)
-- Name: qos_test_type_desc uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_type_desc ALTER COLUMN uid SET DEFAULT nextval('public.qos_test_type_desc_uid_seq'::regclass);


--
-- TOC entry 4137 (class 2604 OID 73704)
-- Name: settings uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.settings ALTER COLUMN uid SET DEFAULT nextval('public.settings_uid_seq'::regclass);


--
-- TOC entry 4138 (class 2604 OID 73705)
-- Name: signal uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.signal ALTER COLUMN uid SET DEFAULT nextval('public.signal_uid_seq'::regclass);


--
-- TOC entry 4139 (class 2604 OID 73706)
-- Name: status uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.status ALTER COLUMN uid SET DEFAULT nextval('public.status_uid_seq'::regclass);


--
-- TOC entry 4140 (class 2604 OID 73707)
-- Name: sync_group uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.sync_group ALTER COLUMN uid SET DEFAULT nextval('public.sync_group_uid_seq'::regclass);


--
-- TOC entry 4091 (class 2604 OID 73708)
-- Name: test uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test ALTER COLUMN uid SET DEFAULT nextval('public.test_uid_seq'::regclass);


--
-- TOC entry 4141 (class 2604 OID 73709)
-- Name: test_ndt uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test_ndt ALTER COLUMN uid SET DEFAULT nextval('public.test_ndt_uid_seq'::regclass);


--
-- TOC entry 4147 (class 2604 OID 73710)
-- Name: test_server uid; Type: DEFAULT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test_server ALTER COLUMN uid SET DEFAULT nextval('public.test_server_uid_seq'::regclass);

--
-- TOC entry 4153 (class 2606 OID 80739)
-- Name: advertised_speed_option advertised_speed_option_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.advertised_speed_option
    ADD CONSTRAINT advertised_speed_option_pkey PRIMARY KEY (uid);


--
-- TOC entry 4155 (class 2606 OID 80741)
-- Name: device_map android_device_map_codename_key; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.device_map
    ADD CONSTRAINT android_device_map_codename_key UNIQUE (codename);


--
-- TOC entry 4157 (class 2606 OID 80743)
-- Name: device_map android_device_map_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.device_map
    ADD CONSTRAINT android_device_map_pkey PRIMARY KEY (uid);


--
-- TOC entry 4161 (class 2606 OID 80745)
-- Name: as2provider as2provider_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.as2provider
    ADD CONSTRAINT as2provider_pkey PRIMARY KEY (uid);


--
-- TOC entry 4164 (class 2606 OID 80747)
-- Name: audit audit_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.audit
    ADD CONSTRAINT audit_pkey PRIMARY KEY (uid);


--
-- TOC entry 4166 (class 2606 OID 80749)
-- Name: cell_location cell_location_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.cell_location
    ADD CONSTRAINT cell_location_pkey PRIMARY KEY (uid);


--
-- TOC entry 4205 (class 2606 OID 80751)
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (uid);


--
-- TOC entry 4207 (class 2606 OID 80753)
-- Name: client client_sync_code; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_sync_code UNIQUE (sync_code);


--
-- TOC entry 4212 (class 2606 OID 80755)
-- Name: client_type client_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.client_type
    ADD CONSTRAINT client_type_pkey PRIMARY KEY (uid);


--
-- TOC entry 4210 (class 2606 OID 80757)
-- Name: client client_uuid_key; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_uuid_key UNIQUE (uuid);


--
-- TOC entry 4159 (class 2606 OID 80759)
-- Name: device_map device_map_fullname_key; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.device_map
    ADD CONSTRAINT device_map_fullname_key UNIQUE (fullname);


--
-- TOC entry 4175 (class 2606 OID 80761)
-- Name: geo_location location_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.geo_location
    ADD CONSTRAINT location_pkey PRIMARY KEY (uid);


--
-- TOC entry 4218 (class 2606 OID 80763)
-- Name: mcc2country mcc2country_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.mcc2country
    ADD CONSTRAINT mcc2country_pkey PRIMARY KEY (mcc);


--
-- TOC entry 4221 (class 2606 OID 80765)
-- Name: mccmnc2name mccmnc2name_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.mccmnc2name
    ADD CONSTRAINT mccmnc2name_pkey PRIMARY KEY (uid);


--
-- TOC entry 4224 (class 2606 OID 80767)
-- Name: mccmnc2provider mccmnc2provider_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.mccmnc2provider
    ADD CONSTRAINT mccmnc2provider_pkey PRIMARY KEY (uid);


--
-- TOC entry 4228 (class 2606 OID 80769)
-- Name: ne_50m_admin_0_countries ne_50m_admin_0_countries_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.ne_50m_admin_0_countries
    ADD CONSTRAINT ne_50m_admin_0_countries_pkey PRIMARY KEY (gid);


--
-- TOC entry 4232 (class 2606 OID 80771)
-- Name: network_type network_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.network_type
    ADD CONSTRAINT network_type_pkey PRIMARY KEY (uid);


--
-- TOC entry 4238 (class 2606 OID 80773)
-- Name: ping ping_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.ping
    ADD CONSTRAINT ping_pkey PRIMARY KEY (uid);


--
-- TOC entry 4241 (class 2606 OID 80775)
-- Name: plz2001 plz2001_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.plz2001
    ADD CONSTRAINT plz2001_pkey PRIMARY KEY (gid);


--
-- TOC entry 4245 (class 2606 OID 80777)
-- Name: provider provider_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.provider
    ADD CONSTRAINT provider_pkey PRIMARY KEY (uid);


--
-- TOC entry 4248 (class 2606 OID 80779)
-- Name: qos_test_desc qos_test_desc_desc_key_lang_key; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_desc
    ADD CONSTRAINT qos_test_desc_desc_key_lang_key UNIQUE (desc_key, lang);


--
-- TOC entry 4250 (class 2606 OID 80781)
-- Name: qos_test_desc qos_test_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_desc
    ADD CONSTRAINT qos_test_desc_pkey PRIMARY KEY (uid);


--
-- TOC entry 4252 (class 2606 OID 80783)
-- Name: qos_test_objective qos_test_objective_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_objective
    ADD CONSTRAINT qos_test_objective_pkey PRIMARY KEY (uid);


--
-- TOC entry 4256 (class 2606 OID 80785)
-- Name: qos_test_result qos_test_result_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_result
    ADD CONSTRAINT qos_test_result_pkey PRIMARY KEY (uid);


--
-- TOC entry 4258 (class 2606 OID 80793)
-- Name: qos_test_type_desc qos_test_type_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_type_desc
    ADD CONSTRAINT qos_test_type_desc_pkey PRIMARY KEY (uid);


--
-- TOC entry 4260 (class 2606 OID 80795)
-- Name: qos_test_type_desc qos_test_type_desc_test_key; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_type_desc
    ADD CONSTRAINT qos_test_type_desc_test_key UNIQUE (test);


--
-- TOC entry 4263 (class 2606 OID 80797)
-- Name: settings settings_key_lang_key; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_key_lang_key UNIQUE (key, lang);


--
-- TOC entry 4265 (class 2606 OID 80799)
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (uid);


--
-- TOC entry 4267 (class 2606 OID 80801)
-- Name: signal signal_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.signal
    ADD CONSTRAINT signal_pkey PRIMARY KEY (uid);


--
-- TOC entry 4270 (class 2606 OID 80803)
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (uid);


--
-- TOC entry 4272 (class 2606 OID 80805)
-- Name: sync_group sync_group_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.sync_group
    ADD CONSTRAINT sync_group_pkey PRIMARY KEY (uid);


--
-- TOC entry 4274 (class 2606 OID 80807)
-- Name: test_ndt test_ndt_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test_ndt
    ADD CONSTRAINT test_ndt_pkey PRIMARY KEY (uid);


--
-- TOC entry 4277 (class 2606 OID 80809)
-- Name: test_ndt test_ndt_test_id_unique; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test_ndt
    ADD CONSTRAINT test_ndt_test_id_unique UNIQUE (test_id);


--
-- TOC entry 4192 (class 2606 OID 80811)
-- Name: test test_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_pkey PRIMARY KEY (uid);


--
-- TOC entry 4279 (class 2606 OID 80813)
-- Name: test_server test_server_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test_server
    ADD CONSTRAINT test_server_pkey PRIMARY KEY (uid);

--
-- TOC entry 4284 (class 2606 OID 80818)
-- Name: test_stat test_stat_pkey; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test_stat
    ADD CONSTRAINT test_stat_pkey PRIMARY KEY (test_uid);


--
-- TOC entry 4286 (class 2606 OID 81068)
-- Name: test_type test_type_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_type
    ADD CONSTRAINT test_type_id_key UNIQUE (id);


--
-- TOC entry 4201 (class 2606 OID 80820)
-- Name: test test_uuid_key; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_uuid_key UNIQUE (uuid);


--
-- TOC entry 4236 (class 2606 OID 80822)
-- Name: news uid; Type: CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.news
    ADD CONSTRAINT uid PRIMARY KEY (uid);


CREATE INDEX audit_uid_hash_idx ON public.audit USING hash (uid);
COMMENT ON INDEX public.audit_uid_hash_idx IS 'Export query optimisation';

CREATE INDEX device_map_codename_hash_idx ON public.device_map USING hash (codename COLLATE pg_catalog."default");
COMMENT ON INDEX public.device_map_codename_hash_idx IS 'Export query optimisation';

--
-- TOC entry 4162 (class 1259 OID 80823)
-- Name: as2provider_provider_id_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX as2provider_provider_id_idx ON public.as2provider USING btree (provider_id);


--
-- TOC entry 4167 (class 1259 OID 80824)
-- Name: cell_location_test_id_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX cell_location_test_id_idx ON public.cell_location USING btree (test_id);


--
-- TOC entry 4168 (class 1259 OID 80825)
-- Name: cell_location_test_id_time_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX cell_location_test_id_time_idx ON public.cell_location USING btree (test_id, "time");


--
-- TOC entry 4203 (class 1259 OID 80826)
-- Name: client_client_type_id_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX client_client_type_id_idx ON public.client USING btree (client_type_id);


--
-- TOC entry 4208 (class 1259 OID 80827)
-- Name: client_sync_group_id_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX client_sync_group_id_idx ON public.client USING btree (sync_group_id);

--
-- TOC entry 4253 (class 1259 OID 80829)
-- Name: fki_qos_test_result_qos_test_uid_fkey; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX fki_qos_test_result_qos_test_uid_fkey ON public.qos_test_result USING btree (qos_test_uid);


--
-- TOC entry 4254 (class 1259 OID 80830)
-- Name: fki_qos_test_result_test_uid; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX fki_qos_test_result_test_uid ON public.qos_test_result USING btree (test_uid);


--
-- TOC entry 4169 (class 1259 OID 80831)
-- Name: geo_location_location_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX geo_location_location_idx ON public.geo_location USING gist (location);


--
-- TOC entry 4170 (class 1259 OID 80832)
-- Name: geo_location_test_id_key; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX geo_location_test_id_key ON public.geo_location USING btree (test_id);


--
-- TOC entry 4171 (class 1259 OID 80833)
-- Name: geo_location_test_id_provider; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX geo_location_test_id_provider ON public.geo_location USING btree (test_id, provider);


--
-- TOC entry 4172 (class 1259 OID 80834)
-- Name: geo_location_test_id_provider_time_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX geo_location_test_id_provider_time_idx ON public.geo_location USING btree (test_id, provider, "time");


--
-- TOC entry 4173 (class 1259 OID 80835)
-- Name: geo_location_test_id_time_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX geo_location_test_id_time_idx ON public.geo_location USING btree (test_id, "time");

--
-- TOC entry 4213 (class 1259 OID 80837)
-- Name: logged_actions_action_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX logged_actions_action_idx ON public.logged_actions USING btree (action);


--
-- TOC entry 4214 (class 1259 OID 80838)
-- Name: logged_actions_action_tstamp_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX logged_actions_action_tstamp_idx ON public.logged_actions USING btree (action_tstamp);


--
-- TOC entry 4215 (class 1259 OID 80839)
-- Name: logged_actions_schema_table_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX logged_actions_schema_table_idx ON public.logged_actions USING btree ((((schema_name || '.'::text) || table_name)));


--
-- TOC entry 4216 (class 1259 OID 80840)
-- Name: mcc2country_mcc; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX mcc2country_mcc ON public.mcc2country USING btree (mcc);


--
-- TOC entry 4219 (class 1259 OID 80841)
-- Name: mccmnc2name_mccmnc; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX mccmnc2name_mccmnc ON public.mccmnc2name USING btree (mccmnc);


--
-- TOC entry 4222 (class 1259 OID 80842)
-- Name: mccmnc2provider_mcc_mnc_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX mccmnc2provider_mcc_mnc_idx ON public.mccmnc2provider USING btree (mcc_mnc_sim, mcc_mnc_network);


--
-- TOC entry 4225 (class 1259 OID 80843)
-- Name: mccmnc2provider_provider_id; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX mccmnc2provider_provider_id ON public.mccmnc2provider USING btree (provider_id);


--
-- TOC entry 4226 (class 1259 OID 80844)
-- Name: ne_50m_admin_0_countries_iso_a2_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX ne_50m_admin_0_countries_iso_a2_idx ON public.ne_50m_admin_0_countries USING btree (iso_a2);


--
-- TOC entry 4229 (class 1259 OID 80845)
-- Name: ne_50m_admin_0_countries_the_geom_gist; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX ne_50m_admin_0_countries_the_geom_gist ON public.ne_50m_admin_0_countries USING gist (the_geom);


--
-- TOC entry 4230 (class 1259 OID 80846)
-- Name: network_type_group_name_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX network_type_group_name_idx ON public.network_type USING btree (group_name);


--
-- TOC entry 4233 (class 1259 OID 80847)
-- Name: network_type_type_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX network_type_type_idx ON public.network_type USING btree (type);


--
-- TOC entry 4234 (class 1259 OID 80848)
-- Name: news_time_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX news_time_idx ON public.news USING btree ("time");


--
-- TOC entry 4239 (class 1259 OID 80849)
-- Name: ping_test_id_key; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX ping_test_id_key ON public.ping USING btree (test_id);


--
-- TOC entry 4242 (class 1259 OID 80850)
-- Name: plz2001_the_geom_gist; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX plz2001_the_geom_gist ON public.plz2001 USING gist (the_geom);


--
-- TOC entry 4243 (class 1259 OID 80851)
-- Name: provider_mcc_mnc_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX provider_mcc_mnc_idx ON public.provider USING btree (mcc_mnc);


--
-- TOC entry 4246 (class 1259 OID 80852)
-- Name: qos_test_desc_desc_key_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX qos_test_desc_desc_key_idx ON public.qos_test_desc USING btree (desc_key);


--
-- TOC entry 4261 (class 1259 OID 80853)
-- Name: settings_key_lang_idx; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX settings_key_lang_idx ON public.settings USING btree (key, lang);


--
-- TOC entry 4268 (class 1259 OID 80854)
-- Name: signal_test_id_key; Type: INDEX; Schema: public; Owner: rmbt
--

CREATE INDEX signal_test_id_key ON public.signal USING btree (test_id);

--
-- Indexes on table public.test
--

CREATE INDEX location_idx ON public.test USING gist (location);
CREATE INDEX download_idx ON public.test USING btree (bytes_download, network_type);
CREATE INDEX test_client_id_idx ON public.test USING btree (client_id);
CREATE INDEX test_deleted_idx ON public.test USING btree (deleted);
CREATE INDEX test_device_idx ON public.test USING btree (device);
CREATE INDEX test_geo_accuracy_idx ON public.test USING btree (geo_accuracy);
CREATE INDEX test_idx ON public.test USING btree (((network_type <> ALL (ARRAY[0, 99]))));
CREATE INDEX test_mobile_network_id_idx ON public.test USING btree (mobile_network_id);
CREATE INDEX test_mobile_provider_id_idx ON public.test USING btree (mobile_provider_id);
CREATE INDEX test_ndt_test_id_idx ON public.test_ndt USING btree (test_id);
CREATE INDEX test_network_operator_idx ON public.test USING btree (network_operator);
CREATE INDEX test_network_type_idx ON public.test USING btree (network_type);
CREATE INDEX test_open_test_uuid_idx ON public.test USING btree (open_test_uuid);
CREATE INDEX test_open_uuid_idx ON public.test USING btree (open_uuid);
CREATE INDEX test_ping_median_log_idx ON public.test USING btree (ping_median_log);
CREATE INDEX test_ping_shortest_log_idx ON public.test USING btree (ping_shortest_log);
CREATE INDEX test_provider_id_idx ON public.test USING btree (provider_id);
CREATE INDEX test_speed_download_log_idx ON public.test USING btree (speed_download_log);
CREATE INDEX test_speed_upload_log_idx ON public.test USING btree (speed_upload_log);
CREATE INDEX test_status_finished_idx ON public.test USING btree ((((deleted = false) AND ((status)::text = 'FINISHED'::text))), network_type);
CREATE INDEX test_status_idx ON public.test USING btree (status);
CREATE INDEX test_test_slot_idx ON public.test USING btree (test_slot);
CREATE INDEX test_time_idx ON public.test USING btree ("time");
CREATE INDEX test_zip_code_idx ON public.test USING btree (zip_code);
CREATE INDEX test_speed_graph_data_len_idx ON public.test USING btree ((cardinality(speed_graph_data)));
CREATE INDEX test_token_idx ON public.test USING btree (token);
CREATE INDEX test_client_id_model_idx ON public.test USING btree (client_id ASC NULLS LAST, model ASC NULLS LAST);
COMMENT ON INDEX public.test_client_id_model_idx IS 'Index to speed up lookups of device names at the /settings endpoint';
CREATE INDEX test_client_id_network_type_idx ON public.test USING btree (client_id ASC NULLS LAST, network_type ASC NULLS LAST);
COMMENT ON INDEX public.test_client_id_network_type_idx IS 'Index to speed up lookups of network types at the /settings endpoint';

--
-- TOC entry 1657 (class 1255 OID 73155)
-- Name: audit_state_change(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.audit_state_change() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF OLD . checked AND NOT NEW . checked THEN
NEW . tags := '{} ';
END IF;
RETURN NEW;
END ;
$$;


ALTER FUNCTION public.audit_state_change() OWNER TO rmbt;

--
-- TOC entry 4306 (class 2620 OID 80892)
-- Name: audit audit_state; Type: TRIGGER; Schema: public; Owner: rmbt
--

CREATE TRIGGER audit_state BEFORE UPDATE ON public.audit FOR EACH ROW EXECUTE PROCEDURE public.audit_state_change();


--
-- TOC entry 4307 (class 2620 OID 80893)
-- Name: test trigger_test; Type: TRIGGER; Schema: public; Owner: rmbt
--

CREATE TRIGGER trigger_test BEFORE INSERT OR UPDATE ON public.test FOR EACH ROW EXECUTE PROCEDURE public.trigger_test();


--
-- TOC entry 4287 (class 2606 OID 80894)
-- Name: as2provider as2provider_provider_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.as2provider
    ADD CONSTRAINT as2provider_provider_id_fkey FOREIGN KEY (provider_id) REFERENCES public.provider(uid);


--
-- TOC entry 4288 (class 2606 OID 80899)
-- Name: audit audit_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.audit
    ADD CONSTRAINT audit_fkey FOREIGN KEY (uid) REFERENCES public.test(uid) ON DELETE CASCADE;


--
-- TOC entry 4289 (class 2606 OID 80904)
-- Name: cell_location cell_location_test_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.cell_location
    ADD CONSTRAINT cell_location_test_id_fkey FOREIGN KEY (test_id) REFERENCES public.test(uid) ON DELETE CASCADE;


--
-- TOC entry 4297 (class 2606 OID 80909)
-- Name: client client_client_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_client_type_id_fkey FOREIGN KEY (client_type_id) REFERENCES public.client_type(uid);


--
-- TOC entry 4298 (class 2606 OID 80914)
-- Name: client client_sync_group_id; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_sync_group_id FOREIGN KEY (sync_group_id) REFERENCES public.sync_group(uid);


--
-- TOC entry 4296 (class 2606 OID 81101)
-- Name: test foreign_test_type; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT foreign_test_type FOREIGN KEY (test_type_id) REFERENCES public.test_type(id);


--
-- TOC entry 4290 (class 2606 OID 80919)
-- Name: geo_location location_test_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.geo_location
    ADD CONSTRAINT location_test_id_fkey FOREIGN KEY (test_id) REFERENCES public.test(uid) ON DELETE CASCADE;


--
-- TOC entry 4299 (class 2606 OID 80924)
-- Name: mccmnc2provider mccmnc2provider_provider_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.mccmnc2provider
    ADD CONSTRAINT mccmnc2provider_provider_id_fkey FOREIGN KEY (provider_id) REFERENCES public.provider(uid);


--
-- TOC entry 4300 (class 2606 OID 80929)
-- Name: ping ping_test_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.ping
    ADD CONSTRAINT ping_test_id_fkey FOREIGN KEY (test_id) REFERENCES public.test(uid) ON DELETE CASCADE;


--
-- TOC entry 4301 (class 2606 OID 80934)
-- Name: qos_test_result qos_test_result_qos_test_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_result
    ADD CONSTRAINT qos_test_result_qos_test_uid_fkey FOREIGN KEY (qos_test_uid) REFERENCES public.qos_test_objective(uid) ON DELETE CASCADE;


--
-- TOC entry 4302 (class 2606 OID 80939)
-- Name: qos_test_result qos_test_result_test_uid; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.qos_test_result
    ADD CONSTRAINT qos_test_result_test_uid FOREIGN KEY (test_uid) REFERENCES public.test(uid) ON DELETE CASCADE;


--
-- TOC entry 4303 (class 2606 OID 80944)
-- Name: signal signal_test_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.signal
    ADD CONSTRAINT signal_test_id_fkey FOREIGN KEY (test_id) REFERENCES public.test(uid) ON DELETE CASCADE;


--
-- TOC entry 4291 (class 2606 OID 80949)
-- Name: test test_adv_spd_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_adv_spd_option_id_fkey FOREIGN KEY (adv_spd_option_id) REFERENCES public.advertised_speed_option(uid);


--
-- TOC entry 4292 (class 2606 OID 80954)
-- Name: test test_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_client_id_fkey FOREIGN KEY (client_id) REFERENCES public.client(uid) ON DELETE CASCADE;


--
-- TOC entry 4293 (class 2606 OID 80959)
-- Name: test test_mobile_provider_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_mobile_provider_id_fkey FOREIGN KEY (mobile_provider_id) REFERENCES public.provider(uid);


--
-- TOC entry 4304 (class 2606 OID 80964)
-- Name: test_ndt test_ndt_test_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test_ndt
    ADD CONSTRAINT test_ndt_test_id_fkey FOREIGN KEY (test_id) REFERENCES public.test(uid) ON DELETE CASCADE;


--
-- TOC entry 4294 (class 2606 OID 80969)
-- Name: test test_provider_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_provider_fkey FOREIGN KEY (provider_id) REFERENCES public.provider(uid);


--
-- TOC entry 4305 (class 2606 OID 80974)
-- Name: test_stat test_stat_test_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test_stat
    ADD CONSTRAINT test_stat_test_uid_fkey FOREIGN KEY (test_uid) REFERENCES public.test(uid);


--
-- TOC entry 4295 (class 2606 OID 80979)
-- Name: test test_test_server_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rmbt
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_test_server_id_fkey FOREIGN KEY (server_id) REFERENCES public.test_server(uid);

--
-- TOC entry 4441 (class 0 OID 0)
-- Dependencies: 210
-- Name: TABLE advertised_speed_option; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.advertised_speed_option FROM PUBLIC;
REVOKE ALL ON TABLE public.advertised_speed_option FROM rmbt;
GRANT ALL ON TABLE public.advertised_speed_option TO rmbt;
GRANT SELECT ON TABLE public.advertised_speed_option TO rmbt_group_read_only;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.advertised_speed_option TO rmbt_web_admin;


--
-- TOC entry 4443 (class 0 OID 0)
-- Dependencies: 212
-- Name: TABLE device_map; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.device_map FROM PUBLIC;
REVOKE ALL ON TABLE public.device_map FROM rmbt;
GRANT ALL ON TABLE public.device_map TO rmbt;
GRANT SELECT ON TABLE public.device_map TO rmbt_group_read_only;


--
-- TOC entry 4445 (class 0 OID 0)
-- Dependencies: 214
-- Name: TABLE as2provider; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.as2provider FROM PUBLIC;
REVOKE ALL ON TABLE public.as2provider FROM rmbt;
GRANT ALL ON TABLE public.as2provider TO rmbt;
GRANT SELECT ON TABLE public.as2provider TO rmbt_group_read_only;


--
-- TOC entry 4448 (class 0 OID 0)
-- Dependencies: 219
-- Name: TABLE cell_location; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.cell_location FROM PUBLIC;
REVOKE ALL ON TABLE public.cell_location FROM rmbt;
GRANT ALL ON TABLE public.cell_location TO rmbt;
GRANT SELECT ON TABLE public.cell_location TO rmbt_group_read_only;
GRANT INSERT ON TABLE public.cell_location TO rmbt_group_control;


--
-- TOC entry 4449 (class 0 OID 0)
-- Dependencies: 221
-- Name: TABLE geo_location; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.geo_location FROM PUBLIC;
REVOKE ALL ON TABLE public.geo_location FROM rmbt;
GRANT ALL ON TABLE public.geo_location TO rmbt;
GRANT SELECT ON TABLE public.geo_location TO rmbt_group_read_only;
GRANT INSERT ON TABLE public.geo_location TO rmbt_group_control;


--
-- TOC entry 4451 (class 0 OID 0)
-- Dependencies: 222
-- Name: TABLE test; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.test FROM PUBLIC;
REVOKE ALL ON TABLE public.test FROM rmbt;
GRANT ALL ON TABLE public.test TO rmbt;
GRANT SELECT ON TABLE public.test TO rmbt_group_read_only;
GRANT INSERT,UPDATE ON TABLE public.test TO rmbt_group_control;


--
-- TOC entry 4454 (class 0 OID 0)
-- Dependencies: 226
-- Name: SEQUENCE cell_location_uid_seq; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON SEQUENCE public.cell_location_uid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.cell_location_uid_seq FROM rmbt;
GRANT ALL ON SEQUENCE public.cell_location_uid_seq TO rmbt;
GRANT USAGE ON SEQUENCE public.cell_location_uid_seq TO rmbt_group_control;


--
-- TOC entry 4455 (class 0 OID 0)
-- Dependencies: 227
-- Name: TABLE client; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.client FROM PUBLIC;
REVOKE ALL ON TABLE public.client FROM rmbt;
GRANT ALL ON TABLE public.client TO rmbt;
GRANT SELECT ON TABLE public.client TO rmbt_group_read_only;
GRANT INSERT,UPDATE ON TABLE public.client TO rmbt_group_control;


--
-- TOC entry 4456 (class 0 OID 0)
-- Dependencies: 228
-- Name: TABLE client_type; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.client_type FROM PUBLIC;
REVOKE ALL ON TABLE public.client_type FROM rmbt;
GRANT ALL ON TABLE public.client_type TO rmbt;
GRANT SELECT ON TABLE public.client_type TO rmbt_group_read_only;


--
-- TOC entry 4459 (class 0 OID 0)
-- Dependencies: 230
-- Name: SEQUENCE client_uid_seq; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON SEQUENCE public.client_uid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.client_uid_seq FROM rmbt;
GRANT ALL ON SEQUENCE public.client_uid_seq TO rmbt;
GRANT USAGE ON SEQUENCE public.client_uid_seq TO rmbt_group_control;


--
-- TOC entry 4461 (class 0 OID 0)
-- Dependencies: 231
-- Name: SEQUENCE location_uid_seq; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON SEQUENCE public.location_uid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.location_uid_seq FROM rmbt;
GRANT ALL ON SEQUENCE public.location_uid_seq TO rmbt;
GRANT USAGE ON SEQUENCE public.location_uid_seq TO rmbt_group_control;


--
-- TOC entry 4462 (class 0 OID 0)
-- Dependencies: 232
-- Name: TABLE logged_actions; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.logged_actions FROM PUBLIC;
REVOKE ALL ON TABLE public.logged_actions FROM rmbt;
GRANT ALL ON TABLE public.logged_actions TO rmbt;
GRANT INSERT ON TABLE public.logged_actions TO rmbt_group_control;


--
-- TOC entry 4463 (class 0 OID 0)
-- Dependencies: 233
-- Name: TABLE mcc2country; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.mcc2country FROM PUBLIC;
REVOKE ALL ON TABLE public.mcc2country FROM rmbt;
GRANT ALL ON TABLE public.mcc2country TO rmbt;
GRANT SELECT ON TABLE public.mcc2country TO rmbt_group_read_only;


--
-- TOC entry 4464 (class 0 OID 0)
-- Dependencies: 234
-- Name: TABLE mccmnc2name; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.mccmnc2name FROM PUBLIC;
REVOKE ALL ON TABLE public.mccmnc2name FROM rmbt;
GRANT ALL ON TABLE public.mccmnc2name TO rmbt;
GRANT SELECT ON TABLE public.mccmnc2name TO rmbt_group_read_only;
GRANT SELECT ON TABLE public.mccmnc2name TO rmbt_group_control;


--
-- TOC entry 4466 (class 0 OID 0)
-- Dependencies: 236
-- Name: TABLE mccmnc2provider; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.mccmnc2provider FROM PUBLIC;
REVOKE ALL ON TABLE public.mccmnc2provider FROM rmbt;
GRANT ALL ON TABLE public.mccmnc2provider TO rmbt;
GRANT SELECT ON TABLE public.mccmnc2provider TO rmbt_group_read_only;


--
-- TOC entry 4469 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE ne_50m_admin_0_countries; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.ne_50m_admin_0_countries FROM PUBLIC;
REVOKE ALL ON TABLE public.ne_50m_admin_0_countries FROM rmbt;
GRANT ALL ON TABLE public.ne_50m_admin_0_countries TO rmbt;
GRANT SELECT ON TABLE public.ne_50m_admin_0_countries TO rmbt_group_read_only;


--
-- TOC entry 4471 (class 0 OID 0)
-- Dependencies: 240
-- Name: TABLE network_type; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.network_type FROM PUBLIC;
REVOKE ALL ON TABLE public.network_type FROM rmbt;
GRANT ALL ON TABLE public.network_type TO rmbt;
GRANT SELECT ON TABLE public.network_type TO rmbt_group_read_only;


--
-- TOC entry 4473 (class 0 OID 0)
-- Dependencies: 242
-- Name: TABLE news; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.news FROM PUBLIC;
REVOKE ALL ON TABLE public.news FROM rmbt;
GRANT ALL ON TABLE public.news TO rmbt;
GRANT SELECT ON TABLE public.news TO rmbt_group_read_only;


--
-- TOC entry 4475 (class 0 OID 0)
-- Dependencies: 244
-- Name: TABLE ping; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.ping FROM PUBLIC;
REVOKE ALL ON TABLE public.ping FROM rmbt;
GRANT ALL ON TABLE public.ping TO rmbt;
GRANT SELECT ON TABLE public.ping TO rmbt_group_read_only;
GRANT INSERT ON TABLE public.ping TO rmbt_group_control;


--
-- TOC entry 4477 (class 0 OID 0)
-- Dependencies: 245
-- Name: SEQUENCE ping_uid_seq; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON SEQUENCE public.ping_uid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.ping_uid_seq FROM rmbt;
GRANT ALL ON SEQUENCE public.ping_uid_seq TO rmbt;
GRANT USAGE ON SEQUENCE public.ping_uid_seq TO rmbt_group_control;


--
-- TOC entry 4479 (class 0 OID 0)
-- Dependencies: 246
-- Name: TABLE plz2001; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.plz2001 FROM PUBLIC;
REVOKE ALL ON TABLE public.plz2001 FROM rmbt;
GRANT ALL ON TABLE public.plz2001 TO rmbt;
GRANT SELECT ON TABLE public.plz2001 TO rmbt_group_read_only;


--
-- TOC entry 4481 (class 0 OID 0)
-- Dependencies: 248
-- Name: TABLE provider; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.provider FROM PUBLIC;
REVOKE ALL ON TABLE public.provider FROM rmbt;
GRANT ALL ON TABLE public.provider TO rmbt;
GRANT SELECT ON TABLE public.provider TO rmbt_group_read_only;


--
-- TOC entry 4483 (class 0 OID 0)
-- Dependencies: 250
-- Name: TABLE qos_test_desc; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.qos_test_desc FROM PUBLIC;
REVOKE ALL ON TABLE public.qos_test_desc FROM rmbt;
GRANT ALL ON TABLE public.qos_test_desc TO rmbt;
GRANT SELECT ON TABLE public.qos_test_desc TO rmbt_group_read_only;


--
-- TOC entry 4485 (class 0 OID 0)
-- Dependencies: 252
-- Name: TABLE qos_test_objective; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.qos_test_objective FROM PUBLIC;
REVOKE ALL ON TABLE public.qos_test_objective FROM rmbt;
GRANT ALL ON TABLE public.qos_test_objective TO rmbt;
GRANT SELECT ON TABLE public.qos_test_objective TO rmbt_group_read_only;


--
-- TOC entry 4487 (class 0 OID 0)
-- Dependencies: 254
-- Name: TABLE qos_test_result; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.qos_test_result FROM PUBLIC;
REVOKE ALL ON TABLE public.qos_test_result FROM rmbt;
GRANT ALL ON TABLE public.qos_test_result TO rmbt;
GRANT SELECT ON TABLE public.qos_test_result TO rmbt_group_read_only;
GRANT INSERT,UPDATE ON TABLE public.qos_test_result TO rmbt_group_control;


--
-- TOC entry 4489 (class 0 OID 0)
-- Dependencies: 255
-- Name: SEQUENCE qos_test_result_uid_seq; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON SEQUENCE public.qos_test_result_uid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.qos_test_result_uid_seq FROM rmbt;
GRANT ALL ON SEQUENCE public.qos_test_result_uid_seq TO rmbt;
GRANT USAGE ON SEQUENCE public.qos_test_result_uid_seq TO rmbt_group_control;


--
-- TOC entry 4490 (class 0 OID 0)
-- Dependencies: 256
-- Name: TABLE qos_test_type_desc; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.qos_test_type_desc FROM PUBLIC;
REVOKE ALL ON TABLE public.qos_test_type_desc FROM rmbt;
GRANT ALL ON TABLE public.qos_test_type_desc TO rmbt;
GRANT SELECT ON TABLE public.qos_test_type_desc TO rmbt_group_read_only;


--
-- TOC entry 4492 (class 0 OID 0)
-- Dependencies: 258
-- Name: TABLE settings; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.settings FROM PUBLIC;
REVOKE ALL ON TABLE public.settings FROM rmbt;
GRANT ALL ON TABLE public.settings TO rmbt;
GRANT SELECT ON TABLE public.settings TO rmbt_group_read_only;


--
-- TOC entry 4494 (class 0 OID 0)
-- Dependencies: 260
-- Name: TABLE signal; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.signal FROM PUBLIC;
REVOKE ALL ON TABLE public.signal FROM rmbt;
GRANT ALL ON TABLE public.signal TO rmbt;
GRANT SELECT ON TABLE public.signal TO rmbt_group_read_only;
GRANT INSERT ON TABLE public.signal TO rmbt_group_control;


--
-- TOC entry 4496 (class 0 OID 0)
-- Dependencies: 261
-- Name: SEQUENCE signal_uid_seq; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON SEQUENCE public.signal_uid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.signal_uid_seq FROM rmbt;
GRANT ALL ON SEQUENCE public.signal_uid_seq TO rmbt;
GRANT USAGE ON SEQUENCE public.signal_uid_seq TO rmbt_group_control;


--
-- TOC entry 4497 (class 0 OID 0)
-- Dependencies: 262
-- Name: TABLE status; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.status FROM PUBLIC;
REVOKE ALL ON TABLE public.status FROM rmbt;
GRANT ALL ON TABLE public.status TO rmbt;
GRANT INSERT,UPDATE ON TABLE public.status TO rmbt_group_control;
GRANT SELECT ON TABLE public.status TO rmbt_group_read_only;


--
-- TOC entry 4499 (class 0 OID 0)
-- Dependencies: 264
-- Name: TABLE sync_group; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.sync_group FROM PUBLIC;
REVOKE ALL ON TABLE public.sync_group FROM rmbt;
GRANT ALL ON TABLE public.sync_group TO rmbt;
GRANT SELECT ON TABLE public.sync_group TO rmbt_group_read_only;
GRANT INSERT,DELETE ON TABLE public.sync_group TO rmbt_group_control;


--
-- TOC entry 4501 (class 0 OID 0)
-- Dependencies: 265
-- Name: SEQUENCE sync_group_uid_seq; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON SEQUENCE public.sync_group_uid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.sync_group_uid_seq FROM rmbt;
GRANT ALL ON SEQUENCE public.sync_group_uid_seq TO rmbt;
GRANT USAGE ON SEQUENCE public.sync_group_uid_seq TO rmbt_group_control;


--
-- TOC entry 4502 (class 0 OID 0)
-- Dependencies: 266
-- Name: TABLE test_ndt; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.test_ndt FROM PUBLIC;
REVOKE ALL ON TABLE public.test_ndt FROM rmbt;
GRANT ALL ON TABLE public.test_ndt TO rmbt;
GRANT SELECT ON TABLE public.test_ndt TO rmbt_group_read_only;
GRANT INSERT,UPDATE ON TABLE public.test_ndt TO rmbt_group_control;


--
-- TOC entry 4504 (class 0 OID 0)
-- Dependencies: 267
-- Name: SEQUENCE test_ndt_uid_seq; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON SEQUENCE public.test_ndt_uid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.test_ndt_uid_seq FROM rmbt;
GRANT ALL ON SEQUENCE public.test_ndt_uid_seq TO rmbt;
GRANT USAGE ON SEQUENCE public.test_ndt_uid_seq TO rmbt_group_control;


--
-- TOC entry 4505 (class 0 OID 0)
-- Dependencies: 268
-- Name: TABLE test_server; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.test_server FROM PUBLIC;
REVOKE ALL ON TABLE public.test_server FROM rmbt;
GRANT ALL ON TABLE public.test_server TO rmbt;
GRANT SELECT ON TABLE public.test_server TO rmbt_group_read_only;


--
-- TOC entry 4511 (class 0 OID 0)
-- Dependencies: 272
-- Name: TABLE test_stat; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.test_stat FROM PUBLIC;
REVOKE ALL ON TABLE public.test_stat FROM rmbt;
GRANT ALL ON TABLE public.test_stat TO rmbt;
GRANT SELECT ON TABLE public.test_stat TO rmbt_group_read_only;
GRANT INSERT,UPDATE ON TABLE public.test_stat TO rmbt_group_control;


--
-- TOC entry 4512 (class 0 OID 0)
-- Dependencies: 275
-- Name: TABLE test_type; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE public.test_type FROM PUBLIC;
GRANT ALL ON TABLE public.test_type TO rmbt;
GRANT SELECT ON TABLE public.test_type TO rmbt_group_read_only;
GRANT INSERT,UPDATE ON TABLE public.test_type TO rmbt_group_control;


--
-- TOC entry 4514 (class 0 OID 0)
-- Dependencies: 273
-- Name: SEQUENCE test_uid_seq; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON SEQUENCE public.test_uid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.test_uid_seq FROM rmbt;
GRANT ALL ON SEQUENCE public.test_uid_seq TO rmbt;
GRANT USAGE ON SEQUENCE public.test_uid_seq TO rmbt_group_control;


--
-- TOC entry 4515 (class 0 OID 0)
-- Dependencies: 274
-- Name: TABLE v_test; Type: ACL; Schema: public; Owner: rmbt
--

REVOKE ALL ON TABLE public.v_test FROM PUBLIC;
REVOKE ALL ON TABLE public.v_test FROM rmbt;
GRANT ALL ON TABLE public.v_test TO rmbt;
GRANT SELECT ON TABLE public.v_test TO rmbt_group_read_only;


-- Completed on 2018-11-06 11:12:43 CET

--
-- PostgreSQL database dump complete
--

