INSERT INTO qos_test_type_desc VALUES (17, 'dns', 'dns_test_desc', 'dns_test_name');
INSERT INTO qos_test_type_desc VALUES (18, 'non_transparent_proxy', 'ntp_test_desc', 'ntp_test_name');
INSERT INTO qos_test_type_desc VALUES (19, 'http_proxy', 'hp_test_desc', 'hp_test_name');
INSERT INTO qos_test_type_desc VALUES (20, 'website', 'web_test_desc', 'web_test_name');
INSERT INTO qos_test_type_desc VALUES (21, 'udp', 'udp_test_desc', 'udp_test_name');
INSERT INTO qos_test_type_desc VALUES (22, 'tcp', 'tcp_test_desc', 'tcp_test_name');
INSERT INTO qos_test_type_desc VALUES (23, 'jitter', 'jitter_test_desc', 'jitter_test_name');

