-- DNS
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_test_desc', 'DNS is a fundamental Internet service. It is used to translate domain names to IP addresses. Depending on the test it is checked if the service is available, if the answers are correct and how fast the server responds.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_test_name', 'DNS', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_summary', 'Target: %PARAMdns_objective_host%
Entry: %PARAMdns_objective_dns_record%
Resolver: %PARAMdns_objective_resolver%'
, 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_positive_desc', 'DNS request for the domain: %PARAMdns_objective_host%                                                                                                        Requested record: %PARAMdns_objective_dns_record%

Test result:
DNS status: %PARAMdns_result_status%
DNS entries: %PARAMdns_result_entries%
Test duration: %EVAL(result = Math.round(dns_result_duration / 1000000))% ms'
, 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_negative_desc', E'A DNS request for a non-existing domain (%PARAMdns_objective_host%) has been run to check the response for the request of the domain\'s DNS A record. The correct answer would be \'NXDOMAIN\' (non-existend domain). DNS status: %PARAMdns_result_status%; Duration: %EVAL(result = Math.round(dns_result_duration / 1000000))% ms', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_positive_ok', 'DNS request successful (resolver: %PARAMdns_objective_resolver%)', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_negative_ok', 'A DNS request for a not existing domain: succeeded, no entries have been returned.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_timeout_ko', 'Test timeout exceeded. The test could not be complete successfully.', 'en');


-- NON TRANSPARENT PROXY
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_test_desc', 'This test checks if a HTTP request is modified by a proxy or other middlebox.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_test_name', 'Non Transparent Proxy', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_summary', 'Port: %PARAMnontransproxy_objective_port%
Request: %PARAMnontransproxy_objective_request%'
, 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_positive_desc', 'An HTTP request with the content: %PARAMnontransproxy_objective_request% has been sent to the test server.
The answer was: %PARAMnontransproxy_result_response%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_positive_ok', 'The HTTP request to the test server was not modified.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_positive_ko', 'The HTTP request to the test server was modified.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_result_ko', 'Invalid HTTP result. Result: %PARAMnontransproxy_result%', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_timeout_ko', 'Test timeout exceeded. The test could not be complete successfully.', 'en');


-- HTTP PROXY
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_test_desc', 'This test downloads a test web resource (e.g. image) and checks if it was modified during transport.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_test_name', 'HTTP Proxy', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_summary', 'Target: %PARAMhttp_objective_url%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_positive_desc', 'Target: %PARAMhttp_objective_url%
Range: %PARAMhttp_objective_range%
Duration: %EVAL(result = duration_ns / 1000000000)% s
Length: %PARAMhttp_result_length%
Status code: %PARAMhttp_result_status%
Hash: %PARAMhttp_result_hash%
Header:
%PARAMhttp_result_header%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_positive_ok', 'This received content is exactly the same as the original one, hence has not been modified.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_positive_ko', 'This received content is not exactly the same as the original one!', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_status_ko', 'Invalid HTTP status code. Status code: %PARAMhttp_result_status%', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_timeout_ko', 'Test timeout exceeded. The test could not be complete successfully.', 'en');


-- WEB
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_test_desc', 'The website test downloads a reference web page (mobile Kepler page by ETSI). It is verified, if the page can be transferred and how long the download of the page takes.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_test_name', 'Website', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_summary', 'Target: %PARAMwebsite_objective_url%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_positive_desc', 'The transfer of %PARAMwebsite_objective_url% took %EVAL(result = (website_result_duration / 1000000000).toFixed(2))% s.

Transfered data downlink: %EVAL(result = (website_result_rx_bytes / 1000).toFixed(2))% kB
Transfered data uplink: %EVAL(result = (website_result_tx_bytes / 1000).toFixed(2))% kB
HTTP status code: %PARAMwebsite_result_status%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_positive_ok', 'The web page has been transfered successfully.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_positive_ko', 'The web page has not been transfered successfully.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_timeout_ko', 'Test timeout exceeded. The test could not be complete successfully.', 'en');


-- TCP
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_test_desc', 'TCP is an importatnt connection oriented Internet protocol. It is used for example for web pages or e-mail.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_test_name', 'TCP', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_summary', 'TCP %EVAL(result = tcp_objective_out_port != null ? " outgoing, port: " + tcp_objective_out_port : "")%%EVAL(result = (tcp_objective_out_port != null && typeof tcp_objective_in_port !== ''undefined'' && tcp_objective_in_port != null) ? ", " : "")%%EVAL(result = typeof tcp_objective_in_port !== ''undefined'' && tcp_objective_in_port != null ? " incomming, port: " + tcp_objective_in_port : "")%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_positive_desc_outgoing', 'TCP outgoing:
It has been attempted to establish an outgoing connection to the QOS server on port: %PARAMtcp_objective_out_port%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_positive_ok', 'The test was successful. A connection could be established.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_positive_ko', 'The test was not successfull. A connection could not be established.', 'en');


-- UDP
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_test_desc', 'UDP is an important connectionless Internet protocol. It is used for real-time communications, e.g. for VoIP and video.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_test_name', 'UDP', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_summary', 'UDP %EVAL(result = udp_objective_out_port != null ? " outgoing, port: " + udp_objective_out_port + ", number of packets: " + udp_objective_out_num_packets : "")%%EVAL(result = (udp_objective_out_port != null && typeof udp_objective_in_port !== ''undefined'' && udp_objective_in_port != null) ? ", " : "")%%EVAL(result = typeof udp_objective_in_port !== ''undefined'' && udp_objective_in_port != null ? " incomming, port: " + udp_objective_in_port  + ", number of packets: " + udp_objective_in_num_packets : "")%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_positive_desc_outgoing', 'UDP Outgoing:
Number of sent packets: %PARAMudp_objective_out_num_packets%, received by the server: %PARAMudp_result_out_num_packets%, came back to the client: %PARAMudp_result_out_response_num_packets%.
Packet loss rate: %PARAMudp_result_out_packet_loss_rate%%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_positive_ok', 'The UDP test was successful. All packets have been transferred successfully.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_positive_ko', 'The UDP test was not successful. Some packets have not been transferred successfully.', 'en');

-- JITTER

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('jitter_test_name', 'JITTER', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('jitter_test_desc', 'Jitter is a measure about how much communication latency changes in time. Better connections have jitter closer to zero.', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('voip.jitter.outgoing.failure', 'The outgoing mean jitter is too high or empty because of missing outgoing voice packets.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('voip.jitter.outgoing.success', 'The outgoing mean jitter is acceptable for a VoIP connection', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('voip.jitter.incoming.success', 'The incoming mean jitter is too high or empty because of missing incoming voice packets.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('voip.jitter.incoming.failure', 'The incoming mean jitter is acceptable for a VoIP connection.', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('voip.outgoing.packet.failure', 'It is possible to send voice packets on port %PARAM voip_objective_in_port%', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('voip.outgoing.packet.success', 'It is not possible to send voice packets on port %PARAM voip_objective_in_port%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('voip.incoming.packet.success', 'It is possible to receive voice packets on port %PARAM voip_objective_in_port%', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('voip.incoming.packet.failure', 'It is not possible to receive voice packets on port %PARAM voip_objective_in_port%', 'en');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('voip_positive_desc', 'Simulated VoIP call with a duration of %PARAM voip_objective_call_duration 1000000 1 f% ms.', 'en');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('voip_summary', 'TEST PARAMETERS Sample rate: %PARAM voip_objective_sample_rate%, bits per sample: %PARAM voip_objective_bits_per_sample% Call duration: %PARAM voip_objective_call_duration 1000000 1 f% ms Packet delay: %PARAM voip_objective_delay 1000000 1 f% ms Payload type: %EVAL result=String(nn.getPayloadType(voip_objective_payload))% TEST RESULTS Incoming voice stream: target port: %PARAM voip_objective_in_port% max. jitter: %PARAM voip_result_in_max_jitter 1000000 2 f% ms mean jitter: %PARAM voip_result_in_mean_jitter 1000000 2 f% ms max. delta: %PARAM voip_result_in_max_delta 1000000 2 f% ms skew: %PARAM voip_result_in_skew 1000000 2 f% ms packets received: %PARAM voip_result_in_num_packets% sequence errors: %PARAM voip_result_in_sequence_error% shortest / longest sequence: %PARAM voip_result_in_short_seq% / %PARAM voip_result_in_long_seq% Outgoing voice stream: target port: %PARAM voip_objective_out_port% max. jitter: %PARAM voip_result_out_max_jitter 1000000 2 f% ms mean jitter: %PARAM voip_result_out_mean_jitter 1000000 2 f% ms max. delta: %PARAM voip_result_out_max_delta 1000000 2 f% ms skew: %PARAM voip_result_out_skew 1000000 2 f% ms packets received: %PARAM voip_result_out_num_packets% sequence errors: %PARAM voip_result_out_sequence_error% shortest / longest sequence: %PARAM voip_result_out_short_seq% / %PARAM voip_result_out_long_seq%', 'en');
