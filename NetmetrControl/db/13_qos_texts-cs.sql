-- DNS
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_test_desc', 'DNS je základní internetová služba. Používá se k překladu doménových jmen na IP adresy. V závislosti na testu se kontoluje zda je služba k dispozici, zda adpovědi jsou správné a jak rychle server odpovídá.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_test_name', 'DNS', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_summary', 'Cíl: %PARAMdns_objective_host%
Záznam: %PARAMdns_objective_dns_record%
Resolver: %PARAMdns_objective_resolver%'
, 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_positive_desc', 'DNS požadavek na doménu: %PARAMdns_objective_host%                                                                                                        Požadovaný záznam: %PARAMdns_objective_dns_record%

Výsledek testu:
DNS status: %PARAMdns_result_status%
DNS záznamy: %PARAMdns_result_entries%
Trvání testu: %EVAL(result = Math.round(dns_result_duration / 1000000))% ms'
, 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_negative_desc', E'DNS test pro neexistující doménu (%PARAMdns_objective_host%) byl spuštěn pro kontrolu odpovědi na požadavek doménového DNS záznamu. Správná odpověď by měla být \'NXDOMAIN\' (non-existend domain). DNS stav: %PARAMdns_result_status%; Trvání: %EVAL(result = Math.round(dns_result_duration / 1000000))% ms', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_positive_ok', 'DNS požadavek byl úspěšný (resolver: %PARAMdns_objective_resolver%)', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_negative_ok', 'DNS požadavek na neexistující doménu : úspěšný, žádné záznamy nebyly vráceny.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('dns_timeout_ko', 'Timeout pro test byl vyčerpán. Test nemohl být úspěšně dokončen.', 'cs');


-- NON TRANSPARENT PROXY
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_test_desc', 'Tento test testuje zda byl HTTP požadavek změněn proxy serverem nebo jiným middleboxem.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_test_name', 'Netransparentní proxy', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_summary', 'Port: %PARAMnontransproxy_objective_port%
Požadavek: %PARAMnontransproxy_objective_request%'
, 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_positive_desc', 'HTTP požadavek s obsahem: %PARAMnontransproxy_objective_request% byl zaslán na testovací server.
Odpověď byla: %PARAMnontransproxy_result_response%', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_positive_ok', 'HTTP požadavek na testovací server nebyl změněn.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_positive_ko', 'HTTP požadavek na testovací server byl změněn.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_result_ko', 'Neplatný HTTP výsledek. Výsledek: %PARAMnontransproxy_result%', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('ntp_timeout_ko', 'Timeout pro test byl vyčerpán. Test nemohl být úspěšně dokončen.', 'cs');


-- HTTP PROXY
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_test_desc', 'Tento test stahuje testovací webový zdroj (např. obrázek) a kontroluje, zda byl změněn během přenosu.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_test_name', 'HTTP Proxy', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_summary', 'Cíl: %PARAMhttp_objective_url%', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_positive_desc', 'Cíl: %PARAMhttp_objective_url%
Rozsah: %PARAMhttp_objective_range%
Trvání: %EVAL(result = duration_ns / 1000000000)% s
Délka: %PARAMhttp_result_length%
Status code: %PARAMhttp_result_status%
Hash: %PARAMhttp_result_hash%
Hlavička:
%PARAMhttp_result_header%', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_positive_ok', 'Přijmutý obsah je přešně stejný jako původní. Nebyl tedy změněn.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_positive_ko', 'Přijmutý obsah není přešně stejný jako původní. Byl tedy změněn!', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_status_ko', 'Neplatný HTTP status code. Status code: %PARAMhttp_result_status%', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('hp_timeout_ko', 'Timeout pro test byl vyčerpán. Test nemohl být úspěšně dokončen.', 'cs');


-- WEB
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_test_desc', 'Website test stahuje referenční webovou stránku (mobilní stránka Kepler od ETSI). Je ověřeno zda webová stránka mohla být přenesena a jak dlouho trvalo stahování.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_test_name', 'Website', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_summary', 'Cíl: %PARAMwebsite_objective_url%', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_positive_desc', 'Přenos stránky %PARAMwebsite_objective_url% trval %EVAL(result = (website_result_duration / 1000000000).toFixed(2))% s.

Stažená data: %EVAL(result = (website_result_rx_bytes / 1000).toFixed(2))% kB
Odeslaná data: %EVAL(result = (website_result_tx_bytes / 1000).toFixed(2))% kB
HTTP status code: %PARAMwebsite_result_status%', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_positive_ok', 'Webová stránka byla přenesena úspěšně.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_positive_ko', 'Webová stránka nebyla přenesena úspěšně!', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('web_timeout_ko', 'Timeout pro test byl vyčerpán. Test nemohl být úspěšně dokončen.', 'cs');


-- TCP
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_test_desc', 'TCP je důležitým spojovaným internetovým protokolem. Je používán například pro webové stránky nebo emaily.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_test_name', 'TCP', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_summary', 'TCP %EVAL(result = tcp_objective_out_port != null ? " odchozí, port: " + tcp_objective_out_port : "")%%EVAL(result = (tcp_objective_out_port != null && typeof tcp_objective_in_port !== ''undefined'' && tcp_objective_in_port != null) ? ", " : "")%%EVAL(result = typeof tcp_objective_in_port !== ''undefined'' && tcp_objective_in_port != null ? " příchozí, port: " + tcp_objective_in_port : "")%', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_positive_desc_outgoing', 'TCP odchozí:
Byl proveden pokus navázat odchozí spojení s QoS serverem na portu: %PARAMtcp_objective_out_port%', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_positive_ok', 'Test byl úspěšný. Spojení bylo navázáno.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('tcp_positive_ko', 'Test nebyl úspěšný. Spojení nebylo navázáno!', 'cs');


-- UDP
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_test_desc', 'UDP je důležitý nespojovaný internetový protokol. Používá se pro real-time komunikaci jako je například VoIP nebo video.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_test_name', 'UDP', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_summary', 'UDP %EVAL(result = udp_objective_out_port != null ? " odchozí, port: " + udp_objective_out_port + ", počet paketů: " + udp_objective_out_num_packets : "")%%EVAL(result = (udp_objective_out_port != null && typeof udp_objective_in_port !== ''undefined'' && udp_objective_in_port != null) ? ", " : "")%%EVAL(result = typeof udp_objective_in_port !== ''undefined'' && udp_objective_in_port != null ? " příchozí, port: " + udp_objective_in_port  + ", počet paketů: " + udp_objective_in_num_packets : "")%', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_positive_desc_outgoing', 'UDP odchozí:
Počet odeslaných paketů: %PARAMudp_objective_out_num_packets%, obdržených serverem: %PARAMudp_result_out_num_packets%, vrátilo se zpět na klienta: %PARAMudp_result_out_response_num_packets%.
Četnost ztráty paketů: %PARAMudp_result_out_packet_loss_rate%%', 'cs');

INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_positive_ok', 'UDP test byl úspěšný. Všechny pakety byly přeneseny úspěšně.', 'cs');
INSERT INTO qos_test_desc (desc_key, value, lang) VALUES ('udp_positive_ko', 'UDP test nebyl úspěšný. Nebyly přeneseny žádné pakety.', 'cs');
